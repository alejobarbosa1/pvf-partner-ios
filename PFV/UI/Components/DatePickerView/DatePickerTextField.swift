//
//  DatePickerTextField.swift
//  Empresariales
//
//  Created by Leonardo Armero Barbosa on 12/13/16.
//  Copyright © 2016 com.transmeta.empresariales.app.empresarialesios. All rights reserved.
//

import UIKit

protocol DatePickerTextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField)
    
    func onOkClick()
    
    func onCancelClick()
}

class DatePickerTextField: UITextField {
    
    fileprivate var datePicker: DatePickerView!
    var datePickerTextFieldDelegate: DatePickerTextFieldDelegate!
    var dateFormatter: String!
    var datePickerMode: UIDatePickerMode!
    
    var selectedDate: Date? {
        if self.text!.isEmpty {
            return nil
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter.date(from: self.text!)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initTf()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initTf()
    }
    
    fileprivate func initTf() {
        self.placeholder = Constants.ViewStrings.DATE_DEFAULT_PLACEHOLDER
    }
}

extension DatePickerTextField: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        datePickerTextFieldDelegate.textFieldDidBeginEditing(textField)
        
        datePicker = DatePickerView(dateFormatter: dateFormatter, delegate: self)
        
        self.firstSuperView().addSubview(datePicker)
        datePicker.show()
        
        return false
    }
}

extension DatePickerTextField: DatePickerViewDelegate {
    func onOkClick(date: String) {
        self.text = date
        datePicker.hide()
        datePickerTextFieldDelegate.onOkClick()
    }
    
    func onCancelClick() {
        if datePicker != nil {
            datePicker.hide()
            datePickerTextFieldDelegate.onCancelClick()
        }
    }
}
