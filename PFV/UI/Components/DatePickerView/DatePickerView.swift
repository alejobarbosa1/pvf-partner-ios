//
//  DatePickerView.swift
//  Empresariales
//
//  Created by Leonardo Armero Barbosa on 12/13/16.
//  Copyright © 2016 com.transmeta.empresariales.app.empresarialesios. All rights reserved.
//

import UIKit

protocol DatePickerViewDelegate {
    func onOkClick(date: String)
    
    func onCancelClick()
}

class DatePickerView: UIView {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var ctTopSpace: NSLayoutConstraint!
    
    // var minDate: Date! // TODO implementar minDate
    var delegate: DatePickerViewDelegate!
    var dateFormatter: String!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(dateFormatter: String, delegate: DatePickerViewDelegate) {
        self.delegate = delegate
        self.dateFormatter = dateFormatter
        
        let deviceSize = Constants.Sizes.SCREEN_SIZE.size
        let frame = CGRect(x: 0,
                           y: deviceSize.height - 246,
                           width: deviceSize.width,
                           height: 246)
        super.init(frame: frame)
        self.frame = frame
        
        Bundle.main.loadNibNamed("DatePickerView", owner: self, options: nil)
        self.view!.frame = CGRect(origin: CGPoint.zero,
                                  size: frame.size)
        self.addSubview(self.view!)
        datePicker.datePickerMode = .date
        datePicker.locale = Constants.Formats.LOCALE
        
        // Se configura la fecha máxima y mínima
        var components = DateComponents()
        components.year = -98
        let minDate = Calendar.current.date(byAdding: components, to: Date())
        
        components.year = 0
        let maxDate = Calendar.current.date(byAdding: components, to: Date())
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
    }
    
    func show() {
        ctTopSpace.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func hide() {
        ctTopSpace.constant = 246
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion:{ finished in
            self.removeFromSuperview()
        })
    }
    
    @IBAction func onOkClick() {
        let formatter = DateFormatter()
        formatter.dateFormat = self.dateFormatter
        delegate.onOkClick(date: formatter.string(from: datePicker.date as Date))
    }
    
    @IBAction func onCancelClick() {
        delegate.onCancelClick()
    }
    
}
