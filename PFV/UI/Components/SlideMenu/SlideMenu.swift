//
//  SlideMenu.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class SlideMenu: LABMenuContainer {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var lblPartnerCode: UILabel!
    @IBOutlet weak var tvOptions: UITableView!
    @IBOutlet weak var viewSupport: UIView!
    fileprivate var restUser = RestUser()
    
    fileprivate let menuItems: [[MenuItem]] = [[MenuItem(icon: #imageLiteral(resourceName: "icon_home"),
                                                         title: Constants.ViewStrings.HOME),
                                                MenuItem(icon: #imageLiteral(resourceName: "icon_info"),
                                                         title: Constants.ViewStrings.TERMS_AND_CONDITIONS),
                                                MenuItem(icon: #imageLiteral(resourceName: "icon_logOut"),
                                                         title: Constants.ViewStrings.LOG_OUT)]]
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(delegate: LABMenuContainerDelegate) {
        super.init(menuProportionalWidth: 0.8, delegate: delegate)
        Bundle.main.loadNibNamed("SlideMenu",
                                 owner: self,
                                 options: nil)
        tvOptions.register(UINib(nibName: Constants.Cells.SLIDE_ITEM_CELL, bundle: nil), forCellReuseIdentifier: Constants.Cells.SLIDE_ITEM_CELL)
        self.view!.frame = CGRect(origin: CGPoint.zero,
                                  size: frame.size)
        self.addSubview(self.view!)
        self.delegate = delegate
        viewSupport.setCustomStyle(borderColor: UIColor.white,
                                   borderWidth: 1,
                                   cornerRadius: viewSupport.frame.height / 10,
                                   views: viewSupport)
        tvOptions.separatorStyle = .none
        let user: User! = restUser.getCurrentUser()
        if user.partner_code != nil {
            lblPartnerCode.text = user.partner_code
        }
    }
    
    
    @IBAction func onClickHelp(_ sender: Any) {
        delegate.onHelpClick()
    }
    
}

extension SlideMenu : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SlideItemCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.SLIDE_ITEM_CELL, for: indexPath) as! SlideItemCell
        cell.setMenuItem(menuItems[indexPath.section][indexPath.row])
        cell.selectionStyle = .none
//        if(cell.isSelected){
//            cell.backgroundColor = UIColor.red
//        }else{
//            cell.backgroundColor = UIColor.clear
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.selectItemAt(indexPath: indexPath)  
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = .gray
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = .clear
    }
}
