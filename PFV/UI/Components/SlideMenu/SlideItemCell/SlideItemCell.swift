//
//  SlideItemCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class SlideItemCell: UITableViewCell {
    
    @IBOutlet weak var itemIcon: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    func setMenuItem(_ menuItem: MenuItem) {
        itemIcon.image = menuItem.itemIcon
        itemTitle.text = menuItem.itemtTitle
    }
}
