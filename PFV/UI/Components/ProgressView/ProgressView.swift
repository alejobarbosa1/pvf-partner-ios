//
//  ProgressView.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 5/12/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

class ProgressView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var aiProgressView: UIActivityIndicatorView!
    @IBOutlet weak var lbTitle: UILabel!
    fileprivate var hostViewController: UIViewController!
    
    init(hostViewController: UIViewController) {
        self.hostViewController = hostViewController
        let deviceSize = Constants.Sizes.SCREEN_SIZE.size
        let frame = CGRect(x: 0, y: 0, width: deviceSize.width, height: deviceSize.height)
        super.init(frame: frame)
        self.frame = frame
        Bundle.main.loadNibNamed("ProgressView", owner: self, options: nil)
        
        self.view!.frame = frame
        self.addSubview(self.view!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func show(_ title: String?) {
        if title != nil {
            lbTitle.text = title
        }
        
        aiProgressView.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        aiProgressView.color = Constants.Colors.BLUE_LIGHT
        aiProgressView.startAnimating()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        hostViewController.view.addSubview(view)
    }
    
    func hide() {
        aiProgressView.stopAnimating()
        view.removeFromSuperview()
    }
}
