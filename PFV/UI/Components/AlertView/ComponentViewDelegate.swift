//
//  ComponentViewDelegate.swift
//  Prontuz
//
//  Created by Alejandro Barbosa on 12/05/17.
//  Copyright © 2017 iMAC Crizz 1. All rights reserved.
//

import UIKit

protocol ComponentViewDelegate {
    func add(view: UIView)
    
    func center(view: UIView)
    
    func bottom()
}
