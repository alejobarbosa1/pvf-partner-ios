//
//  AlertView.swift
//  Empresariales
//
//  Created by Leonardo Armero Barbosa on 5/10/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

/**
 Delegate of alert view. BaseViewController should implements this to use
 alertsButtons delegates.
 */
protocol AlertViewDelegate: ComponentViewDelegate {
    /**
     Delegate of alert view ok Click.
     
     - parameter alertTag: Identifier tag of alert view.
     */
    func onOkClick(_ alertTag: Int)
    
    /**
     Delegate of alert view cancel Click.
     
     - parameter alertTag: Identifier tag of alert view.
     */
    func onCancelClick(_ alertTag: Int)
}

class AlertView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var vCircularIcon: UIView!
    @IBOutlet weak var icAlert: UIImageView!
    @IBOutlet weak var btOk: UIButton!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var lbMessage: UILabel!
    fileprivate var delegate: AlertViewDelegate!
    fileprivate var alertTag: Int!
    
    init(icAlert: UIImage,
         message: String,
         positiveButton: String?,
         negativeButton: String?,
         tag: Int,
         delegate: AlertViewDelegate)
    {
        let frame = CGRect(x: 0, y: 0, width: 300, height: 350)
        super.init(frame: frame)
        self.alertTag = tag
        Bundle.main.loadNibNamed("AlertView", owner: self, options: nil)
        
        self.icAlert.image = icAlert
        self.lbMessage.text = message
        self.delegate = delegate
        
        if positiveButton != nil {
            self.btOk.isHidden = false
            self.btOk.setTitle(positiveButton, for: .normal)
        }
        
        if negativeButton != nil {
            self.btCancel.isHidden = false
            self.btCancel.setTitle(negativeButton, for: .normal)
        }
        vCircularIcon.setCustomStyle(borderColor: UIColor.lightGray,
                                     borderWidth: Constants.Sizes.TEXT_FIELD_BORDER_WIDTH,
                                     cornerRadius: vCircularIcon.frame.width/2)
        
        self.view!.frame = frame
        self.view!.setCustomStyle(borderColor: UIColor.clear,
                                  borderWidth: 0,
                                  cornerRadius: Constants.Sizes.TEXT_FIELD_BORDER_WIDTH)
        
        let shadowPath = UIBezierPath(rect: view.bounds)
        self.btOk.layer.cornerRadius = self.btOk.frame.height / 4
        self.btCancel.layer.cornerRadius = self.btCancel.frame.height / 4
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 20
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.view.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.addSubview(self.view!)
        self.delegate.center(view: self)
        self.delegate.add(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    @IBAction func onOkClick() {
        delegate.onOkClick(self.alertTag)
    }
    
    @IBAction func onCancelClick() {
        delegate.onCancelClick(self.alertTag)
    }
    
}
