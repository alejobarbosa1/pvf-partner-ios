//
//  TextField.swift
//  Empresariales
//
//  Created by Leonardo Armero Barbosa on 11/25/16.
//  Copyright © 2016 com.transmeta.empresariales.app.empresarialesios. All rights reserved.
//

import UIKit

class TextField: UITextField {
    
    var spacelessText: String? {
        get {
            while text != nil && text!.hasSuffix(" ") {
                text = text!.substring(to: text!.index(before: text!.endIndex))
            }
            
            return text
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setStyle()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setStyle()
    }

    private func setStyle() {
//        self.layoutIfNeeded()
//        self.backgroundColor = UIColor.clear
//        self.borderStyle = .none
//        
//        let border = CALayer()
//        let borderWidth = Constants.Sizes.DEFAULT_BORDER_WIDTH
//        border.borderColor = Constants.Colors.PRIMARY.cgColor
//        border.frame = CGRect(x: 5,
//                              y: self.frame.size.height - borderWidth,
//                              width: self.frame.size.width - 15,
//                              height: self.frame.size.height)
//        border.borderWidth = borderWidth
//        self.layer.addSublayer(border)
//        
//        self.layer.cornerRadius = Constants.Sizes.DEFAULT_CORNER_RADIUS
//        self.font = UIFont(name: Constants.Formats.DEFAULT_FONT,
//                           size: Constants.Sizes.DEFAULT_FONT)
//        self.textColor = Constants.Colors.PRIMARY
//        self.tintColor = Constants.Colors.PRIMARY
//        self.layer.masksToBounds = true
    }
}
