//
//  CardNumberTextField.swift
//  Empresariales
//
//  Created by Leonardo Armero Barbosa on 12/13/16.
//  Copyright © 2016 com.transmeta.empresariales.app.empresarialesios. All rights reserved.
//

import UIKit

protocol CardNumberTextFieldDelegate {
    func isVisa()
    
    func isMastercard()
    
    func isAmex()
    
    func isDinners()
    
    func isNotIdentified()
}

class CardNumberTextField: UITextField {
    
    var cardNumberDelegate: CardNumberTextFieldDelegate!
    var lbPlaceholder: UILabel!
    var type: Int!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareTextField()
    }
    
    private func prepareTextField() {
        lbPlaceholder = UILabel(frame: CGRect(x: 0,
                                              y: 5,
                                              width: self.frame.width - 10,
                                              height: self.frame.height - 10))
        lbPlaceholder.textColor = self.textColor!.withAlphaComponent(0.7)
        lbPlaceholder.font = self.font
        lbPlaceholder.text = Constants.ViewStrings.CARD_NUMBER_PLACEHOLDER
        addSubview(lbPlaceholder)
    }
    
    func getCardType() -> String{
        if type != nil {
            return Constants.CardFormats.CARD_TYPE[type]
        }
        else {return ""}
    }
    
    func getCardNumber() -> String{
        return self.text!.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range:nil)
    }
}

extension CardNumberTextField: UITextFieldDelegate {
    
    func setCardNumber(cardNumber: String) {
        let array = Array(cardNumber.characters)
        var range: NSRange!
        
        for char in array {
            
            range = NSRange(location: self.text!.length,
                            length: 0)
            
            if textField(self,
                         shouldChangeCharactersIn: range,
                         replacementString: String(char))
            {
                return
            }
        }
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        var updatedString = string
        
        if range.location == 4 ||
            range.location == 9 ||
            range.location == 14
        {
            updatedString = " " + string
        }
        
        var updatedText: String!
        // si location es menor, se está borrando
        if range.location < self.text!.length {
            var pos: String.Index!
            let firstXIndex = lbPlaceholder.text!.range(of: Constants.CardFormats.CARD_NUMBER_CHAR)
            
            if textField.text!.length <= lbPlaceholder.text!.length {
                if firstXIndex == nil {
                    pos = lbPlaceholder.text!.endIndex
                } else {
                    pos = lbPlaceholder.text!.index(firstXIndex!.upperBound, offsetBy: -1)
                }
                
                let charInPos = lbPlaceholder.text![lbPlaceholder.text!.distance(from: lbPlaceholder.text!.startIndex, to: lbPlaceholder.text!.index(before: pos))]
        
                if charInPos == " " {
                    pos = lbPlaceholder.text!.index(before: pos)
                }
                
                lbPlaceholder.text!.replaceSubrange(lbPlaceholder.text!.index(before: pos)..<pos, with: Constants.CardFormats.CARD_NUMBER_CHAR)
            }
            
            // Se actualiza el campo
            let lastChar = textField.text!.substring(from: textField.text!.index(before: textField.text!.endIndex))
            updatedText = textField.text!.substring(to: textField.text!.index(before: textField.text!.endIndex))
            
            if lastChar == " " {
                updatedText = updatedText.substring(to: updatedText.index(before: updatedText.endIndex))
            }
            
            self.text = updatedText
        } else {
            // Se reemplaza la primer X encontrada en el placeholder por el caracter nuevo
            lbPlaceholder.text = lbPlaceholder.text!.replaceFirstOccurrenceOf(target: Constants.CardFormats.CARD_NUMBER_CHAR, withString: string)
            // Se actualiza el campo
            updatedText = textField.text! + updatedString
            
            updateText(string: updatedText)
        }
        
        
        // Se valida si es Visa
        if updatedText.hasPrefix(Constants.CardFormats.VISA_PREFIX) {
            self.type = 0
            cardNumberDelegate.isVisa()
            return false
        }
        
        // Se valida si es MasterCard
        let temporalText = updatedText.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range:nil)
        for masterCardPrefixRange in Constants.CardFormats.MASTERCARD_PREFIX where updatedText.length > 1{
            if temporalText.hasPrefix(masterCardPrefixRange){
                self.type = 1
                cardNumberDelegate.isMastercard()
                return false
            }
        }
        // Se valida si es Dinners
        for dinnersPrefixRange in Constants.CardFormats.DINERS_PREFIX where updatedText.length > 1{
            if updatedText.hasPrefix(dinnersPrefixRange){
                self.type = 3
                cardNumberDelegate.isDinners()
                return false
            }
        }
        // Se valida si es Amex
        for amexPrefixRange in Constants.CardFormats.AMEX_PREFIX where updatedText.length > 1{
            if updatedText.hasPrefix(amexPrefixRange) {
                self.type = 2
                cardNumberDelegate.isAmex()
                return false
            }
        }
        self.type = nil
        cardNumberDelegate.isNotIdentified()
        return false
    }
    
    private func updateText(string: String) {
        if self.text!.length == 19
        {
            return
        }
        
        self.text = string
    }
}
