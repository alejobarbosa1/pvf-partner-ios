//
//  CurrencyTextField.swift
//  ProjectTest
//
//  Created by Alejandro Barbosa on 22/12/17.
//  Copyright © 2017 CRIZZ Digital Agency All rights reserved.
//

import UIKit

class CurrencyTextField: UITextField, UITextFieldDelegate {

    var limitLength = Constants.Sizes.LIMIT_LENGTH_CURRENCY_TF
    var unformattedText = String()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
    }
    
    internal func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location < textField.text!.count {
            setCurrency(text: nil)
        } else {
            if textField.text!.count <= limitLength {
                setCurrency(text: string)
            }
        }
        return false
    }
    
    private func setCurrency(text: String?){
        if text != nil {
            unformattedText += text!
        } else {
            if !(unformattedText.isEmpty){
                unformattedText.removeLast()
                self.text = unformattedText
            }
            
        }
        if let number = Int(unformattedText) {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = numberFormatter.string(from: NSNumber(value : number))
            self.text = number! + Constants.Commons.COP
        }
        
    }

}
