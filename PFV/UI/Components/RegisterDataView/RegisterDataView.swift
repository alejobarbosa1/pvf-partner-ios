//
//  RegisterDataView.swift
//  PFV
//
//  Created by mac on 20/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterDataDelegate: ComponentViewDelegate {
    
    func onContinueClick(user: User)
    
    func onCancelClick()
    
    func addPicker(_ pickerView: PickerView)
    
}

class RegisterDataView: UIView {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfDocumentType: PickerTextField!
    @IBOutlet weak var tfDocumentNumber: UITextField!
    @IBOutlet weak var btContinue: UIButton!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var lblAlert: UILabel!
    fileprivate var delegate: RegisterDataDelegate!
    var pickerView: PickerView?
    
    init(delegate: RegisterDataDelegate, documentTypes: [DocumentType]){
        self.delegate = delegate
        let deviceSize = Constants.Sizes.SCREEN_SIZE.size
        let frame = CGRect(x: 0, y: 0, width: deviceSize.width * 0.9, height: 450)
        super.init(frame: frame)
        Bundle.main.loadNibNamed("RegisterDataView", owner: self, options: nil)
        self.view!.frame = frame
        self.view!.setCustomStyle(borderColor: UIColor.clear,
                                  borderWidth: 0,
                                  cornerRadius: Constants.Sizes.TEXT_FIELD_BORDER_WIDTH)
        tfDocumentType.delegate = self
        tfDocumentType.pickerDataSource = documentTypes
        let shadowPath = UIBezierPath(rect: view.bounds)
        self.btContinue.layer.cornerRadius = self.btContinue.frame.height / 10
        self.btCancel.layer.cornerRadius = self.btCancel.frame.height / 10
        self.layer.masksToBounds = false
        
        self.layer.shadowPath = shadowPath.cgPath
        self.view.layer.shadowPath = shadowPath.cgPath
        self.view.layer.cornerRadius = 5
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterDataView.hideKeyboard)))
        self.addSubview(self.view!)
        self.delegate.center(view: self)
        self.delegate.add(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @objc func hideKeyboard(){
        self.endEditing(true)
    }
    
    func show(){
        self.delegate.center(view: self.view)
        self.delegate.add(view: self)
    }
    
    func close(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveLinear, animations: {
                self.layer.shadowOpacity = 0
                self.layoutIfNeeded()
            }, completion: {(finished:Bool) in
                self.removeFromSuperview()
            })
        }
    }
    
    @IBAction func onOkClick() {
        if (tfPhone.text?.isEmpty)! {
            lblAlert.text =  "Registra algún número de celular"
            lblAlert.isHidden = false
            return
        }
        if (tfDocumentType.text?.isEmpty)! {
            lblAlert.text =  "Registra algún tipo de documento"
            lblAlert.isHidden = false
            return
        }
        if (tfDocumentNumber.text?.isEmpty)! {
            lblAlert.text =  "Registra algún número de documento"
            lblAlert.isHidden = false
            return
        }
        let user = User()
        user.phoneper = tfPhone.text
        user.iddoctype = tfDocumentType.selectedItem?.getCode()
        user.numberidenf = tfDocumentNumber.text
        delegate.onContinueClick(user: user)
    }

    @IBAction func onCancelClick(_ sender: Any) {
        delegate.onCancelClick()
    }
    
    func showPicker(_ pickerTextField: PickerTextField) {
        self.pickerView = PickerView(pickerTextField: pickerTextField, pickerViewDelegate: self)
        self.pickerView?.show(pickerTextField.pickerDataSource, parent: nil)
    }
    
}

extension RegisterDataView : IPickerViewDelegate {
    
    func onPickerCancelClick() {
        pickerView?.dismiss()
    }
    
    func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        if tag == Constants.Tags.DOCUMENT_YPE_TF {
            tfDocumentType.text = (pickerModel as! DocumentType).initialdoctype
        }
        pickerView?.dismiss()
    }
    
    func addPicker(_ pickerView: PickerView) {
        self.delegate.addPicker(pickerView)
    }
    
}

extension RegisterDataView : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField is PickerTextField {
            showPicker(textField as! PickerTextField)
            return false
        }
        return true
    }
    
}
