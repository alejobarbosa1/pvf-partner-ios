//
//  InfoAlertView.swift
//  Empresariales
//
//  Created by Alejandro Barbosa on 5/10/16.
//  Copyright © 2016 CRIZZ. All rights reserved.
//

import UIKit

class InfoAlertView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    fileprivate var delegate: ComponentViewDelegate!
    
    init(_ view: UIView,
         delegate: ComponentViewDelegate,
         isPartner: Bool)
    {
        if !isPartner{
            let frame = CGRect(x: 0, y: 0, width: 250, height: 115)
            super.init(frame: frame)
        } else {
            let frame = CGRect(x: 0, y: 0, width: 300, height: 300)
            super.init(frame: frame)
        }
        self.tag = tag
        Bundle.main.loadNibNamed("InfoAlertView", owner: self, options: nil)
        self.delegate = delegate
        self.view!.frame = frame
        self.view!.setCustomStyle(borderColor: UIColor.clear,
                                  borderWidth: 0,
                                  cornerRadius: Constants.Sizes.DEFAULT_CORNER_RADIUS)
        if !isPartner {
            lblMessage.text = "Este campo de texto es solo informativo, no se verá reflejado en la solicitud"
            lblMessage.textColor = UIColor.white
        }
        let shadowPath = UIBezierPath(rect: view.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 20
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.view.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.addSubview(self.view!)
        self.delegate.center(view: self)
        self.delegate.add(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
