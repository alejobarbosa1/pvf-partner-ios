//
//  LoginAlertView.swift
//  PFV
//
//  Created by mac on 9/05/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

protocol LoginAlertViewDelegate: ComponentViewDelegate {
    func onAcceptClick(dontShowAgain: Bool)
}

class LoginAlertView: UIView {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var viewSelector: UIView!
    @IBOutlet weak var viewYellow: UIView!
    @IBOutlet weak var btAccept: UIButton!
    fileprivate var delegate: LoginAlertViewDelegate!
    fileprivate var dontShowAgain = false
    
    init(delegate: LoginAlertViewDelegate){
        self.delegate = delegate
        let screenWidth = Constants.Sizes.SCREEN_SIZE.size.width
        let frame = CGRect(x: 0, y: 0, width: screenWidth * 0.85, height: 250)
        super.init(frame: frame)
        Bundle.main.loadNibNamed("LoginAlertView", owner: self, options: nil)
        self.view!.frame = frame
        self.view!.setCustomStyle(borderColor: UIColor.clear,
                                  borderWidth: 0,
                                  cornerRadius: Constants.Sizes.TEXT_FIELD_BORDER_WIDTH)
        self.viewSelector.setCustomStyle(borderColor: Constants.Colors.GRAY,
                                         borderWidth: 4,
                                         cornerRadius: self.viewSelector.frame.height / 2,
                                         views: self.viewSelector)
        let shadowPath = UIBezierPath(rect: view.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 20
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.btAccept.layer.cornerRadius = self.btAccept.frame.height / 10
        self.view.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 10
        self.addSubview(self.view!)
        self.delegate.center(view: self)
        self.delegate.add(view: self)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func onDontShowAgainClick(_ sender: Any) {
        if dontShowAgain{
            dontShowAgain = false
            viewYellow.isHidden = true
        }
        else{
            dontShowAgain = true
            viewYellow.isHidden = false
        }
    }
    
    @IBAction func onAcceptClick(_ sender: Any) {
        delegate.onAcceptClick(dontShowAgain: dontShowAgain)
    }
    
}
