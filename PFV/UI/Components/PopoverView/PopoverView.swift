//
//  PopoverView.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 4/26/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

class PopoverView: UIViewController {
    

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var message: String?
    var icon: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMessage.text = message
        imgIcon.image = icon
    }
}
