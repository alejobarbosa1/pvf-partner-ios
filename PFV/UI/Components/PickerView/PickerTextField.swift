//
//  PickerTextField.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 5/4/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

protocol IPickerTextFieldDelegate {
    func onItemSelected(selectedItem: IBasePickerModel)
}

class PickerTextField: UITextField {
    
    var pickerDelegate: IPickerTextFieldDelegate?
    var pickerDataSource: [IBasePickerModel]!
    var lastSelectedItem: IBasePickerModel?
    var parent: UIView?
    var selectedItem: IBasePickerModel? {
        willSet {
            lastSelectedItem = selectedItem
        }
        didSet {
            text = selectedItem!.getText()
            
            if pickerDelegate != nil {
                pickerDelegate!.onItemSelected(selectedItem: selectedItem!)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initTf()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initTf()
    }
    
    fileprivate func initTf() {
        pickerDataSource = [IBasePickerModel]()
//        self.setImage(image: #imageLiteral(resourceName: "icon_arrow_down"), orientation: Constants.Position.RIGHT)
    }
}
