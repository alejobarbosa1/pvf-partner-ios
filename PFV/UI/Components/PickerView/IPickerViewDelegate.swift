//
//  IPickerViewDelegate.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 5/4/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import Foundation

protocol IPickerViewDelegate {
    
    /**
     Show a pickerView in the pickerViewDelegate
     */
    func addPicker(_ pickerView: PickerView)
    
    /**
     Notify a picker ok Click with the model selected
     
     - parameter pickerModel: PickerModel selected.
     */
    func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int)
    
    /**
     Notify a picker cancel Click
     */
    func onPickerCancelClick()
}
