//
//  PickerView.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 5/4/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

class PickerView: UIView, UIPickerViewDataSource, UIPickerViewDelegate {
    
    fileprivate var pickerViewDelegate: IPickerViewDelegate!
    fileprivate var pickerTextField: PickerTextField!
    @IBOutlet var view: UIView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var pickerContainerToBottom: NSLayoutConstraint!
    
    fileprivate var pickerDataSource: [IBasePickerModel]!
    
    init(pickerTextField: PickerTextField, pickerViewDelegate: IPickerViewDelegate) {
        self.pickerViewDelegate = pickerViewDelegate
        self.pickerTextField = pickerTextField
        let deviceSize = Constants.Sizes.SCREEN_SIZE.size
        let frame = CGRect(x: 0, y: 0, width: deviceSize.width, height: deviceSize.height)
        super.init(frame: frame)
        self.frame = frame
        Bundle.main.loadNibNamed("PickerView", owner: self, options: nil)
        self.view!.frame = frame
        self.addSubview(self.view!)
        picker.dataSource = self
        picker.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func show(_ pickerDataSource: [IBasePickerModel], parent: UIView?) {
        self.pickerDataSource = pickerDataSource
        if parent != nil{
            self.frame.size = CGSize(width: (parent?.frame.width)!, height: (parent?.frame.height)!)
        }
        pickerViewDelegate.addPicker(self)
        self.view.backgroundColor = UIColor.clear.withAlphaComponent(0.5)
        self.picker.backgroundColor = UIColor.white
        self.pickerContainerToBottom.constant = 0
        
        self.picker.superview?.window?.endEditing(true)
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
    }
    func movePickerview()
    {
        self.pickerContainerToBottom.constant = 0
    }
    
    func dismiss() {
        self.pickerContainerToBottom.constant = -239
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
            }, completion:{ finished in
                self.removeFromSuperview()
        })
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row].getText()
    }
    
    @IBAction func onCancelClick(_ sender: AnyObject) {
        pickerViewDelegate.onPickerCancelClick()
    }
    
    @IBAction func onOkClick(_ sender: AnyObject) {
        var selectedItem: IBasePickerModel?
        if pickerDataSource.count > 0 {
            selectedItem = pickerDataSource[picker.selectedRow(inComponent: 0)]
            pickerTextField.text = selectedItem!.getText()
            pickerTextField.selectedItem = selectedItem
        }
        
        pickerViewDelegate.onPickerOkClick(selectedItem, tag: pickerTextField.tag)
    }
}
