//
//  PhotoAlertView.swift
//  virtual-clinic-ios
//
//  Created by Leonardo Armero Barbosa on 8/11/16.
//  Copyright © 2016 Exsis. All rights reserved.
//

import UIKit

protocol PhotoAlertViewDelegate {
    func onCameraClick()
    
    func onGalleryClick()
    
    func onCancelClick(_ alertTag: Int)
}

class PhotoAlertView: UIView {
    
    fileprivate var delegate: PhotoAlertViewDelegate!
    @IBOutlet var view: UIView!
    @IBOutlet weak var btCancel: UIButton!
    
    init(delegate: PhotoAlertViewDelegate) {
        self.delegate = delegate
        let frame = CGRect(x: 0, y: 0, width: 300, height: 360)
        super.init(frame: frame)
        self.frame = frame
        Bundle.main.loadNibNamed("PhotoAlertView", owner: self, options: nil)
        
        self.view!.frame = frame
        self.addSubview(self.view!)
        self.btCancel.layer.cornerRadius = self.btCancel.frame.height / 10
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func onCameraClick(_ sender: AnyObject) {
        delegate.onCameraClick()
    }
    
    @IBAction func onGalleryClick(_ sender: AnyObject) {
        delegate.onGalleryClick()
    }
    @IBAction func onCancelClick(_ sender: Any) {
        delegate.onCancelClick(tag)
    }
}
