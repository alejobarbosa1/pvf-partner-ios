//
//  MenuItem.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
import UIKit
@objcMembers
class MenuItem : NSObject, IBaseModel {

    var itemtTitle: String!
    var itemIcon: UIImage!
    
    required override init() {}
    
    init(icon: UIImage, title: String) {
        self.itemIcon = icon
        self.itemtTitle = title
    }
    
}
