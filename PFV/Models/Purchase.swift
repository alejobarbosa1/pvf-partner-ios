//
//  Purchase.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 13/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Purchase : NSObject, IBaseModel {
    
    required override init(){
        super.init()
    }
    
    var idproj: NSNumber!
    var iduser: NSNumber!
    var num_pers: NSNumber!
    var id_financing: NSNumber!
    var type_hou: String!
    var floor_num: NSNumber!
    var porcen_credict: NSNumber!
    var year_pay: NSNumber!
    var price_hou: NSNumber!
    var id_model: NSNumber!
    var numero: NSNumber!
    var lugar: String!
    
    var full_name_per_2: String!
    var borndate_per_2: String!
    var dateexped_per_2: String!
    var leveleducation_per_2: String!
    var civilstatus_per_2: String!
    var act_lab_per_2: String!
    var old_lab_per_2: NSNumber!
    var act_inde_per_2: String!
    var type_cont_per_2: String!
    var email_per_2: String!
    var income_per_2: NSNumber!
    var phoneper_per_2: String!
    var name_country_per_2: String!
    var pay_month: NSNumber!
    var doctype_per_2: String!
    var numberidenf_per_2: NSNumber!
    
}
