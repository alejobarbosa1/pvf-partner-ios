//
//  Information.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 15/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Information: NSObject, IBaseModel {
    
    var old_lab: String!
    var act_lab: String!
    var type_cont: String!
    var salary: String!
    var income: String!
    
    override required init() {
        super.init()
    }
}
