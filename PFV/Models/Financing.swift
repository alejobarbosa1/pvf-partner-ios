//
//  Financing.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers

class Financing: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var financing_name: String!
    var id_financing: NSNumber!
    var porcentage: NSNumber!
    var cuotas: NSNumber!
    
    init( id_financing: NSNumber,
          financing_name: String) {
        self.id_financing = id_financing
        self.financing_name = financing_name
    }
    
    func getCode() -> String {
        return self.id_financing.description
    }
    
    func getText() -> String {
        return self.financing_name
    }
    
    func getPercentage() -> String {
        return self.porcentage.description
    }
    
    func getDues() -> String {
        return cuotas.description
    }
}
