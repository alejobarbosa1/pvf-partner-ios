//
//  Coordinates.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Coordinates: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var lat: String!
    var lng: String!

}
