//
//  Flat.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Flat: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var id_model: NSNumber!
    var piso: NSNumber!
    var numero: String!
    var precio: NSNumber!
    var lugar: String!
    var status_model: String!
    var isSelected: NSNumber = 0
    var price_string: String!
}
