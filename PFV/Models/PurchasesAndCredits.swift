//
//  PurchasesAndCredits.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 14/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class PurchasesAndCredits: NSObject, IBaseModel {
    
    required override init(){
        super.init()
    }
        
    var compras: [Project]!
    var creditos: [Project]!
    
    
}
