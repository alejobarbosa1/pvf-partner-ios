//
//  ProjectDb.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 1/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class ProjectDb: NSObject, IBaseModel {

    var projectId: NSNumber!
    var projectLogo: String?
    var builderLogo: String?
    var projectName: String!
    var basicPrice: NSNumber!
    var isFavorite: NSNumber!
    var rooms: NSNumber!
    var bathrooms: NSNumber!
    var area: NSNumber!
    var hasParking: NSNumber!
    var address: String!
    var latitude: String?
    var longitude: String?
    var endDate: String!
    var modelId: NSNumber!
    var modelName: String!
    var modelStatus: String!
    var modelType: String!
    var isFlatSelected: NSNumber!
    var flatNumber: NSNumber?
    var flatPlace: String?
    var flatPrice: NSNumber?
    var flat: NSNumber?
    var financingName: String?
    var financingId: String?
    var isCredit: NSNumber!
    var financingInterest: String?
    var financingValue: NSNumber?
    var termInYears: String?
    var monthlyValue: NSNumber?
    var interestRate: String?
    var interestRateValue: NSNumber?
    var isSecondHolder: NSNumber!
    var fullNameSH: String?
    var documentTypeSH: String?
    var docmuentNumberSH: String?
    var emailSH: String?
    var cellPhoneSH: String?
    var customerTypeSH: String?
    var bornDateSH: NSNumber?
    var expeditionDateSH: NSNumber?
    var educationalLevelSH: String?
    var civilStatusSH: String?
    var workActivitySH: String?
    var antiquityYearsSH: String?
    var antiquityMonthsSH: String?
    var contractTypeSH: String?
    var salarySH: String?
    
    //
    
    var basicPriceString: String?
    var images: [Photo]?
    
    override required init(){
        super.init()
    }
}
