//
//  Settings.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class Settings: NSObject, IBaseModel {
    override required init() {
        super.init()
    }
    
    var remember : NSNumber!
    var login: NSNumber!
    var loginAlert: NSNumber!
    
    func isRemember() -> Bool{
        switch self.remember {
        case 0:
            return false
        case 1:
            return true
        default:
            return false
        }
    }
    
    func isLogin() -> Bool{
        switch self.login {
        case 0:
            return false
        case 1:
            return true
        default:
            return false
        }
    }
    
    func showLoginAlert() -> Bool{
        switch self.loginAlert {
        case 0:
            return false
        case 1:
            return true
        default:
            return true
        }
    }
}
