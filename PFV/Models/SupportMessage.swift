//
//  SupportMessage.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers

class SupportMessage: NSObject, IBaseModel {
    
    var iduser: NSNumber!
    var comment: String!
    var idchannel: NSNumber!
    var idrol: NSNumber! = 3
    var messageId: NSNumber!
    
    override required init() {
        super.init()
    }
    
    init(iduser: NSNumber,
         comment: String,
         idchannel: NSNumber,
         messageId: NSNumber) {
        self.iduser = iduser
        self.comment = comment
        self.idchannel = idchannel
        self.messageId = messageId
    }
    
}
