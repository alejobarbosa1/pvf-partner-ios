//
//  NewTicket.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class NewTicket: NSObject, IBaseModel {
    
    var iduser: NSNumber!
    var title: String!
    var message: String!
    var idrol: NSNumber! = 3
    var messageId: NSNumber!
    
    override required init() {
        super.init()
    }
    
}
