//
//  ApplicantsNumber.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 31/10/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class ApplicantsNumber: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var applicantsNumber: String!
    
    init(_ applicantsNumber: String) {
        self.applicantsNumber = applicantsNumber
    }
    

    
    func getCode() -> String{
        return applicantsNumber
    }
    
    func getText() -> String{
        return applicantsNumber
    }
    
}
