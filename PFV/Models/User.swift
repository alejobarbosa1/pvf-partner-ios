//
//  User.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
import ParceSwift
@objcMembers
class User: NSObject, IBaseModel {
    
    var idrol: NSNumber! 
    var token_firebase: String!
    var iduser: NSNumber!
    var email: String!
    var password: String!
    var image: String!
    var token: String!
    var status: NSNumber!
    var createdAt: String!
    var updatedAt: String!
    var remember: NSNumber!
    var iddoctype: String!
    var numberidenf: String!
    var firstname: String! {
        didSet {
            self.firstName = firstname
        }
    }
    var lastname: String! {
        didSet {
            self.lastName = lastname
        }
    }
    var firstName: String!
    var lastName: String!
    var phoneper: String!
    var code: String!
    var rol: NSNumber!
    
    var partner_code: String!
    var idcity: NSNumber!
    var borndate: String!
    var dateexped: String!
    var leveleducation: String!
    var civilstatus: String!
    var id_clients: String!
    var act_lab: String!
    var old_lab: String!
    var type_cont: String!
    var income: String!
    var name_country: String!
    var id_country: NSNumber!
    var namecity: String!
    var yearAtiquity: String!
    var monthsAntiquity: String!
    var other_information: Information!
    var name_city: String!
    var name_client: String!
    
    //Datos que no pide ningún servicio
    var initialDocType: String!
    var docType: String!
    var customerType: String!
    var educationalLevel: String!
    var contractType: String!
    var fullname: String!
    var fullid: String!
    
    // Facebook data
    var first_name: String! {
        didSet {
            self.firstName = first_name
        }
    }
    var last_name: String! {
        didSet {
            self.lastName = last_name
        }
    }
    var picture: Picture!
    var provider_id: NSNumber!
    var id: NSNumber!{
        didSet{
            self.provider_id = id
            self.password = id.description
        }
    }
    
    required override init(){
    }
    
    init( email: String, password: String) {
        self.email = email
        self.password = password
    }
}

class Picture: NSObject, IBaseModel {
    override required init() {
        
    }
    
    var data: Datta!
    
}
class Datta: NSObject, IBaseModel {
    override required init() {
        
    }
    
    var is_silhouette: String!
    var url: String!
    
}

