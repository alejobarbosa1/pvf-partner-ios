//
//  Response.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

/**
 Response Model
 */
class Response: NSObject, IBaseModel {
    var ResponseCode: String?
    var ResponseMessage: String?
    
    required override init() {}
    
    init(ResponseCode: String, ResponseMessage: String) {
        super.init()
        
        self.ResponseCode = ResponseCode
        self.ResponseMessage = ResponseMessage
    }
}

