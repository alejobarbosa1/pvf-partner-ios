//
//  CustomerType.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 9/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class CustomerType: NSObject, IBasePickerModel {
    required override init() {
        super.init()
    }
    
    var name_client: String!
    var id_clients: NSNumber!
    
    init( id_clients: NSNumber,
          name_client: String) {
        self.id_clients = id_clients
        self.name_client = name_client
    }
    
    func getCode() -> String {
        return self.id_clients.description
    }
    
    func getText() -> String {
        return self.name_client
    }
}
