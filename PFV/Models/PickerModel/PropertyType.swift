//
//  PropertyType.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 13/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class PropertyType: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var type: String!
    
    
    init(_ type: String) {
        self.type = type
    }
    
    func getCode() -> String{
        return type
    }
    
    func getText() -> String{
        return type
    }
    
}
