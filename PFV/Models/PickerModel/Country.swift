//
//  Country.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 9/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Country: NSObject, IBasePickerModel {
    required override init() {
        super.init()
    }
    
    var name_country: String!
    var id_country: NSNumber!
    
    init( id_country: NSNumber,
          name_country: String) {
        self.id_country = id_country
        self.name_country = name_country
    }
    
    func getCode() -> String {
        return self.id_country.description
    }
    
    func getText() -> String {
        return self.name_country
    }
}
