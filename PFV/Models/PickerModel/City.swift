//
//  City.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 9/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class City: NSObject, IBasePickerModel {
    required override init() {
        super.init()
    }
    
    var idcity: String!
    var namecity: String!
    var id_country: NSNumber!
    
    init( idcity: String,
          namecity: String) {
        self.idcity = idcity
        self.namecity = namecity
    }
    
    func getCode() -> String {
        return idcity
    }
    
    func getText() -> String {
        return namecity
    }
}
