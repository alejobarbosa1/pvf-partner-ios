//
//  FinancingInteres.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 17/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class FinancingInterest: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var interest: String!
    var interestToShow: String!
    
    init(_ interest: String, interestToShow: String) {
        self.interest = interest
        self.interestToShow = interestToShow
    }
    
    func getCode() -> String{
        return interest
    }
    
    func getText() -> String{
        return interestToShow
    }
    
}
