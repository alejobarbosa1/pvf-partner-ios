//
//  EducationalLevel.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 31/10/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class EducationalLevel: NSObject, IBasePickerModel{
    
    override required init() {
        super.init()
    }
    
    var id_level: NSNumber!
    var name_level: String!
    
    init( id_level: NSNumber,
          name_level: String) {
        self.id_level = id_level
        self.name_level = name_level
    }
   
    func getCode() -> String {
        return id_level.description
    }
    
    func getText() -> String {
        return name_level
    }
    
}
