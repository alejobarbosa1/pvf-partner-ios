//
//  Year.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 28/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Year: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var year: String!
    var yearToShow: String!
    
    
    init(_ year: String, yearToShow: String) {
        self.year = year
        self.yearToShow = yearToShow
    }
    
    func getCode() -> String{
        return year
    }
    
    func getText() -> String{
        return yearToShow
    }
    
}
