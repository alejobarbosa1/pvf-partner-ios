//
//  DocumentType.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class DocumentType: NSObject, IBasePickerModel {
    
    override required init() {
        super.init()
    }
    
    var iddoctype: NSNumber!
    var initialdoctype: String!
    var namedoctype: String!
    
    init( iddoctype: NSNumber,
          namedoctype: String) {
        self.iddoctype = iddoctype
        self.namedoctype = namedoctype
    }
    
    func getText() -> String {
        return self.namedoctype
    }
    
    func getCode() -> String {
        return self.iddoctype.description
    }
    
}
