//
//  PriceUntil.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class PriceUntil: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var value: String!
    var valueWithDots: String!
    
    init(_ value: String, valueWithDots: String) {
        self.value = value
        self.valueWithDots = valueWithDots
    }
    
    func getCode() -> String{
        return value
    }
    
    func getText() -> String{
        return valueWithDots
    }
    
}
