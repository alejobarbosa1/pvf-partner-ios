//
//  WorkActivity.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 31/10/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class WorkActivity: NSObject, IBasePickerModel {
    
    override required init() {
        super.init()
    }
    
    var workActivity: String!
    
    init(_ workActivity: String) {
        self.workActivity = workActivity
    }
    
    func getCode() -> String {
        return workActivity
    }
    
    func getText() -> String {
        return workActivity
    }
}
