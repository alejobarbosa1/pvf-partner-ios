//
//  Bank.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 28/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Bank: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var namefinancial: String!
    var idfinancial: NSNumber!
    
    init(_ idfinancial: NSNumber, namefinancial: String) {
        self.idfinancial = idfinancial
        self.namefinancial = namefinancial
    }
    
    func getCode() -> String{
        return idfinancial.description
    }
    
    func getText() -> String{
        return namefinancial
    }
    
}
