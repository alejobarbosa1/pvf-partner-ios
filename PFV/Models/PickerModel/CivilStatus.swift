//
//  CivilStatus.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 31/10/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class CivilStatus: NSObject, IBasePickerModel{
    
    override required init() {
        super.init()
    }
    
    var civilStatus: String!
    
    init(_ civilStatus: String) {
        self.civilStatus = civilStatus
    }
    
    func getCode() -> String {
        return civilStatus
    }
    
    func getText() -> String {
        return civilStatus
    }
}
