//
//  ContractType.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 31/10/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class ContractType: NSObject, IBasePickerModel{
    
    override required init() {
        super.init()
    }
    
    var id_type_cont: NSNumber!
    var name_contract: String!
    
    init( id_type_cont: NSNumber,
          name_contract: String) {
        self.id_type_cont = id_type_cont
        self.name_contract = name_contract
    }
    
    func getCode() -> String {
        return id_type_cont.description
    }
    
    func getText() -> String {
        return name_contract
    }
    
}
