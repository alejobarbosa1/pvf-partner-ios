//
//  PriceFrom.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class PriceFrom: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var valueWithDots: String!
    var value: String!
    
    init(_ value: String, valueWithDots: String) {
        self.value = value
        self.valueWithDots = valueWithDots
    }
    
    func getCode() -> String{
        return value
    }
    
    func getText() -> String{
        return valueWithDots
    }
    
}
