//
//  AnualFees.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 17/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class AnualFees: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var fee: String!
    var feeToShow: String!

    
    init(_ fee: String, feeToShow: String) {
        self.fee = fee
        self.feeToShow = feeToShow
    }
    
    func getCode() -> String{
        return fee
    }
    
    func getText() -> String{
        return feeToShow
    }
    
}
