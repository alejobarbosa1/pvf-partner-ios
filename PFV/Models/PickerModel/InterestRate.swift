//
//  InterestRate.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 17/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class InterestRate: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var value: String!
    var valueToShow: String!
    
    init(_ value: String, valueToShow: String) {
        self.value = value
        self.valueToShow = valueToShow
    }
    
    func getCode() -> String{
        return value
    }
    
    func getText() -> String{
        return valueToShow
    }
    
}
