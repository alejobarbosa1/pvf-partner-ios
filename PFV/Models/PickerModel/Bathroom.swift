//
//  Bathroom.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Bathroom: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var value: String!
    
    init(_ value: String) {
        self.value = value
    }
    
    func getCode() -> String{
        return value
    }
    
    func getText() -> String{
        return value
    }
    
}
