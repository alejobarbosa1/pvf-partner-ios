//
//  Month.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 1/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Month: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var month: String!
    var monthToShow: String!
    
    
    init(_ month: String, monthToShow: String) {
        self.month = month
        self.monthToShow = monthToShow
    }
    
    func getCode() -> String{
        return month
    }
    
    func getText() -> String{
        return monthToShow
    }
    
}
