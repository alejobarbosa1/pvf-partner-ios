//
//  Filter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 23/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Filter: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var idcity: NSNumber!
    var iduser: NSNumber!
    var banhos: NSNumber!
    var tyep_pro: [String]!
    var type_inmu: [String]!
    var precioMe: NSNumber!
    var precioMa: NSNumber!
    var hab: NSNumber!
    var start: NSNumber!
}
