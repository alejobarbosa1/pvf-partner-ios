//
//  Builder.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 17/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Builder: NSObject, IBaseModel {
    
    var builder_iduser: NSNumber!
    var logo_builder: String!
    var namebuilder: String!
    var builder_phone: String!
    
    var iduser: NSNumber!
    var nit: String!
    var phone: String!
    var perbuilder: String!
    var phoneperbuilder: String!
    var email_builder: String!
    
    required override init(){
    }
    
    init( builder_iduser: NSNumber, namebuilder: String, logo_builder: String, builder_phone: String) {
        self.builder_iduser = builder_iduser
        self.namebuilder = namebuilder
        self.logo_builder = logo_builder
        self.builder_phone = builder_phone
    }
}
