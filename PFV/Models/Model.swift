//
//  Model.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Model: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var id_model: String!
    var nameModel: String!
    var status_model: String!
    var banhos: NSNumber!
    var tyep_pro: String!
    var type_inmu: String!
    var hab: String!
    var desc: String!
    var area: NSNumber!
    var piso: [Flat]!
    var photo: [Photo]!
    var year_pay: NSNumber!
    var pay_month: String!
    var porcen_credict: NSNumber!
    var cant_p: String!
    
    //
    
    var price_hou: NSNumber!
    var floor_num: NSNumber!
    var numero: NSNumber!
    var lugar: String!
    
    
}
