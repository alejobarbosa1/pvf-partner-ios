//
//  Credit.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 20/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Credit: NSObject, IBaseModel {
    
    required override init() {
        super.init()
    }
    
    var iduser: NSNumber!
    var num_pers: NSNumber!
    var id_financing: NSNumber!
    var type_hou: String!
    var porcen_credict: NSNumber!
    var year_pay: NSNumber!
    var price_hou: NSNumber!
    var pay_month: NSNumber!
    var type_proyect: String!
    var statuscredict_other: NSNumber!
    var financing_name: String!
    var tea: String!
}
