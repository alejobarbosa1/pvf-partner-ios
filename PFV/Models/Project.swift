//
//  Project.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 10/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Project: NSObject, IBaseModel {
    
    var compras: NSNumber!
    var creditos: NSNumber!
    var favoritos: NSNumber!
    var idcity: String!
    var iduser: NSNumber!
    var start: NSNumber!
    var length: NSNumber!
    
    required override init(){
        super.init()
    }
    
    var builder_iduser: NSNumber!
    var builder_phone: String!
    var namebuilder: String!
    var logo_builder: String!
    var namecity: String!
    var name_country: String!
    var idproj: NSNumber!
    var logoproj: String!
    var nameproj: String!
    var desccriptionproj: String!
    var favorite: NSNumber!
    var basic_price: NSNumber!
    var parking: NSNumber!
    var num_floor: NSNumber!
    var videolink: String!
    var enddate: String!
    var direcction: String!
    var basic_price_string: String! = ""
    var lat_log: Coordinates!
    var kindproj: [Model]!
    var floors : [Floor]!
    //Credit
    var statuscredict: NSNumber!
    var financing_name: String!
    var id_financing: NSNumber!
    //Purchase
    var statusshop: NSNumber!

    
    func getOutsideFlats(model: Model)->[[Flat]]{
        var arrays: [[Flat]] = []
        for _ in 0..<self.num_floor.intValue{
            let flat: [Flat] = []
            arrays.append(flat)
        }
        for flat in model.piso where flat.lugar == Constants.Commons.OUTSIDE{
            arrays[flat.piso.intValue - 1].append(flat)
        }
        return arrays
    }
    
    func getInsideFlats(model: Model)->[[Flat]]{
        var arrays: [[Flat]] = []
        for _ in 0..<self.num_floor.intValue{
            let flat: [Flat] = []
            arrays.append(flat)
        }
        for flat in model.piso where flat.lugar == Constants.Commons.INSIDE{
            arrays[flat.piso.intValue - 1].append(flat)
        }
        return arrays
    }
    
}
