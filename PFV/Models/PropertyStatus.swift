//
//  PropertyStatus.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 13/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class PropertyStatus: NSObject, IBasePickerModel {
    
    required override init() {
        super.init()
    }
    
    var status: String!
    
    
    init(_ status: String) {
        self.status = status
    }
    
    func getCode() -> String{
        return status
    }
    
    func getText() -> String{
        return status
    }
    
}
