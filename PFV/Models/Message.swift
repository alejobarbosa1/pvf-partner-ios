//
//  Message.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers

class Message: NSObject, IBaseModel {
    
    var idrol: NSNumber!
    var messageId: NSNumber!
    var status: NSNumber!
    var body: String!
    var date: NSNumber!
    var sender_id: NSNumber!
    var receiver_id: NSNumber!
    var builder_iduser: NSNumber!
    var adm_iduser: NSNumber!
    var rol_main: NSNumber! 
    
    var id_user_main: NSNumber!
    var id_user_sec: NSNumber!
    var text: String!
    var send_id: NSNumber!
    var rol: NSNumber!
    
    required override init() {
        super.init()
    }
    
    init(messageId: NSNumber,
         rolId: NSNumber,
         body: String,
         date: NSNumber,
         sender_id: NSNumber,
         receiver_id: NSNumber,
         status: NSNumber)
    {
        self.messageId = messageId
        self.idrol = rolId
        self.body = body
        self.date = date
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.status = status
    }
    
}
