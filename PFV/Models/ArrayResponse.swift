//
//  ArrayResponse.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class ArrayResponse: NSObject, IBaseModel{
    
    // MARK: Properties
    
    var objectsArray: [[String: AnyObject]]
    
    required override init() {
        objectsArray = [[String: AnyObject]]()
    }
    
    init(objectsArray: [[String: AnyObject]]) {
        self.objectsArray = objectsArray
    }
    
    // MARK: Actions
    
    func appendElement(_ element: [String: AnyObject]) {
        objectsArray.append(element)
    }
}

