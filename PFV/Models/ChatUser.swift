//
//  ChatUser.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class ChatUser: NSObject, IBaseModel {
    
    var id: NSNumber!
    var rolId: NSNumber!
    var phone: String?
    
    required override init() {
        super.init()
    }
    
    init(id: NSNumber, rolId: NSNumber, phone: String?) {
        self.id = id
        self.rolId = rolId
        self.phone = phone
    }
    
}
