//
//  News.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class News: NSObject, IBaseModel {
    
    var id: NSNumber!
    var title: String!
    var body: String!
    var image: String!
    var date: NSNumber!
    var viewed: NSNumber!
    var createdAt: String!
    
    required override init() {
        super.init()
    }
    
    init(id: NSNumber,
         image: String,
         title: String,
         body: String,
         date: NSNumber,
         viewed: NSNumber) {
        self.id = id
        self.image = image
        self.title = title
        self.body = body
        self.date = date
        self.viewed = viewed
    }
}
