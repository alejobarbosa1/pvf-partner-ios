//
//  Floor.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Floor: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var idproj: NSNumber!
    var logoproj: String!
    var nameproj: String!
    var basic_price: String!
    var favorite: NSNumber!
    var logo_builder: String!
    
    init(idproj: NSNumber,
         logoproj: String,
         nameproj: String,
         basic_price: String,
         favorite: NSNumber,
         logo_builder: String) {
        
        self.idproj = idproj
        self.logoproj = logoproj
        self.nameproj = nameproj
        self.basic_price = basic_price
        self.favorite = favorite
        self.logo_builder = logo_builder
        
    }
}
