//
//  Photo.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Photo: NSObject, IBaseModel {
    
    override required init() {
        super.init()
    }
    
    var photo: String!
}
