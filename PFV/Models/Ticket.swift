//
//  Ticket.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class Ticket: NSObject, IBaseModel {
    
    var idchannel: NSNumber!
    var createdAt: String!
    var statuschannel: NSNumber!
    var namechannel: String!
    
    var id: NSNumber!
    var date: NSNumber!
    var title: String!
    var message: String!
    var status: NSNumber!
    var viewed: NSNumber!
    
    override required init() {
        super.init()
    }
    
}
