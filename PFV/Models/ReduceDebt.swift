//
//  ReduceDebt.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 9/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
@objcMembers
class ReduceDebt: NSObject, IBaseModel {
    
    required override init() {
        super.init()
    }
    
    var iduser: NSNumber!
    var idfinancial: NSNumber!
    var num_creditc: String!
    var type_hou: String!
    var price_hou: NSNumber!
    var balance_credict: NSNumber!
    var num_cuotas: NSNumber!
    var num_cuotas_rest: NSNumber!
    var pay_monthly: NSNumber!
    var segurity: NSNumber!
    
    var full_name_per_2: String!
    var borndate_per_2: String!
    var dateexped_per_2: String!
    var leveleducation_per_2: String!
    var civilstatus_per_2: String!
    var old_lab_per_2: String!
    var act_inde_per_2: String!
    var type_cont_per_2: String!
    var email_per_2: String!
    var income_per_2: String!
    var phoneper_per_2: String!
    var doctype_per_2: String!
    var numberidenf_per_2: String!
    
    
}
