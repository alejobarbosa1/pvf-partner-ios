//
//  CreditPVF.swift
//  PFV
//
//  Created by Alejandro Barbosa on 12/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

class CreditPVF: NSObject, Commission, IBaseModel {
    
    fileprivate let numberFormatter = NumberFormatter()
    
    var iduser: NSNumber!
    var firstname: String!
    var lastname: String!
    var idrol: NSNumber!
    var fullid: String!
    var fullname: String!
    var image: String!
    
    var logoproj: String!
    var direcction: String!
    var enddate: String!
    var kindproj: [Model]!
    var photo: [Photo]!
    var lat_log: Coordinates!
    var nameproj: String!
    var namebuilder: String!
    var logo_builder: String!
    var idproj: NSNumber!
    var id_financing: NSNumber!
    var price_hou: NSNumber!
    var porcen_credict: NSNumber!
    var year_pay: NSNumber!
    var pay_month: String!
    var model: [Flat]!
    var nameModel: String!
    var baths: String!
    var rooms: String!
    var area: String!
    var parking: String!
    var financing_name: String!
    var commission: String!
    var monto: String! {
        didSet {
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = NSNumber(value: Int(monto)!)
            self.commission = numberFormatter.string(from: number)! + " COP"
        }
    }
    
    override required init() { super.init() }
    
}
