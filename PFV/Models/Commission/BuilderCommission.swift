//
//  BuilderCommission.swift
//  PFV
//
//  Created by Alejandro Barbosa on 12/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

class BuilderCommission: NSObject, Commission, IBaseModel {
    
    fileprivate let numberFormatter = NumberFormatter()
    
    var namebuilder: String!
    var nit: String!
    var phone: String!
    var perbuilder: String!
    var phoneperbuilder: String!
    var email_builder: String!
    var commission: String!
    var monto: String! {
        didSet {
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = NSNumber(value: Int(monto)!)
            self.commission = numberFormatter.string(from: number)! + " COP"
        }
    }
    
    override required init() { super.init() }
    
}
