//
//  GiantCommission.swift
//  PFV
//
//  Created by Alejandro Barbosa on 12/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation

class GiantCommission: NSObject, IBaseModel {
    
    lazy var commission = [Commission]()
    
    var credictpvf: [CreditPVF]!
//    {
//        didSet {
//            for i in credictpvf {
//                commission.append(i)
//            }
//        }
//    }
    
    var buypvf: [ProjectPVF]!
//    {
//        didSet {
//            for i in buypvf {
//                commission.append(i)
//            }
//        }
//    }
    
    var othc: [OtherCredit]!
//    {
//        didSet {
//            for i in othc {
//                commission.append(i)
//            }
//        }
//    }
    
    var builder: [BuilderCommission]!
//    {
//        didSet {
//            for i in builder {
//                commission.append(i)
//            }
//        }
//    }
    
    override required init() { super.init() }
}
