//
//  OtherCredit.swift
//  PFV
//
//  Created by Alejandro Barbosa on 12/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

class OtherCredit: NSObject, Commission, IBaseModel {
    
    fileprivate let numberFormatter = NumberFormatter()

    var iduser: NSNumber!
    var firstname: String!
    var lastname: String!
    var fullname: String!
    var fullid: String!
    var image: String!
    
    var id_financing: NSNumber!
    var price_hou: NSNumber!
    var porcen_credict: NSNumber!
    var year_pay: NSNumber!
    var type_proyect: String!
    var type_hou: String!
    var commission: String!
    var financing_name: String!
    var monto: String! {
        didSet {
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = NSNumber(value: Int(monto)!)
            self.commission = numberFormatter.string(from: number)! + " COP"
        }
    }
    
    override required init() { super.init() }
}
