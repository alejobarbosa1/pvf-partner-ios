//
//  ProjectPVF.swift
//  PFV
//
//  Created by Alejandro Barbosa on 12/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

class ProjectPVF: NSObject, Commission, IBaseModel {
    
    fileprivate let numberFormatter = NumberFormatter()
    
    var iduser: NSNumber!
    var firstname: String!
    var lastname: String!
    var idrol: NSNumber!
    var image: String!
    var fullname: String!
    var fullid: String!
    
    var nameModel: String!
    var photo: [Photo]!
    var baths: String!
    var rooms: String!
    var area: String!
    var parking: String!
    var kindproj: [Model]!
    var logoproj: String!
    var direcction: String!
    var enddate: String!
    var lat_log: Coordinates!
    var idproj: NSNumber!
    var price_hou: NSNumber!
    var nameproj: String!
    var idbuilder: NSNumber!
    var namebuilder: String!
    var logo_builder: String!
    var idpays: NSNumber!
    var model_buy: [Flat]!
    var type_hou: String!
    var commission: String!
    var monto: String! {
        didSet {
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = NSNumber(value: Int(monto)!)
            self.commission = numberFormatter.string(from: number)! + " COP"
        }
    }
    
    override required init() { super.init() }
}
