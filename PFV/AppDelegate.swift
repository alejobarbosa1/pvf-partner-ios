//
//  AppDelegate.swift
//  PVF Partner
//
//  Created by Pablo Linares on 19/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let restUser = RestUser()
    
    var currentViewController: BaseViewController? {
        return (self.window!.rootViewController as! UINavigationController).viewControllers.last as? BaseViewController
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        UIApplication.shared.statusBarStyle = .lightContent
        GMSServices.provideAPIKey("AIzaSyBMzepZGfg6a6aTGWmwGRG0ZtJzFF-UUUo")
        UINavigationBar.appearance().titleTextAttributes  = [NSAttributedStringKey.font: Constants.Formats.DEFAULT_FONT]
        FirebaseApp.configure()
        
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
            if error != nil {
                print("Authorization Unsuccessfull")
            } else {
                print("Authorization Successfull")
            }
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .InstanceIDTokenRefresh,
                                               object: nil)
        
        
        
        return true
    }
    
    @objc func tokenRefreshNotification() {
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        let restUser = RestUser()
        if restUser.getCurrentUser() != nil {
            let user = User()
            user.token_firebase = token
            user.iduser = restUser.getCurrentUser()?.iduser
            restUser.updateFireBaseToken(user, listener: self)
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        if restUser.getCurrentUser() != nil {
            let user = User()
            user.token_firebase = fcmToken
            user.iduser = restUser.getCurrentUser()?.iduser
            restUser.updateFireBaseToken(user, listener: self)
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        
        // Print full message.
        processNotification(withData: userInfo as! [String: AnyObject],
                            content_available: false)
        print(userInfo)
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        let controller = ((self.window!.rootViewController as! UINavigationController).viewControllers.last as! HostViewController).internalNavigationController.viewControllers.last
        
        if controller == nil {
            return
        }
        
        let chat = userInfo[Constants.Keys.CHAT] as? String
        let newsDictionary = userInfo[Constants.Keys.NEWS] as? String
        let support = userInfo[Constants.Keys.SUPPORT] as? String
        
        let timeInterval = NSNumber(value: NSDate().timeIntervalSince1970)
        
        if chat != nil {
        
            var message: Message? = Message()
            do {
                try message!.fromJSON(chat!)
            } catch {
                message = nil
            }
            let messageId = NSNumber(value: Date().millisecondsSince1970)
            let body = (userInfo[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.BODY] as? String
            let rol = (userInfo[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.ID_ROL] as? NSNumber
            if rol == Constants.Rols.BUILDER {
                message = Message(messageId: messageId,
                                  rolId: rol!,
                                  body: body!,
                                  date: timeInterval,
                                  sender_id: message!.builder_iduser,
                                  receiver_id: restUser.getCurrentUser()!.iduser,
                                  status: Constants.StatusMessage.SEND)
            } else if rol == Constants.Rols.ADMINISTRATOR {
                message = Message(messageId: messageId,
                                  rolId: rol!,
                                  body: body!,
                                  date: timeInterval,
                                  sender_id: 1,
                                  receiver_id: restUser.getCurrentUser()!.iduser,
                                  status: Constants.StatusMessage.SEND)
            }
            if message == nil {
                return
            }
            
            DbManager.insertMessage(message!)
            
            if controller is ChatViewController {
                print("Is ChatViewController")
                (controller as! ChatViewController).messages.append(message!)
                (controller as! ChatViewController).prepareCells()
                return
            }
            
            if rol == nil {
                completionHandler(UIBackgroundFetchResult.newData)
                return
            }
            return
        }
        
        if newsDictionary != nil {
            
            var news: News? = News()
            do {
                try news!.fromJSON(newsDictionary!)
            } catch {
                news = nil
            }
            let newsId = NSNumber(value: Date().millisecondsSince1970)
            let date = NSNumber(value: Date().timeIntervalSince1970)
            let image = (userInfo[Constants.Keys.NEWS] as! String).convertToDictionary()![Constants.Keys.IMAGE] as? String
            let title = (userInfo[Constants.Keys.NEWS] as! String).convertToDictionary()![Constants.Keys.TITLE] as? String
            let body = (userInfo[Constants.Keys.NEWS] as! String).convertToDictionary()![Constants.Keys.BODY] as? String
            
            news = News(id: newsId,
                        image: image!,
                        title: title!,
                        body: body!,
                        date: date,
                        viewed: Constants.ViewedStatus.UNVIEWED)
            
            if news == nil {
                return
            }
            
            DbManager.inserNews(news!)
            
            if controller is NewsListViewController {
                print("Is NewsViewController")
                (controller as! NewsListViewController).arrayNews.append(news!)
                (controller as! NewsListViewController).prepareCells()
                return
            }
            completionHandler(UIBackgroundFetchResult.newData)
            return
            
        }
        
        if support != nil {
            
            let close = (userInfo[Constants.Keys.SUPPORT] as! String).convertToDictionary()![Constants.Keys.CLOSE] as? NSNumber
            
            var message: Message? = Message()
            do {
                try message!.fromJSON(support!)
            } catch {
                message = nil
            }
            let messageId = NSNumber(value: Date().millisecondsSince1970)
            let body = (userInfo[Constants.Keys.SUPPORT] as! String).convertToDictionary()![Constants.Keys.BODY] as? String
            let channel_id = NSNumber(value: Int(((userInfo[Constants.Keys.SUPPORT] as! String).convertToDictionary()![Constants.Keys.CHANNEL_ID] as? String)!)!)
            
            message = Message(messageId: messageId,
                              rolId: Constants.Rols.SUPPORT,
                              body: body!,
                              date: timeInterval,
                              sender_id: channel_id,
                              receiver_id: restUser.getCurrentUser()!.iduser,
                              status: Constants.StatusMessage.SEND)
            
            if message == nil {
                return
            }
            
            DbManager.insertMessage(message!)
            DbManager.updateViewedSupportTicket(viewed: Constants.ViewedStatus.UNVIEWED, id: channel_id)
            
            if close != nil {
                DbManager.updateStatusSupportTicket(whitStatus: Constants.StatusTicket.CLOSED, id: channel_id)
            }
            
            
            if controller is SupportChatViewController {
                print("Is SupportChatViewController")
                if close != nil {
                    (controller as! SupportChatViewController).isClosed = true
                    (controller as! SupportChatViewController).hideView()
                }
                (controller as! SupportChatViewController).messages.append(message!)
                (controller as! SupportChatViewController).tvChat.reloadData()
                return
            }
            
            return
            
        }
        
    }
    
    func createNotification(_ title: String, body: String, userInfo: [AnyHashable: Any]) {
        
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        content.userInfo = userInfo
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {
                // Something went wrong
            }
        })
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(messaging)
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        // TODO Change to prod to production build
        Messaging.messaging().apnsToken = deviceToken
        self.tokenRefreshNotification()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate  : IResponse{
    
    func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        
    }
    
    func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        
    }
    
    func onFailedPending(_ response: Response, tag: Constants.RepositoriesTags) {
        
    }
    
    
}

extension AppDelegate : UNUserNotificationCenterDelegate, MessagingDelegate {
    
    func processNotification(withData data: [String: AnyObject],
                             content_available: Bool)
    {
        print(data)
        for hostVC in (self.window!.rootViewController as! UINavigationController).viewControllers where hostVC is HostViewController {
            for homeVC in (hostVC as! HostViewController).internalNavigationController.viewControllers where homeVC is HomeViewController {
                let controller = ((self.window!.rootViewController as! UINavigationController).viewControllers.last as! HostViewController).internalNavigationController.viewControllers.last
                
                if data[Constants.Keys.CHAT] != nil {
                    
                    let rol: NSNumber = (data[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.ID_ROL] as! NSNumber
                    let message = (data[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.BODY]! as! String
                    switch rol {
                    case Constants.Rols.ADMINISTRATOR:
                        (homeVC as! HomeViewController).navigateToChatVC(message: message, isBuilder: false, builder: nil)
                        break
                    case Constants.Rols.BUILDER:
                        let builder = Builder()
                        let builderId: NSNumber = NSNumber(value: Int((data[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.BUILDER_ID]! as! String)!)
                        let builderName: String = (data[Constants.Keys.CHAT] as! String).convertToDictionary()![Constants.Keys.TITLE]! as! String
                        builder.builder_iduser = builderId
                        builder.namebuilder = builderName
                        (homeVC as! HomeViewController).navigateToChatVC(message: message, isBuilder: true, builder: builder)
                        break
                    default:
                        break
                    }
                    
                    return
                }
                
                if data[Constants.Keys.NEWS] != nil {
                    if controller is NewsListViewController {
                        return
                    }
                    (homeVC as! HomeViewController).navigateTo(Constants.ViewControllers.NEWS_LIST_VIEW_CONTROLLER)
                    return
                }
                
                if data[Constants.Keys.SUPPORT] != nil {
                    if controller is SupportChatViewController {
                        return
                    }
                    (homeVC as! HomeViewController).navigateTo(Constants.ViewControllers.SUPPORT_VIEW_CONTROLLER)
                }
                
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler(.alert)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let userInfo = response.notification.request.content.userInfo as! [String: AnyObject]
        
        processNotification(withData: userInfo,
                            content_available: false)
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        let userInfo = remoteMessage.appData as! [String: AnyObject]
        
        processNotification(withData: userInfo,
                            content_available: false)
    }
}

