//
//  HomeBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class HomeBL: BaseBL {
	
	// MARK: Properties
    
    var restUser = RestUser()
    var restProject = RestProject()
    var restCredit = RestCredit()
    
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IHomeListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IHomeListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IHomeListener {
        return listener as! IHomeListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .VERIFY_PROFILE:
            getListener().showFullProfile()
            break
        case .LOG_OUT:
            getListener().onSuccesLogout()
            break
        case .LIST_CREDITS:
            if response is ArrayResponse{
                let array = (response as! ArrayResponse).objectsArray
                let credits: [Credit] = Credit.fromJsonArray(array)
                getListener().showCreditsList(credits)
            } else if response is Response {
                getListener().showEmptyCredit()
            }
            break
        default:
            break
        }
    }
    
    override func onFailedPending(_ response: Response, tag: Constants.RepositoriesTags) {
        switch tag {
        case .VERIFY_PROFILE:
            getListener().showIncompleteProfile()
            break
        default:
            break
        }
    }
}

// MARK: BL
extension HomeBL : IHomeBL {
    
    // MARK: Listeners
    
    func verifyProfile() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        restUser.verifyProfile(user, listener: self)
    }
    
    func getCredits() {
        let user = User()
        user.iduser = restUser.getCurrentUser()!.iduser
        restCredit.getCreditsList(user, listener: self)
    }
    
}
