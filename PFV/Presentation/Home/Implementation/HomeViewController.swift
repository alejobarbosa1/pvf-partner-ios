//
//  HomeViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class HomeViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var btMyReferrals: UIButton!
    @IBOutlet weak var btNewReferred: UIButton!
    @IBOutlet weak var btGenerateCredit: UIButton!
    @IBOutlet weak var btSupport: UIButton!
    @IBOutlet weak var btNews: UIButton!
    @IBOutlet weak var btProfile: UIButton!
    fileprivate var isCompletingProfile = false
    
    fileprivate var projectsViewContoroller: SearchProjectsViewController!

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IHomePresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IHomePresenter {
        return presenter as! IHomePresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = HomePresenter(view: self)
        btMyReferrals.centerTextAndImage()
        btNewReferred.centerTextAndImage()
        btGenerateCredit.centerTextAndImage()
        btSupport.centerTextAndImage()
        btNews.centerTextAndImage()
        btProfile.centerTextAndImage()
        setCorner(btMyReferrals.frame.height / 10,
                  views: btMyReferrals,
                  btNewReferred,
                  btGenerateCredit,
                  btSupport,
                  btNews,
                  btProfile)
        addShadow(btMyReferrals,
                  btNewReferred,
                  btGenerateCredit,
                  btSupport,
                  btNews,
                  btProfile)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.navigationItem.rightBarButtonItem = nil
        hostViewController.setTitle(title: Constants.Titles.CATEGORIES)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().verifyProfile()
        }
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        if alertTag == Constants.Tags.COMPLETE_PROFILE {
            navigateTo(Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER)
        }
    }
    
    func navigateTo(_ viewControllerIdentifier: String){
        switch viewControllerIdentifier {
        case Constants.ViewControllers.PROFILE_VIEW_CONTROLLER:
            let profileViewController: ProfileViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! ProfileViewController
            hostViewController.pushViewController(profileViewController, animated: true)
            break
        case Constants.ViewControllers.MY_REFERRALS_VIEW_CONTROLLER:
            let myReferrals: MyReferralsViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! MyReferralsViewController
            hostViewController.pushViewController(myReferrals, animated: true)
            break
        case Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER:
            let completeProfileViewController: CompleteProfileViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! CompleteProfileViewController
            completeProfileViewController.isCompletingProfile = isCompletingProfile
            completeProfileViewController.isEditingProfile = isCompletingProfile
            hostViewController.pushViewController(completeProfileViewController, animated: true)
            break
        case Constants.ViewControllers.SUPPORT_VIEW_CONTROLLER:
            let supportViewController: SupportViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! SupportViewController
            hostViewController.pushViewController(supportViewController, animated: true)
            break
        case Constants.ViewControllers.NEWS_LIST_VIEW_CONTROLLER:
            let newsList: NewsListViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! NewsListViewController
            hostViewController.pushViewController(newsList, animated: true)
            break
        case Constants.ViewControllers.NEW_REFERRED_VIEW_CONTROLLER:
            let newReferredVC: NewReferredViewController! = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! NewReferredViewController
            hostViewController.pushViewController(newReferredVC, animated: true)
        default:
            break
        }
    }
    
    func navigateToChatVC(message: String, isBuilder: Bool, builder: Builder?){
        let chatVC: ChatViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.CHAT_VIEW_CONTROLLER) as! ChatViewController
        chatVC.message = message
        chatVC.builder = builder
        chatVC.isBuilder = isBuilder
        hostViewController.pushViewController(chatVC, animated: true)
    }

    @IBAction func onMyReferralsClick(_ sender: Any) {
        navigateTo(Constants.ViewControllers.MY_REFERRALS_VIEW_CONTROLLER)
    }
    
    @IBAction func onNewReferredClick(_ sender: Any) {
        navigateTo(Constants.ViewControllers.NEW_REFERRED_VIEW_CONTROLLER)
    }
    
    @IBAction func onGenerateCreditClick(_ sender: Any) {
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getCredits()
        }
    }
    
    @IBAction func onSupportClick(_ sender: Any) {
        navigateTo(Constants.ViewControllers.SUPPORT_VIEW_CONTROLLER)
    }
    
    @IBAction func onNewsClick(_ sender: Any) {
        navigateTo(Constants.ViewControllers.NEWS_LIST_VIEW_CONTROLLER)
    }
    
    @IBAction func onProfileClick(_ sender: Any) {
        navigateTo(Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER)
    }
    
}

// MARK: View
extension HomeViewController : IHomeView {
    
    func onSuccesLogout() {
        hideProgress()
        DispatchQueue.main.async {
            self.hostViewController.logOut()
        }
    }
    
    func showFullProfile() {
        hideProgress()
        self.isCompletingProfile = false
    }
    
    func showIncompleteProfile() {
        hideProgress()
        self.isCompletingProfile = true
        DispatchQueue.main.async {
            self.showAlert(alertType: .WARNING,
                           message: Constants.Dialogs.COMPLETE_PROFILE,
                           hasCancelButton: false,
                           tag: Constants.Tags.COMPLETE_PROFILE)
        }
        
    }
    
    func showCreditsList(_ credits: [Credit]) {
        DispatchQueue.main.async {
            let creditsViewController: CreditsViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.CREDITS_VIEW_CONTROLLER) as! CreditsViewController
            self.hostViewController.pushViewController(creditsViewController, animated: true)
        }
    }
    
    func showEmptyCredit() {
        hideProgress()
        DispatchQueue.main.async {
            let generateCreditViewController: GenerateCreditViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.GENERATE_CREDIT) as! GenerateCreditViewController
            generateCreditViewController.isShowingInfo = false
            self.hostViewController.pushViewController(generateCreditViewController, animated: true)
        }
    }
  
}
