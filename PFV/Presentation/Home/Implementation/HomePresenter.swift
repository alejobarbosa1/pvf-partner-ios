//
//  HomePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class HomePresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IHomeBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IHomeView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IHomeView) {
        super.init(baseView: view)
        self.bl = HomeBL(listener: self)
    }
    
    fileprivate func getView() -> IHomeView {
        return view as! IHomeView
    }
    
    fileprivate func getBL() -> IHomeBL {
        return bl as! IHomeBL
    }
}

// MARK: Presenter
extension HomePresenter: IHomePresenter {

      // MARK: Action
    
    func verifyProfile() {
        getBL().verifyProfile()
    }
    
    func getCredits() {
        getBL().getCredits()
    }

}

// MARK: Listener
extension HomePresenter: IHomeListener {

      // MARK: Listeners
    
    func onSuccesLogout() {
        getView().onSuccesLogout()
    }
    
    func showFullProfile() {
        getView().showFullProfile()
    }
    
    func showIncompleteProfile() {
        getView().showIncompleteProfile()
    }
    
    func showCreditsList(_ credits: [Credit]) {
        getView().showCreditsList(credits)
    }
    
    func showEmptyCredit() {
        getView().showEmptyCredit()
    }
    
}

