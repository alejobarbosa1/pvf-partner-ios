//
//  IHomePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IHomePresenter: IBasePresenter {
    
    func verifyProfile()
    
    func getCredits()
  
}
