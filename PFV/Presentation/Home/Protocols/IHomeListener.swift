//
//  IHomeListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IHomeListener: IBaseListener {
    
    func onSuccesLogout()
    
    func showFullProfile()
    
    func showIncompleteProfile()
    
    func showCreditsList(_ credits: [Credit])
    
    func showEmptyCredit()
  
}
