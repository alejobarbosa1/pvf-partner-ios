//
//  IHomeBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IHomeBL: IBaseBL {
  
    func verifyProfile()
    
    func getCredits()
    
}
