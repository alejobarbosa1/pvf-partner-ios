//
//  NewsListPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class NewsListPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? INewsListBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? INewsListView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: INewsListView) {
        super.init(baseView: view)
        self.bl = NewsListBL(listener: self)
    }
    
    fileprivate func getView() -> INewsListView {
        return view as! INewsListView
    }
    
    fileprivate func getBL() -> INewsListBL {
        return bl as! INewsListBL
    }
}

// MARK: Presenter
extension NewsListPresenter: INewsListPresenter {

    // MARK: Actions
    
    func getNews() {
        getBL().getNews()
    }
    
    func changeViewedStatus(byId id: NSNumber) {
        getBL().changeViewedStatus(byId: id)
    }

}

// MARK: Listener
extension NewsListPresenter: INewsListListener {

    // MARK: Listeners
    
    func setNews(_ arrayNews: [News]) {
        getView().setNews(arrayNews)
    }

    func notNews() {
        getView().notNews()
    }
    
}

