//
//  NewsListBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class NewsListBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? INewsListListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: INewsListListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> INewsListListener {
        return listener as! INewsListListener
    }
    
    // MARK: Listeners
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        if tag == Constants.RepositoriesTags.GET_NEWS{
            if (response is ArrayResponse) {
                let array = (response as! ArrayResponse).objectsArray
                let news: [News] = News.fromJsonArray(array)
                getListener().setNews(news)
            } else {
                getListener().notNews()
            }
        }
    }
}

// MARK: BL
extension NewsListBL : INewsListBL {
    
    // MARK: Actions
    
    func getNews() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        user.rol = Constants.Rols.PARTNER
        restUser.getNews(user, listener: self)
    }
    
    func changeViewedStatus(byId id: NSNumber) {
        DbManager.updateViewedNews(viewed: Constants.ViewedStatus.VIEWED, id: id)
    }
    
}
