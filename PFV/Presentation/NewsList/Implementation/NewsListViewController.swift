//
//  NewsListViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class NewsListViewController: BaseViewController {

    // MARK: Outlets
    
    @IBOutlet weak var imgWaves: UIImageView!
    @IBOutlet weak var imgFooter: UIImageView!
    @IBOutlet weak var tvNews: UITableView!
    var arrayNews = [News]()
    var arrayCells = [NewsCell]()
  
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? INewsListPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> INewsListPresenter {
        return presenter as! INewsListPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = NewsListPresenter(view: self)
        tvNews.delegate = self
        tvNews.dataSource = self
        tvNews.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.NEWS)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getNews()
        }
    }
    
    func prepareCells() {
        arrayCells = [NewsCell]()
        for news in arrayNews {
            let newCell = tvNews.dequeueReusableCell(withIdentifier: Constants.Cells.NEWS_CELL) as! NewsCell
            newCell.setCell(news)
            newCell.selectionStyle = .none
            arrayCells.append(newCell)
        }
        tvNews.reloadData()
    }

    // MARK: Actions
}

// MARK: View
extension NewsListViewController : INewsListView {
    
    func setNews(_ arrayNews: [News]) {
        hideProgress()
        DispatchQueue.main.async {
            if !arrayNews.isEmpty {
                self.arrayNews = arrayNews
                self.prepareCells()
            } else {
                self.imgWaves.isHidden = false
            }
        }
    }
    
    func notNews() {
        hideProgress()
        
    }
}

extension NewsListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayCells.count <= 6{
            imgWaves.isHidden = false
        } else {
            imgFooter.isHidden = false
        }
        return arrayCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayCells[indexPath.row].heightCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsVC: NewsViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.NEWS_VIEW_CONTROLLER) as! NewsViewController
        let news = arrayNews[indexPath.row]
        newsVC.news = news
        hostViewController.pushViewController(newsVC, animated: true)
    }
}
