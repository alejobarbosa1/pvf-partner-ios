//
//  NewsCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var heightViewContent: NSLayoutConstraint!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    var heightCell: CGFloat!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCell(_ news: News) {
        if news.viewed == Constants.ViewedStatus.UNVIEWED {
            setCustomStyle(borderColor: .clear, borderWidth: 0, cornerRadius: viewStatus.frame.height / 2, views: viewStatus)
            viewStatus.isHidden = false
        }
        let date = news.createdAt.toDate(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED)
        self.lblDate.text = date!.toString(withFormat: Constants.Formats.DATE_TO_CHAT)
        self.lblTitle.text = news.title
        setCustomStyle(borderColor: .clear, borderWidth: 0, cornerRadius: viewContent.frame.height / 10, views: viewContent)
        self.heightViewContent.constant = 36 + self.lblTitle.frame.height +  self.lblDate.frame.height
        self.heightCell = heightViewContent.constant + 15
    }

}
