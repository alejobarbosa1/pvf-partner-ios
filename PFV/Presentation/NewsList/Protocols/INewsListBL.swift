//
//  INewsListBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol INewsListBL: IBaseBL {
    
    func getNews()
    
    func changeViewedStatus(byId id: NSNumber)
	
}
