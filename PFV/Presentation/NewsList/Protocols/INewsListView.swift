//
//  INewsListView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: View.

protocol INewsListView: IBaseView {
    
    func setNews(_ arrayNews: [News])
    
    func notNews()
	
}
