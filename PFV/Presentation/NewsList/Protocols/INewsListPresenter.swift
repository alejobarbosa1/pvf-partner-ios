//
//  INewsListPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol INewsListPresenter: IBasePresenter {
	
    func getNews()
    
    func changeViewedStatus(byId id: NSNumber)
    
}
