//
//  AuthorizationViewController.swift
//  PFV
//
//  Created by mac on 28/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class AuthorizationViewController: BaseViewController {

    @IBOutlet weak var tvAuthorization: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCorner(tvAuthorization.frame.height / 20, views: tvAuthorization)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hostViewController.setTitle(title: Constants.Titles.AUTHORIZATION)
    }

}
