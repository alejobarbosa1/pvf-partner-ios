//
//  CompleteProfileBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class CompleteProfileBL: BaseBL {
	
	// MARK: Properties
    fileprivate var country: Country!
    var restUser = RestUser()
    var userSaved = User()
    var restCommon = RestCommon()
    var user = User()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ICompleteProfileListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ICompleteProfileListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ICompleteProfileListener {
        return listener as! ICompleteProfileListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        super.onDataResponse(response, tag: tag)
        switch tag {
        case .PICKER_VIEW_DOCUMENT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let documentList: [DocumentType] = DocumentType.fromJsonArray(array)
            getListener().setDocumentList(documentList)
            break
        case .COUNTRIES:
            let array = (response as! ArrayResponse).objectsArray
            let countries: [Country] = Country.fromJsonArray(array)
            getListener().setCountries(countries)
            break
        case .CITIES:
            let array = (response as! ArrayResponse).objectsArray
            let cities: [City] = City.fromJsonArray(array)
            getListener().setCities(cities)
            break
        case .CUSTOMER_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let customerType: [CustomerType] = CustomerType.fromJsonArray(array)
            getListener().setCustomerType(customerType)
            break
        case .EDUCATIONAL_LEVEL:
            let array = (response as! ArrayResponse).objectsArray
            let educationalLevel: [EducationalLevel] = EducationalLevel.fromJsonArray(array)
            getListener().setEducationalLevel(educationalLevel)
            break
        case .CONTRACT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let contractType: [ContractType] = ContractType.fromJsonArray(array)
            getListener().setContractType(contractType)
            break
        case .COMPLETE_PERFIL:
            let array = (response as! ArrayResponse).objectsArray
            let users: [User] = User.fromJsonArray(array)
            if users.count > 0{
                user.borndate = ((user.borndate.toDate(withFormat: Constants.Formats.DATE_TO_SERVER))?.toString(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED))
                user.dateexped = ((user.dateexped.toDate(withFormat: Constants.Formats.DATE_TO_SERVER))?.toString(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED))
                restUser.setCurrentUser(user)
            }
            getListener().onSuccessCompleteProfile()
            break
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        switch tag {
        case .COMPLETE_PERFIL:
            getListener().onFailedCompleteProfile(response.ResponseMessage!)
            break
        case .COUNTRIES:
            restUser.getCountries(self)
            break
        case .CITIES:
            restUser.getCities(self.country, listener: self)
            break
        case .CUSTOMER_TYPE:
            restUser.getCustomerType(self)
            break
        case .EDUCATIONAL_LEVEL:
            restUser.getEducationalList(self)
            break
        case .CONTRACT_TYPE:
            restUser.getContractTypeList(self)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension CompleteProfileBL : ICompleteProfileBL {
    
    // MARK: Listeners
    
    func getCurrentUser(){
        let user = restUser.getCurrentUser()
        self.userSaved = user!
        getListener().setCurrentUser(user!)
    }
    
    func getDocumentTypes() {
        restCommon.getDocumentTypes(self)
    }
    
    func getCountries() {
        restUser.getCountries(self)
    }
    
    func getCities(byCountry country: Country) {
        self.country = country
        restUser.getCities(country, listener: self)
    }
    
    func getCustomerType(){
        restUser.getCustomerType(self)
    }
    
    func getEducationalLevel(){
        restUser.getEducationalList(self)
    }
    
    func getContractType(){
        restUser.getContractTypeList(self)
    }
    
    func completeProfile(_ user: User) {
        if user.email == nil{
            getListener().onEmptyEmail()
            return
        }
        if user.phoneper == nil {
            getListener().onEmptyPhone()
            return
        }
        if user.idcity == nil {
            getListener().onEmptyCity()
            return
        }
        if user.borndate == nil {
            getListener().onEmptyBornDate()
            return
        }
        if user.dateexped == nil {
            getListener().onEmptyExpeditionDate()
            return
        }
        if user.leveleducation == nil {
            getListener().onEmptyEducationalLevel()
            return
        }
        if user.civilstatus == nil {
            getListener().onEmptyCivilStatus()
            return
        }
        if user.act_lab == nil {
            getListener().onEmptyWorkActivity()
            return
        }
        user.old_lab = "\(user.yearAtiquity!)\(",")\(user.monthsAntiquity!)"
        if user.old_lab == nil {
            getListener().onEmptyLaborAntiquity()
            return
        }
        if user.type_cont == nil {
            getListener().onEmptyContractType()
            return
        }
        if user.income == nil {
            getListener().onEmptySalary()
            return
        }
        self.user = user
        restUser.completeProfile(user, listener: self)
    }
    
    func updateImage(_ userImage: User) {
        restUser.updateImage(userImage, listener: self)
    }
    
}
