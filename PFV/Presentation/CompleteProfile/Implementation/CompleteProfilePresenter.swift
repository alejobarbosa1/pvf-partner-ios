//
//  CompleteProfilePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class CompleteProfilePresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ICompleteProfileBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ICompleteProfileView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ICompleteProfileView) {
        super.init(baseView: view)
        self.bl = CompleteProfileBL(listener: self)
    }
    
    fileprivate func getView() -> ICompleteProfileView {
        return view as! ICompleteProfileView
    }
    
    fileprivate func getBL() -> ICompleteProfileBL {
        return bl as! ICompleteProfileBL
    }
}

// MARK: Presenter
extension CompleteProfilePresenter: ICompleteProfilePresenter {

      // MARK: Actions
    
    func getCurrentUser(){
        getBL().getCurrentUser()
    }
    
    func getDocumentTypes() {
        getBL().getDocumentTypes()
    }
    
    func getCountries() {
        getBL().getCountries()
    }
    
    func getCities(byCountry country: Country) {
        getBL().getCities(byCountry: country)
    }
    
    func getCustomerType(){
        getBL().getCustomerType()
    }
    
    func getEducationalLevel(){
        getBL().getEducationalLevel()
    }
    
    func getContractType(){
        getBL().getContractType()
    }
    
    func completeProfile(_ user: User){
        getBL().completeProfile(user)
    }
    
    func updateImage(_ userImage: User) {
        getBL().updateImage(userImage)
    }

}

// MARK: Listener
extension CompleteProfilePresenter: ICompleteProfileListener {

      // MARK: Listeners
    
    func setCurrentUser(_ user: User){
        getView().setCurrentUser(user)
    }
    
    func setDocumentList(_ documentList: [DocumentType]) {
        getView().setDocumentList(documentList)
    }
    
    func setCountries(_ countries: [Country]) {
        getView().setCountries(countries)
    }
    
    func setCities(_ cities: [City]) {
        getView().setCities(cities)
    }
    
    func setCustomerType(_ customerType: [CustomerType]) {
        getView().setCustomerType(customerType)
    }
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel]) {
        getView().setEducationalLevel(educationalLevel)
    }
    
    func setContractType(_ contractType: [ContractType]) {
        getView().setContractType(contractType)
    }
    
    func onEmptyEmail() {
        getView().onEmptyEmail()
    }
    
    func onEmptyPhone() {
        getView().onEmptyPhone()
    }
    
    func onEmptyCity() {
        getView().onEmptyCity()
    }
    
    func onEmptyBornDate() {
        getView().onEmptyBornDate()
    }
    
    func onEmptyExpeditionDate() {
        getView().onEmptyExpeditionDate()
    }
    
    func onEmptyEducationalLevel() {
        getView().onEmptyEducationalLevel()
    }
    
    func onEmptyCivilStatus() {
        getView().onEmptyCivilStatus()
    }
    
    func onEmptyWorkActivity() {
        getView().onEmptyWorkActivity()
    }
    
    func onEmptyLaborAntiquity() {
        getView().onEmptyLaborAntiquity()
    }
    
    func onEmptyContractType() {
        getView().onEmptyContractType()
    }
    
    func onEmptySalary() {
        getView().onEmptySalary()
    }
    
    func onSuccessCompleteProfile() {
        getView().onSuccessCompleteProfile()
    }
    
    func onFailedCompleteProfile(_ responseMessage: String) {
        getView().onFailedCompleteProfile(responseMessage)
    }
    
}

