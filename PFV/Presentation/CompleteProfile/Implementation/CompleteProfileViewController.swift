//
//  CompleteProfileViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import Kingfisher
import Gallery

// MARK: Base
class CompleteProfileViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var btUploadPhoto: UIButton!
    
    // Info Show Profile
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    //
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfCellPhone: UITextField!
    @IBOutlet weak var tfDocumentType: PickerTextField!
    @IBOutlet weak var tfDocumentNumber: UITextField!
    @IBOutlet weak var tfCity: PickerTextField!
    @IBOutlet weak var tfCountry: PickerTextField!
//    @IBOutlet weak var tfCustomerType: PickerTextField!
    @IBOutlet weak var tfBornDate: DatePickerTextField!
    @IBOutlet weak var tfDocumentExpeditionDate: DatePickerTextField!
    @IBOutlet weak var tfEducationalLevel: PickerTextField!
    @IBOutlet weak var tfCivilStatus: PickerTextField!
//    @IBOutlet weak var tfWorkActivity: PickerTextField!
    @IBOutlet weak var tfYearAntiquity: PickerTextField!
    @IBOutlet weak var tfMonthsAntiquity: PickerTextField!
//    @IBOutlet weak var tfContractType: PickerTextField!
//    @IBOutlet weak var tfSalary: CurrencyTextField!
    @IBOutlet weak var btSaveData: UIButton!
    @IBOutlet weak var btEditProfile: UIButton!
    
    // Animation
    @IBOutlet weak var iconCity: UIImageView!
    @IBOutlet weak var viewSeparatorCity: UIView!
    @IBOutlet weak var topIconCity: NSLayoutConstraint!
    //

    fileprivate var user: User!
    fileprivate let numberFormatter = NumberFormatter()
    fileprivate var listDocuments = [DocumentType]()
    fileprivate var listCountries: [Country] = []
    fileprivate var listCities: [City] = []
    fileprivate var listCustomerTyper: [CustomerType] = []
    fileprivate var listEducationalLevel: [EducationalLevel] = []
    fileprivate var listCivilStatus: [CivilStatus] = []
    fileprivate var listWorkActivity: [WorkActivity] = []
    fileprivate var listContractType: [ContractType] = []
    fileprivate var listYear = [Year]()
    fileprivate var listMonth = [Month]()
    fileprivate var country = Country()
    
    var isEditingProfile: Bool!
    var isCompletingProfile = false
    var isCredit = false
    
    fileprivate var count = 0

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ICompleteProfilePresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ICompleteProfilePresenter {
        return presenter as! ICompleteProfilePresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        self.allowRepeatedViewControllers = true 
        super.viewDidLoad()
        presenter = CompleteProfilePresenter(view: self)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getCurrentUser()
            self.getPresenter().getDocumentTypes()
            self.getPresenter().getCountries()
            self.getPresenter().getCustomerType()
            self.getPresenter().getEducationalLevel()
            self.getPresenter().getContractType()
        }
        self.setCorner(viewImage.frame.height / 2, views: viewImage)
        self.setCorner(imgUserProfile.frame.height / 2, views: imgUserProfile)
        self.setCorner(btUploadPhoto.frame.height / 10, views: btUploadPhoto,btEditProfile)
        self.setCorner(btSaveData.frame.height / 10, views: btSaveData)
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CompleteProfileViewController.onScrollClick)))
        tfBornDate.delegate = tfBornDate
        tfBornDate.datePickerTextFieldDelegate = self
        tfBornDate.dateFormatter = Constants.Formats.DATE_TO_SERVER
        tfDocumentExpeditionDate.delegate = tfDocumentExpeditionDate
        tfDocumentExpeditionDate.datePickerTextFieldDelegate = self
        tfDocumentExpeditionDate.dateFormatter = Constants.Formats.DATE_TO_SERVER
        if isEditingProfile {
            editProfile()
            btUploadPhoto.isHidden = false
            btSaveData.isHidden = false 
            if isCompletingProfile{
                if user.image != nil {
                    if user.provider_id != nil {
                        imgUserProfile.kf.setImage(with: URL(string: user.image!))
                    } else {
                        imgUserProfile.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(user.image!)"))
                    }
                } else {
                    imgUserProfile.image = #imageLiteral(resourceName: "default_image")
                }
                hostViewController.setTitle(title: Constants.Titles.COMPLETE_PROFILE)
            } else {
                setData()
                hostViewController.setTitle(title: Constants.Titles.EDIT_PROFILE)
            }
        } else {
            setData()
            viewUserName.isHidden = false
            btEditProfile.isHidden = false
            hostViewController.setTitle(title: Constants.Titles.PROFILE)
        }
        if user.provider_id != nil {
            imgUserProfile.kf.setImage(with: URL(string: user.image!))
        } else if user.image != nil {
            imgUserProfile.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(user.image!)"))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isEditingProfile {
            if isCompletingProfile{
                hostViewController.setTitle(title: Constants.Titles.COMPLETE_PROFILE)
            } else {
                hostViewController.setTitle(title: Constants.Titles.EDIT_PROFILE)
            }
        } else {
            hostViewController.setTitle(title: Constants.Titles.PROFILE)
        }
    }
    
    @objc func onScrollClick() {
        endEditing()
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    func editProfile() {
        tfDocumentType.delegate = self
        tfDocumentType.pickerDataSource = listDocuments
        
//        tfSalary.delegate = tfSalary
        
        tfCountry.delegate = self
        tfCountry.pickerDataSource = listCountries
        
        tfCity.delegate = self
        
//        tfCustomerType.delegate = self
//        tfCustomerType.pickerDataSource = listCustomerTyper
        
        tfEducationalLevel.delegate = self
        tfEducationalLevel.pickerDataSource = listEducationalLevel
        
//        tfContractType.delegate = self
//        tfContractType.pickerDataSource = listContractType
        
        tfYearAntiquity.delegate = self
        for y in 1...40 {
            listYear.append(Year(y.description, yearToShow: y.description + Constants.Commons.YEARS))
        }
        tfYearAntiquity.pickerDataSource = listYear
        tfYearAntiquity.selectedItem = listYear.first
        tfMonthsAntiquity.delegate = self
        for m in 1...11 {
            listMonth.append(Month(m.description, monthToShow: m.description + Constants.Commons.MONTHS))
        }
        tfMonthsAntiquity.pickerDataSource = listMonth
        tfMonthsAntiquity.selectedItem = listMonth.first
        setCivilStatus()
        setWorkActivity()
        tfEmail.text = user.email
        tfCellPhone.text = user.phoneper
    }
    
    func setTfDisbaled(){
        tfEmail.isEnabled = false
        tfCellPhone.isEnabled = false
        tfDocumentType.isEnabled = false
        tfDocumentNumber.isEnabled = false
        tfCountry.isEnabled = false
        topIconCity.constant = 20
        iconCity.isHidden = false
        tfCity.isHidden = false
        viewSeparatorCity.isHidden = false
        tfCity.isEnabled = false
//        tfCustomerType.isEnabled = false
        tfBornDate.isEnabled = false
        tfDocumentExpeditionDate.isEnabled = false
        tfEducationalLevel.isEnabled = false
        tfCivilStatus.isEnabled = false
//        tfWorkActivity.isEnabled = false
        tfYearAntiquity.isEnabled = false
        tfMonthsAntiquity.isEnabled = false
//        tfContractType.isEnabled = false
//        tfSalary.isEnabled = false
    }
    
    func setData(){
        showProgress(withTitle: nil)
        if count < 5 {
            return
        }
        if !isEditingProfile {
            setTfDisbaled()
        }
        if user.image != nil {
            if user.provider_id != nil {
                imgUserProfile.kf.setImage(with: URL(string: user.image!))
            } else {
                imgUserProfile.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(user.image!)"))
            }
        } else {
            imgUserProfile.image = #imageLiteral(resourceName: "default_image")
        }
        lblUserName.text = self.user!.firstName! + " " + self.user!.lastName!
        tfEmail.text = user.email
        tfCellPhone.text = user.phoneper
        if isCompletingProfile {
            hideProgress()
            return
        }
        topIconCity.constant = 20
        tfCity.isHidden = false
        iconCity.isHidden = false
        viewSeparatorCity.isHidden = false
        
        for d in listDocuments where d.iddoctype.description == user.iddoctype {
            tfDocumentType.selectedItem = d
            tfDocumentType.text = d.initialdoctype
        }
        let country = Country()
        country.id_country = user.id_country
        country.name_country = user.name_country
        self.country = country
        tfDocumentNumber.text = user.numberidenf
        tfCountry.selectedItem = Country(id_country: user.id_country, name_country: user.name_country)
        tfCountry.text = user.name_country
        tfCity.selectedItem = City(idcity: user.idcity.description, namecity: user.namecity)
//        for t in listCustomerTyper where t.id_clients.description == user.id_clients {
//            tfCustomerType.selectedItem = t
//        }
        for e in listEducationalLevel where e.id_level.description == user.leveleducation {
            tfEducationalLevel.selectedItem = e
        }
        
//        if user.other_information != nil {
//            if user.other_information.type_cont != nil {
//                for h in listContractType where h.id_type_cont.description == user.other_information.type_cont {
//                    tfContractType.selectedItem = h
//                }
//            }
//        } else {
//            for h in listContractType where h.id_type_cont.description == user.type_cont {
//                tfContractType.selectedItem = h
//            }
//        }
        let bornDate = user.borndate.toDate(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED)
        let expeditionDate = user.dateexped.toDate(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED)
        tfBornDate.text = bornDate?.toString(withFormat: Constants.Formats.DATE_TO_SERVER)
        tfDocumentExpeditionDate.text = expeditionDate?.toString(withFormat: Constants.Formats.DATE_TO_SERVER)
        tfCivilStatus.selectedItem = CivilStatus(user.civilstatus)
//        if user.act_lab != nil {
//            tfWorkActivity.selectedItem = WorkActivity(user.act_lab)
//        } else if user.other_information.act_lab != nil {
//            tfWorkActivity.selectedItem = WorkActivity(user.other_information.act_lab)
//        }
        if user.other_information != nil && user.other_information.old_lab != nil {
            let antiquity = user.other_information.old_lab
            let antiquityArray = antiquity!.components(separatedBy: ",")
            let yearAtiquity: String = antiquityArray[0]
            let monthsAntiquity: String = antiquityArray[1]
            tfYearAntiquity.selectedItem = Year(yearAtiquity, yearToShow: yearAtiquity)
            tfMonthsAntiquity.selectedItem = Month(monthsAntiquity, monthToShow: monthsAntiquity)
        } else {
            let antiquity = user.old_lab
            let antiquityArray = antiquity!.components(separatedBy: ",")
            let yearAtiquity: String = antiquityArray[0]
            let monthsAntiquity: String = antiquityArray[1]
            tfYearAntiquity.selectedItem = Year(yearAtiquity, yearToShow: yearAtiquity)
            tfMonthsAntiquity.selectedItem = Month(monthsAntiquity, monthToShow: monthsAntiquity)
        }
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
//        if user.income != nil  && !user.income.isEmpty {
//            let salary = NSNumber(value: Int(user.income)!)
//            tfSalary.text = numberFormatter.string(from: salary)
//        } else if user.other_information.income != nil && !user.other_information.income.isEmpty {
//            let salary = NSNumber(value: Int(user.other_information.income)!)
//            tfSalary.text = numberFormatter.string(from: salary)
//        }
        hideProgress()
    }
    
    func setCivilStatus(){
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.SINGLE))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.FREE_UNION))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.MARRIED))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.DIVORCED))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.WIDOWER))
        tfCivilStatus.delegate = self
        tfCivilStatus.pickerDataSource = listCivilStatus
        tfCivilStatus.selectedItem = listCivilStatus.first
    }
    
    func setWorkActivity(){
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.EMPLOYEE))
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.INDEPENDENT))
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.RETIRED))
//        tfWorkActivity.delegate = self
//        tfWorkActivity.pickerDataSource = listWorkActivity
//        tfWorkActivity.selectedItem = listWorkActivity.first
    }

    @IBAction func onClickUploadPhoto(_ sender: Any) {
        let gallery = GalleryController()
        gallery.delegate = self
        present(gallery, animated: true, completion: nil)
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        switch tag {
        case Constants.Tags.COUNTRY_TEXT_FIELD:
            let model = (pickerModel as! Country)
            let country = Country()
            country.id_country = model.id_country
            country.name_country = model.name_country
            self.country = country
            tfCity.pickerDataSource = listCities
            getPresenter().getCities(byCountry: model)
            if tfCity.isHidden{
                showAnimate()
            }
            break
        case Constants.Tags.DOCUMENT_YPE_TF:
            tfDocumentType.text = (pickerModel as! DocumentType).initialdoctype
            break
        default:
            break
        }
    }
    
    func showAnimate() {
        DispatchQueue.main.async(execute: { () -> Void in
            self.topIconCity.constant = 20
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                self.tfCity.isEnabled = true
                self.tfCity.isHidden = false
                self.iconCity.isHidden = false
                self.viewSeparatorCity.isHidden = false
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }

    @IBAction func onClickSaveData(_ sender: Any) {
        showProgress(withTitle: nil)
        let user = self.user!
        user.email = tfEmail.text
        user.iddoctype = tfDocumentType.selectedItem?.getCode()
        user.initialDocType = (tfDocumentType.selectedItem as? DocumentType)?.initialdoctype
        user.docType = tfDocumentType.selectedItem?.getText()
        user.numberidenf = tfDocumentNumber.text
        user.borndate = self.tfBornDate.selectedDate?.toString(withFormat: "yyyy-MM-dd")
        user.dateexped = self.tfDocumentExpeditionDate.selectedDate?.toString(withFormat: "yyyy-MM-dd")
        user.phoneper = tfCellPhone.text
        if tfCity.selectedItem?.getCode() != nil {
            user.namecity = tfCity.selectedItem?.getText()
            user.idcity = NSNumber(value: Int((tfCity.selectedItem?.getCode())!)!)
            user.id_country = self.country.id_country
            user.name_country = self.country.name_country
        }
        user.borndate = tfBornDate.text
        user.dateexped = tfDocumentExpeditionDate.text
        if tfEducationalLevel.selectedItem?.getCode() != nil {
            user.leveleducation = tfEducationalLevel.selectedItem?.getCode()
            user.educationalLevel = tfEducationalLevel.selectedItem?.getText()
        }
        user.civilstatus = tfCivilStatus.text
        user.id_clients = ""
        user.act_lab = ""
        user.yearAtiquity = tfYearAntiquity.text
        user.monthsAntiquity = tfMonthsAntiquity.text
        user.type_cont = ""
        user.contractType = ""
        user.id_clients = ""
        user.customerType = ""
        user.income = ""
        DispatchQueue.global().async {
            self.getPresenter().completeProfile(user)
        }
    }
    
    @IBAction func onClickEditProfile(_ sender: Any) {
        let editProfileViewController: CompleteProfileViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER) as! CompleteProfileViewController
        editProfileViewController.isEditingProfile = true
        hostViewController.pushViewController(editProfileViewController, animated: true)
    }
    
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        switch alertTag {
        case Constants.Tags.DEFAULT_OK:
            hideAlertView()
            break
        case Constants.Tags.SUCCESS_COMPLETE_PROFILE:
            if isCompletingProfile{
                if isCredit{
                    hostViewController.onBackClick()
                } else {
                    for homeVC in hostViewController.internalNavigationController.viewControllers where homeVC is HomeViewController{
                        self.navigationController!.popToViewController(homeVC, animated: true)
                    }
                }
            } else {
                onBackClick()
            }
            break
        default:
            break
        }
    }
    
    
}

// MARK: View
extension CompleteProfileViewController : ICompleteProfileView {
    
    func setCurrentUser(_ user: User){
        self.user = user
    }
    
    func setDocumentList(_ documentList: [DocumentType]) {
        DispatchQueue.main.async {
            self.listDocuments = documentList
            self.tfDocumentType.pickerDataSource = self.listDocuments
            self.tfDocumentType.selectedItem = self.listDocuments.first
            self.tfDocumentType.text = self.listDocuments.first?.initialdoctype
            self.count += 1
            self.setData()
        }
    }
    
    func setCountries(_ countries: [Country]) {
        self.tfCountry.isEnabled = true
        DispatchQueue.main.async {
            self.listCountries = countries
            self.tfCountry.pickerDataSource = self.listCountries
            self.count += 1
            self.setData()
        }
    }
    
    func setCities(_ cities: [City]) {
        self.tfCity.isEnabled = true
        DispatchQueue.main.async {
            self.listCities = cities
            self.tfCity.pickerDataSource = self.listCities
            self.tfCity.selectedItem = self.listCities.first
        }
    }
    
    func setCustomerType(_ customerType: [CustomerType]) {
//        self.tfCustomerType.isEnabled = true
        DispatchQueue.main.async {
            self.listCustomerTyper = customerType
//            self.tfCustomerType.pickerDataSource = self.listCustomerTyper
//            self.tfCustomerType.selectedItem = self.listCustomerTyper.first
            self.count += 1
            self.setData()
        }
    }
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel]) {
        self.tfEducationalLevel.isEnabled = true
        DispatchQueue.main.async {
            self.listEducationalLevel = educationalLevel
            self.tfEducationalLevel.pickerDataSource = self.listEducationalLevel
            self.tfEducationalLevel.selectedItem = self.listEducationalLevel.first
            self.count += 1
            self.setData()
        }
    }
    
    func setContractType(_ contractType: [ContractType]) {
//        self.tfContractType.isEnabled = true
        DispatchQueue.main.async {
            self.listContractType = contractType
//            self.tfContractType.pickerDataSource = self.listContractType
            self.count += 1
            self.setData()
        }
    }
    
    func onEmptyEmail() {
        hideProgress()
        showPopover(tfEmail,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyPhone() {
        hideProgress()
        showPopover(tfCellPhone,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyCity() {
        hideProgress()
        showPopover(tfCity,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyBornDate() {
        hideProgress()
        showPopover(tfBornDate,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyExpeditionDate() {
        hideProgress()
        showPopover(tfDocumentExpeditionDate,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyEducationalLevel() {
        hideProgress()
        showPopover(tfEducationalLevel,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyCivilStatus() {
        hideProgress()
        showPopover(tfCivilStatus,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyWorkActivity() {
        hideProgress()
//        showPopover(tfWorkActivity,
//                    message: Constants.Dialogs.FIELD_REQUIRED,
//                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyLaborAntiquity() {
        hideProgress()
        showPopover(tfYearAntiquity,
                    message: Constants.Dialogs.FIELD_REQUIRED,
                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptyContractType() {
        hideProgress()
//        showPopover(tfContractType,
//                    message: Constants.Dialogs.FIELD_REQUIRED,
//                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onEmptySalary() {
        hideProgress()
//        showPopover(tfSalary,
//                    message: Constants.Dialogs.FIELD_REQUIRED,
//                    icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onSuccessCompleteProfile() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: Constants.AlertTypes.DONE,
                      message: Constants.Dialogs.SUCCESSFUL_OPERATION,
                      hasCancelButton: false,
                      tag: Constants.Tags.SUCCESS_COMPLETE_PROFILE)
        }
        
    }
    
    func onFailedCompleteProfile(_ responseMessage: String) {
        hideProgress()
        showAlert(alertType: Constants.AlertTypes.ERROR,
                  message: responseMessage,
                  hasCancelButton: false,
                  tag: Constants.Tags.DEFAULT_OK)
    }
    
}

extension CompleteProfileViewController : DatePickerTextFieldDelegate {
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == tfBornDate {
//            endEditing()
//        }
    }
    
    func onOkClick() {
    }
    
}

extension CompleteProfileViewController : UINavigationControllerDelegate {
}

extension CompleteProfileViewController: GalleryControllerDelegate{
    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
        showProgress(withTitle: nil)
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            self?.hideProgress()
            let chosenImage = resolvedImages[0]
            self?.imgUserProfile.image = chosenImage
            let imageData: Data = UIImageJPEGRepresentation(chosenImage!, 0)!
            let base64String = imageData.base64EncodedString(options: .lineLength64Characters)
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async(execute: {
                self?.user.image = base64String
                let userImage = User()
                userImage.image = Constants.Commons.BASE_IMAGE + base64String
                userImage.iduser = self?.user.iduser
                self?.getPresenter().updateImage(userImage)
            })
        })
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
}
