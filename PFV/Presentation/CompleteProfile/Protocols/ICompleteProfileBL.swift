//
//  ICompleteProfileBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol ICompleteProfileBL: IBaseBL {
    
    func getCurrentUser()
    
    func getDocumentTypes()
    
    func getCountries()
    
    func getCities(byCountry country: Country)
    
    func getCustomerType()
  
    func getEducationalLevel()
    
    func getContractType()
    
    func completeProfile(_ user: User)
    
    func updateImage(_ userImage: User)
    
}
