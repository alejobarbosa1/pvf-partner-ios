//
//  ICompleteProfileView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol ICompleteProfileView: IBaseView {
    
    func setCurrentUser(_ user: User)
    
    func setDocumentList(_ documentList: [DocumentType])
    
    func setCountries(_ countries: [Country])
    
    func setCities(_ cities: [City])
    
    func setCustomerType(_ customerType: [CustomerType])
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel])
    
    func setContractType(_ contractType: [ContractType])
    
    func onEmptyEmail()
    
    func onEmptyPhone()
    
    func onEmptyCity()
    
    func onEmptyBornDate()
    
    func onEmptyExpeditionDate()
    
    func onEmptyEducationalLevel()
    
    func onEmptyCivilStatus()
    
    func onEmptyWorkActivity()
    
    func onEmptyLaborAntiquity()
    
    func onEmptyContractType()
    
    func onEmptySalary()
    
    func onSuccessCompleteProfile()
    
    func onFailedCompleteProfile(_ responseMessage: String)
  
}
