//
//  IBuilderReferredPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IBuilderReferredPresenter: IBasePresenter {
	
}
