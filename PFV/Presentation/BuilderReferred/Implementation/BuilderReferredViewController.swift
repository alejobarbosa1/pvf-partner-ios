//
//  BuilderReferredViewController.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class BuilderReferredViewController: BaseViewController {

    // MARK: Outlets
    @IBOutlet weak var lblBuilderName: UILabel!
    @IBOutlet weak var lblBuilderNIT: UILabel!
    @IBOutlet weak var lblBuilderEmail: UILabel!
    @IBOutlet weak var lblBuilderPhone: UILabel!
    @IBOutlet weak var lblOwnerName: UILabel!
    @IBOutlet weak var lblOwnerCellPhone: UILabel!
    var builder: Builder!
    var builderCommission: BuilderCommission!
    
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IBuilderReferredPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IBuilderReferredPresenter {
        return presenter as! IBuilderReferredPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = BuilderReferredPresenter(view: self)
        setData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.BUILDER)
    }

    // MARK: Actions
    
    func setData(){
        if builder != nil {
            lblBuilderName.text = builder.namebuilder
            lblBuilderNIT.text = builder.nit
            lblBuilderEmail.text = builder.email_builder
            lblBuilderPhone.text = builder.phone
            lblOwnerName.text = builder.perbuilder
            lblOwnerCellPhone.text = builder.phoneperbuilder
        } else {
            lblBuilderName.text = builderCommission.namebuilder
            lblBuilderNIT.text = builderCommission.nit
            lblBuilderEmail.text = builderCommission.email_builder
            lblBuilderPhone.text = builderCommission.phone
            lblOwnerName.text = builderCommission.perbuilder
            lblOwnerCellPhone.text = builderCommission.phoneperbuilder
        }
    }
    
}

// MARK: View
extension BuilderReferredViewController : IBuilderReferredView {
    
}
