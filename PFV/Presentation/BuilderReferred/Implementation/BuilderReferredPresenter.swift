//
//  BuilderReferredPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class BuilderReferredPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IBuilderReferredBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IBuilderReferredView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IBuilderReferredView) {
        super.init(baseView: view)
        self.bl = BuilderReferredBL(listener: self)
    }
    
    fileprivate func getView() -> IBuilderReferredView {
        return view as! IBuilderReferredView
    }
    
    fileprivate func getBL() -> IBuilderReferredBL {
        return bl as! IBuilderReferredBL
    }
}

// MARK: Presenter
extension BuilderReferredPresenter: IBuilderReferredPresenter {

    // MARK: Actions

}

// MARK: Listener
extension BuilderReferredPresenter: IBuilderReferredListener {

    // MARK: Listeners
    
}

