//
//  BuilderReferredBL.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class BuilderReferredBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IBuilderReferredListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IBuilderReferredListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IBuilderReferredListener {
        return listener as! IBuilderReferredListener
    }
    
    // MARK: Listeners
}

// MARK: BL
extension BuilderReferredBL : IBuilderReferredBL {
    
    // MARK: Actions
    
}
