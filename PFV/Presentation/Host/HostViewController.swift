//
//  HostViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 3/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class HostViewController: LABMenuViewController {
    
    var menuButton: UIBarButtonItem!
    var backButton: UIBarButtonItem!
    fileprivate var rightButton: UIBarButtonItem!
    fileprivate var restUser  = RestUser()
    
    override func viewDidLoad() {
        barColor = .clear
        menuProportionalWidth = 0.8
        hideMenuButtonWhenShow = true
        shouldNavigateToPreviousViewController = true
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.lightGray], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor : UIColor.white], for: .normal)
        setProperties()
        navigateToHome()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func setProperties(){
        backPosition = .right
        menuView.setContentView(contentView: SlideMenu(delegate: self))
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icon_menu"), for: .normal)
        button.backgroundColor = Constants.Colors.YELLOW
        menuButton = UIBarButtonItem(customView: button)
        menuButton.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LABMenuViewController.toggleLeft)))
        setMenuButton(button: menuButton)
        backButton = UIBarButtonItem.barButton(nil,
                                               image: #imageLiteral(resourceName: "icon_arrow_left"),
                                               titleColor: UIColor.white,
                                               font: UIFont.systemFont(ofSize: 12),
                                               inContext: self,
                                               selector: #selector(LABMenuViewController.onBackClick))
        setBackButton(button: backButton)
    }
    
    func showMenu() {
        toggleLeft()
    }
    
    override func selectItemAt(indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                navigateToHome()
                break
            case 1:
                navigateToTerms()
                break
            case 2:
                showAlert(alertType: Constants.AlertTypes.WARNING,
                          message: Constants.Dialogs.SIGN_OFF,
                          hasCancelButton: true,
                          tag: Constants.Tags.LOGOUT_ACCOUNT)
                break
            default:
                break
            }
            
            break
        default:
            break
        }
        menuView.hide()
    }
    
    
    override func onHelpClick() {
        showInfoAlert()
    }
    
    func backVC(){
        onBackClick()
    }
    
    func setTitle(title: String) {
        self.title = title
    }
    
    func showAlert(alertType: Constants.AlertTypes,
                   message: String,
                   hasCancelButton: Bool,
                   tag: Int)
    {
        let currentViewController = internalNavigationController.viewControllers.last! as! BaseViewController
        currentViewController.showAlert(alertType: alertType, message: message, hasCancelButton: hasCancelButton, tag: tag)
    }
    
    func showInfoAlert(){
        let currentViewController = internalNavigationController.viewControllers.last! as! BaseViewController
        currentViewController.showInfoAlert(isPartner: true)
    }
    
    
    func logOut(){
        for loginVC in navigationController!.viewControllers where loginVC is LoginViewController {
            navigationController!.popToViewController(loginVC, animated: true)
            DbManager.cleanData()
            restUser.setCurrentUser(nil)
            return
        }
        let loginVC: LoginViewController!
        loginVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.LOGIN_VIEW_CONTROLLER) as! LoginViewController
        navigationController!.viewControllers.insert(loginVC, at: 1)
        DbManager.cleanData()
        restUser.setCurrentUser(nil)
        navigationController!.popToViewController(loginVC, animated: true)
    }
    
    func navigateToHome(){
        let homeViewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.ViewControllers.HOME_VIEW_CONTROLLER)
        self.pushViewController(homeViewController,
                                animated: true)
    }
    
    func navigateToTerms(){
        let termsViewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.ViewControllers.TERMS_AND_CONDITIONS)
        self.pushViewController(termsViewController,
                                animated: true)
    }
}
