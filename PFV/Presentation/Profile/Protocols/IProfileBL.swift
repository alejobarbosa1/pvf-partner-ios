//
//  IProfileBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IProfileBL: IBaseBL {
  
    func getCurrentUser()
    
}
