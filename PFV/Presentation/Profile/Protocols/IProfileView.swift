//
//  IProfileView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol IProfileView: IBaseView {
  
    func setCurrentUser(_ user: User)
    
}
