//
//  IProfilePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IProfilePresenter: IBasePresenter {
  
    func getCurrentUser()
    
}
