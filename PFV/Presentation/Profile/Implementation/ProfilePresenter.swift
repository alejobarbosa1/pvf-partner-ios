//
//  ProfilePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ProfilePresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IProfileBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IProfileView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IProfileView) {
        super.init(baseView: view)
        self.bl = ProfileBL(listener: self)
    }
    
    fileprivate func getView() -> IProfileView {
        return view as! IProfileView
    }
    
    fileprivate func getBL() -> IProfileBL {
        return bl as! IProfileBL
    }
}

// MARK: Presenter
extension ProfilePresenter: IProfilePresenter {

      // MARK: Actions

    func getCurrentUser(){
        getBL().getCurrentUser()
    }
    
}

// MARK: Listener
extension ProfilePresenter: IProfileListener {

      // MARK: Listeners
    
    func setCurrentUser(_ user: User){
        getView().setCurrentUser(user)
    }
    
}

