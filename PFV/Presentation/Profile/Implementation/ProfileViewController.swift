//
//  ProfileViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import Kingfisher


// MARK: Base
class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCellPhone: UILabel!
    @IBOutlet weak var btCompleteProfile: UIButton!
    var user: User!
    
  
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IProfilePresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IProfilePresenter {
        return presenter as! IProfilePresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProfilePresenter(view: self)
        self.setCorner(viewImage.frame.height / 2, views: viewImage)
        self.setCorner(imgUser.frame.height / 2, views: imgUser)
        self.setCorner(btCompleteProfile.frame.height / 10, views: btCompleteProfile)
        DispatchQueue.global().async {
            self.getPresenter().getCurrentUser()
        }
        setProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.PROFILE)
    }
    
    private func setProfile() {
        imgUser.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(user.image)"))
        lblName.text = user!.firstName! + " " + user!.lastName!
        lblEmail.text = user.email
        lblCellPhone.text = user.phoneper
    }

    @IBAction func onCompleteProfileClick(_ sender: Any) {
        let completeProfileViewController: CompleteProfileViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER) as! CompleteProfileViewController
        completeProfileViewController.isEditingProfile = true
        completeProfileViewController.isCompletingProfile = true
        hostViewController.pushViewController(completeProfileViewController, animated: true)
    }
}

// MARK: View
extension ProfileViewController : IProfileView {
  
    func setCurrentUser(_ user: User){
        DispatchQueue.main.async {
            self.user = user
        }
    }
    
}
