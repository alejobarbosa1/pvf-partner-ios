//
//  ProfileBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ProfileBL: BaseBL {
	
	// MARK: Properties
    
    var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IProfileListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IProfileListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IProfileListener {
        return listener as! IProfileListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension ProfileBL : IProfileBL {
    
    // MARK: Listeners
    
    func getCurrentUser(){
        let user = restUser.getCurrentUser()
        getListener().setCurrentUser(user!)
    }
    
}
