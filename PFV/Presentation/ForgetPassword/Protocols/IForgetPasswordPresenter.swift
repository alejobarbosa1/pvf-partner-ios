//
//  IForgetPasswordPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IForgetPasswordPresenter: IBasePresenter {
    
    func sendEmail(_ email : String)
    
    func validateCode(_ user: User)
  
}
