//
//  IForgetPasswordView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol IForgetPasswordView: IBaseView {
    
    func notifyEmptyEmail()
    
    func notifyWrongEmail()
    
    func notifySuccesEmail(_ response: Response)
    
    func notifyChangePassword(_ response: Response)
    
    func notifyEmptyCode()
    
    func notifyEmptyPassword()
  
}
