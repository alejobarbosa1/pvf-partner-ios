//
//  IForgetPasswordBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IForgetPasswordBL: IBaseBL {
    
    func sendEmail(_ email: String)
    
    func validateCode(_ user: User)
  
}
