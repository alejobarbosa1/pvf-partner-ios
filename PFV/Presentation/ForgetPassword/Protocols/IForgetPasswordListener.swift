//
//  IForgetPasswordListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IForgetPasswordListener: IBaseListener {
    
    func onEmptyEmail()
    
    func onWrongEmail()
    
    func onSuccessEmail(_ response : Response)
    
    func onSuccesChangePassword(_ response : Response)
    
    func onEmptyCode()
    
    func onEmptyPassword()
  
}
