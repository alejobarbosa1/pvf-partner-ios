//
//  ForgetPasswordBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ForgetPasswordBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate let restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IForgetPasswordListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IForgetPasswordListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IForgetPasswordListener {
        return listener as! IForgetPasswordListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        if tag == Constants.RepositoriesTags.FORGET_PASSWORD {
            getListener().onSuccessEmail(response as! Response)
        }
        if tag == Constants.RepositoriesTags.SET_PASSWORD {
            getListener().onSuccesChangePassword(response as! Response)
        }
    }
}

// MARK: BL
extension ForgetPasswordBL : IForgetPasswordBL {
    
    // MARK: Listeners
    
    func sendEmail(_ email: String) {
        if email.isEmpty {
            getListener().onEmptyEmail()
            return
        }
        
        if !Validator.isValidEmail(email) {
            getListener().onWrongEmail()
            return
        }
        let user = User()
        user.email = email
        restUser.forgetPassword(user, listener: self)
    }
    
    func validateCode(_ user: User) {
        if (user.code?.isEmpty)! {
            getListener().onEmptyCode()
            return
        }
        if user.password.isEmpty {
            getListener().onEmptyPassword()
            return
        }
        restUser.setPassword(user, listener: self)
        
    }
    
}
