//
//  ForgetPasswordViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class ForgetPasswordViewController: BaseViewController {
  
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btForgetPassword: UIButton!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var tfCode: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var topViewPassword: NSLayoutConstraint!
    
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IForgetPasswordPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IForgetPasswordPresenter {
        return presenter as! IForgetPasswordPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ForgetPasswordPresenter(view: self)
        self.setCorner(btForgetPassword.frame.height / 10, views: btForgetPassword)
    }
    @IBAction func onClickBack(_ sender: Any) {
        self.onBackClick()
    }
    
    @IBAction func onClickForgetPassword(_ sender: Any) {
        if viewPassword.isHidden {
            showProgress(withTitle: nil)
            DispatchQueue.global().async {
                self.getPresenter().sendEmail(self.tfEmail.text!)
                }
            }
            else{
                showProgress(withTitle: nil)
                let user = User()
                user.email = self.tfEmail.text
                user.code = self.tfCode.text
                user.password = self.tfPassword.text
                DispatchQueue.global().async {
                    self.getPresenter().validateCode(user)
            }
        }
    }
    
    func showAnimate() {
        DispatchQueue.main.async(execute: { () -> Void in
            self.hideProgress()
            self.btForgetPassword.setTitle(Constants.Dialogs.CHANGE_PASSWORD, for: UIControlState())
            self.topViewPassword.constant = 0
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                self.viewPassword.isHidden = false
                self.viewPassword.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        if alertTag == Constants.Tags.SUCCES_PASSWORD {
            self.onBackClick()
        }
        if alertTag == Constants.Tags.SEND_CODE {
            if viewPassword.isHidden{
                showAnimate()
            }
        }
    }
}

// MARK: View
extension ForgetPasswordViewController : IForgetPasswordView {
    
    func notifyEmptyEmail() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfEmail,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifyWrongEmail() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfEmail,
                             message: Constants.Dialogs.EMAIL_INVALID,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifySuccesEmail(_ response: Response) {
        hideProgress()
        tfEmail.isEnabled = false
        DispatchQueue.main.async {
            self.showAlert(alertType: Constants.AlertTypes.DONE,
                           message: response.ResponseMessage!,
                           hasCancelButton: false,
                           tag: Constants.Tags.SEND_CODE)
        }
    }
    
    func notifyChangePassword(_ response: Response) {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: Constants.AlertTypes.DONE,
                           message: response.ResponseMessage!,
                           hasCancelButton: false,
                           tag: Constants.Tags.SUCCES_PASSWORD)
        }
    }
    
    func notifyEmptyCode() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfCode,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    func notifyEmptyPassword() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfPassword,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
  
}
