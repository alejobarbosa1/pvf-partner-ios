//
//  ForgetPasswordPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ForgetPasswordPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IForgetPasswordBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IForgetPasswordView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IForgetPasswordView) {
        super.init(baseView: view)
        self.bl = ForgetPasswordBL(listener: self)
    }
    
    fileprivate func getView() -> IForgetPasswordView {
        return view as! IForgetPasswordView
    }
    
    fileprivate func getBL() -> IForgetPasswordBL {
        return bl as! IForgetPasswordBL
    }
}

// MARK: Presenter
extension ForgetPasswordPresenter: IForgetPasswordPresenter {

      // MARK: Actions
    
    func sendEmail(_ email: String) {
        getBL().sendEmail(email)
    }
    
    func validateCode(_ user: User) {
        getBL().validateCode(user)
    }

}

// MARK: Listener
extension ForgetPasswordPresenter: IForgetPasswordListener {

      // MARK: Listeners
    
    func onEmptyEmail() {
        getView().notifyEmptyEmail()
    }
    
    func onWrongEmail() {
        getView().notifyWrongEmail()
    }
    
    func onSuccessEmail(_ response: Response) {
        getView().notifySuccesEmail(response)
    }
    
    func onSuccesChangePassword(_ response: Response) {
        getView().notifyChangePassword(response)
    }
    
    func onEmptyCode() {
        getView().notifyEmptyCode()
    }
    func onEmptyPassword() {
        getView().notifyEmptyPassword()
    }
    
}

