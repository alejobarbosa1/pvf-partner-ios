//
//  BuildingViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 6/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class BuildingViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btOutsides: UIButton!
    @IBOutlet weak var btInsides: UIButton!
    @IBOutlet weak var btNext: UIButton!
    var project: Project!
    var modelSelected: Model!
    fileprivate var isOutsides: Bool! = true
    fileprivate var flatSelected = Flat()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        hostViewController.setTitle(title: Constants.Titles.APPARTMENT)
        updateTableContentInset()
        setCorner(btOutsides.frame.height / 5, views: btOutsides, btInsides)
        setCorner(btNext.frame.height / 10, views: btNext)
    }
    
    func updateTableContentInset() {
        let numRows = tableView(tableView, numberOfRowsInSection: 0)
        var contentInsetTop = self.tableView.bounds.size.height
        for i in 0..<numRows {
            contentInsetTop -= tableView(tableView, heightForRowAt: IndexPath(item: i, section: 0))
            if contentInsetTop <= 0 {
                contentInsetTop = 0
            }
        }
        tableView.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0)
    }
    
    @IBAction func onOutsidesClick(_ sender: Any) {
        if !isOutsides {
            btOutsides.backgroundColor = Constants.Colors.PINK
            btInsides.backgroundColor = Constants.Colors.BLUE_DARK
            isOutsides = true
            tableView.reloadData()
        }
    }
    
    @IBAction func onInsidesClick(_ sender: Any) {
        if isOutsides {
            btOutsides.backgroundColor = Constants.Colors.BLUE_DARK
            btInsides.backgroundColor = Constants.Colors.PINK
            isOutsides = false
            tableView.reloadData()
        }
    }
    
    @IBAction func onSelectClick(_ sender: Any) {
        for vc in self.navigationController!.viewControllers where vc is PropertyDetailViewController {
            (vc as! PropertyDetailViewController).isFlatSelected = true
            (vc as! PropertyDetailViewController).flatSelected = self.flatSelected
        }
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension BuildingViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return project.num_floor.intValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.BUILDING_CELL, for: indexPath) as! BuildingCell
        if isOutsides {
            cell.setCell(project.getOutsideFlats(model: modelSelected)[(project.num_floor.intValue - 1) - indexPath.row])
            cell.navigationDelegate = self
        } else {
            cell.setCell(project.getInsideFlats(model: modelSelected)[(project.num_floor.intValue - 1) - indexPath.row])
            cell.navigationDelegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}

extension BuildingViewController : BuildingCellDelegate {
    
    func onCellClick(numero: String) {
        for i in 0..<self.project.kindproj.count{
            for j in 0..<self.project.kindproj[i].piso.count{
                if self.project.kindproj[i].piso[j].numero == numero{
                    self.project.kindproj[i].piso[j].isSelected = 1
                    self.flatSelected = self.project.kindproj[i].piso[j]
                }
                else{
                    self.project.kindproj[i].piso[j].isSelected = 0
                }
            }
        }
        self.tableView.reloadData()
        self.btNext.isEnabled = true
    }
    
    func onDisableCellClick(flat: Flat) {
        
    }
    
}
