//
//  FlatCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 6/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

protocol FlatCellDelegate {
}

class FlatCell: UICollectionViewCell {
    @IBOutlet weak var labelNumber: UILabel!
    @IBOutlet weak var viewContent: UIView!
    
    
    func setCell(_ flat: Flat){
        self.viewContent.layer.cornerRadius = self.viewContent.frame.height / 10
        labelNumber.text = flat.numero
        if flat.status_model != "1" {
            labelNumber.textColor = Constants.Colors.GRAY
        }
    }
}
