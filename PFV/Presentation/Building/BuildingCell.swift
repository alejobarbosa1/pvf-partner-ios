//
//  BuildingCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 6/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

protocol BuildingCellDelegate {
    func onCellClick(numero: String)
    
    func onDisableCellClick(flat: Flat)
}

class BuildingCell: UITableViewCell {
    var array: [Flat]!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var navigationDelegate: BuildingCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(_ array: [Flat]){
        self.array = array
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    }

}

extension BuildingCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if array[indexPath.row].isSelected == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.FLAT_CELL, for: indexPath) as! FlatCell
            cell.setCell(array[indexPath.row])
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.FLAT_CELL_2, for: indexPath) as! FlatCell
            cell.setCell(array[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if array[indexPath.row].status_model == "1" {
            navigationDelegate.onCellClick(numero: array[indexPath.row].numero)
        } else {
            navigationDelegate.onDisableCellClick(flat: array[indexPath.row])
        }
    }
    
}

extension BuildingCell : FlatCellDelegate {
    
}
