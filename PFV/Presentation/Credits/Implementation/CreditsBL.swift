//
//  CreditsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class CreditsBL: BaseBL {
	
	// MARK: Properties
    
    var restCredit = RestCredit()
    var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ICreditsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ICreditsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ICreditsListener {
        return listener as! ICreditsListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        super.onDataResponse(response, tag: tag)
        switch tag {
        case .LIST_CREDITS:
            let array = (response as! ArrayResponse).objectsArray
            let credits: [Credit] = Credit.fromJsonArray(array)
            getListener().setCreditsList(credits)
            break
        default:
            break
        }
    }
    
}

// MARK: BL
extension CreditsBL : ICreditsBL {
    
    // MARK: Listeners
    
    func getCreditsList() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        restCredit.getCreditsList(user, listener: self)
    }
    
}
