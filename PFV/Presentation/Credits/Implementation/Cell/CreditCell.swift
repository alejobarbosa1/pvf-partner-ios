//
//  CreditCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class CreditCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblCreditPrice: UILabel!
    @IBOutlet weak var viewCreditStatus: UIView!
    @IBOutlet weak var lblCreditStatus: UILabel!
    fileprivate let numberFormatter = NumberFormatter()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(credit: Credit) {
        self.viewContainer.layer.cornerRadius = viewContainer.frame.height / 20
        self.viewCreditStatus.layer.cornerRadius = viewCreditStatus.frame.height / 2
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        self.lblCreditPrice.text = numberFormatter.string(from: credit.price_hou)
        if credit.statuscredict_other == 0 {
            lblCreditStatus.text = Constants.ProjectStatus.REJECTED
            viewCreditStatus.layer.backgroundColor = Constants.Colors.RED.cgColor
        } else if credit.statuscredict_other.intValue == 1{
            lblCreditStatus.text = Constants.ProjectStatus.APPROVED
            viewCreditStatus.layer.backgroundColor = Constants.Colors.GREEN_LIGHT.cgColor
        } else if credit.statuscredict_other.intValue >= 2{
            lblCreditStatus.text = Constants.ProjectStatus.IN_PROCESS
            viewCreditStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
        }
    }

}
