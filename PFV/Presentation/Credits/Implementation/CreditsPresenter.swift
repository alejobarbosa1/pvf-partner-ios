//
//  CreditsPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class CreditsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ICreditsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ICreditsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ICreditsView) {
        super.init(baseView: view)
        self.bl = CreditsBL(listener: self)
    }
    
    fileprivate func getView() -> ICreditsView {
        return view as! ICreditsView
    }
    
    fileprivate func getBL() -> ICreditsBL {
        return bl as! ICreditsBL
    }
}

// MARK: Presenter
extension CreditsPresenter: ICreditsPresenter {

      // MARK: Actions
    
    func getCreditsList() {
        getBL().getCreditsList()
    }

}

// MARK: Listener
extension CreditsPresenter: ICreditsListener {

      // MARK: Listeners

    func setCreditsList(_ credits: [Credit]) {
        getView().setCreditsList(credits)
    }
    
}

