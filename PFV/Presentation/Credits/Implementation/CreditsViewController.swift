//
//  CreditsViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class CreditsViewController: BaseViewController {
  
  	// MARK: Properties

    @IBOutlet weak var footerWaves: UIImageView!
    @IBOutlet weak var bgWaves: UIImageView!
    @IBOutlet weak var tvCredits: UITableView!
    @IBOutlet weak var btGenerateCredit: UIButton!
    var creditList = [Credit]()
    
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ICreditsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ICreditsPresenter {
        return presenter as! ICreditsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CreditsPresenter(view: self)
        setCorner(btGenerateCredit.frame.height / 10, views: btGenerateCredit)
        tvCredits.delegate = self
        tvCredits.dataSource = self
        tvCredits.separatorStyle = .none
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.CREDITS)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getCreditsList()
        }
    }
    
    @IBAction func onGenerateCreditClick(_ sender: Any) {
        let generateCredit: GenerateCreditViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.GENERATE_CREDIT) as! GenerateCreditViewController
        generateCredit.isShowingInfo = false
        hostViewController.pushViewController(generateCredit, animated: true)
    }
}

// MARK: View
extension CreditsViewController : ICreditsView {
    
    func setCreditsList(_ credits: [Credit]) {
        hideProgress()
        DispatchQueue.main.async {
            self.creditList = credits
            if self.creditList.count <= 6 {
                self.bgWaves.isHidden = false
                self.footerWaves.isHidden = true
            } else {
                self.bgWaves.isHidden = true
                self.footerWaves.isHidden = false
            }
            self.tvCredits.reloadData()
        }
    }
  
}

extension CreditsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creditList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tvCredits.dequeueReusableCell(withIdentifier: Constants.Cells.CREDIT_CELL) as! CreditCell
        cell.setData(credit: creditList[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let generateCredit: GenerateCreditViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.GENERATE_CREDIT) as! GenerateCreditViewController
        generateCredit.credit = creditList[indexPath.row]
        generateCredit.isShowingInfo = true
        self.hostViewController.pushViewController(generateCredit, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
