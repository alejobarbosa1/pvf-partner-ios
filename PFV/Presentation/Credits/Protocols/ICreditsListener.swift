//
//  ICreditsListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol ICreditsListener: IBaseListener {
    
    func setCreditsList(_ credits: [Credit])
  
}
