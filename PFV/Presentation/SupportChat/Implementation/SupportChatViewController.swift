//
//  SupportChatViewController.swift
//  PFV
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright (c) 2018 CRIZZ Digital Agency. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class SupportChatViewController: BaseViewController {
    
    @IBOutlet weak var tvChat: UITableView!
    @IBOutlet weak var topTableView: NSLayoutConstraint!
    @IBOutlet weak var viewTf: UIView!
    @IBOutlet weak var tfMessage: UITextField!
    @IBOutlet weak var bottomViewTf: NSLayoutConstraint!
    
    // MARK: Properties
    var ticket: Ticket!
    var arrayCells = [MessageSupportCell]()
    var cellTitle: TitleMessageCell!
    var messages = [Message]()
    var user = User()
    var isClosed: Bool!
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ISupportChatPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ISupportChatPresenter {
        return presenter as! ISupportChatPresenter
    }
    
    // MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SupportChatPresenter(view: self)
        tvChat.delegate = self
        tvChat.dataSource = self
        tvChat.separatorStyle = .none
        showProgress(withTitle: nil)
        setTitleCell()
        tvChat.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SupportChatViewController.endEditing)))
        DispatchQueue.global().async {
            self.getPresenter().getMessages(by: self.ticket.id)
            self.getPresenter().getCurrentUser()
        }
        if isClosed {
            
            hideView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.SUPPORT)
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        super.keyboardWillShow(notification: notification)
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.topTableView.constant += keyboardHeight
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    override func endEditing() {
        super.endEditing()
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.topTableView.constant = 15
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    func hideView() {
        viewTf.isHidden = true
        bottomViewTf.constant = -60
    }
    
    func setTitleCell() {
        cellTitle = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.TITLE_MESSAGE_CELL) as! TitleMessageCell
        cellTitle.selectionStyle = .none
        cellTitle.setCell(ticket)
    }
    
    func prepareCells(){
        //        cellTitle = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.TITLE_MESSAGE_CELL) as! TitleMessageCell
        //        cellTitle.selectionStyle = .none
        //        cellTitle.setCell(ticket)
        for message in messages {
            if message.sender_id == ticket.id {
                let newCell: MessageSupportCell = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SUPPORT_CELL_RECEIVED) as! MessageSupportCell
                newCell.setCell(message)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            } else if message.receiver_id == ticket.id {
                let newCell: MessageSupportCell = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SUPPORT_CELL_SENT) as! MessageSupportCell
                newCell.setCell(message)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            }
        }
        tvChat.reloadData()
    }
    
    func changeStatusMessage(withId id: NSNumber, status: NSNumber){
        for message in messages where message.messageId == id{
            message.status = status
        }
        tvChat.reloadData()
        self.tvChat.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
        //        prepareCells()
    }
    
    @IBAction func onSendClick(_ sender: Any) {
        let message = Message()
        let messageId = NSNumber(value: Date().millisecondsSince1970)
        let date = NSNumber(value: Date().timeIntervalSince1970)
        message.body = tfMessage.text
        message.status = Constants.StatusMessage.PENDING
        message.messageId = messageId
        message.receiver_id = ticket.id
        message.date = date
        message.sender_id = user.iduser
        message.idrol = Constants.Rols.USER
        getPresenter().sendMessage(message)
        self.tfMessage.text = ""
        messages.append(message)
        //        prepareCells()
        tvChat.reloadData()
        self.tvChat.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
    }
    
}

// MARK: View
extension SupportChatViewController : ISupportChatView {
    
    func onMessagesReceived(_ messages: [Message]) {
        hideProgress()
        DispatchQueue.main.async {
            if !messages.isEmpty {
                self.messages = messages
                //                self.prepareCells()
                self.tvChat.reloadData()
                self.tvChat.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                return
            }
            self.tvChat.reloadData()
        }
    }
    
    func setCurrentUser(_ user: User) {
        self.user = user
    }
    
    func onMessageSent(_ messageId: NSNumber) {
        changeStatusMessage(withId: messageId, status: Constants.StatusMessage.SEND)
    }
    
    func onFailedMessage(_ messageId: NSNumber) {
        changeStatusMessage(withId: messageId, status: Constants.StatusMessage.FAILED)
    }
    
}

extension SupportChatViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return arrayCells.count + 1
        return messages.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if indexPath.row == 0 {
        //            return cellTitle
        //        } else {
        //            return arrayCells[indexPath.row - 1]
        //        }
        if indexPath.row == 0 {
            return cellTitle
        } else {
            if messages[indexPath.row - 1].sender_id == ticket.id {
                let newCell: MessageSupportCell = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SUPPORT_CELL_RECEIVED) as! MessageSupportCell
                newCell.setCell(messages[indexPath.row - 1])
                newCell.selectionStyle = .none
                return newCell
            } else {
                let newCell: MessageSupportCell = tvChat.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SUPPORT_CELL_SENT) as! MessageSupportCell
                newCell.setCell(messages[indexPath.row - 1])
                newCell.selectionStyle = .none
                return newCell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return cellTitle.getHeight()
        } else {
            //            return arrayCells[indexPath.row - 1].heightCell
            let char = messages[indexPath.row - 1].body.count
            return (CGFloat((char * 3) / 13)+70)
        }
    }
}

