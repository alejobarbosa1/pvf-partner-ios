//
//  MessageSupportCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class MessageSupportCell: UITableViewCell {
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    var heightCell: CGFloat!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(_ message: Message){
        if message.status == Constants.StatusMessage.FAILED {
            lblMessage.textColor = Constants.Colors.GRAY
        }
        setCustomStyle(borderColor: .clear, borderWidth: 0, cornerRadius: viewContent.frame.height / 10, views: viewContent)
        let date = (Date(timeIntervalSince1970: TimeInterval(message.date))).toString(withFormat: Constants.Formats.DATE_TO_CHAT)
        lblDate.text = date
        lblMessage.text = message.body
        heightCell = (lblDate.frame.height + lblMessage.frame.height + 40)
    }

}
