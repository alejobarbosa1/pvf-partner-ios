//
//  SupportChatBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SupportChatBL: BaseBL {
	
	// MARK: Properties
    var restUser = RestUser()
    var restSupport = RestSupport()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ISupportChatListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ISupportChatListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ISupportChatListener {
        return listener as! ISupportChatListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        if tag == Constants.RepositoriesTags.SEND_SUPPORT_MESSAGE {
            let array = (response as! ArrayResponse).objectsArray
            let arrayMessages: [SupportMessage] = SupportMessage.fromJsonArray(array)
            let supportMessage = arrayMessages.first!
            let messageId = NSNumber(value: Int(supportMessage.messageId))
            DbManager.updateMessage(withStatus: Constants.StatusMessage.SEND, idMessage: messageId)
            getListener().onMessageSent(messageId)
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.SEND_SUPPORT_MESSAGE {
            let messageId: NSNumber = NSNumber(value: Int(response.ResponseMessage!)!)
            DbManager.updateMessage(withStatus: Constants.StatusMessage.FAILED, idMessage: messageId)
            getListener().onFailedMessage(messageId)
        }
    }
}

// MARK: BL
extension SupportChatBL : ISupportChatBL {
    
    // MARK: Listeners
    
    func getMessages(by id: NSNumber) {
        let idUser = restUser.getCurrentUser()!.iduser
        var messages = [Message]()
        messages = DbManager.getMessages(withChatUserId: id, andUserId: idUser!)!
        getListener().onMessagesReceived(messages)
    }
    
    func getCurrentUser() {
        let user = restUser.getCurrentUser()
        getListener().setCurrentUser(user!)
    }
    
    func sendMessage(_ message: Message) {
        let supportMessage: SupportMessage!
        supportMessage = SupportMessage(iduser: message.sender_id,
                                       comment: message.body,
                                       idchannel: message.receiver_id,
                                       messageId: message.messageId)
        restSupport.sendMessage(supportMessage, listener: self)
        DbManager.insertMessage(message)
    }
    
}
