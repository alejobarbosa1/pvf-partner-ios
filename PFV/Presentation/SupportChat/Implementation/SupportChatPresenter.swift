//
//  SupportChatPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SupportChatPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ISupportChatBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ISupportChatView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ISupportChatView) {
        super.init(baseView: view)
        self.bl = SupportChatBL(listener: self)
    }
    
    fileprivate func getView() -> ISupportChatView {
        return view as! ISupportChatView
    }
    
    fileprivate func getBL() -> ISupportChatBL {
        return bl as! ISupportChatBL
    }
}

// MARK: Presenter
extension SupportChatPresenter: ISupportChatPresenter {

      // MARK: Actions
    
    func getMessages(by id: NSNumber) {
        getBL().getMessages(by: id)
    }
    
    func getCurrentUser() {
        getBL().getCurrentUser()
    }
    
    func sendMessage(_ message: Message) {
        getBL().sendMessage(message)
    }

}

// MARK: Listener
extension SupportChatPresenter: ISupportChatListener {

      // MARK: Listeners
    
    func onMessagesReceived(_ messages: [Message]) {
        getView().onMessagesReceived(messages)
    }
    
    func setCurrentUser(_ user: User) {
        getView().setCurrentUser(user)
    }
    
    func onMessageSent(_ messageId: NSNumber) {
        getView().onMessageSent(messageId)
    }
    
    func onFailedMessage(_ messageId: NSNumber) {
        getView().onFailedMessage(messageId)
    }
    
}

