//
//  ISupportChatPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol ISupportChatPresenter: IBasePresenter {
  
    func getMessages(by id: NSNumber)
    
    func getCurrentUser()
    
    func sendMessage(_ message: Message)
    
}
