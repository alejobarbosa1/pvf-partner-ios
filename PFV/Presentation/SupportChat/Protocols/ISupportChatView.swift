//
//  ISupportChatView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 30/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol ISupportChatView: IBaseView {
    
    func onMessagesReceived(_ messages: [Message])
    
    func onMessageSent(_ messageId: NSNumber)
    
    func setCurrentUser(_ user: User)
    
    func onFailedMessage(_ messageId: NSNumber)
  
}
