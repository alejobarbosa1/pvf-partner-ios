//
//  IGenerateCreditPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IGenerateCreditPresenter: IBasePresenter {
    
    func getFinancingList()
    
    func generateCredit(_ credit: Credit)
  
}
