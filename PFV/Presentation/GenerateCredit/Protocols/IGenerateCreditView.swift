//
//  IGenerateCreditView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol IGenerateCreditView: IBaseView {
    
    func setFinancingList(_ financingList: [Financing])
    
    func onSuccessCredit(_ response: String)
    
    func onEmptyPriceHouse()
    
    func onEmptyFinancingType()
    
    func onEmptyInterest()
    
    func onEmptyYears()
  
}
