//
//  GenerateCreditBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class GenerateCreditBL: BaseBL {
	
	// MARK: Properties
    
    var restProject = RestProject()
    var restUser = RestUser()
    var restCredit = RestCredit()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IGenerateCreditListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IGenerateCreditListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IGenerateCreditListener {
        return listener as! IGenerateCreditListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .FINANCING_LIST:
            let array = (response as! ArrayResponse).objectsArray
            let financingList: [Financing] = Financing.fromJsonArray(array)
            getListener().setFinancingList(financingList)
            break
        case .GENERATE_CREDIT:
            getListener().onSuccessCredit((response as! Response).ResponseMessage!)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension GenerateCreditBL : IGenerateCreditBL {
    
    // MARK: Listeners
    
    func getFinancingList() {
        restProject.getFinancingList(self)
    }
    
    func generateCredit(_ credit: Credit) {
        if credit.id_financing == nil {
            getListener().onEmptyFinancingType()
            return
        }
        if credit.price_hou == nil {
            getListener().onEmptyPriceHouse()
            return
        }
        if credit.id_financing != 4 {
            if credit.porcen_credict == nil {
                getListener().onEmptyInterest()
                return
            }
        }
        if credit.year_pay == nil {
            getListener().onEmptyYears()
            return
        }
        credit.iduser = restUser.getCurrentUser()?.iduser
        restCredit.generateCredit(credit, listener: self)
    }
    
}
