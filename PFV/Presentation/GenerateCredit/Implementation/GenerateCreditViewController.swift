//
//  GenerateCreditViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class GenerateCreditViewController: BaseViewController {
  
  	// MARK: Properties

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewHousingType: UIView!
    @IBOutlet weak var viewHousingStatus: UIView!
    @IBOutlet weak var viewHousingPrices: UIView!
    @IBOutlet weak var viewFinancingType: UIView!
    @IBOutlet weak var viewFinancingPrice: UIView!
    @IBOutlet weak var viewTermInYears: UIView!
    @IBOutlet weak var viewInterestRate: UIView!
    @IBOutlet weak var viewFinalMensualRate: UIView!
    @IBOutlet weak var viewFinalFee: UIView!
    @IBOutlet weak var btNextStep: UIButton!
    
    @IBOutlet weak var tfHousingType: PickerTextField!
    @IBOutlet weak var tfHousingStatus: PickerTextField!
    @IBOutlet weak var tfHousingPrice: CurrencyTextField!
    @IBOutlet weak var tfFinancingType: PickerTextField!
    @IBOutlet weak var lblFinancingPrice: UILabel!
    @IBOutlet weak var tfFinancingInteres: PickerTextField!
    @IBOutlet weak var tfYears: PickerTextField!
    @IBOutlet weak var lblInterestRate: UILabel!
    @IBOutlet weak var sliderInterestRate: UISlider!
    
    @IBOutlet weak var heigthViewMain: NSLayoutConstraint!
    @IBOutlet weak var topViewHousingType: NSLayoutConstraint!
    @IBOutlet weak var topViewTerms: NSLayoutConstraint!
    @IBOutlet weak var topViewFinalFee: NSLayoutConstraint!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblFinalMensualRate: UILabel!
    @IBOutlet weak var lblFinalFee: UILabel!
    
    @IBOutlet weak var viewAuth: UIView!
    @IBOutlet weak var viewYellowAuth: UIView!
    @IBOutlet weak var lblAuthorization: UILabel!
    fileprivate var isAuthorizated = false
    
    fileprivate var propertyTypeList = [PropertyType]()
    fileprivate var propertyStatusList = [PropertyStatus]()
    fileprivate var financingList: [Financing] = []
    fileprivate var financingInterestList: [FinancingInterest] = []
    fileprivate var anualFeesList: [AnualFees] = []
    fileprivate var interestRateList: [InterestRate] = []
    fileprivate var byFactor: Int!
    fileprivate let numberFormatter = NumberFormatter()
    fileprivate var financingPrice: Float!
    fileprivate var termInYearsPerMonth: Float!
    fileprivate var interesRate: Double! = 8.0
    var isShowingInfo = false
    var credit: Credit!
    var otherCredit: OtherCredit!
    var isNotLeasing = false
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IGenerateCreditPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IGenerateCreditPresenter {
        return presenter as! IGenerateCreditPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = GenerateCreditPresenter(view: self)
        setCorner(viewHousingType.frame.height / 10,
                  views: viewHousingType,
                  viewHousingStatus,
                  viewHousingPrices,
                  viewFinancingType,
                  viewFinancingPrice,
                  viewTermInYears,
                  viewInterestRate,
                  viewFinalMensualRate,
                  viewFinalFee)
        setCorner(btNextStep.frame.height / 10, views: btNextStep)
        self.setCustomStyle(borderColor: Constants.Colors.GRAY, borderWidth: 4, cornerRadius: viewAuth.frame.height / 2, views: viewAuth)
        viewAuth.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GenerateCreditViewController.onAuthorizationClick)))
        lblAuthorization.isUserInteractionEnabled = true
        lblAuthorization.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GenerateCreditViewController.onInfoAuthClick)))
        viewContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GenerateCreditViewController.endEditing)))
        if isShowingInfo {
            tfHousingType.isEnabled = false
            tfHousingStatus.isEnabled = false
            tfHousingPrice.isEnabled = false
            tfFinancingType.isEnabled = false
            viewAuth.isHidden = true
            lblAuthorization.isHidden = true
            setData()
        } else {
            showProgress(withTitle: nil)
            tfHousingPrice.delegate = tfHousingPrice
            tfHousingPrice.limitLength = 20
            DispatchQueue.global().async {
                self.getPresenter().getFinancingList()
            }
            for u in stride(from: 8.00, to: 20.01, by: 0.01){
                interestRateList.append(InterestRate(u.description, valueToShow: (u.description + Constants.Commons.INTEREST_SIGN)))
            }
            
            tfHousingType.delegate = self
            tfFinancingType.delegate = self
            propertyTypeList.append(PropertyType(Constants.HousingType.HOUSE))
            propertyTypeList.append(PropertyType(Constants.HousingType.APARTMENT))
            propertyTypeList.append(PropertyType(Constants.HousingType.LOT))
            propertyTypeList.append(PropertyType(Constants.HousingType.CELLAR))
            propertyTypeList.append(PropertyType(Constants.HousingType.LOCAL))
            propertyTypeList.append(PropertyType(Constants.HousingType.OFFICE))
            tfHousingType.pickerDataSource = propertyTypeList
            
            tfHousingStatus.delegate = self
            propertyStatusList.append(PropertyStatus(Constants.Categories.NEW))
            propertyStatusList.append(PropertyStatus(Constants.Categories.USED))
            propertyStatusList.append(PropertyStatus(Constants.Categories.OVER_FLAT_FOR_PICKERTF))
            tfHousingStatus.pickerDataSource = propertyStatusList
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.GENERATE_CREDIT)
    }
    
    @objc func onAuthorizationClick(){
        if isAuthorizated {
            isAuthorizated = false
            viewYellowAuth.isHidden = true
        } else {
            isAuthorizated = true
            viewYellowAuth.isHidden = false
        }
    }
    
    @objc func onInfoAuthClick(){
        let authorizationVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.AUTHORIZATION_VIEW_CONTROLLER) as! AuthorizationViewController
        hostViewController.pushViewController(authorizationVC, animated: true)
    }
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        let value = sender.value
        let valueArrayLarge = Array(value.description)
        var valueArrayShort = [String.Element]()
        if valueArrayLarge.count > 4{
            for (index, i) in valueArrayLarge.enumerated() {
                var maxValue = 3
                if Double(value) >= 10.0 {
                    maxValue = 4
                } else {
                    maxValue = 3
                }
                if index <= maxValue{
                    valueArrayShort.append(i)
                }
            }
        } else {
            valueArrayShort = valueArrayLarge
        }
        self.interesRate = Double(String(valueArrayShort))
        self.lblInterestRate.text = "\(String(describing: interesRate!.description))\(" %")"
        
        var fee = Double()
        if !tfYears.text!.isEmpty {
            if isNotLeasing {
                let number1 = (1 / Double(12))
                let number2 = (1 + (interesRate / 100))
                let number3 = (pow(number2, number1))
                let number4 = (number3 - 1)
                let mensualRate = number4
                let n = mensualRate * 100
                let m = Array(n.description)
                var l = [String.Element]()
                if m.count > 4{
                    for (index, i) in m.enumerated() {
                        if index <= 4{
                            l.append(i)
                        }
                    }
                }
                
                lblFinalMensualRate.text = "\(String(l))\(Constants.Commons.INTEREST_SIGN)"
                fee = (Double(financingPrice) * (mensualRate * (pow((1 + mensualRate), Double(tfYears.selectedItem!.getCode())!)))) / ((pow((1 + mensualRate), Double(tfYears.selectedItem!.getCode())!)) -  1)
            } else {
                let interesRatePercentage = (interesRate / 100) / Double(12)
                fee = (Double(tfHousingPrice.unformattedText)! * (interesRatePercentage * (pow((1 + interesRatePercentage), Double(tfYears.selectedItem!.getCode())!)))) / ((pow((1 + interesRatePercentage), Double(tfYears.selectedItem!.getCode())!)) -  1)
            }
            btNextStep.isEnabled = true
            lblFinalFee.text = numberFormatter.string(from: NSNumber(value: fee.rounded(toPlaces: 0)))! + Constants.Commons.COP_MONTH
        }
        
    }
    
    func setData(){
        let heightScreen = (Constants.Sizes.SCREEN_SIZE.height - self.hostViewController.navigationController!.navigationBar.frame.height)
        tfHousingType.text = credit.type_hou
        tfHousingStatus.text = credit.type_proyect
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        tfHousingPrice.text = numberFormatter.string(from: credit.price_hou)! + Constants.Commons.COP
        tfFinancingType.text = credit.financing_name
        var fee = Double()
        if credit.id_financing != 4 {
            self.topViewHousingType.constant = 10
            self.topViewTerms.constant = 10
            self.topViewFinalFee.constant = 10
            self.viewHousingType.isHidden = false
            self.viewHousingStatus.isHidden = false
            self.viewTermInYears.isHidden = false
            self.viewFinalMensualRate.isHidden = false
            self.viewFinancingPrice.isHidden = false
            self.heigthViewMain.constant = 900
            self.lblValue.text = Constants.ViewStrings.HOUSE_VALUE
            tfFinancingInteres.text = credit.porcen_credict.description + Constants.Commons.INTEREST_SIGN
            let financingPrice: NSNumber = NSNumber(value: ((credit.price_hou.intValue * credit.porcen_credict.intValue) / 100))
            lblFinancingPrice.text = numberFormatter.string(from: financingPrice)! + Constants.Commons.COP
            tfYears.text = credit.year_pay.description + Constants.Commons.MONTHS
            let number1 = (1 / Double(12))
            let number2 = (1 + (Double(credit.tea)! / 100))
            let number3 = (pow(number2, number1))
            let number4 = (number3 - 1)
            let mensualRate = number4
            let n = mensualRate * 100
            let m = Array(n.description)
            var l = [String.Element]()
            if m.count > 4{
                for (index, i) in m.enumerated() {
                    if index <= 4{
                        l.append(i)
                    }
                }
            }
            
            lblFinalMensualRate.text = "\(String(l))\(Constants.Commons.INTEREST_SIGN)"
            fee = (Double(financingPrice) * (mensualRate * (pow((1 + mensualRate), Double(credit.year_pay))))) / ((pow((1 + mensualRate), Double(credit.year_pay))) -  1)
        } else {
            tfYears.text = credit.year_pay.description + Constants.Commons.MONTHS
            self.heigthViewMain.constant = Int(Constants.Sizes.SCREEN_SIZE.height) < 600 ? 550 : heightScreen
            self.viewFinancingPrice.isHidden = true
            self.topViewTerms.constant = -75
            self.lblValue.text = Constants.ViewStrings.CREDIT_VALUE
            let interesRate = (Double(credit.tea)! / 100) / Double(12)
            fee = (Double(credit.price_hou) * (interesRate * (pow((1 + interesRate), Double())))) / ((pow((1 + interesRate), Double(credit.year_pay))) -  1)
        }
        let value = Float(Double(credit.tea)!)
        self.sliderInterestRate.setValue(value, animated: true)
        self.sliderInterestRate.isUserInteractionEnabled = false
        lblInterestRate.text = "\(credit.tea!)\(Constants.Commons.INTEREST_SIGN)"
        lblFinalFee.text = numberFormatter.string(from: NSNumber(value: fee))! + Constants.Commons.COP
        lblAuthorization.isHidden = true
        lblAuthorization.isHidden = true
        btNextStep.isHidden = true
    }
    
    func showAnimate(){
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                if !self.isNotLeasing{
                    self.isNotLeasing = true
                    self.topViewHousingType.constant = 10
                    self.viewHousingType.isHidden = false
                    self.viewHousingStatus.isHidden = false
                    
                    self.viewFinancingPrice.isHidden = false
                    self.topViewTerms.constant = 10
                    self.lblValue.text = Constants.ViewStrings.HOUSE_VALUE
                    self.tfYears.isEnabled = false
                    self.viewFinalMensualRate.isHidden = false
                    self.topViewFinalFee.constant = 10
                    
                    self.heigthViewMain.constant = 1030
                } else {
                    self.isNotLeasing = false
                    self.topViewHousingType.constant = -160
                    self.viewHousingType.isHidden = true
                    self.viewHousingStatus.isHidden = true
                    
                    self.viewFinancingPrice.isHidden = true
                    self.topViewTerms.constant = -75
                    self.topViewFinalFee.constant = -75
                    self.viewFinalMensualRate.isHidden = true
                    self.lblValue.text = Constants.ViewStrings.CREDIT_VALUE
                    self.tfYears.isEnabled = true
                    
                    self.heigthViewMain.constant = 690
                }
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        switch tag {
        case Constants.Tags.FINANCING_TYPE_TEXT_FIELD:
            if (pickerModel as! Financing).id_financing != 4 {
                if !isNotLeasing {
                    showAnimate()
                }
                let dues = (pickerModel as! Financing).cuotas
                if dues!.intValue == 0 {
                    byFactor = -1
                } else {
                    byFactor = 1
                }
                for i in stride(from: 60, to: dues!.intValue + 1, by: byFactor){
                    if i == 1 {
                        anualFeesList.append(AnualFees(i.description, feeToShow: (i.description + Constants.Commons.MONTH)))
                    } else {
                        anualFeesList.append(AnualFees(i.description, feeToShow: (i.description + Constants.Commons.MONTHS)))
                        tfYears.delegate = self
                        tfYears.pickerDataSource = anualFeesList
                    }
                }
            } else {
                if isNotLeasing {
                    showAnimate()
                }
                let dues = (pickerModel as! Financing).cuotas
                if dues!.intValue == 0 {
                    byFactor = -1
                } else {
                    byFactor = 1
                }
                for i in stride(from: 5, to: (dues?.intValue)! + 1, by: byFactor){
                    if i == 1 {
                        anualFeesList.append(AnualFees(i.description, feeToShow: (i.description + Constants.Commons.MONTH)))
                    } else {
                        anualFeesList.append(AnualFees(i.description, feeToShow: (i.description + Constants.Commons.MONTHS)))
                        tfYears.delegate = self
                        tfYears.pickerDataSource = anualFeesList
                    }
                }
            }
            
            financingInterestList = [FinancingInterest]()
            anualFeesList = [AnualFees]()
            lblFinancingPrice.text = "$ 0 COP"
            tfFinancingInteres.text = ""
            tfYears.text = ""
            lblFinalMensualRate.text = "0 %"
            lblFinalFee.text = "$ 0 COP"
            btNextStep.isEnabled = false
            let percentage = (pickerModel as! Financing).porcentage
            if percentage!.intValue == 0 {
                byFactor = -1
            } else {
                byFactor = 1
            }
            for i in stride(from: 1, to: percentage!.intValue + 1, by: byFactor){
                financingInterestList.append(FinancingInterest(i.description, interestToShow: (i.description +     Constants.Commons.INTEREST_SIGN)))
                tfFinancingInteres.delegate = self
                tfFinancingInteres.pickerDataSource = financingInterestList
            }
            tfFinancingInteres.isEnabled = true
            break
        case Constants.Tags.PERCENTAGE_TEXT_FILD:
            if tfHousingPrice.text!.isEmpty {
                showPopover(tfHousingPrice, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
            } else {
                if Int(tfHousingPrice.unformattedText)! > 0 {
                    lblFinalMensualRate.text = "0 %"
                    lblFinalFee.text = "$ 0 COP"
                    financingPrice = Float(tfHousingPrice.unformattedText)! * (Float(tfFinancingInteres.selectedItem!.getCode())! / 100)
                    numberFormatter.numberStyle = .currency
                    numberFormatter.locale = Constants.Formats.LOCALE
                    let number = numberFormatter.string(from: NSNumber(value: financingPrice))
                    lblFinancingPrice.text = number! + Constants.Commons.COP
                    tfYears.isEnabled = true
                } else {
                    showPopover(tfHousingPrice, message: Constants.Dialogs.PRICE_INVALID, icon: #imageLiteral(resourceName: "icon_warning"))
                }
            }
            break
        case Constants.Tags.YEARS_TEXT_FIEL:
            if tfHousingPrice.text!.isEmpty {
                showPopover(tfHousingPrice, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
            } else {
                if Int(tfHousingPrice.unformattedText)! > 0 {
                    lblFinalMensualRate.text = "0 %"
                    lblFinalFee.text = "$ 0 COP"
                    if isNotLeasing {
                        termInYearsPerMonth = financingPrice / (Float(tfYears.selectedItem!.getCode())!)
                        btNextStep.isEnabled = true
                    } else {
                        termInYearsPerMonth = Float(tfHousingPrice.unformattedText)! / Float(tfYears.selectedItem!.getCode())!
                        btNextStep.isEnabled = true
                    }
                } else {
                    showPopover(tfHousingPrice, message: Constants.Dialogs.PRICE_INVALID, icon: #imageLiteral(resourceName: "icon_warning"))
                }
            }
            break
        case Constants.Tags.INTERES_TEXT_FIELD:
            if tfHousingPrice.text!.isEmpty {
                showPopover(tfHousingPrice, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
            } else {
                if Int(tfHousingPrice.unformattedText)! > 0 {
                    let model = (pickerModel as! InterestRate)
                    var fee = Double()
                    if isNotLeasing {
                        let number1 = (1 / Double(12))
                        let number2 = (1 + (Double(model.value)! / 100))
                        let number3 = (pow(number2, number1))
                        let number4 = (number3 - 1)
                        let mensualRate = number4
                        let n = mensualRate * 100
                        let m = Array(n.description)
                        var l = [String.Element]()
                        if m.count > 4{
                            for (index, i) in m.enumerated() {
                                if index <= 4{
                                    l.append(i)
                                }
                            }
                        }
                        
                        lblFinalMensualRate.text = "\(String(l))\(Constants.Commons.INTEREST_SIGN)"
                        fee = (Double(financingPrice) * (mensualRate * (pow((1 + mensualRate), Double(tfYears.selectedItem!.getCode())!)))) / ((pow((1 + mensualRate), Double(tfYears.selectedItem!.getCode())!)) -  1)
                    } else {
                        let interesRate = (Double(model.value)! / 100) / Double(12)
                        fee = (Double(tfHousingPrice.unformattedText)! * (interesRate * (pow((1 + interesRate), Double(tfYears.selectedItem!.getCode())!)))) / ((pow((1 + interesRate), Double(tfYears.selectedItem!.getCode())!)) -  1)
                    }
                    numberFormatter.numberStyle = .currency
                    numberFormatter.locale = Constants.Formats.LOCALE
                    btNextStep.isEnabled = true
                    lblFinalFee.text = numberFormatter.string(from: NSNumber(value: fee.rounded(toPlaces: 0)))! + Constants.Commons.COP_MONTH
                } else {
                    showPopover(tfHousingPrice, message: Constants.Dialogs.PRICE_INVALID, icon: #imageLiteral(resourceName: "icon_warning"))
                }
            }
            break
        default:
            break
        }
        super.onPickerOkClick(pickerModel, tag: tag)
    }
    

    @IBAction func onInfoClick(_ sender: Any) {
        showInfoAlert(isPartner: false)
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        switch alertTag {
        case Constants.Tags.SUCCESS_CREDIT:
            hostViewController.onBackClick()
            break
        case Constants.Tags.COMPLETE_PROFILE:
            let completeProfileVC = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.COMPLETE_PROFILE_VIEW_CONTROLLER) as! CompleteProfileViewController
            completeProfileVC.isCompletingProfile = true
            completeProfileVC.isEditingProfile = true
            completeProfileVC.isCredit = true
            hostViewController.pushViewController(completeProfileVC, animated: true)
            break
        default:
            break
        }
    }
    
    override func notifyFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.GENERATE_CREDIT {
            hideProgress()
            showAlert(alertType: .ERROR,
                      message: Constants.Dialogs.COMPLETE_PROFILE,
                      hasCancelButton: true,
                      tag: Constants.Tags.COMPLETE_PROFILE)
            return
        }
        super.notifyFailedResponse(response, tag: tag)
    }
    
    @IBAction func onNextStepClick(_ sender: Any) {
        if !isAuthorizated {
            showAlert(alertType: .WARNING,
                      message: Constants.Dialogs.AUTHORIZATION_REQUIRED,
                      hasCancelButton: false,
                      tag: Constants.Tags.DEFAULT_OK)
            return
        }
        showProgress(withTitle: nil)
        let credit = Credit()
        credit.price_hou = NSNumber(value: Int(tfHousingPrice.unformattedText)!)
        credit.year_pay = NSNumber(value: Int((tfYears.selectedItem?.getCode())!)!)
        if isNotLeasing{
            credit.type_hou = tfHousingType.selectedItem?.getText()
            credit.type_proyect = tfHousingStatus.selectedItem?.getText()
            credit.id_financing = NSNumber(value: Int((tfFinancingType.selectedItem?.getCode())!)!)
            credit.porcen_credict = NSNumber(value: Int((tfFinancingInteres.selectedItem?.getCode())!)!)
        }
        DispatchQueue.global().async {
            self.getPresenter().generateCredit(credit)
        }
    }
    
}

// MARK: View
extension GenerateCreditViewController : IGenerateCreditView {
    
    func setFinancingList(_ financingList: [Financing]) {
        hideProgress()
        DispatchQueue.main.async {
            self.financingList = financingList
            self.financingList.forEach({financing in
                if financing.cuotas == 0 {
                    self.financingList.remove(at: financingList.index(of: financing)!)
                }
            })
            self.tfFinancingType.pickerDataSource = self.financingList
        }
        
    }
    
    func onSuccessCredit(_ response: String) {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .DONE,
                           message: response,
                           hasCancelButton: false,
                           tag: Constants.Tags.SUCCESS_CREDIT)
        }
    }
    
    func onEmptyPriceHouse() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfHousingPrice,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyFinancingType() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfFinancingType,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
  
    func onEmptyInterest() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfFinancingInteres,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyYears() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfYears,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
}

