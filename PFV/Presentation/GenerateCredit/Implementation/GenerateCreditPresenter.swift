//
//  GenerateCreditPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class GenerateCreditPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IGenerateCreditBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IGenerateCreditView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IGenerateCreditView) {
        super.init(baseView: view)
        self.bl = GenerateCreditBL(listener: self)
    }
    
    fileprivate func getView() -> IGenerateCreditView {
        return view as! IGenerateCreditView
    }
    
    fileprivate func getBL() -> IGenerateCreditBL {
        return bl as! IGenerateCreditBL
    }
}

// MARK: Presenter
extension GenerateCreditPresenter: IGenerateCreditPresenter {

      // MARK: Actions
    
    func getFinancingList() {
        getBL().getFinancingList()
    }
    
    func generateCredit(_ credit: Credit) {
        getBL().generateCredit(credit)
    }

}

// MARK: Listener
extension GenerateCreditPresenter: IGenerateCreditListener {

      // MARK: Listeners
    
    func setFinancingList(_ financingList: [Financing]) {
        getView().setFinancingList(financingList)
    }
    
    func onSuccessCredit(_ response: String) {
        getView().onSuccessCredit(response)
    }
    
    func onEmptyPriceHouse() {
        getView().onEmptyPriceHouse()
    }
    
    func onEmptyFinancingType() {
        getView().onEmptyFinancingType()
    }
    
    func onEmptyInterest() {
        getView().onEmptyInterest()
    }
    
    func onEmptyYears() {
        getView().onEmptyYears()
    }
    
}

