//
//  NewReferredViewController.swift
//  PFV
//
//  Created by Alejandro Barbosa on 10/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class NewReferredViewController: BaseViewController {

    // MARK: Outlets
    
    @IBOutlet weak var btUser: UIButton!
    @IBOutlet weak var btBuilder: UIButton!
    @IBOutlet weak var viewUserSelector: UIView!
    @IBOutlet weak var viewBuilderSelector: UIView!
    
    @IBOutlet weak var viewUserContent: UIView!
    @IBOutlet weak var tfUserFirstName: UITextField!
    @IBOutlet weak var tfUserLastName: UITextField!
    @IBOutlet weak var tfUserDocumentType: PickerTextField!
    @IBOutlet weak var tfUserDocumentNumber: UITextField!
    @IBOutlet weak var tfUserEmail: UITextField!
    @IBOutlet weak var tfUserPhone: UITextField!
    @IBOutlet weak var btRegisterUser: UIButton!
    @IBOutlet weak var lblTermsAndConditions: UILabel!
    
    @IBOutlet weak var viewBuilderContent: UIView!
    @IBOutlet weak var tfBuilderName: UITextField!
    @IBOutlet weak var tfBuilderNIT: UITextField!
    @IBOutlet weak var tfBuilderEmail: UITextField!
    @IBOutlet weak var tfBuilderPhone: UITextField!
    @IBOutlet weak var tfOwnerName: UITextField!
    @IBOutlet weak var tfOwnerPhone: UITextField!
    @IBOutlet weak var btRegisterBuilder: UIButton!
    
    fileprivate var isUser = true
    fileprivate var isBuilder = false
  
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? INewReferredPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> INewReferredPresenter {
        return presenter as! INewReferredPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = NewReferredPresenter(view: self)
        lblTermsAndConditions.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NewReferredViewController.onTermsClick)))
        setCorner(btRegisterUser.frame.height / 10, views: btRegisterUser)
        setCorner(btRegisterBuilder.frame.height / 10, views: btRegisterBuilder)
        viewBuilderContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NewReferredViewController.endEditing)))
        DispatchQueue.global().async {
            self.getPresenter().getDocumentTypes()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hostViewController.setTitle(title: Constants.Titles.NEW_REFERRED)
    }

    // MARK: Actions
    
    @objc func onTermsClick(){
        let termsViewController = self.storyboard!.instantiateViewController(withIdentifier: Constants.ViewControllers.TERMS_AND_CONDITIONS)
        hostViewController.pushViewController(termsViewController, animated: true)
    }
    
    @IBAction func onUserCick(_ sender: Any) {
        if !isUser {
            isUser = true
            isBuilder = false
            btUser.tintColor = UIColor.white
            btBuilder.tintColor = Constants.Colors.PURPLE_LIGHT
            viewUserSelector.isHidden = false
            viewBuilderSelector.isHidden = true
            viewUserContent.isHidden = false
            viewBuilderContent.isHidden = true
        }
    }
    
    @IBAction func onBuilderClick(_ sender: Any) {
        if !isBuilder {
            isUser = false
            isBuilder = true
            btUser.tintColor = Constants.Colors.PURPLE_LIGHT
            btBuilder.tintColor = UIColor.white
            viewUserSelector.isHidden = true
            viewBuilderSelector.isHidden = false
            viewUserContent.isHidden = true
            viewBuilderContent.isHidden = false
        }
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        if tag == Constants.Tags.DOCUMENT_YPE_TF {
            tfUserDocumentType.text = (pickerModel as! DocumentType).initialdoctype
        }
    }
    
    @IBAction func onRegisterUserClick(_ sender: Any) {
        showProgress(withTitle: nil)
        let user = User()
        user.firstName = tfUserFirstName.text
        user.lastName = tfUserLastName.text
        user.iddoctype = tfUserDocumentType.selectedItem?.getCode()
        user.numberidenf = tfUserDocumentNumber.text
        user.email = tfUserEmail.text
        user.phoneper = tfUserPhone.text
        user.status = 0
        user.idrol = Constants.Rols.USER
        DispatchQueue.global().async {
            self.getPresenter().registerUser(user)
        }
    }
    
    @IBAction func onRegisterBuilderClick(_ sender: Any) {
        showProgress(withTitle: nil)
        let builder = Builder()
        builder.namebuilder = tfBuilderName.text
        builder.nit = tfBuilderNIT.text
        builder.email_builder = tfBuilderEmail.text
        builder.phone = tfBuilderPhone.text
        builder.perbuilder = tfOwnerName.text
        builder.phoneperbuilder = tfOwnerPhone.text
        DispatchQueue.global().async {
            self.getPresenter().registerBuilder(builder)
        }
    }
}

// MARK: View
extension NewReferredViewController : INewReferredView {
    
    func setDocumentTypes(_ documentTypes: [DocumentType]) {
        DispatchQueue.main.async {
            self.tfUserDocumentType.delegate = self
            self.tfUserDocumentType.pickerDataSource = documentTypes
            self.tfUserDocumentType.isEnabled = true
        }
    }
    
    func onEmptyFirstName() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserFirstName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyLastName() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserLastName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyDocType() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserDocumentType,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyDocNumber() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserDocumentNumber,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyEmail() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserEmail,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyUserPhone() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfUserPhone,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyBuilderName() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfBuilderName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyNIT() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfBuilderNIT,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyBuilderEmail() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfBuilderEmail,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyBuilderPhone() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfBuilderPhone,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyOwnerName() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfOwnerName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyOwnerPhone() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfOwnerPhone,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onSuccessUserRegister() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.SUCCESSFUL_OPERATION,
                           hasCancelButton: false,
                           tag: Constants.Tags.DEFAULT_OK)
        }
    }
    
    func onSuccessBuilderRegister() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.SUCCESSFUL_OPERATION,
                           hasCancelButton: false,
                           tag: Constants.Tags.DEFAULT_OK)
        }
    }
    
}
