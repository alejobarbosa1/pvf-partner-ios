//
//  NewReferredPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 10/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class NewReferredPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? INewReferredBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? INewReferredView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: INewReferredView) {
        super.init(baseView: view)
        self.bl = NewReferredBL(listener: self)
    }
    
    fileprivate func getView() -> INewReferredView {
        return view as! INewReferredView
    }
    
    fileprivate func getBL() -> INewReferredBL {
        return bl as! INewReferredBL
    }
}

// MARK: Presenter
extension NewReferredPresenter: INewReferredPresenter {

    // MARK: Actions
    
    func getDocumentTypes() {
        getBL().getDocumentTypes()
    }
    
    func registerUser(_ user: User) {
        getBL().registerUser(user)
    }
    
    func registerBuilder(_ builder: Builder) {
        getBL().registerBuilder(builder)
    }

}

// MARK: Listener
extension NewReferredPresenter: INewReferredListener {

    // MARK: Listeners
    
    func setDocumentTypes(_ documentTypes: [DocumentType]) {
        getView().setDocumentTypes(documentTypes)
    }
    
    func onEmptyFirstName() {
        getView().onEmptyFirstName()
    }
    
    func onEmptyLastName(){
        getView().onEmptyLastName()
    }
    
    func onEmptyDocType(){
        getView().onEmptyDocType()
    }
    
    func onEmptyDocNumber(){
        getView().onEmptyDocNumber()
    }
    
    func onEmptyEmail(){
        getView().onEmptyEmail()
    }
    
    func onEmptyUserPhone(){
        getView().onEmptyUserPhone()
    }
    
    func onEmptyBuilderName(){
        getView().onEmptyBuilderName()
    }
    
    func onEmptyNIT(){
        getView().onEmptyNIT()
    }
    
    func onEmptyBuilderEmail(){
        getView().onEmptyBuilderEmail()
    }
    
    func onEmptyBuilderPhone(){
        getView().onEmptyBuilderPhone()
    }
    
    func onEmptyOwnerName(){
        getView().onEmptyOwnerName()
    }
    
    func onEmptyOwnerPhone(){
        getView().onEmptyOwnerPhone()
    }
    
    func onSuccessUserRegister() {
        getView().onSuccessUserRegister()
    }
    
    func onSuccessBuilderRegister() {
        getView().onSuccessBuilderRegister()
    }
}

