//
//  NewReferredBL.swift
//  PFV
//
//  Created by Alejandro Barbosa on 10/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class NewReferredBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restCommon = RestCommon()
    fileprivate var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? INewReferredListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: INewReferredListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> INewReferredListener {
        return listener as! INewReferredListener
    }
    
    // MARK: Listeners
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .PICKER_VIEW_DOCUMENT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let documentTypes: [DocumentType] = DocumentType.fromJsonArray(array)
            getListener().setDocumentTypes(documentTypes)
            break
        case .REGISTER_USER:
            getListener().onSuccessUserRegister()
            break
        case .REGISTER_BUILDER:
            getListener().onSuccessBuilderRegister()
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.PICKER_VIEW_DOCUMENT_TYPE {
            restCommon.getDocumentTypes(self)
            return
        } else {
            super.onFailedResponse(response, tag: tag)
        }
    }
}

// MARK: BL
extension NewReferredBL : INewReferredBL {
    
    // MARK: Actions
    
    func getDocumentTypes() {
        restCommon.getDocumentTypes(self)
    }
    
    func registerUser(_ user: User) {
        if user.firstName.isEmpty {
            getListener().onEmptyFirstName()
            return
        }
        if user.lastName.isEmpty{
            getListener().onEmptyLastName()
            return
        }
        if user.iddoctype == nil || user.iddoctype.isEmpty {
            getListener().onEmptyDocType()
            return
        }
        if user.numberidenf.isEmpty {
            getListener().onEmptyDocNumber()
            return
        }
        if user.email.isEmpty {
            getListener().onEmptyEmail()
            return
        }
        if user.phoneper.isEmpty {
            getListener().onEmptyUserPhone()
            return
        }
        user.partner_code = restUser.getCurrentUser()?.partner_code
        restUser.verifyuser(user, listener: self)
    }
    
    func registerBuilder(_ builder: Builder) {
        if builder.namebuilder.isEmpty{
            getListener().onEmptyBuilderName()
            return
        }
        if builder.nit.isEmpty{
            getListener().onEmptyNIT()
            return
        }
        if builder.email_builder.isEmpty{
            getListener().onEmptyBuilderEmail()
            return
        }
        if builder.phone.isEmpty{
            getListener().onEmptyBuilderPhone()
            return
        }
        if builder.perbuilder.isEmpty{
            getListener().onEmptyOwnerName()
            return
        }
        if builder.phoneperbuilder.isEmpty{
            getListener().onEmptyOwnerPhone()
            return
        }
        builder.iduser = restUser.getCurrentUser()?.iduser
        restUser.registerBuilder(builder, listener: self)
    }
}
