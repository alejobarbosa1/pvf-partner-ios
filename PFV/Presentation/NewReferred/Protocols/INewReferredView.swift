//
//  INewReferredView.swift
//  PFV
//
//  Created by Alejandro Barbosa on 10/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: View.

protocol INewReferredView: IBaseView {
    
    func setDocumentTypes(_ documentTypes: [DocumentType])
    
    func onEmptyFirstName()
    
    func onEmptyLastName()
    
    func onEmptyDocType()
    
    func onEmptyDocNumber()
    
    func onEmptyEmail()
    
    func onEmptyUserPhone()
    
    func onEmptyBuilderName()
    
    func onEmptyNIT()
    
    func onEmptyBuilderEmail()
    
    func onEmptyBuilderPhone()
    
    func onEmptyOwnerName()
    
    func onEmptyOwnerPhone()
    
    func onSuccessUserRegister()
    
    func onSuccessBuilderRegister()
	
}
