//
//  INewReferredBL.swift
//  PFV
//
//  Created by Alejandro Barbosa on 10/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol INewReferredBL: IBaseBL {
    
    func getDocumentTypes()
    
    func registerUser(_ user: User)
    
    func registerBuilder(_ builder: Builder)
	
}
