//
//  IOnBaseFinishedListener.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Finished listener base protocol. All of BaseFinishedListener must extends of this.
 */
protocol IBaseListener {
    /**
     Notify that any operation is failed.
     
     - parameter response: Response with code and message.
     */
    func onFailed(_ response: Response, tag: Constants.RepositoriesTags)
    
    func successLogOut()
    
}
