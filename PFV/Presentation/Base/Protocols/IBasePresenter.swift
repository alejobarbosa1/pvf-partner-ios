//
//  IBasePresenter.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Presenter base protocol. All of presenters must implements this.
 */
protocol IBasePresenter {
    
    func logOut()
    
}

