//
//  IBaseBL.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Bussines Logic layer Base. All of BL's class must implements this.
 */
protocol IBaseBL: IResponse {
    
    func logOut()
    
}
