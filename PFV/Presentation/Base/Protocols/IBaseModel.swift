//
//  BaseModel.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Model Base protocol. All of models must implements this protocol to use ParceSwift functions.
 */
protocol IBaseModel {
    init()
    
    func fromDictionary(_ json: [String: AnyObject])
    static func fromJsonArray<T : NSObject>(_ array: [[String: AnyObject]]) -> [T]
    func toDictionary() -> [String: AnyObject]
}
