//
//  IBaseView.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 View Base protocol. All of views protocols must extends of this.
 */
protocol IBaseView {
    
    /**
     Open a webView.
     
     - parameter url: Url to load in webView.
     - parameter title: WebView title.
     */
    //func openWebView(_ url: String, title: String)
    
    /**
     Notify that the current user isn't authorized anymore.
     */
    //func notifyUnauthorizedUser()
    
    /**
     Notify that the response was failed.
     
     - parameter response: Failed response to notify.
    */
    func notifyFailedResponse(_ response: Response, tag: Constants.RepositoriesTags)
    
    func successLogOut()
    
}
