//
//  IBasePickerModel.swift
//  EVA Rentista
//
//  Created by Alejandro Barbosa on 24/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//
import Foundation

/**
 Model Base protocol to pickers. All of models to be used in pickers must implements this protocol.
 */
protocol IBasePickerModel: IBaseModel {
    
    /**
     Get code identifier
     
     - return: Model code identifier
     */
    func getCode() -> String
    
    /**
     Get text to show in picker
     
     - return: Model text to show in picker
     */
    func getText() -> String
    
    
    
}
