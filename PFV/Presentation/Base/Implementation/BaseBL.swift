//
//  BaseBL.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 BL Base. All of BL's must extends this.
 */
class BaseBL: IBaseBL {
    
    
    // MARK: Properties
    
    internal var listener: IBaseListener!
    fileprivate var restUser = RestUser()
    
    required init(baseListener: IBaseListener) {
        self.listener = baseListener
    }
    
    // MARK: Listeners
    
    func onDataResponse<T : IBaseModel>(_ response: T, tag: Constants.RepositoriesTags) {
//        preconditionFailure("This method must be overridden")
        if tag == Constants.RepositoriesTags.LOG_OUT{
            listener.successLogOut()
        }
    }
    
    func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        listener.onFailed(response, tag: tag)
    }
    
    func onFailedPending(_ response: Response, tag: Constants.RepositoriesTags) {

    }
    
    func logOut() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        restUser.logOut(self, user: user)
    }

    
}

