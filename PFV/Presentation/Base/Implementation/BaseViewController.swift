//
//  BaseViewController.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import UIKit
import JTProgressHUD

class BaseViewController: LABMenuInternalViewController, IPickerViewDelegate, IBaseView {
    
    internal var presenter: IBasePresenter!
    var isTutorial: Bool = false
    var progressIndicatorView: ProgressView!
    var pickerView: PickerView?
    var alertView: SwiftAlertView?
    var registerDataView: RegisterDataView?
    var popoverViewController: PopoverView?
    var popoverContentView: UIPopoverPresentationController?
    var restUser = RestUser()
    var hostViewController: HostViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        setHostVC()
//        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setHostVC(){
        if navigationController!.navigationController != nil {
            for hostVC in navigationController!.navigationController!.viewControllers where hostVC is HostViewController {
                hostViewController = hostVC as! HostViewController
            }
        }
    }
    
    func onBackClick() {
        DispatchQueue.main.async(execute: { () -> Void in
            if self.navigationController != nil {
                self.navigationController!.popViewController(animated: true)
            }
        })
    }
    
    func onShowHidePassword(_ sender : AnyObject)
    {
        let textField = sender.superview as! UITextField
        textField.setSecurePassword()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let firstResponder = UIResponder.getCurrentFirstResponder() as? UIView
        if firstResponder == nil {
            return
        }
        
        var yFirstResponder = firstResponder?.frame.origin.y
        let heightFirstResponder = firstResponder?.frame.size.height
        
        let userInfo:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardY = self.view.frame.size.height - keyboardRectangle.height
        
        var currentView = firstResponder
        while currentView?.superview?.frame.origin.y > 0 {
            yFirstResponder = yFirstResponder! + (currentView?.superview?.frame.origin.y)!
            currentView = currentView?.superview
        }
        
        if (yFirstResponder! + heightFirstResponder!) < keyboardY {
            return
        }
        
        let heightDif = ((yFirstResponder! + heightFirstResponder!) - keyboardY) + 5
        let j = abs(heightDif) * -1
        self.view.frame.origin.y = j
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    fileprivate func scrollIfNeeds(_ sourceView: UIView) {
        for scroll in view.subviews where scroll is UIScrollView && !(scroll as! UIScrollView).bounds.contains(sourceView.frame.origin)
        {
            DispatchQueue.main.async(execute: { () -> Void in
                (scroll as! UIScrollView).setContentOffset(CGPoint(x: 0, y: sourceView.frame.origin.y), animated: true)
            })
            
            usleep(400000)
        }
    }
    
    func showPopover(_ sourceView: UIView, message: String, icon: UIImage) {
        scrollIfNeeds(sourceView)
        showPopover(sourceView, message: message, icon: icon, large: false)
    }
    
    func showLargePopover(_ sourceView: UIView, message: String, icon: UIImage) {
        showPopover(sourceView, message: message, icon: icon, large: true)
    }
    
    fileprivate func showPopover(_ sourceView: UIView, message: String, icon: UIImage, large: Bool) {
        DispatchQueue.main.async(execute: { () -> Void in
            if large {
                self.popoverViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.LARGE_POPOVER_VIEW) as? PopoverView
                self.popoverViewController!.preferredContentSize = CGSize(width: 300, height: 100)
            } else {
                self.popoverViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.POPOVER_VIEW) as? PopoverView
                self.popoverViewController!.preferredContentSize = CGSize(width: 300, height: 60)
            }
            self.popoverViewController!.message = message
            self.popoverViewController!.icon = icon
            self.popoverViewController!.modalPresentationStyle = UIModalPresentationStyle.popover
            self.popoverViewController!.popoverPresentationController!.delegate = self
            
            self.popoverContentView = self.popoverViewController!.popoverPresentationController
            self.popoverContentView?.permittedArrowDirections = .up
            self.popoverContentView?.backgroundColor = UIColor.white
            self.popoverContentView?.delegate = self
            self.popoverContentView?.sourceView = sourceView
            self.popoverContentView?.sourceRect = CGRect(x: 100,y: 20,width: 100,height: 1)
            
            self.present(
                self.popoverViewController!,
                animated: true,
                completion: nil)
            self.hideProgress()
        })
    }
    
//    func showProgress(withTitle title: String?) {
//        self.view.endEditing(true)
//        if progressIndicatorView == nil {
//            self.progressIndicatorView = ProgressView(hostViewController: self)
//        }
//        DispatchQueue.main.async(execute: { () -> Void in
//            self.progressIndicatorView.show(title)
//        })
//    }
    
    func showProgress(withTitle title: String?) {
        self.view.endEditing(true)
        DispatchQueue.main.async {
            JTProgressHUD.show()
        }
    }
    
    func showRegisterDataView(delegate: RegisterDataDelegate, documentTypes: [DocumentType]){
        if alertView != nil {
            hideAlertView()
        }
        let contentView: RegisterDataView = RegisterDataView(delegate: delegate, documentTypes: documentTypes)
        self.alertView = SwiftAlertView(contentView: contentView,
                                        delegate: self,
                                        cancelButtonTitle: nil)
        self.alertView?.show(true)
    }
    
    func hideRegisterDataView(){
        if registerDataView != nil {
            registerDataView?.close()
            registerDataView = nil
        }
    }
    
    func showAlert(alertType: Constants.AlertTypes,
                   message: String,
                   hasCancelButton: Bool,
                   tag: Int)
    {
        self.view.endEditing(true)
        if hasCancelButton {
            showAlert(alertType: alertType,
                      message: message,
                      positiveButton: Constants.ViewStrings.OK_BUTTON,
                      negativeButton: Constants.ViewStrings.CANCEL_BUTTON,
                      tag: tag)
        } else {
            showAlert(alertType: alertType,
                      message: message,
                      positiveButton: Constants.ViewStrings.OK_BUTTON,
                      negativeButton: nil,
                      tag: tag)
        }
    }
//    TODO Implementar Cierre de sesion
    func closeSession(){
    }
    
    func showAlert(alertType: Constants.AlertTypes,
                   message: String,
                   positiveButton: String?,
                   negativeButton: String?,
                   tag: Int)
    {
        if alertView != nil {
            hideAlertView()
        }
        
        var icAlert: UIImage!
        
        // TODO Actualizar iconos
        switch alertType {
        case Constants.AlertTypes.DONE:
            icAlert = #imageLiteral(resourceName: "icon_done")
            break
        case Constants.AlertTypes.INFO:
            icAlert = #imageLiteral(resourceName: "icon_warning")
            break
        case Constants.AlertTypes.QUEST:
            icAlert = #imageLiteral(resourceName: "icon_warning")
            break
        case Constants.AlertTypes.WARNING:
            icAlert = #imageLiteral(resourceName: "icon_warning")
            break
        case Constants.AlertTypes.ERROR:
            icAlert = #imageLiteral(resourceName: "icon_warning")
            break
        case Constants.AlertTypes.NOT_INTERNET:
            icAlert = #imageLiteral(resourceName: "icon_warning")
            break
        }
        
        DispatchQueue.main.async {
            let contentView: AlertView = AlertView(icAlert: icAlert,
                                        message: message,
                                        positiveButton: positiveButton,
                                        negativeButton: negativeButton,
                                        tag: tag,
                                        delegate: self)
            
//            self.alertView = SwiftAlertView(contentView: contentView,
//                                            delegate: self,
//                                            cancelButtonTitle: nil)
            self.alertView = SwiftAlertView(contentView: contentView,
                                            delegate: self,
                                            cancelButtonTitle: nil)
            
            self.alertView?.tag = tag
            self.alertView?.show(true)
    
        }
    }
    
    func logOut(){
        DispatchQueue.global().async {
            self.presenter.logOut()
        }
    }
    
    func hideAlertView() {
        self.alertView?.dismiss()
        self.alertView = nil
    }
    
    func showInfoAlert(isPartner: Bool){
        if alertView != nil {
            hideAlertView()
        }
        DispatchQueue.main.async {
            let contentView = InfoAlertView(self.view, delegate: self, isPartner: isPartner)
            self.alertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
            self.alertView?.dismissOnOutsideClicked = true
            //            self.alertView?.tag = Constants.Tags.DEFAULT_OK
            self.alertView?.show(true)
        }
    }

    
//    func hideProgress() {
//        DispatchQueue.main.async(execute: { () -> Void in
//            if self.progressIndicatorView != nil {
//                self.progressIndicatorView.hide()
//            }
//        })
//    }
    
    func hideProgress() {
        DispatchQueue.main.async {
            JTProgressHUD.hide()
        }
    }
    
    func showPhotoAlert()
    {
        self.view.endEditing(true)
        DispatchQueue.main.async(execute: { () -> Void in
            self.hideProgress()
            let contentView = PhotoAlertView(delegate: self)
            
            self.alertView = SwiftAlertView(contentView: contentView, delegate: self, cancelButtonTitle: nil)
            
            self.alertView?.show(true)
        })
    }
    
    func showPicker(_ pickerTextField: PickerTextField) {
        self.pickerView = PickerView(pickerTextField: pickerTextField, pickerViewDelegate: self)
        self.pickerView?.show(pickerTextField.pickerDataSource, parent: pickerTextField.parent)
    }
    
    func addPicker(_ pickerView: PickerView) {
        if alertView != nil {
            let window: UIWindow = UIApplication.shared.keyWindow!
            window.addSubview(pickerView)
        } else {
            self.view.addSubview(pickerView)
        }
    }
    
    func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        pickerView?.dismiss()
    }
    
    func onPickerCancelClick() {
        pickerView?.dismiss()
    }

    
    
    //MARK View
    
    func notifyFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        hideProgress()
        if response.ResponseMessage == Constants.ViewStrings.UNAUTHORIZED {
            showAlert(alertType: Constants.AlertTypes.ERROR,
                      message: Constants.Dialogs.SESSION_EXPIRED,
                      hasCancelButton: false,
                      tag: Constants.Tags.LOGOUT_ACCOUNT)
            return
        }
        showAlert(alertType: Constants.AlertTypes.ERROR,
                  message: response.ResponseMessage!,
                  hasCancelButton: false,
                  tag: Constants.Tags.DEFAULT_OK)
    }
    
    func successLogOut(){
        hideProgress()
        DispatchQueue.main.async {
            self.hostViewController.logOut()
        }
    }
    
}
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

extension BaseViewController: UIPopoverPresentationControllerDelegate{
    func adaptivePresentationStyle(
        for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

extension BaseViewController : RegisterDataDelegate {
    
    func onContinueClick(user: User) {
        hideAlertView()
    }
    
    func onCancelClick() {
        hideAlertView()
    }
    
}

extension BaseViewController: UITextFieldDelegate{
    
//    extension LoginViewController: UITextFieldDelegate {
//        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//            if textField == tfEmail {
//                tfEmail.resignFirstResponder()
//                tfPassword.becomeFirstResponder()
//            } else if textField == tfPassword {
//                view.endEditing(true)
//            }
//            return true
//        }
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField is PickerTextField {
            showPicker(textField as! PickerTextField)
            return false
        }
        return true
    }

    func endEditing() {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {}
    
    func textFieldDidEndEditing(_ textField: UITextField) {}
    
}

extension BaseViewController: AlertViewDelegate{
    
    func onOkClick(_ alertTag: Int){
        switch alertTag {
        case Constants.Tags.LOGOUT_ACCOUNT:
            showProgress(withTitle: nil)
            logOut()
            break
        default:
            break
        }
        self.alertView?.dismiss()
    }
    
    func onCancelClick(_ alertTag: Int){
        self.alertView?.dismiss()
    }
    
}

extension BaseViewController: PhotoAlertViewDelegate{
    
    func onCameraClick() {
        
    }
    func onGalleryClick() {
        
    }
}

extension BaseViewController: ComponentViewDelegate {
    func add(view: UIView) {
        view.alpha = 0
        self.view.addSubview(view)
        
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {
                        view.alpha = 1
        },
                       completion: nil)
    }
    
    func center(view: UIView) {
        view.center = self.view.center
    }
    
    func bottom() {
        
    }
}

extension BaseViewController: SwiftAlertViewDelegate{
    
}


