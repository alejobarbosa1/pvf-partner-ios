//
//  BasePresenter.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Presenter Base. All of Presenters must extends this.
 */
class BasePresenter: IBasePresenter, IBaseListener {
    
    // MARK: Properties
    
    internal var view: IBaseView!
    internal var bl: IBaseBL!
    
    required init(baseView: IBaseView) {
        self.view = baseView
    }
    
    func logOut() {
        bl.logOut()
    }
    
    // MARK: Listeners
    
    func onFailed(_ response: Response, tag: Constants.RepositoriesTags) {
        self.view.notifyFailedResponse(response, tag: tag)
    }
    
    func successLogOut(){
        self.view.successLogOut()
    }

}
