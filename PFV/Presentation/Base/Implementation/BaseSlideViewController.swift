//
//  SlideViewController.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import UIKit

class BaseSlideViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hideBar(inContext: self, shouldHide: true)
    }
    
    override func onBackClick() {
    }
}
