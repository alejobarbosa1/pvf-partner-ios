//
//  SupportBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SupportBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restSupport = RestSupport()
    fileprivate var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ISupportListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ISupportListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ISupportListener {
        return listener as! ISupportListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension SupportBL : ISupportBL {
    
    // MARK: Listeners
    
    func getTickets() {
        let ticketsList = DbManager.getSupportTickets()
        getListener().setTickesList(ticketsList!)
    }
    
    func changeViewed(byId id: NSNumber) {
        DbManager.updateViewedSupportTicket(viewed: Constants.ViewedStatus.VIEWED, id: id)
    }
    
}
