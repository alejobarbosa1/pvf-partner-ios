//
//  TicketCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class TicketCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewViewed: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblTitle.text = ""
        lblDate.text = ""
        viewViewed.isHidden = true
    }
    
    func setCell(_ ticket: Ticket){
        if ticket.viewed == Constants.ViewedStatus.UNVIEWED {
            setCustomStyle(borderColor: .clear, borderWidth: 0, cornerRadius: viewViewed.frame.height / 2, views: viewViewed)
            self.viewViewed.isHidden = false
        }
        setCustomStyle(borderColor: .clear, borderWidth: 0, cornerRadius: viewContent.frame.height / 20, views: viewContent)
        lblTitle.text = ticket.title
        let date = (Date(timeIntervalSince1970: TimeInterval(ticket.date))).toString(withFormat: Constants.Formats.DATE_TO_CHAT)
        lblDate.text = date
        if ticket.viewed == 0 {
            viewViewed.isHidden = false
        }
    }


}
