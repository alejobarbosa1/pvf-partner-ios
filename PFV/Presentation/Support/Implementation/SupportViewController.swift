//
//  SupportViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class SupportViewController: BaseViewController {
  
  	// MARK: Properties

    @IBOutlet weak var btPending: UIButton!
    @IBOutlet weak var btSolved: UIButton!
    @IBOutlet weak var pendingSelector: UIView!
    @IBOutlet weak var solvedSelector: UIView!
    @IBOutlet weak var tvTickets: UITableView!
    fileprivate var ticketsList = [Ticket]()
    fileprivate var viewBack: UIView!
    fileprivate var viewEdit: UIView!
    
    fileprivate var isPendingSelected = true
    fileprivate var isSolvedSelected = false
    fileprivate var arrayCells = [TicketCell]()
    
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ISupportPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ISupportPresenter {
        return presenter as! ISupportPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SupportPresenter(view: self)
        tvTickets.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.SUPPORT)
        tvTickets.delegate = self
        tvTickets.dataSource = self
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getTickets()
        }
        setNavigationBar()
    }
    
    
    func setNavigationBar(){
        for view in (hostViewController.navigationController?.navigationBar.subviews)! {
            if view.frame.width >= (hostViewController.navigationController?.navigationBar.frame.width)! && view.isUserInteractionEnabled{
                for otherView in view.subviews{
                    if !(otherView is UILabel){
                        otherView.isHidden = true
                    }
                }
            }
        }

        if self.viewBack != nil  && self.viewEdit != nil {
            self.viewBack.isHidden = false
            self.viewEdit.isHidden = false
            return
        } else {
            viewBack = UIView(frame: CGRect(x: -5, y: ((self.navigationController?.navigationBar.frame.height)! - 40) / 2, width: 60, height: 40))
            viewBack.backgroundColor = UIColor.clear
            let backButton = UIImageView(frame: CGRect(x: 0, y: -5, width: 50, height: 50))
            backButton.image = #imageLiteral(resourceName: "icon_arrow_left")
            backButton.tintColor = UIColor.white
            viewBack.addSubview(backButton)
            viewBack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SupportViewController.clickBack)))
            self.hostViewController?.navigationController?.navigationBar.addSubview(viewBack)
            viewEdit = UIView(frame: CGRect(x: Constants.Sizes.SCREEN_SIZE.width - 60, y: ((self.navigationController?.navigationBar.frame.height)! - 40) / 2, width: 60, height: 40))
            viewEdit.backgroundColor = UIColor.clear
            let editButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            editButton.image = #imageLiteral(resourceName: "icon_edit")
            editButton.tintColor = UIColor.white
            viewEdit.addSubview(editButton)
            viewEdit.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SupportViewController.clickEdit)))
            self.hostViewController?.navigationController?.navigationBar.addSubview(viewEdit)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        for view in (hostViewController.navigationController?.navigationBar.subviews)! {
            if view.frame.width >= (hostViewController.navigationController?.navigationBar.frame.width)! && view.isUserInteractionEnabled{
                for otherView in view.subviews{
                    otherView.isHidden = false
                }
            }
        }
        viewEdit.isHidden = true
        viewBack.isHidden = true

    }
    
    @objc func clickEdit(){
        let createTicketVC: CreateTicketViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.CREATE_TICKET_VIEW_CONTROLLER) as! CreateTicketViewController
        hostViewController.pushViewController(createTicketVC, animated: true)
    }
    
    @objc func clickBack(){
        hostViewController.onBackClick()
    }
    
    @IBAction func onPendingClick(_ sender: Any) {
        if isSolvedSelected {
            btPending.setTitleColor(UIColor.white, for: .normal)
            btSolved.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            pendingSelector.isHidden = false
            solvedSelector.isHidden = true
            isSolvedSelected = false
            isPendingSelected = true
            prepareCells()
        }
    }
    
    @IBAction func onSolvedClick(_ sender: Any) {
        if isPendingSelected {
            btSolved.setTitleColor(UIColor.white, for: .normal)
            btPending.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            pendingSelector.isHidden = true
            solvedSelector.isHidden = false
            isPendingSelected = false
            isSolvedSelected = true
            prepareCells()
        }
    }
    
    func prepareCells() {
        if isPendingSelected {
            arrayCells = []
            for ticket in ticketsList where ticket.status == 1 {
                let newCell: TicketCell = tvTickets.dequeueReusableCell(withIdentifier: Constants.Cells.TICKET_CELL) as! TicketCell
                newCell.setCell(ticket)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            }
            tvTickets.reloadData()
            return
        } else {
            arrayCells = []
            for ticket in ticketsList where ticket.status == 0 {
                let newCell: TicketCell = tvTickets.dequeueReusableCell(withIdentifier: Constants.Cells.TICKET_CELL) as! TicketCell
                newCell.setCell(ticket)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            }
            tvTickets.reloadData()
        }
    }
}

// MARK: View
extension SupportViewController : ISupportView {
    
    func setTickesList(_ ticketsList: [Ticket]) {
        hideProgress()
        DispatchQueue.main.async {
            self.ticketsList = ticketsList
            self.prepareCells()
        }
    }
  
}

extension SupportViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.global().async {
            self.getPresenter().changeViewed(byId: self.ticketsList[indexPath.row].id)
        }
        let supportChatVC: SupportChatViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.SUPPORT_CHAT_VIEW_CONTROLLER) as! SupportChatViewController
        supportChatVC.ticket = ticketsList[indexPath.row]
        if ticketsList[indexPath.row].status == Constants.StatusTicket.CLOSED {
            supportChatVC.isClosed = true
        } else {
            supportChatVC.isClosed = false
        }
        hostViewController.pushViewController(supportChatVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
