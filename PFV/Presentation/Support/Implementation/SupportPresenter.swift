//
//  SupportPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SupportPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ISupportBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ISupportView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ISupportView) {
        super.init(baseView: view)
        self.bl = SupportBL(listener: self)
    }
    
    fileprivate func getView() -> ISupportView {
        return view as! ISupportView
    }
    
    fileprivate func getBL() -> ISupportBL {
        return bl as! ISupportBL
    }
}

// MARK: Presenter
extension SupportPresenter: ISupportPresenter {

      // MARK: Actions
    
    func getTickets() {
        getBL().getTickets()
    }
    
    func changeViewed(byId id: NSNumber) {
        getBL().changeViewed(byId: id)
    }

}

// MARK: Listener
extension SupportPresenter: ISupportListener {

      // MARK: Listeners
    
    func setTickesList(_ ticketsList: [Ticket]) {
        getView().setTickesList(ticketsList)
    }
    
}

