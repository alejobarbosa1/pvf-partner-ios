//
//  ILauncherBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol ILauncherBL: IBaseBL {
  
    func getCurrentUser()
    
}
