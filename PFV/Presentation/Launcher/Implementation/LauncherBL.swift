//
//  LauncherBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class LauncherBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ILauncherListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ILauncherListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ILauncherListener {
        return listener as! ILauncherListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension LauncherBL : ILauncherBL {
    
    // MARK: Listeners
    
    func getCurrentUser(){
        let user = restUser.getCurrentUser()
        if user != nil && user?.partner_code != nil{
            getListener().navigateToHost()
        } else {
            DbManager.cleanData()
            getListener().navigateToLogin()
        }
    }
    
}
