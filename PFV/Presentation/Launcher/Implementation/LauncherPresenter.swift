//
//  LauncherPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 2/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class LauncherPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ILauncherBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ILauncherView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ILauncherView) {
        super.init(baseView: view)
        self.bl = LauncherBL(listener: self)
    }
    
    fileprivate func getView() -> ILauncherView {
        return view as! ILauncherView
    }
    
    fileprivate func getBL() -> ILauncherBL {
        return bl as! ILauncherBL
    }
}

// MARK: Presenter
extension LauncherPresenter: ILauncherPresenter {

      // MARK: Actions
    
    func getCurrentUser() {
        getBL().getCurrentUser()
    }

}

// MARK: Listener
extension LauncherPresenter: ILauncherListener {

      // MARK: Listeners
    
    func navigateToLogin() {
        getView().navigateToLogin()
    }
    
    func navigateToHost() {
        getView().navigateToHost()
    }
    
}

