//
//  UserReferredBL.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class UserReferredBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IUserReferredListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IUserReferredListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IUserReferredListener {
        return listener as! IUserReferredListener
    }
    
    // MARK: Listeners
}

// MARK: BL
extension UserReferredBL : IUserReferredBL {
    
    // MARK: Actions
    
}
