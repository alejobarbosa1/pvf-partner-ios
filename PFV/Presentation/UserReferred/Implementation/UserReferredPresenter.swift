//
//  UserReferredPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class UserReferredPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IUserReferredBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IUserReferredView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IUserReferredView) {
        super.init(baseView: view)
        self.bl = UserReferredBL(listener: self)
    }
    
    fileprivate func getView() -> IUserReferredView {
        return view as! IUserReferredView
    }
    
    fileprivate func getBL() -> IUserReferredBL {
        return bl as! IUserReferredBL
    }
}

// MARK: Presenter
extension UserReferredPresenter: IUserReferredPresenter {

    // MARK: Actions

}

// MARK: Listener
extension UserReferredPresenter: IUserReferredListener {

    // MARK: Listeners
    
}

