//
//  UserReferredViewController.swift
//  PFV
//
//  Created by Alejandro Barbosa on 13/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class UserReferredViewController: BaseViewController {

    // MARK: Outlets
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblDocument: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblCellPhone: UILabel!
    var user: User!
    
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IUserReferredPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IUserReferredPresenter {
        return presenter as! IUserReferredPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = UserReferredPresenter(view: self)
        setData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.USER)
    }
    
    // MARK: Actions
    
    func setData(){
        lblFirstName.text = user.firstName
        lblLastName.text = user.lastName
        lblDocument.text = user.fullid
        lblEmail.text = user.email
        lblCellPhone.text = user.phoneper
    }
    
}

// MARK: View
extension UserReferredViewController : IUserReferredView {
    
}
