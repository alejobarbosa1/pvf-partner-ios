//
//  FilterSearchBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class FilterSearchBL: BaseBL {
	
	// MARK: Properties
    
    var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IFilterSearchListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IFilterSearchListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IFilterSearchListener {
        return listener as! IFilterSearchListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .CITIES:
            let array = (response as! ArrayResponse).objectsArray
            let cities: [City] = City.fromJsonArray(array)
            getListener().setCities(cities)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension FilterSearchBL : IFilterSearchBL {
    
    // MARK: Listeners
    
    func getCities(byCountry country: Country) {
        restUser.getCities(country, listener: self)
    }
    
}
