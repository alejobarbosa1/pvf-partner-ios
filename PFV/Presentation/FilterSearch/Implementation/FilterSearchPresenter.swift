//
//  FilterSearchPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class FilterSearchPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IFilterSearchBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IFilterSearchView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IFilterSearchView) {
        super.init(baseView: view)
        self.bl = FilterSearchBL(listener: self)
    }
    
    fileprivate func getView() -> IFilterSearchView {
        return view as! IFilterSearchView
    }
    
    fileprivate func getBL() -> IFilterSearchBL {
        return bl as! IFilterSearchBL
    }
}

// MARK: Presenter
extension FilterSearchPresenter: IFilterSearchPresenter {

      // MARK: Actions
    
    func getCities(byCountry country: Country) {
        getBL().getCities(byCountry: country)
    }

}

// MARK: Listener
extension FilterSearchPresenter: IFilterSearchListener {

      // MARK: Listeners
    
    func setCities(_ cities: [City]) {
        getView().setCities(cities)
    }
    
}

