//
//  FilterSearchViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 26/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class FilterSearchViewController: BaseViewController {
  
  	// MARK: Properties
    
    // PickerTextFiles:
    @IBOutlet weak var tfCity: PickerTextField!
    @IBOutlet weak var tfRoomsNumber: PickerTextField!
    @IBOutlet weak var tfPriceFrom: PickerTextField!
    @IBOutlet weak var tfPriceUntil: PickerTextField!
    
    // Housing Type
    // Selectors:
    @IBOutlet weak var houseSelector: UIView!
    @IBOutlet weak var apartmentSelector: UIView!
    @IBOutlet weak var lotSelector: UIView!
    @IBOutlet weak var cellarSelector: UIView!
    @IBOutlet weak var localSelector: UIView!
    @IBOutlet weak var officeSelector: UIView!
    // Buttons:
    @IBOutlet weak var btHouse: UIButton!
    @IBOutlet weak var btApartment: UIButton!
    @IBOutlet weak var btLot: UIButton!
    @IBOutlet weak var btCellar: UIButton!
    @IBOutlet weak var btLocal: UIButton!
    @IBOutlet weak var btOffice: UIButton!
    
    // Category
    // Selectors:
    @IBOutlet weak var overFlatSelector: UIView!
    @IBOutlet weak var newSelector: UIView!
    @IBOutlet weak var usedSelector: UIView!
    //Buttons:
    @IBOutlet weak var btOverFlat: UIButton!
    @IBOutlet weak var btNew: UIButton!
    @IBOutlet weak var btUsed: UIButton!
    
    @IBOutlet weak var btApplyFilters: UIButton!
    
    fileprivate var isHouseSelected: Bool! = false
    fileprivate var isApartmentSelected: Bool! = false
    fileprivate var isLotSelected: Bool! = false
    fileprivate var isCellarSelected: Bool! = false
    fileprivate var isLocalSelected: Bool! = false
    fileprivate var isOfficeSelected: Bool! = false
    
    fileprivate var isOverFlatSelected: Bool! = false
    fileprivate var isNewSelected: Bool! = false
    fileprivate var isUsedSelected: Bool! = false
    
    fileprivate var listCities = [City]()
    fileprivate var listRooms = [Room]()
    fileprivate var listPriceFrom = [PriceFrom]()
    fileprivate var listPriceUntil = [PriceUntil]()
    
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IFilterSearchPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IFilterSearchPresenter {
        return presenter as! IFilterSearchPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FilterSearchPresenter(view: self)
        self.setCorner(btApplyFilters.frame.height / 10, views: btApplyFilters)
        tfCity.delegate = self
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            let country = Country()
            country.id_country = Constants.Commons.COLOMBIA_ID as NSNumber
            self.getPresenter().getCities(byCountry: country)
        }
        setPickerTextFiles()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.FILTER_SEARCH)
    }
    
    func setPickerTextFiles(){
        for i in 1...10{
            listRooms.append(Room(i.description))
        }
        tfRoomsNumber.delegate = self
        tfRoomsNumber.pickerDataSource = listRooms
        for p in stride(from: 5000, to: 301000, by: 1000){
            listPriceFrom.append(PriceFrom(p.description, valueWithDots: (p * 10000).formattedWithSeparator + Constants.Commons.COP))
        }
        tfPriceFrom.delegate = self
        tfPriceFrom.pickerDataSource = listPriceFrom
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        if tag == Constants.Tags.PRICE_FROM_TF {
            tfPriceUntil.text = ""
            listPriceUntil = [PriceUntil]()
            tfPriceUntil.isEnabled = true
            let priceFrom: Int = Int(pickerModel!.getCode())! / 10000
            for p in stride(from: priceFrom, to: 301000, by: 1000){
                listPriceUntil.append(PriceUntil(p.description, valueWithDots: p.formattedWithSeparator + Constants.Commons.COP))
            }
            tfPriceUntil.delegate = self
            tfPriceUntil.pickerDataSource = listPriceUntil
        }
    }
    
    func changeStatus(view: UIView) -> Bool{
        if view.isHidden {
            view.isHidden = false
            return true
        } else {
            view.isHidden = true
            return false
        }
    }
    
    // Housing Type:
    @IBAction func onHouseClick(_ sender: Any) {
        isHouseSelected = changeStatus(view: houseSelector)
        
    }
    @IBAction func btOnApartmentClick(_ sender: Any) {
        isApartmentSelected = changeStatus(view: apartmentSelector)
    }
    @IBAction func onLotClick(_ sender: Any) {
        isLotSelected = changeStatus(view: lotSelector)
    }
    @IBAction func onCellarClick(_ sender: Any) {
        isCellarSelected = changeStatus(view: cellarSelector)
    }
    @IBAction func onLocalClick(_ sender: Any) {
        isLocalSelected = changeStatus(view: localSelector)
    }
    @IBAction func onOfficeClick(_ sender: Any) {
        isOfficeSelected = changeStatus(view: officeSelector)
    }
    
    //Category:
    @IBAction func onOverFlatClick(_ sender: Any) {
        isOverFlatSelected = changeStatus(view: overFlatSelector)
    }
    @IBAction func onNewClick(_ sender: Any) {
        isNewSelected = changeStatus(view: newSelector)
    }
    @IBAction func onUsedClick(_ sender: Any) {
        isUsedSelected = changeStatus(view: usedSelector)
    }
    
    
    @IBAction func onApplyFiltersClick(_ sender: Any) {
        let filter = Filter()
        if tfCity.selectedItem?.getCode() != nil {
            filter.idcity = NSNumber(value: Int(tfCity.selectedItem!.getCode())!)
        }
        if isHouseSelected || isApartmentSelected || isLotSelected || isCellarSelected || isLocalSelected || isOfficeSelected {
            filter.tyep_pro = []
        }
        if isHouseSelected {
            filter.tyep_pro.append(Constants.HousingType.HOUSE)
        }
        if isApartmentSelected {
            filter.tyep_pro.append(Constants.HousingType.APARTMENT)
        }
        if isLotSelected {
            filter.tyep_pro.append(Constants.HousingType.LOT)
        }
        if isCellarSelected {
            filter.tyep_pro.append(Constants.HousingType.CELLAR)
        }
        if isLocalSelected {
            filter.tyep_pro.append(Constants.HousingType.LOCAL)
        }
        if isOfficeSelected {
            filter.tyep_pro.append(Constants.HousingType.OFFICE)
        }
        if isOverFlatSelected || isNewSelected || isUsedSelected {
            filter.type_inmu = []
        }
        if isOverFlatSelected {
            filter.type_inmu.append(Constants.Categories.OVER_FLAT)
        }
        if isNewSelected {
            filter.type_inmu.append(Constants.Categories.NEW)
        }
        if isUsedSelected {
            filter.type_inmu.append(Constants.Categories.USED)
        }
        if tfRoomsNumber.selectedItem?.getCode() != nil {
            filter.hab = NSNumber(value: Int(tfRoomsNumber.selectedItem!.getCode())!)
        }
        if tfPriceFrom.selectedItem?.getCode() != nil {
            filter.precioMe = NSNumber(value: Int(tfPriceFrom.selectedItem!.getCode())!)
        }
        if tfPriceUntil.selectedItem?.getCode() != nil {
            filter.precioMa = NSNumber(value: Int(tfPriceUntil.selectedItem!.getCode())!)
        }
        filter.start = 0
//        DispatchQueue.global().async {
//            self.getPresenter().filterSearch(filter)
//        }
        for vc in self.navigationController!.viewControllers where vc is FilterPropertyViewController {
            (vc as! FilterPropertyViewController).filter = filter
            (vc as! FilterPropertyViewController).isFilter = true
        }
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: View
extension FilterSearchViewController : IFilterSearchView {
    
    func setCities(_ cities: [City]) {
        hideProgress()
        DispatchQueue.main.async {
            self.listCities = cities
            self.tfCity.pickerDataSource = self.listCities
            self.tfCity.isEnabled = true
        }
    }
  
}
