//
//  LoginPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class LoginPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ILoginBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ILoginView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ILoginView) {
        super.init(baseView: view)
        self.bl = LoginBL(listener: self)
    }
    
    fileprivate func getView() -> ILoginView {
        return view as! ILoginView
    }
    
    fileprivate func getBL() -> ILoginBL {
        return bl as! ILoginBL
    }
}

// MARK: Presenter
extension LoginPresenter: ILoginPresenter {
    
     // MARK: Actions
    
    func getSettings() {
        getBL().getSettings()
    }
    
    func setSettings(_ settings: Settings) {
        getBL().setSettings(settings)
    }
    
    func getCurrentUser() {
        getBL().getCurrentUser()
    }
    
    func login(user: User, isRemember: Bool, isLoginFB: Bool){
        getBL().login(user : user, isRemember: isRemember, isLoginFB: isLoginFB)
    }
    
    func updateFireBaseToken(_ token: String) {
        getBL().updateFireBaseToken(token)
    }
    
    func closeSession(){
        getBL().closeSession()
    }
}

// MARK: Listener
extension LoginPresenter: ILoginListener {
    
    // MARK: Listeners
    
    func showLoginAlert(_ show: Bool) {
        getView().showLoginAlert(show)
    }
    
    func setCurrentUser(_ user: User) {
        getView().setCurrentUser(user)
    }
    
    func onLoginSuccess() {
        getView().onLoginSuccess()
    }
    
    func onEmptyEmail(){
        getView().notifyEmptyEmail()
    }
    
    func onEmptyPassword(){
        getView().notifyEmptyPassword()
        
    }
    
    func onInvalidEmail(){
        getView().notifyInvalidEmail()
        
    }
    
    func onFailedLogin(isUser: Bool) {
        getView().onFailedLogin(isUser: isUser)
    }

}

