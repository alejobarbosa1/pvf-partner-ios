 //
//  LoginViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import FirebaseMessaging
import FBSDKCoreKit
import FBSDKLoginKit

// MARK: Base
class LoginViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var viewSelectorLogin: UIView!
    
    @IBOutlet weak var contentRegister: UIScrollView!

    @IBOutlet weak var viewRegister: UIView!
    @IBOutlet weak var lblRegister: UILabel!
    @IBOutlet weak var viewSelectorRegister: UIView!

    @IBOutlet weak var lblRemember: UILabel!
    @IBOutlet weak var viewLoginContent: UIView!
    @IBOutlet weak var tfEmailLogin: UITextField!
    @IBOutlet weak var tfPasswordLogin: UITextField!
    @IBOutlet weak var viewRemember: UIView!
    @IBOutlet weak var viewRememberYellow: UIView!
    @IBOutlet weak var btLogin: UIButton!
    @IBOutlet weak var btLoginFb: UIButton!
    @IBOutlet weak var btForgetPassword: UIButton!
//    internal var presenter: ISlideMenuPresenter!
//    var progressIndicatorView: ProgressView!
    
    fileprivate var isRemember: Bool = false
    fileprivate var isLogin: Bool = true
    fileprivate var isRegister: Bool = false
    fileprivate var currentUser: User!

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ILoginPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ILoginPresenter {
        return presenter as! ILoginPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = LoginPresenter(view: self)
        DispatchQueue.global().async {
            self.getPresenter().getSettings()
        }
        self.setCorner(btLogin.frame.height / 10, views: btLogin,btLoginFb)
        self.setCorner(btForgetPassword.frame.height / 10, views: btForgetPassword)
        self.setCustomStyle(borderColor: Constants.Colors.GRAY, borderWidth: 4, cornerRadius: viewRemember.frame.height / 2, views: viewRemember)
        viewLogin.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onClickSelectorLogin)))
        viewRegister.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onClickSelectorRegister)))
        viewRemember.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onClickRemember)))
        lblRemember.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.onClickRemember)))
        let RegisterViewController: RegisterViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.REGISTER_VIEW_CONTROLLER) as! RegisterViewController
        self.addChildViewController(RegisterViewController)
        RegisterViewController.view.frame = CGRect(x: contentRegister.frame.origin.x, y: 0, width: contentRegister.frame.width, height: 640)
        contentRegister.contentSize.height = 640
        self.contentRegister.addSubview(RegisterViewController.view)
        RegisterViewController.didMove(toParentViewController: self)
        self.buttonOnViewPassword(self, textFields: tfPasswordLogin)
        onClickSelectorLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func onClickRemember(){
        
        if isRemember{
            isRemember = false
            viewRememberYellow.isHidden = true
        }
        else{
            isRemember = true
            viewRememberYellow.isHidden = false
        }
    }
    
    @objc func onClickSelectorLogin(){
        if !isLogin {
            isLogin = true
            isRegister = false
            viewLoginContent.isHidden = false
            contentRegister.isHidden = true
            lblLogin.textColor = UIColor.white
            lblRegister.textColor = Constants.Colors.GRAY
            viewSelectorLogin.isHidden = false
            viewSelectorRegister.isHidden = true
        }
    }
    
    @objc func onClickSelectorRegister(){
        if !isRegister {
            isLogin = false
            isRegister = true
            viewLoginContent.isHidden = true
            contentRegister.isHidden = false
            lblLogin.textColor = Constants.Colors.GRAY
            lblRegister.textColor = UIColor.white
            viewSelectorLogin.isHidden = true
            viewSelectorRegister.isHidden = false
        }
    }
    
    
    
    @IBAction func onClickLogin(_ sender: Any) {
        showProgress(withTitle: nil)
        let user = User(email: self.tfEmailLogin.text!, password: self.tfPasswordLogin.text!)
        DispatchQueue.global().async {
            self.getPresenter().login(user: user, isRemember: self.isRemember, isLoginFB: false)

        }
    }
    
    @IBAction func onClickForgetPassword(_ sender: Any) {
        navigateTo(Constants.ViewControllers.FORGET_PASSWORD_VIEW_CONTROLLER)
    }
    
    @IBAction func onLoginFbClick(_ sender: Any) {
        DispatchQueue.global().async {
            self.logingFacebook(false)
        }
    }
    
    func logingFacebook(_ isRegister: Bool){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if( fbloginresult.grantedPermissions != nil && fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData(isRegister)
                }
                else {
                    self.hideProgress()
                    return}
            }
            else{
                print(error!)
            }
        }
    }
    
    func getFBUserData(_ isRegister: Bool){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let json = result as! [String: AnyObject]
                    let user = User()
                    user.fromDictionary(json)
                    self.showProgress(withTitle: nil)
                    self.getPresenter().login(user: user, isRemember: self.isRemember, isLoginFB: true)
                }
                else{
                    print(error!)
                }
            })
        }
    }
    
    func navigateTo(_ viewControllerIdentifier: String){
        switch viewControllerIdentifier {
        case Constants.ViewControllers.HOST_VIEW_CONTROLLER:
            let hostViewController: HostViewController!
            hostViewController = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! HostViewController
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(hostViewController, animated: true)
            }
            self.tfEmailLogin.text = ""
            self.tfPasswordLogin.text = ""
            break
        case Constants.ViewControllers.FORGET_PASSWORD_VIEW_CONTROLLER:
            let ForgetController: ForgetPasswordViewController!
            ForgetController = self.storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifier) as! ForgetPasswordViewController
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(ForgetController, animated: true)
            }
            break
        default:
            break
        }
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmailLogin {
            tfEmailLogin.resignFirstResponder()
            tfPasswordLogin.becomeFirstResponder()
        } else if textField == tfPasswordLogin {
            view.endEditing(true)
        }
        return true
    }
    
    func showLoginAlert(){
        DispatchQueue.main.async {
            let contentView: LoginAlertView = LoginAlertView(delegate: self)
            self.alertView = SwiftAlertView(contentView: contentView,
                                            delegate: self,
                                            cancelButtonTitle: nil)
            self.alertView?.show(true)
            
        }
    }
    
}

// MARK: View
extension LoginViewController : ILoginView {
    
    func showLoginAlert(_ show: Bool) {
        if show{
            DispatchQueue.main.async {
                self.showLoginAlert()
            }
        }
    }
    
    func setCurrentUser(_ user: User) {
        self.currentUser = user
    }
    
    func onLoginSuccess() {
        hideProgress()
        navigateTo(Constants.ViewControllers.HOST_VIEW_CONTROLLER)
        let token = Messaging.messaging().fcmToken
        DispatchQueue.global().async {
            self.getPresenter().updateFireBaseToken(token!)
        }
    }
    
    func notifyEmptyEmail() {
        hideProgress()
        showPopover(tfEmailLogin, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func notifyEmptyPassword() {
        hideProgress()
        showPopover(tfPasswordLogin, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func notifyInvalidEmail() {
        hideProgress()
        showPopover(tfEmailLogin, message: Constants.Dialogs.EMAIL_INVALID, icon: #imageLiteral(resourceName: "icon_warning"))
    }
    
    func onFailedLogin(isUser: Bool) {
        hideProgress()
        DispatchQueue.main.async {
            if isUser {
                self.showAlert(alertType: Constants.AlertTypes.WARNING,
                               message: Constants.Dialogs.PARTNER_NOT_REGISTER,
                               hasCancelButton: false,
                               tag: Constants.Tags.DEFAULT_OK)
            } else {
                self.showAlert(alertType: Constants.AlertTypes.WARNING,
                               message: Constants.Dialogs.PARTNER_NOT_ACTIVED,
                               hasCancelButton: false,
                               tag: Constants.Tags.DEFAULT_OK)
            }
            
        }
    }

}
 
 extension LoginViewController : LoginAlertViewDelegate{
    
    func onAcceptClick(dontShowAgain: Bool) {
        hideAlertView()
        if dontShowAgain{
            DispatchQueue.global().async {
                let settings = Settings()
                settings.loginAlert = 0
                self.getPresenter().setSettings(settings)
            }
        }
    }
    
 }

