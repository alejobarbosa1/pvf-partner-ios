//
//  LoginBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class LoginBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate let restUser = RestUser()
    fileprivate let restCommon = RestCommon()
    fileprivate var isRemember: Bool!
    fileprivate var user: User!
    fileprivate var documentList = [DocumentType]()
    fileprivate var educationalLevelList = [EducationalLevel]()
    fileprivate var contractTypeList = [ContractType]()
    fileprivate var count = 0
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ILoginListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ILoginListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ILoginListener {
        return listener as! ILoginListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .LOGIN:
//            let user = response as! User
            let array = (response as! ArrayResponse).objectsArray
            let users: [User] = User.fromJsonArray(array)
            let user = users.first!
            
            if SystemPreferencesManager.getCurrentUser()?.token != nil && !(SystemPreferencesManager.getCurrentUser()?.token.isEmpty)!
            {
                user.token = restUser.getCurrentUser()?.token
            }
            user.remember = isRemember! as NSNumber
//            setData()
//            let settings = SystemPreferencesManager.getSettings()
//            settings?.login = 1
//            restUser.setSettings(settings!)
            if user.status.intValue > 0 && (user.partner_code != nil) {
//                if (user.leveleducation != nil && !user.leveleducation.isEmpty) && (user.other_information.type_cont != nil && !user.other_information.type_cont.isEmpty) {
//                    DbManager.insertUser(ChatUser(id: user.iduser, rolId: Constants.Rols.USER, phone: user.phoneper))
//                    restCommon.getDocumentTypes(self)
//                    restUser.getEducationalList(self)
//                    restUser.getContractTypeList(self)
//                    self.user = user
//                    return
//                }
                DbManager.insertUser(ChatUser(id: user.iduser, rolId: Constants.Rols.USER, phone: user.phoneper))
                restUser.setCurrentUser(user)
                getListener().onLoginSuccess()
            }
            if user.status == 0 && (user.partner_code == nil || user.partner_code.isEmpty) {
                getListener().onFailedLogin(isUser: true)
                return
            }
            if user.status.intValue > 0 && (user.partner_code == nil) {
                getListener().onFailedLogin(isUser: false)
            }
            break
        case .CLOSE_SESSION:
            restUser.setCurrentUser(nil)
            break
        case .PICKER_VIEW_DOCUMENT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let documentList: [DocumentType] = DocumentType.fromJsonArray(array)
            self.documentList = documentList
            self.count += 1
//            setData()
            break
        case .EDUCATIONAL_LEVEL:
            let array = (response as! ArrayResponse).objectsArray
            let educationalLevelList: [EducationalLevel] = EducationalLevel.fromJsonArray(array)
            self.educationalLevelList = educationalLevelList
            self.count += 1
//            setData()
            break
        case .CONTRACT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let contractTypeList: [ContractType] = ContractType.fromJsonArray(array)
            self.contractTypeList = contractTypeList
            self.count += 1
//            setData()
            break
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.PICKER_VIEW_DOCUMENT_TYPE{
            restCommon.getDocumentTypes(self)
            return
        }
        
        if tag == Constants.RepositoriesTags.EDUCATIONAL_LEVEL {
            restUser.getEducationalList(self)
            return
        }
        
        if tag == Constants.RepositoriesTags.CONTRACT_TYPE {
            restUser.getContractTypeList(self)
            return
        }
        
        super.onFailedResponse(response, tag: tag)
    }
    
//    func setData(){
//        if count < 3 {
//            return
//        }
//        for document in documentList where document.iddoctype.description == user.iddoctype {
//            user.docType = document.namedoctype
//            user.initialDocType = document.initialdoctype
//        }
//        for level in educationalLevelList where level.id_level.description == user.leveleducation {
//            user.educationalLevel = level.name_level
//        }
//        for contract in contractTypeList where contract.id_type_cont.description == user.other_information.type_cont {
//            user.contractType = contract.name_contract
//        }
//        restUser.setCurrentUser(user)
//        getListener().onLoginSuccess()
//    }
    
}

// MARK: BL
extension LoginBL : ILoginBL {
    
    // MARK: Listeners
    
    func getSettings() {
        let settings = restUser.getSettings()
        if settings != nil {
            getListener().showLoginAlert(settings!.showLoginAlert())
        } else {
            getListener().showLoginAlert(true)
        }
    }
    
    func setSettings(_ settings: Settings) {
        restUser.setSettings(settings)
    }
    
    func getCurrentUser() {
        let user = restUser.getCurrentUser()
        getListener().setCurrentUser(user!)
    }
    
    func login(user: User, isRemember: Bool, isLoginFB: Bool) {
        if !isLoginFB {
            if user.email.isEmpty {
                getListener().onEmptyEmail()
                return
            }
            
            if user.password.isEmpty {
                getListener().onEmptyPassword()
                return
            }
            if !Validator.isValidEmail(user.email) {
                getListener().onInvalidEmail()
                return
            }
        }
        self.isRemember = isRemember
        self.user = user
        restUser.login(user, listener: self)
    }
    
    func updateFireBaseToken(_ token: String) {
        let user = User()
        user.token_firebase = token
        user.iduser = restUser.getCurrentUser()?.iduser
        restUser.updateFireBaseToken(user, listener: self)
    }
    
    func closeSession(){
        let user = User()
        user.email = self.user.email
        restUser.closeSession(user, listener: self)
    }
    
}
