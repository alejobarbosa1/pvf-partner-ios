//
//  ILoginListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol ILoginListener: IBaseListener {
    
    func showLoginAlert(_ show: Bool)
    
    func setCurrentUser(_ user: User)
    
    func onLoginSuccess()
    
    func onEmptyEmail()
    
    func onEmptyPassword()
    
    func onInvalidEmail()
    
    func onFailedLogin(isUser: Bool)
  
}
