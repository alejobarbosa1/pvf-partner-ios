//
//  ILoginBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol ILoginBL: IBaseBL {
    
    func getSettings()
    
    func setSettings(_ settings: Settings)
    
    func getCurrentUser()
    
    func login(user: User, isRemember: Bool, isLoginFB: Bool)
    
    func updateFireBaseToken(_ token: String)
    
    func closeSession()
  
}
