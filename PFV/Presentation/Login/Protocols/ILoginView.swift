//
//  ILoginView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 19/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol ILoginView: IBaseView {
    
    func showLoginAlert(_ show: Bool)
    
    func setCurrentUser(_ user: User)
    
    func onLoginSuccess()
  
    func notifyEmptyEmail()
    
    func notifyEmptyPassword()
    
    func notifyInvalidEmail()
    
    func onFailedLogin(isUser: Bool)
    
}
