//
//  IRegisterBL.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IRegisterBL: IBaseBL {
    
    func getDocumentTypes()
    
    func sendVerification(_ user: User)
    
    func registerByFB(_ user: User)
  
}
