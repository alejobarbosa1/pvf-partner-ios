//
//  IRegisterListener.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IRegisterListener: IBaseListener {
    
    func setDocumentTypes(_ documentsList: [DocumentType])
    
    func onEmptyEmail()
    
    func onEmptyPassword()
    
    func onEmptyName()
    
    func onEmptyLastName()
    
    func onEmptyPhone()
    
    func onEmptyDocumentType()
    
    func onEmptyDocumentNumber()
    
    func userVerifed()
  
}
