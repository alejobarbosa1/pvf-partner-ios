//
//  IRegisterView.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol IRegisterView: IBaseView {
    
    func setDocumentTypes(_ documentsList: [DocumentType])
    
    func notifyEmptyEmail()
    
    func notifyEmptyPassword()
    
    func notifyEmptyName()
    
    func notifyEmptyLastName()
    
    func notifyEmptyPhone()
    
    func onEmptyDocumentType()
    
    func onEmptyDocumentNumber()
    
    func userVerifed()
  
}
