//
//  RegisterPresenter.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class RegisterPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IRegisterBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IRegisterView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IRegisterView) {
        super.init(baseView: view)
        self.bl = RegisterBL(listener: self)
    }
    
    fileprivate func getView() -> IRegisterView {
        return view as! IRegisterView
    }
    
    fileprivate func getBL() -> IRegisterBL {
        return bl as! IRegisterBL
    }
}

// MARK: Presenter
extension RegisterPresenter: IRegisterPresenter {

      // MARK: Actions
    
    func getDocumentTypes() {
        getBL().getDocumentTypes()
    }
    
    func sendVerification(_ user: User) {
        getBL().sendVerification(user)
    }
    
    func registerByFB(_ user: User) {
        getBL().registerByFB(user)
    }
}

// MARK: Listener
extension RegisterPresenter: IRegisterListener {

      // MARK: Listeners
    
    func setDocumentTypes(_ documentsList: [DocumentType]) {
        getView().setDocumentTypes(documentsList)
    }
    
    func onEmptyEmail(){
        getView().notifyEmptyEmail()
    }
    
    func onEmptyPassword(){
        getView().notifyEmptyPassword()
    }
    
    func onEmptyName(){
        getView().notifyEmptyName()
    }
    
    func onEmptyLastName(){
        getView().notifyEmptyLastName()
    }
    
    func onEmptyPhone(){
        getView().notifyEmptyPhone()
    }
    
    func onEmptyDocumentType(){
        getView().onEmptyDocumentType()
    }
    
    func onEmptyDocumentNumber(){
        getView().onEmptyDocumentNumber()
    }
    
    func userVerifed() {
        getView().userVerifed()
    }
    
}

