//
//  RegisterViewController.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import FirebaseMessaging
import FBSDKCoreKit
import FBSDKLoginKit

// MARK: Base
class RegisterViewController: BaseViewController{
  
  	// MARK: Properties

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var tfDocumentType: PickerTextField!
    @IBOutlet weak var tfDocumentNumber: UITextField!
    @IBOutlet weak var btRegister: UIButton!
    @IBOutlet weak var btRegisterFb: UIButton!
    @IBOutlet weak var lblTerms: UILabel!
    fileprivate var listDocuments: [DocumentType] = []
    fileprivate var userFB: User!
    
    
  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IRegisterPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IRegisterPresenter {
        return presenter as! IRegisterPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RegisterPresenter(view: self)
//        showProgress(withTitle: nil)
//        DispatchQueue.global().async {
//            self.getPresenter().getDocumentTypes()
//        }
        self.navigationController?.isNavigationBarHidden = true
        self.setCorner(btRegister.frame.height / 10, views: btRegister, btRegisterFb)
        buttonOnViewPassword(self, textFields: tfPassword)
        lblTerms.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterViewController.onTermsClick)))
        tfDocumentType.delegate = self
        tfDocumentType.pickerDataSource = listDocuments
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func onClickRegister(_ sender: Any) {
        showProgress(withTitle: nil)
        let user = User()
        user.email = tfEmail.text
        user.idrol = Constants.Rols.PARTNER as NSNumber
        user.password = tfPassword.text
        user.firstName = tfName.text
        user.lastName = tfLastName.text
        user.phoneper = tfPhone.text
        user.iddoctype = tfDocumentType.selectedItem?.getCode()
        user.numberidenf = tfDocumentNumber.text
        DispatchQueue.global().async {
            self.getPresenter().sendVerification(user)
        }
    }
    
    @IBAction func onRegisterFbClick(_ sender: Any) {
        DispatchQueue.global().async {
            self.logingFacebook(true)
        }
    }
    
    func logingFacebook(_ isRegister: Bool){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if( fbloginresult.grantedPermissions != nil && fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData(isRegister)
                }
                else {
                    self.hideProgress()
                    return}
            }
            else{
                print(error!)
            }
        }
    }
    
    override func onContinueClick(user: User) {
        super.onContinueClick(user: user)
        showProgress(withTitle: nil)
        userFB.phoneper = user.phoneper
        userFB.iddoctype = user.iddoctype
        userFB.numberidenf = user.numberidenf
        userFB.idrol = Constants.Rols.PARTNER
        DispatchQueue.global().async {
            self.getPresenter().registerByFB(self.userFB)
        }
    }
    
    func getFBUserData(_ isRegister: Bool){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let json = result as! [String: AnyObject]
                    let user = User()
                    user.fromDictionary(json)
                    if isRegister{
                        user.image = user.picture.data.url
                        self.userFB = user
                        DispatchQueue.main.async {
                            self.showRegisterDataView(delegate: self, documentTypes: self.listDocuments)
                        }
                    }
                }
                else{
                    print(error!)
                }
            })
        }
    }
    
    @objc func onTermsClick(){
        let termsAndConditions: TermsAndConditionsViewController!
        termsAndConditions = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.TERMS_AND_CONDITIONS_2) as! TermsAndConditionsViewController
        termsAndConditions.isRegister = true
        navigationController?.pushViewController(termsAndConditions, animated: true)
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        if tag == Constants.Tags.DOCUMENT_YPE_TF {
            tfDocumentType.text = (pickerModel as! DocumentType).initialdoctype
        }
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        switch alertTag {
        case Constants.Tags.USER_VERIFED:
            tfName.text = ""
            tfLastName.text = ""
            tfEmail.text = ""
            tfPassword.text = ""
            tfPhone.text = ""
            tfDocumentType.text = ""
            tfDocumentNumber.text = ""
            break
        default:
            break
        }
    }
    
}

// MARK: View
extension RegisterViewController : IRegisterView {

    func setDocumentTypes(_ documentsList: [DocumentType]) {
        hideProgress()
        DispatchQueue.main.async {
            self.listDocuments = documentsList
            self.tfDocumentType.pickerDataSource = documentsList
            self.tfDocumentType.isEnabled = true
        }
    }
    
    func notifyEmptyEmail(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfEmail,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifyEmptyPassword(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfPassword,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifyEmptyName(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifyEmptyLastName(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfLastName,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func notifyEmptyPhone(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfPhone,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyDocumentType(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfDocumentType,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onEmptyDocumentNumber(){
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfDocumentNumber,
                             message: Constants.Dialogs.FIELD_REQUIRED,
                             icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func userVerifed() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: Constants.AlertTypes.DONE,
                           message: Constants.Dialogs.REGISTER,
                           hasCancelButton: false,
                           tag: Constants.Tags.USER_VERIFED)
        }
    }

}
