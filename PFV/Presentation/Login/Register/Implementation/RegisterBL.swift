//
//  RegisterBL.swift
//  PVF Partner
//
//  Created by Pablo Linares on 20/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class RegisterBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate let restCommon = RestCommon()
    fileprivate let restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IRegisterListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IRegisterListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IRegisterListener {
        return listener as! IRegisterListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .PICKER_VIEW_DOCUMENT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let documentsList: [DocumentType] = DocumentType.fromJsonArray(array)
            getListener().setDocumentTypes(documentsList)
            break
        case .REGISTER_PARTNER:
            getListener().userVerifed()
            break
        case .REGISTER_FB:
            getListener().userVerifed()
            break
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.PICKER_VIEW_DOCUMENT_TYPE{
            restCommon.getDocumentTypes(self)
        } else {
            super.onFailedResponse(response, tag: tag)
        }
    }
}

// MARK: BL
extension RegisterBL : IRegisterBL {
    
    // MARK: Listeners
    
    func getDocumentTypes() {
        restCommon.getDocumentTypes(self)
    }
    
    func sendVerification(_ user: User) {
        if user.email.isEmpty{
            getListener().onEmptyEmail()
            return
        }
        if user.password.isEmpty{
            getListener().onEmptyPassword()
            return
        }
        if user.firstName.isEmpty{
            getListener().onEmptyName()
            return
        }
        if user.lastName.isEmpty{
            getListener().onEmptyLastName()
            return
        }
        if user.phoneper.isEmpty{
            getListener().onEmptyPhone()
            return
        }
        if user.iddoctype.isEmpty {
            getListener().onEmptyDocumentType()
            return
        }
        if user.numberidenf.isEmpty {
            getListener().onEmptyDocumentNumber()
            return
        }
        user.idrol = Constants.Rols.PARTNER as NSNumber
        user.status = 0
        restUser.registerPartner(user, listener: self)
    }
    
    func registerByFB(_ user: User) {
        user.status = 0
        restUser.setCurrentUser(user)
        restUser.registerByFB(user, listener: self)
    }
    
}
