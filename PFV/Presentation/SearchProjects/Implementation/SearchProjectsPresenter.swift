//
//  SearchProjectsPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SearchProjectsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ISearchProjectsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ISearchProjectsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ISearchProjectsView) {
        super.init(baseView: view)
        self.bl = SearchProjectsBL(listener: self)
    }
    
    fileprivate func getView() -> ISearchProjectsView {
        return view as! ISearchProjectsView
    }
    
    fileprivate func getBL() -> ISearchProjectsBL {
        return bl as! ISearchProjectsBL
    }
}

// MARK: Presenter
extension SearchProjectsPresenter: ISearchProjectsPresenter {

      // MARK: Actions
    
    func getDrafts() {
        getBL().getDrafts()
    }
    
    func getFavorites() {
        getBL().getFavorites()
    }
    
    func getCities(byCountry country: Country) {
        getBL().getCities(byCountry: country)
    }
    
    func getPurchasesAndCredits() {
        getBL().getPurchasesAndCredits()
    }

}

// MARK: Listener
extension SearchProjectsPresenter: ISearchProjectsListener {

      // MARK: Listeners
    
    func setDrafts(_ projectsDb: [ProjectDb]) {
        getView().setDrafts(projectsDb)
    }
    
    func setFavorites(_ favorites: Project) {
        getView().setFavorites(favorites)
    }
    
    func setCities(_ cities: [City]) {
        getView().setCities(cities)
    }
    
    func setPurchasesAndCredits(_ purchasesAndCredits: PurchasesAndCredits) {
        getView().setPurchasesAndCredits(purchasesAndCredits)
    }
    
}

