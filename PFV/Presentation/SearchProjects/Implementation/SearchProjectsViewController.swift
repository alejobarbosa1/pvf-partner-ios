//
//  SearchProjectsViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class SearchProjectsViewController: BaseViewController {
  
  	// MARK: Properties
    @IBOutlet weak var tfCity: PickerTextField!
    @IBOutlet weak var btSearchProject: UIButton!
    
    @IBOutlet weak var viewProjects: UIView!
    @IBOutlet weak var iconProjects: UIImageView!
    @IBOutlet weak var viewDrafts: UIView!
    @IBOutlet weak var iconDrafts: UIImageView!
    @IBOutlet weak var viewFavorites: UIView!
    @IBOutlet weak var iconFavorites: UIImageView!
    fileprivate var listCountries: [Country] = []
    fileprivate var listCities: [City] = []
    
  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ISearchProjectsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> ISearchProjectsPresenter {
        return presenter as! ISearchProjectsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SearchProjectsPresenter(view: self)
        setCorner(btSearchProject.frame.height / 10, views: btSearchProject)
        setCorner(viewProjects.frame.height / 10, views: viewProjects,viewDrafts,viewFavorites)
        tfCity.delegate = self
        viewProjects.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SearchProjectsViewController.onClickProjects)))
        viewFavorites.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SearchProjectsViewController.onClickFavorites)))
        viewDrafts.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SearchProjectsViewController.onDraftsClick)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.PROJECTS)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getDrafts()
            self.getPresenter().getFavorites()
            let country = Country()
            country.id_country = Constants.Commons.COLOMBIA_ID as NSNumber
            self.getPresenter().getCities(byCountry: country)
        }
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        switch tag {
        case Constants.Tags.CITY_TEXT_FIELD:
            btSearchProject.isEnabled = true
            break
        default:
            break
        }
        
    }

    @IBAction func onSearchProjectClick(_ sender: Any) {
        let filterPropertyViewController: FilterPropertyViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.FILTER_PROPERTY_VIEW_CONTROLLER) as! FilterPropertyViewController
        filterPropertyViewController.idcity = tfCity.selectedItem?.getCode()
        hostViewController.pushViewController(filterPropertyViewController, animated: true)
    }
    
    @objc func onClickProjects(){
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getPurchasesAndCredits()
        }
    }
    
    @objc func onDraftsClick() {
        let viewDrafts: FilterPropertyViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.FILTER_PROPERTY_VIEW_CONTROLLER) as! FilterPropertyViewController
        viewDrafts.isProjectsSaved = true
        hostViewController.pushViewController(viewDrafts, animated: true)
    }
    
    @objc func onClickFavorites(){
        let viewFavorites: FilterPropertyViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.FILTER_PROPERTY_VIEW_CONTROLLER) as! FilterPropertyViewController
        viewFavorites.isShowingFavorites = true
        self.hostViewController.pushViewController(viewFavorites, animated: true)
    }
    
}

// MARK: View
extension SearchProjectsViewController : ISearchProjectsView {
    
    func setDrafts(_ projectsDb: [ProjectDb]) {
        DispatchQueue.main.async {
            if !projectsDb.isEmpty && projectsDb.count > 0 {
                self.iconDrafts.image = #imageLiteral(resourceName: "icon_edit")
                self.iconDrafts.tintColor = UIColor.white
                self.viewDrafts.isUserInteractionEnabled = true
            } else {
                self.iconDrafts.image = #imageLiteral(resourceName: "icon_edit")
                self.iconDrafts.tintColor = UIColor.gray
                self.viewDrafts.isUserInteractionEnabled = false
            }
        }
    }
    
    func setFavorites(_ favorites: Project) {
        if favorites.compras.intValue > 0 || favorites.creditos.intValue > 0 {
            self.iconProjects.image = #imageLiteral(resourceName: "icon_myProjects")
            viewProjects.isUserInteractionEnabled = true
        } else {
            self.iconProjects.image = #imageLiteral(resourceName: "icon_myProjects_gray")
            viewProjects.isUserInteractionEnabled = false
        }
        if favorites.favoritos.intValue > 0 {
            self.iconFavorites.image = #imageLiteral(resourceName: "icon_favorite")
            viewFavorites.isUserInteractionEnabled = true
        } else {
            self.iconFavorites.image = #imageLiteral(resourceName: "icon_favorite_gray")
            viewFavorites.isUserInteractionEnabled = false
        }
    }
    
    func setCities(_ cities: [City]) {
        hideProgress()
        DispatchQueue.main.async {
            self.listCities = cities
            self.tfCity.pickerDataSource = self.listCities
            self.tfCity.isEnabled = true
        }
    }
    
    func setPurchasesAndCredits(_ purchasesAndCredits: PurchasesAndCredits) {
        hideProgress()
        DispatchQueue.main.async {
            let viewMyProjects: ViewMyProjectsViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.VIEW_MY_PROJECTS_VIEW_CONTROLLER) as! ViewMyProjectsViewController
            if (purchasesAndCredits.compras.count == 1 && purchasesAndCredits.creditos.count == 0) || ((purchasesAndCredits.compras.count == 0 && purchasesAndCredits.creditos.count == 1)) {
                if purchasesAndCredits.compras.count == 1 {
                    viewMyProjects.project = purchasesAndCredits.compras.first!
                    viewMyProjects.isCredit = false
                } else {
                    viewMyProjects.project = purchasesAndCredits.creditos.first!
                    viewMyProjects.isCredit = true
                }
                self.hostViewController.pushViewController(viewMyProjects, animated: true)
            } else  {
                let myProjectsList: MyProjectsListViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.MY_PROJECTS_LIST_VIEW_CONTROLLER) as! MyProjectsListViewController
                myProjectsList.purchasesAndCredits = purchasesAndCredits
                self.hostViewController.pushViewController(myProjectsList, animated: true)
            }
        }
    }

}
