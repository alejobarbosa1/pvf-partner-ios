//
//  SearchProjectsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class SearchProjectsBL: BaseBL {
	
	// MARK: Properties
    fileprivate var restUser = RestUser()
    fileprivate var restProject = RestProject()
    fileprivate var restCommon = RestCommon()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ISearchProjectsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ISearchProjectsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ISearchProjectsListener {
        return listener as! ISearchProjectsListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .FAVORITE_PROJECTS:
            let array = (response as! ArrayResponse).objectsArray
            let arrayFavorite: [Project] = Project.fromJsonArray(array)
            let favorites = arrayFavorite.first!
            getListener().setFavorites(favorites)
            break
        case .CITIES:
            let array = (response as! ArrayResponse).objectsArray
            let cities: [City] = City.fromJsonArray(array)
            getListener().setCities(cities)
            break
        case .PURCHASES_AND_CREDITS:
            let array = (response as! ArrayResponse).objectsArray
            let purchasesAndCredits: [PurchasesAndCredits] = PurchasesAndCredits.fromJsonArray(array)
            if purchasesAndCredits.first!.compras != nil {
                for project in purchasesAndCredits.first!.compras {
                    DbManager.insertUser(ChatUser(id: project.builder_iduser, rolId: Constants.Rols.BUILDER, phone: project.builder_phone))
                }
            }
            if purchasesAndCredits.first!.creditos != nil {
                for project in purchasesAndCredits.first!.creditos {
                    DbManager.insertUser(ChatUser(id: project.builder_iduser, rolId: Constants.Rols.BUILDER, phone: project.builder_phone))
                }
            }
            getListener().setPurchasesAndCredits(purchasesAndCredits.first!)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension SearchProjectsBL : ISearchProjectsBL {
    
    // MARK: Listeners
    
    func getDrafts() {
        var projectsDb = [ProjectDb]()
        projectsDb = DbManager.getInfoMainProject()!
        getListener().setDrafts(projectsDb)
    }
    
    func getFavorites() {
        let user = restUser.getCurrentUser()
        restProject.getFavorites(user!, listener: self)
    }
    
    func getCities(byCountry country: Country) {
        restUser.getCities(country, listener: self)
    }
    
    func getPurchasesAndCredits() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        restProject.getPurchasesAndCredits(user: user, listener: self)
    }
    
}
