//
//  ISearchProjectsView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: View.

protocol ISearchProjectsView: IBaseView {
    
    func setDrafts(_ projectsDb: [ProjectDb])
    
    func setFavorites(_ favorites: Project)
    
    func setCities(_ cities: [City])
    
    func setPurchasesAndCredits(_ purchasesAndCredits: PurchasesAndCredits)
}
