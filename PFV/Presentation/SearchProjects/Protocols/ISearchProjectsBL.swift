//
//  ISearchProjectsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol ISearchProjectsBL: IBaseBL {
    
    func getDrafts()
    
    func getFavorites()
    
    func getCities(byCountry country: Country)
    
    func getPurchasesAndCredits()
    
}
