//
//  PropertyTypeCellTableViewCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 11/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

protocol PropertyTypeCellDelegate {
    func selectModel(_ model: Model)
    
    func onScrollImageClick(_ scrollImage: ImageSlideshow)
}

class PropertyTypeCell: UITableViewCell {

    @IBOutlet weak var scrollImage: ImageSlideshow!
    @IBOutlet weak var lblRoomsNumber: UILabel!
    @IBOutlet weak var lblBathsNumber: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var lblModelName: UILabel!
    @IBOutlet weak var lblModelPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var viewPoint: UIView!
    @IBOutlet weak var btSelectType: UIButton!
    @IBOutlet weak var viewContentWhite: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var heightViewDescription: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
    fileprivate let numberFormatter = NumberFormatter()
    var navigationDelegate: PropertyTypeCellDelegate!
    var heightCell: CGFloat!
    var model = Model()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setCell(_ model: Model) {
        
    }
    
    func setModelCell(_ model: Model) {
        var arrayImages = [InputSource]()
        if model.photo == nil {
            arrayImages.append(ImageSource(image: #imageLiteral(resourceName: "default_image")))
        } else {
            for image in model.photo {
                arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(image.photo!)")!)
            }
        }
        scrollImage.setImageInputs(arrayImages)
        scrollImage.contentScaleMode = .scaleAspectFill
        scrollImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PropertyTypeCell.didTap)))
        self.viewMain.layer.cornerRadius = self.viewMain.frame.height / 30
        self.viewContentWhite.layer.cornerRadius = self.viewContentWhite.frame.height / 20
        self.viewPoint.layer.cornerRadius = self.viewPoint.frame.height / 2
        self.btSelectType.layer.cornerRadius = self.btSelectType.frame.height / 10
        self.lblRoomsNumber.text = model.hab
        self.lblBathsNumber.text = model.banhos.description
        self.lblArea.text = model.area.description
        self.lblModelName.text = model.nameModel
        if model.cant_p != nil {
            self.lblParking.text = model.cant_p
        } else {
            self.lblParking.text = "0"
        }
        self.lblModelPrice.text = "\("$ ")\(model.piso.first!.precio!)\(" COP")"
        self.lblDescription.text = model.desc
        self.layoutIfNeeded()
        self.heightViewDescription.constant = CGFloat(100 + self.lblDescription.frame.height)
        self.heightCell =  CGFloat(self.scrollImage.frame.height + self.viewInfo.frame.height + self.heightViewDescription.constant + self.btSelectType.frame.height + 90)
        self.viewContentWhite.layoutIfNeeded()
        self.model = model
    }
    
    @objc func didTap() {
        navigationDelegate.onScrollImageClick(self.scrollImage)
    }
    
    @IBAction func onSelectTypeClick(_ sender: Any) {
        navigationDelegate.selectModel(self.model)
    }
    
}
