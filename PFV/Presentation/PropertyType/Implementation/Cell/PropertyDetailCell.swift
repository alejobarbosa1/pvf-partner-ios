//
//  PropertyDetailCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 18/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class PropertyDetailCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imageBuilder: UIImageView!
    @IBOutlet weak var scrollImage: ImageSlideshow!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblProjectPrice: UILabel!
    @IBOutlet weak var buttonFavorite: UIButton!
    fileprivate let numberFormatter = NumberFormatter()
    fileprivate var delegate: PropertyDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setProjectCell(_ project: Project, delegate: PropertyDelegate) {
        self.delegate = delegate
        var arrayImages = [InputSource]()
        if project.logoproj == nil {
            arrayImages.append(ImageSource(image: #imageLiteral(resourceName: "default_image")))
        } else {
            arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(project.logoproj!)")!)
            
        }
        scrollImage.setImageInputs(arrayImages)
        scrollImage.contentScaleMode = .scaleAspectFill
        scrollImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PropertyDetailCell.didTap)))
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        imageBuilder.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(project.logo_builder!)"))
        lblProjectName.text = project.nameproj
        lblProjectPrice.text = numberFormatter.string(from: project.basic_price)
        DispatchQueue.main.async {
            self.viewContent.layoutIfNeeded()
            self.viewContent.layer.cornerRadius = self.viewContent.frame.height / 30
            self.viewContent.layer.masksToBounds = true
            //            self.imgProject.image = self.imgProject.image?.crop(rect: CGRect(x: 0, y: 0, width: 1024, height: 768))
            
        }
    }
    
    @objc func didTap() {
        delegate.onScrollImageClick(self.scrollImage)
    }

    @IBAction func onFavoriteClick(_ sender: Any) {
        delegate.clickFavorite()
    }
}

protocol PropertyDelegate {
    
    func clickFavorite()
    
    func onScrollImageClick(_ scrollImage: ImageSlideshow)
}
