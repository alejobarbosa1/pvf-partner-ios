//
//  PropertyTypePresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class PropertyTypePresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IPropertyTypeBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IPropertyTypeView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IPropertyTypeView) {
        super.init(baseView: view)
        self.bl = PropertyTypeBL(listener: self)
    }
    
    fileprivate func getView() -> IPropertyTypeView {
        return view as! IPropertyTypeView
    }
    
    fileprivate func getBL() -> IPropertyTypeBL {
        return bl as! IPropertyTypeBL
    }
}

// MARK: Presenter
extension PropertyTypePresenter: IPropertyTypePresenter {

      // MARK: Actions
    
    func setFavorite(_ project: Project) {
        getBL().setFavorite(project)
    }

    func removeFavorite(_ project: Project) {
        getBL().removeFavorite(project)
    }
    
}

// MARK: Listener
extension PropertyTypePresenter: IPropertyTypeListener {

      // MARK: Listeners
    
}

