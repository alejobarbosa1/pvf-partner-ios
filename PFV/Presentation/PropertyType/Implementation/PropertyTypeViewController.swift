//
//  PropertyTypeViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
// MARK: Base
class PropertyTypeViewController: BaseViewController {
  
  	// MARK: Properties

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgFooter: UIImageView!
    fileprivate let numberFormatter = NumberFormatter()
    var project: Project!
    var arrayModels = [Model]()
    var arrayCells = [PropertyTypeCell]()
    var heightCells = [Int]()
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IPropertyTypePresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IPropertyTypePresenter {
        return presenter as! IPropertyTypePresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PropertyTypePresenter(view: self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        prepareCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.PROPERTY_TYPE)
    }
    
    func prepareCells() {
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        for model in arrayModels {
            let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.PROPERTY_TYPE_CELL) as! PropertyTypeCell
            newCell.setModelCell(model)
            newCell.selectionStyle = .none
            newCell.navigationDelegate = self
            arrayCells.append(newCell)
        }
    }
}

// MARK: View
extension PropertyTypeViewController : IPropertyTypeView {
  
}

extension PropertyTypeViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCells.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        self.imgFooter.isHidden = false
        if indexPath.row == 0 {
            let cell = project.favorite == 0 ?tableView.dequeueReusableCell(withIdentifier: Constants.Cells.PROPERTY_DETAIL_CELL) as! PropertyDetailCell: tableView.dequeueReusableCell(withIdentifier: Constants.Cells.PROPERTY_DETAIL_CELL_2) as! PropertyDetailCell
            cell.setProjectCell(project, delegate: self)
            cell.selectionStyle = .none
            return cell
        } else {
            return arrayCells[indexPath.row - 1]
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 380
        } else {
            return arrayCells[indexPath.row - 1].heightCell
        }
    }

}

extension PropertyTypeViewController : PropertyTypeCellDelegate {
    
    func selectModel(_ model: Model) {
        let propertyDetail: PropertyDetailViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_DETAIL_VIEW_CONTROLLER) as! PropertyDetailViewController
        propertyDetail.project = self.project
        propertyDetail.modelSelected = model
        hostViewController.pushViewController(propertyDetail, animated: true)
    }
    
    func onScrollImageClick(_ scrollImage: ImageSlideshow) {
        scrollImage.presentFullScreenController(from: self)
    }
    
}

extension PropertyTypeViewController: PropertyDelegate{
    func clickFavorite() {
        if self.project.favorite == 0 {
            self.project.favorite = 1
            DispatchQueue.global().async {
                self.getPresenter().setFavorite(self.project)
            }
        } else {
            self.project.favorite = 0
            DispatchQueue.global().async {
                self.getPresenter().removeFavorite(self.project)
            }
        }
        self.tableView.reloadData()
    }
}
