//
//  PropertyTypeBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class PropertyTypeBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restProject = RestProject()
    fileprivate var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IPropertyTypeListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IPropertyTypeListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IPropertyTypeListener {
        return listener as! IPropertyTypeListener
    }
    
    // MARK: Actions
    
}

// MARK: BL
extension PropertyTypeBL : IPropertyTypeBL {
    
    // MARK: Listeners
    
    func setFavorite(_ project: Project) {
        project.iduser = restUser.getCurrentUser()?.iduser
        restProject.setFavorite(project, listener: self)
    }
    
    func removeFavorite(_ project: Project) {
        project.iduser = restUser.getCurrentUser()!.iduser
        restProject.removeFavorite(project, listener: self)
    }
    
}
