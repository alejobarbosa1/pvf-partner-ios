//
//  PropertyDetailBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class PropertyDetailBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restProject = RestProject()
    fileprivate var restUser = RestUser()
    fileprivate var restCommon = RestCommon()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IPropertyDetailListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IPropertyDetailListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IPropertyDetailListener {
        return listener as! IPropertyDetailListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .PICKER_VIEW_DOCUMENT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let documentList: [DocumentType] = DocumentType.fromJsonArray(array)
            getListener().setDocumentList(documentList)
            break
        case .FINANCING_LIST:
            let array = (response as! ArrayResponse).objectsArray
            let financinList: [Financing] = Financing.fromJsonArray(array)
            getListener().setFinancingList(financinList)
            break
        case .CUSTOMER_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let customerType: [CustomerType] = CustomerType.fromJsonArray(array)
            getListener().setCustomerType(customerType)
            break
        case .EDUCATIONAL_LEVEL:
            let array = (response as! ArrayResponse).objectsArray
            let educationalLevel: [EducationalLevel] = EducationalLevel.fromJsonArray(array)
            getListener().setEducationalLevel(educationalLevel)
            break
        case .CONTRACT_TYPE:
            let array = (response as! ArrayResponse).objectsArray
            let contractType: [ContractType] = ContractType.fromJsonArray(array)
            getListener().setContractType(contractType)
            break
        case .APPLY_TO_PURCHASE:
            getListener().onSuccessPurchase()
            break
        case .APPLY_TO_PURCHASE_CREDIT:
            getListener().onSuccessPurchase()
            break
        default:
            break
        }
    }
    
    override func onFailedPending(_ response: Response, tag: Constants.RepositoriesTags) {
        if tag == Constants.RepositoriesTags.APPLY_TO_PURCHASE_CREDIT || tag == Constants.RepositoriesTags.APPLY_TO_PURCHASE {
            getListener().onFailedPurchase(response.ResponseMessage!)
        }
    }
}

// MARK: BL
extension PropertyDetailBL : IPropertyDetailBL {
    
    // MARK: Listeners
    
    func getDocumentTypes() {
        restCommon.getDocumentTypes(self)
    }
    
    func getFinancingList() {
        restProject.getFinancingList(self)
    }
    
    func getCustomerType() {
        restUser.getCustomerType(self)
    }
    
    func getEducationalLevel() {
        restUser.getEducationalList(self)
    }
    
    func getContractType() {
        restUser.getContractTypeList(self)
    }
    
    func applyToPurchase(_ purchase: Purchase, isSecondHolder: Bool, isCredit: Bool) {
        purchase.iduser = restUser.getCurrentUser()!.iduser
        
        if isSecondHolder{
            if purchase.full_name_per_2 == nil  {
                getListener().missingFullName()
                return
            }
            if purchase.borndate_per_2 == nil {
                getListener().missingBornDate()
                return
            }
            if purchase.dateexped_per_2 == nil {
                getListener().missingExpirationDate()
                return
            }
            if purchase.leveleducation_per_2 == nil {
                getListener().missingEducationalLevel()
                return
            }
            if purchase.civilstatus_per_2 == nil {
                getListener().missingCivilStatus()
                return
            }
            if purchase.act_lab_per_2 == nil {
                getListener().missingWorkActivity()
                return
            }
            if purchase.type_cont_per_2 == nil {
                getListener().missingContractType()
                return
            }
            if purchase.email_per_2 == nil {
                getListener().missingEmail()
                return
            }
            if purchase.income_per_2 == nil {
                getListener().missingSalary()
                return
            }
            if purchase.phoneper_per_2 == nil {
                getListener().missingPhoneNumber()
                return
            }
            if purchase.doctype_per_2 == nil {
                getListener().missingDocumentType()
                return
            }
            if purchase.numberidenf_per_2 == nil {
                getListener().missingDocumentNumber()
                return
            }
        }
        if isCredit {
            restProject.applyToPurchaseCredit(purchase, listener: self)
        } else {
            restProject.applyToPurchase(purchase, listener: self)
        }
        
    }
    
    func saveProject(_ projectDb: ProjectDb) {
        DbManager.insertProject(projectDb)
        getListener().onProjectSaved()
    }
    
    func deleteProject(byId id: NSNumber) {
        DbManager.deleteProject(withId: id)
        getListener().onProjectDeleted()
    }
    
}
