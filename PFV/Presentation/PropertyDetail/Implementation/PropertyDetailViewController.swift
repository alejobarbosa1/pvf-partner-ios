//
//  PropertyDetailViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import Kingfisher

// MARK: Base
class PropertyDetailViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var imgBuilderLogo: UIImageView!
    @IBOutlet weak var lblBuilderName: UILabel!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblProjectPrice: UILabel!
    @IBOutlet weak var lblRoomsNumber: UILabel!
    @IBOutlet weak var lblBathsNumber: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDeliveryTime: UILabel!
    @IBOutlet weak var lblPropertyType: UILabel!
    @IBOutlet weak var lblFlatPrice: UILabel!
    @IBOutlet weak var lblFlatNumber: UILabel!
    @IBOutlet weak var tfFinancingType: PickerTextField!
    @IBOutlet weak var tfFinancingInteres: PickerTextField!
    @IBOutlet weak var lblFinancingPrice: UILabel!
    @IBOutlet weak var tfYears: PickerTextField!
    @IBOutlet weak var lblPricePerMonth: UILabel!
    @IBOutlet weak var tfInteres: PickerTextField!
    @IBOutlet weak var lblInteresPrice: UILabel!
    @IBOutlet weak var arrowModel: UIImageView!
    @IBOutlet var scrollImage: ImageSlideshow!
    
    // Corner Design
    @IBOutlet weak var viewBuilder: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var viewUbication: UIView!
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var viewPropertyType: UIView!
    @IBOutlet weak var viewFlat: UIView!
    @IBOutlet weak var viewFinancingType: UIView!
    @IBOutlet weak var viewFinancingPrice: UIView!
    @IBOutlet weak var viewInteresRate: UIView!
    @IBOutlet weak var viewInteresPrice: UIView!
    @IBOutlet weak var btApplyToPurchase: UIButton!
    @IBOutlet weak var viewAddSecondHolder: UIView!
    @IBOutlet weak var viewAddSecondHolderYellow: UIView!
    @IBOutlet weak var lblAddSecondHolder: UILabel!
    @IBOutlet weak var btSaveDraft: UIButton!
    
    @IBOutlet weak var higthSuperView: NSLayoutConstraint!
    @IBOutlet weak var topViewFinancingPrice: NSLayoutConstraint!
    
    // Second Holder
    @IBOutlet weak var viewSecondHolder: UIView!
    @IBOutlet weak var topViewSecondHolder: NSLayoutConstraint!
    @IBOutlet weak var tfNameSH: UITextField!
    @IBOutlet weak var tfDocumentTypeSH: PickerTextField!
    @IBOutlet weak var tfDocumentNumberSH: UITextField!
    @IBOutlet weak var tfEmailSH: UITextField!
    @IBOutlet weak var tfCellPhoneSH: UITextField!
    @IBOutlet weak var tfCustomerTypeSH: PickerTextField!
    @IBOutlet weak var tfBornDateSH: DatePickerTextField!
    @IBOutlet weak var tfExpeditionDateSH: DatePickerTextField!
    @IBOutlet weak var tfEducationalLevelSH: PickerTextField!
    @IBOutlet weak var tfCivilStatusSH: PickerTextField!
    @IBOutlet weak var tfWorkActivitySH: PickerTextField!
    @IBOutlet weak var tfAntiquityYearsSH: PickerTextField!
    @IBOutlet weak var tfAntiquityMonthsSH: PickerTextField!
    @IBOutlet weak var tfContractTypeSH: PickerTextField!
    @IBOutlet weak var tfSalarySH: UITextField!
    
    // PickerDataSource
    fileprivate var financingTypeList: [Financing] = []
    fileprivate var interestRateList: [InterestRate] = []
    fileprivate var financingInterestList: [FinancingInterest] = []
    fileprivate var anualFeesList: [AnualFees] = []
    
    fileprivate var listCountries: [Country] = []
    fileprivate var listCities: [City] = []
    fileprivate var listCustomerTyper: [CustomerType] = []
    fileprivate var listEducationalLevel: [EducationalLevel] = []
    fileprivate var listCivilStatus: [CivilStatus] = []
    fileprivate var listWorkActivity: [WorkActivity] = []
    fileprivate var listContractType: [ContractType] = []
    fileprivate var listYear = [Year]()
    fileprivate var listMonth = [Month]()
    fileprivate var listDocuments = [DocumentType]()
    
    fileprivate var byFactor: Int!
    fileprivate let numberFormatter = NumberFormatter()
    fileprivate var financingPrice: Float?
    fileprivate var termInYearsPerMonth: Float?
    fileprivate var interesPricePerMonth: Float?
    fileprivate var isFavorite: Bool!
    fileprivate let pricePerMonth: Float! = nil
    fileprivate let interesPrice: Float! = nil
    var projectPVF: ProjectPVF!
    var creditPVF: CreditPVF!
    var isProjectSaved = false
    var projectDb = ProjectDb()
    var project = Project()
    var modelSelected = Model()
    var flatSelected = Flat()
    var isFlatSelected = false
    var isSecondHolder = false
    var isCredit = false
    var isShowingInfo = false
    
  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IPropertyDetailPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IPropertyDetailPresenter {
        return presenter as! IPropertyDetailPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PropertyDetailPresenter(view: self)
        applyCorners()
        scrollImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PropertyDetailViewController.didTap)))
        if isCredit {
            if creditPVF.lat_log != nil {
                viewUbication.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PropertyDetailViewController.onClickAddress)))
            }
            self.higthSuperView.constant = 1200
            self.topViewFinancingPrice.constant = 10
            self.viewFinancingPrice.isHidden = false
            self.viewInteresRate.isHidden = false
            self.viewInteresPrice.isHidden = true
        } else {
            if projectPVF.lat_log != nil {
                viewUbication.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PropertyDetailViewController.onClickAddress)))
            }
            self.higthSuperView.constant = 1010
        }
        self.tfFinancingType.isEnabled = false
        self.viewAddSecondHolder.isHidden = true
        self.lblAddSecondHolder.isHidden = true
        self.btApplyToPurchase.isHidden = true
        self.btSaveDraft.isHidden = true
        self.arrowModel.isHidden = true
        setData()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.PROPERTY_DETAIL)
    }
    
    func setProjectSavedProperties(){
        
    }
    
    @objc func didTap() {
        scrollImage.presentFullScreenController(from: self)
    }
    
    func applyCorners(){
        setCorner(viewBuilder.frame.height / 10,
                  views: viewBuilder,
                  viewUbication,
                  viewTime,
                  viewPropertyType,
                  viewFlat,
                  viewFinancingType,
                  viewFinancingPrice,
                  viewInteresRate,
                  viewInteresPrice)
        setCorner(viewInfo.frame.height / 30 , views: viewInfo)
        setCorner(btApplyToPurchase.frame.height / 10, views: btApplyToPurchase, btSaveDraft)
        setCustomStyle(borderColor: Constants.Colors.GRAY, borderWidth: 4,
                       cornerRadius: viewAddSecondHolder.frame.height / 2,
                       views: viewAddSecondHolder,
                       viewAddSecondHolderYellow)
    }
    
    func setData() {
        var arrayImages = [InputSource]()
        if isCredit {
            if creditPVF.logoproj == nil {
                arrayImages.append(ImageSource(image: #imageLiteral(resourceName: "default_image")))
            } else {
                arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(creditPVF.logoproj!)")!)
            }
            if (creditPVF.photo != nil) && (!creditPVF.photo.isEmpty){
                for image in creditPVF.photo {
                    arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(image.photo!)")!)
                }
            }
            scrollImage.setImageInputs(arrayImages)
            scrollImage.contentScaleMode = .scaleAspectFill
            if creditPVF.logo_builder != nil {
                imgBuilderLogo.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(creditPVF.logo_builder!)"))
            }
            lblBuilderName.text = creditPVF.namebuilder
            lblProjectName.text = creditPVF.nameproj
            tfFinancingType.text = creditPVF.financing_name
            tfFinancingInteres.text = "\(creditPVF.porcen_credict!)\(Constants.Commons.INTEREST_SIGN)"
            let years = creditPVF.year_pay.intValue / 12
            if years > 1 {
                tfYears.text = "\(years.description)\(Constants.Commons.YEARS)"
            } else {
                tfYears.text = "\(years.description)\(Constants.Commons.YEAR)"
            }
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let financingPrice = Float(creditPVF.price_hou.intValue) * (Float(creditPVF.porcen_credict.intValue) / 100)
            let number = numberFormatter.string(from: NSNumber(value: financingPrice))
            lblFinancingPrice.text = number! + Constants.Commons.COP
            let termInYearsPerMonth = financingPrice / (Float(years * 12))
            let number2 = numberFormatter.string(from: NSNumber(value: termInYearsPerMonth))
            lblPricePerMonth.text = number2! + Constants.Commons.COP_MONTH
            let deliveryTime = creditPVF.enddate.toDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            lblDeliveryTime.text = deliveryTime!.toString(withFormat: Constants.Formats.SIMPLE_DATE)
            lblPropertyType.text = creditPVF.nameModel
            lblFlatPrice.text = numberFormatter.string(from: creditPVF.price_hou)
            lblProjectPrice.text = numberFormatter.string(from: creditPVF.price_hou)
            lblFlatNumber.text = creditPVF.model.first!.numero
            lblAddress.text = creditPVF.direcction
            lblBathsNumber.text = creditPVF.baths
            lblRoomsNumber.text = creditPVF.rooms
            lblArea.text = creditPVF.area
            lblParking.text = creditPVF.parking
        } else {
            
            if projectPVF.logoproj == nil {
                arrayImages.append(ImageSource(image: #imageLiteral(resourceName: "default_image")))
            } else {
                arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(projectPVF.logoproj!)")!)
            }
            if (projectPVF.photo != nil) && (!projectPVF.photo.isEmpty){
                for image in projectPVF.photo {
                    arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(image.photo!)")!)
                }
            }
            scrollImage.setImageInputs(arrayImages)
            scrollImage.contentScaleMode = .scaleAspectFill
            if projectPVF.logo_builder != nil {
                imgBuilderLogo.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(projectPVF.logo_builder!)"))
            }
            lblBuilderName.text = projectPVF.namebuilder
            lblProjectName.text = projectPVF.nameproj
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let deliveryTime = projectPVF.enddate.toDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            lblDeliveryTime.text = deliveryTime!.toString(withFormat: Constants.Formats.SIMPLE_DATE)
            lblPropertyType.text = projectPVF.nameModel
            lblFlatPrice.text = numberFormatter.string(from: projectPVF.price_hou)
            lblFlatNumber.text = projectPVF.model_buy.first!.numero
            tfFinancingType.text = "Contado"
            lblProjectPrice.text = numberFormatter.string(from: projectPVF.price_hou)
            lblAddress.text = projectPVF.direcction
            lblBathsNumber.text = projectPVF.baths
            lblRoomsNumber.text = projectPVF.rooms
            lblArea.text = projectPVF.area
            lblParking.text = projectPVF.parking
            hideProgress()
            
        }
    }
    
    @objc func onClickAddress(){
        let mapViewController: MapViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.MAP_VIEW_CONTROLLER) as! MapViewController
        if isCredit {
            mapViewController.latitude = creditPVF.lat_log.lat
            mapViewController.longitude = creditPVF.lat_log.lng
            mapViewController.nameProject = creditPVF.nameproj
        } else {
            mapViewController.latitude = projectPVF.lat_log.lat
            mapViewController.longitude = projectPVF.lat_log.lng
            mapViewController.nameProject = projectPVF.nameproj
        }
        hostViewController.pushViewController(mapViewController, animated: true)
    }
    
    func onClickPropertyType(){
        let propertyTyperViewController: PropertyTypeViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_TYPE_VIEW_CONTROLLER) as! PropertyTypeViewController
        propertyTyperViewController.arrayModels = project.kindproj
        hostViewController.pushViewController(propertyTyperViewController, animated: true)
    }
    
    func onClickFlat(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.BUILDING_VIEW_CONTROLLER) as! BuildingViewController
        controller.project = self.project
        controller.modelSelected = self.modelSelected
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func onClickAddSecondHolder(){
        if viewAddSecondHolderYellow.isHidden {
            viewAddSecondHolderYellow.isHidden = false
            secondHolder()
            
        } else {
            viewAddSecondHolderYellow.isHidden = true
            secondHolder()
        }
    }
    
    func secondHolder(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                if !self.isCredit{
                    
                    if self.isSecondHolder {
                        self.higthSuperView.constant = 1220
                        self.topViewSecondHolder.constant = -940
                        self.viewSecondHolder.isHidden = true
                        self.isSecondHolder = false
                    } else {
                        self.higthSuperView.constant = 2180
                        self.topViewSecondHolder.constant = 20
                        self.viewSecondHolder.isHidden = false
                        self.isSecondHolder = true
                    }
                    
                } else {
                    
                    if self.isSecondHolder {
                        self.higthSuperView.constant = 1450
                        self.topViewSecondHolder.constant = -940
                        self.viewSecondHolder.isHidden = true
                        self.isSecondHolder = false
                    } else {
                        self.higthSuperView.constant = 2410
                        self.topViewSecondHolder.constant = 20
                        self.viewSecondHolder.isHidden = false
                        self.isSecondHolder = true
                    }
                    
                }
                self.view.layoutIfNeeded()
            }, completion: nil )
        }
    }
    
    func showHideCreditInfo(){
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut, animations: {
                if !self.isCredit {
                    self.isCredit = true
                    self.topViewFinancingPrice.constant = 10
                    self.viewFinancingPrice.isHidden = false
                    self.viewInteresRate.isHidden = false
                    self.viewInteresPrice.isHidden = false
                    
                    if self.isSecondHolder {
                        self.higthSuperView.constant = 2420
                    } else {
                        self.higthSuperView.constant = 1450
                    }
                    
                } else {
                    self.isCredit = false
                    self.topViewFinancingPrice.constant = -225
                    self.viewFinancingPrice.isHidden = true
                    self.viewInteresRate.isHidden = true
                    self.viewInteresPrice.isHidden = true
                    if self.isSecondHolder {
                        self.higthSuperView.constant = 2190
                    } else {
                        self.higthSuperView.constant = 1220
                    }
                }
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    func setSecondHolderData(){
        tfCustomerTypeSH.delegate = self
        tfCustomerTypeSH.pickerDataSource = listCustomerTyper
        
        tfBornDateSH.delegate = tfBornDateSH
        tfBornDateSH.datePickerTextFieldDelegate = self
        tfBornDateSH.dateFormatter = Constants.Formats.SIMPLE_DATE
        
        tfExpeditionDateSH.delegate = tfExpeditionDateSH
        tfExpeditionDateSH.datePickerTextFieldDelegate = self
        tfExpeditionDateSH.dateFormatter = Constants.Formats.SIMPLE_DATE
        
        tfEducationalLevelSH.delegate = self
        tfEducationalLevelSH.pickerDataSource = listEducationalLevel
        
        tfContractTypeSH.delegate = self
        tfContractTypeSH.pickerDataSource = listContractType
        
        tfAntiquityYearsSH.delegate = self
        for y in 1...40 {
            if y == 1{
                listYear.append(Year(y.description, yearToShow: y.description + Constants.Commons.YEAR))
            } else {
                listYear.append(Year(y.description, yearToShow: y.description + Constants.Commons.YEARS))
            }
        }
        tfAntiquityYearsSH.pickerDataSource = listYear
        
        tfAntiquityMonthsSH.delegate = self
        for m in 1...11 {
            if m == 1 {
                listMonth.append(Month(m.description, monthToShow: m.description + Constants.Commons.MONTH))
            } else {
                listMonth.append(Month(m.description, monthToShow: m.description + Constants.Commons.MONTHS))
            }
        }
        tfAntiquityMonthsSH.pickerDataSource = listMonth
        
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getDocumentTypes()
            self.getPresenter().getCustomerType()
            self.getPresenter().getEducationalLevel()
            self.getPresenter().getContractType()
        }
        setCivilStatus()
        setWorkActivity()
    }
    
    func setCivilStatus(){
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.SINGLE))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.FREE_UNION))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.MARRIED))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.DIVORCED))
        listCivilStatus.append(CivilStatus(Constants.CivilStatus.WIDOWER))
        tfCivilStatusSH.delegate = self
        tfCivilStatusSH.pickerDataSource = listCivilStatus
    }
    
    func setWorkActivity(){
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.EMPLOYEE))
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.INDEPENDENT))
        listWorkActivity.append(WorkActivity(Constants.WorkActivity.RETIRED))
        tfWorkActivitySH.delegate = self
        tfWorkActivitySH.pickerDataSource = listWorkActivity
    }
    
    override func onPickerOkClick(_ pickerModel: IBasePickerModel?, tag: Int) {
        super.onPickerOkClick(pickerModel, tag: tag)
        switch tag {
        case Constants.Tags.FINANCING_TYPE_TEXT_FIELD:
            if isFlatSelected {
                if  tfFinancingType.selectedItem?.getText() != "Contado" {
                    if !isCredit {
                        showHideCreditInfo()
                    }
                    financingTypeList = [Financing]()
                    financingInterestList = [FinancingInterest]()
                    anualFeesList = [AnualFees]()
                    lblFinancingPrice.text = "$ 0 COP"
                    lblPricePerMonth.text = "$ 0 COP"
                    lblInteresPrice.text = "$ 0 COP"
                    tfInteres.text = ""
                    tfFinancingInteres.text = ""
                    tfYears.text = ""
                    tfInteres.isEnabled = false
                    tfYears.isEnabled = false
                    btApplyToPurchase.isEnabled = false
                    let percentage = (pickerModel as! Financing).porcentage
                    if percentage!.intValue == 0 {
                        byFactor = -1
                    } else {
                        byFactor = 1
                    }
                    for i in stride(from: 1, to: (percentage!.intValue + 1), by: byFactor){
                        financingInterestList.append(FinancingInterest(i.description, interestToShow:   (i.description +  Constants.Commons.INTEREST_SIGN)))
                        tfFinancingInteres.delegate = self
                        tfFinancingInteres.pickerDataSource = financingInterestList
                    }
                    let dues = (pickerModel as! Financing).cuotas
                    if dues!.intValue == 0 {
                        byFactor = -1
                    } else {
                        byFactor = 1
                    }
                    let duesYear = dues!.intValue / 12
                    for i in stride(from: 1, to: (duesYear + 1), by: byFactor){
                        if i == 1 {
                            anualFeesList.append(AnualFees(i.description, feeToShow: i.description + Constants.Commons.YEAR))
                        } else {
                            anualFeesList.append(AnualFees(i.description, feeToShow: i.description +   Constants.Commons.YEARS))
                            tfYears.delegate = self
                            tfYears.pickerDataSource = anualFeesList
                        }
                    }
                } else {
                    btApplyToPurchase.isEnabled = true
                    if isCredit {
                        showHideCreditInfo()
                    }
                }
            } else {
                tfFinancingType.text = ""
                showAlert(alertType: .WARNING,
                          message: Constants.Dialogs.SELECT_APARTMENT,
                          hasCancelButton: false,
                          tag: Constants.Tags.DEFAULT_OK)
            }
            tfFinancingInteres.isEnabled = true
            break
        case Constants.Tags.PERCENTAGE_TEXT_FILD:
//            text!.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range:nil)
            self.financingPrice = Float(Int(self.flatSelected.precio.description)!) * (Float(tfFinancingInteres.selectedItem!.getCode())! / 100)
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = numberFormatter.string(from: NSNumber(value: financingPrice!))
            lblFinancingPrice.text = number! + Constants.Commons.COP
            tfYears.isEnabled = true
            break
        case Constants.Tags.YEARS_TEXT_FIEL:
            termInYearsPerMonth = financingPrice! / (Float(tfYears.selectedItem!.getCode())! * 12)
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = numberFormatter.string(from: NSNumber(value: termInYearsPerMonth!))
            lblPricePerMonth.text = number! + Constants.Commons.COP_MONTH
            tfInteres.isEnabled = true
            break
        case Constants.Tags.INTERES_TEXT_FIELD:
            interesPricePerMonth = (termInYearsPerMonth! * (Float(tfInteres.selectedItem!.getCode())! / 100)) + termInYearsPerMonth!
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = Constants.Formats.LOCALE
            let number = numberFormatter.string(from: NSNumber(value: interesPricePerMonth!))
            lblInteresPrice.text = number! + Constants.Commons.COP_MONTH
            btApplyToPurchase.isEnabled = true
            break
        default:
            break
        }
    }
    
    @IBAction func onApplyToPurchaseClick(_ sender: Any) {
        showProgress(withTitle: nil)
        let purchase = Purchase()
        purchase.idproj = self.project.idproj
        if isCredit {
            purchase.porcen_credict = NSNumber(value: Int((self.tfFinancingInteres.selectedItem?.getCode())!)!)
            purchase.year_pay = NSNumber(value: Int((self.tfYears.selectedItem?.getCode())!)! * 12)
            purchase.pay_month = NSNumber(value: self.financingPrice!)
        }
        purchase.id_financing = NSNumber(value: Int((self.tfFinancingType.selectedItem?.getCode())!)!)
        purchase.type_hou = self.modelSelected.type_inmu
        purchase.floor_num = self.flatSelected.piso
        purchase.price_hou = self.flatSelected.precio
        purchase.id_model = NSNumber(value: Int((self.modelSelected.id_model)!)!)
        purchase.numero = NSNumber(value: Int((self.flatSelected.numero)!)!)
        purchase.lugar = self.flatSelected.lugar
        if isSecondHolder {
            purchase.full_name_per_2 = self.tfNameSH.text
            let dayBornDate = self.tfBornDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.day, from: self.tfBornDateSH.selectedDate!))
            let monthBornDate = self.tfBornDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.month, from: self.tfBornDateSH.selectedDate!))
            let yearBornDate = self.tfBornDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.year, from: self.tfBornDateSH.selectedDate!))
            purchase.borndate_per_2 = "\(yearBornDate)\("-")\(monthBornDate)\("-")\(dayBornDate)"
            let dayExpeditionDate = self.tfExpeditionDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.day, from: self.tfExpeditionDateSH.selectedDate!))
            let monthExpeditionDate = self.tfExpeditionDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.month, from: self.tfExpeditionDateSH.selectedDate!))
            let yearExpeditionDate = self.tfExpeditionDateSH.selectedDate == nil ? "" : String(Calendar.current.component(.year, from: self.tfExpeditionDateSH.selectedDate!))
            purchase.dateexped_per_2 = "\(yearExpeditionDate)\("-")\(monthExpeditionDate)\("-")\(dayExpeditionDate)"
            purchase.leveleducation_per_2 = self.tfEducationalLevelSH.selectedItem?.getText()
            purchase.civilstatus_per_2 = self.tfCivilStatusSH.selectedItem?.getText()
            purchase.act_lab_per_2 = self.tfWorkActivitySH.selectedItem?.getText()
            if tfAntiquityYearsSH.text != nil && tfAntiquityMonthsSH.text != nil {
                purchase.old_lab_per_2 = NSNumber(value: Int(("\(String(describing: self.tfAntiquityYearsSH.text))\(",")\(String(describing: tfAntiquityMonthsSH.text))"))!)
            }
            purchase.type_cont_per_2 = self.tfContractTypeSH.selectedItem?.getText()
            purchase.email_per_2 = self.tfEmailSH.text
            purchase.income_per_2 = NSNumber(value: Int((self.tfSalarySH.text)!)!)
            purchase.phoneper_per_2 = self.tfCellPhoneSH.text
            purchase.doctype_per_2 = self.tfDocumentTypeSH.selectedItem?.getText()
            purchase.numberidenf_per_2 = NSNumber(value: Int((self.tfDocumentNumberSH.text)!)!)
        }
        DispatchQueue.global().async {
            self.getPresenter().applyToPurchase(purchase, isSecondHolder: self.isSecondHolder, isCredit: self.isCredit)
        }
    }
    
    @IBAction func onSaveDraftClick(_ sender: Any) {
        showProgress(withTitle: nil)
        let projectDb = ProjectDb()
        projectDb.projectId = self.project.idproj
        if isProjectSaved {
            DispatchQueue.global().async {
                self.getPresenter().deleteProject(byId: projectDb.projectId)
            }
            return
        } else {
            projectDb.projectLogo = self.project.logoproj
            projectDb.builderLogo = self.project.logo_builder
            projectDb.projectName = self.project.nameproj
            projectDb.basicPrice = self.project.basic_price
            projectDb.isFavorite = self.project.favorite
            projectDb.rooms = NSNumber(value: Int(self.modelSelected.hab)!)
            projectDb.bathrooms = self.modelSelected.banhos
            projectDb.area = self.modelSelected.area
            if self.project.parking != nil && self.project.parking.intValue > 0{
                projectDb.hasParking = 1
            } else {
                projectDb.hasParking = 0
            }
            projectDb.address = self.project.direcction
            if self.project.lat_log != nil {
                projectDb.latitude = self.project.lat_log.lat
                projectDb.longitude = self.project.lat_log.lng
            }
            projectDb.endDate = self.project.enddate
            projectDb.modelId = NSNumber(value: Int(self.modelSelected.id_model)!)
            projectDb.modelName = self.modelSelected.nameModel
            projectDb.modelStatus = self.modelSelected.tyep_pro
            projectDb.modelType = self.modelSelected.type_inmu
            projectDb.isFlatSelected = self.isFlatSelected as NSNumber
            if isFlatSelected {
                projectDb.flatNumber = NSNumber(value: Int(self.flatSelected.numero)!)
                projectDb.flatPlace = self.flatSelected.lugar
                projectDb.flatPrice = self.flatSelected.precio
                projectDb.flat = self.flatSelected.piso
            }
            projectDb.financingId = self.tfFinancingType.selectedItem?.getCode()
            projectDb.financingName = self.tfFinancingType.selectedItem?.getText()
            projectDb.isCredit = self.isCredit as NSNumber
            if isCredit {
                projectDb.financingInterest = self.tfFinancingInteres.selectedItem?.getCode()
                projectDb.financingValue = self.financingPrice as NSNumber?
                projectDb.termInYears = self.tfYears.selectedItem?.getCode()
                projectDb.monthlyValue = self.termInYearsPerMonth as NSNumber?
                projectDb.interestRate = self.tfInteres.selectedItem?.getCode()
                projectDb.interestRateValue = self.interesPricePerMonth as NSNumber?
            }
            projectDb.isSecondHolder = self.isSecondHolder as NSNumber!
            if isSecondHolder {
                projectDb.fullNameSH = self.tfNameSH.text
                projectDb.documentTypeSH = (self.tfDocumentTypeSH.selectedItem as? DocumentType)?.initialdoctype
                projectDb.docmuentNumberSH = self.tfDocumentNumberSH.text
                projectDb.emailSH = self.tfEmailSH.text
                projectDb.cellPhoneSH = self.tfCellPhoneSH.text
                projectDb.customerTypeSH = self.tfCustomerTypeSH.selectedItem?.getText()
                projectDb.bornDateSH = self.tfBornDateSH.selectedDate?.timeIntervalSince1970 as NSNumber?
                projectDb.expeditionDateSH = self.tfExpeditionDateSH.selectedDate?.timeIntervalSince1970 as NSNumber?
                projectDb.educationalLevelSH = self.tfEducationalLevelSH.selectedItem?.getText()
                projectDb.civilStatusSH = self.tfCivilStatusSH.selectedItem?.getText()
                projectDb.workActivitySH = self.tfWorkActivitySH.selectedItem?.getText()
                projectDb.antiquityYearsSH = self.tfAntiquityYearsSH.selectedItem?.getCode()
                projectDb.antiquityMonthsSH = self.tfAntiquityMonthsSH.selectedItem?.getCode()
                projectDb.contractTypeSH = self.tfContractTypeSH.selectedItem?.getText()
                projectDb.salarySH = self.tfSalarySH.text
            }
            DispatchQueue.global().async {
                self.getPresenter().saveProject(projectDb)
            }
        }
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        switch alertTag {
        case Constants.Tags.SUCCESS_PURCHASE:
            let viewMyProjects: ViewMyProjectsViewController!
            viewMyProjects = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.VIEW_MY_PROJECTS_VIEW_CONTROLLER) as! ViewMyProjectsViewController
            self.navigationController!.viewControllers.insert(viewMyProjects, at: 2)
            self.navigationController!.popToViewController(viewMyProjects, animated: true)
            break
        default:
            break
        }
    }
    
}

// MARK: View
extension PropertyDetailViewController : IPropertyDetailView {
    
    func setDocumentList(_ documentList: [DocumentType]) {
        hideProgress()
        DispatchQueue.main.async {
            self.listDocuments = documentList
            self.tfDocumentTypeSH.pickerDataSource = self.listDocuments
        }
    }
    
    func setFinancingList(_ financinList: [Financing]) {
        hideProgress()
        DispatchQueue.main.async {
            self.financingTypeList = financinList
            self.tfFinancingType.pickerDataSource = self.financingTypeList
        }
    }
    
    func setCustomerType(_ customerType: [CustomerType]) {
        hideProgress()
        self.tfCustomerTypeSH.isEnabled = true
        DispatchQueue.main.async {
            self.listCustomerTyper = customerType
            self.tfCustomerTypeSH.pickerDataSource = self.listCustomerTyper
        }
    }
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel]) {
        hideProgress()
        self.tfEducationalLevelSH.isEnabled = true
        DispatchQueue.main.async {
            self.listEducationalLevel = educationalLevel
            self.tfEducationalLevelSH.pickerDataSource = self.listEducationalLevel
            //              self.tfEducationalLevel.selectedItem = self.listEducationalLevel[0]
        }
    }
    
    func setContractType(_ contractType: [ContractType]) {
        hideProgress()
        self.tfContractTypeSH.isEnabled = true
        DispatchQueue.main.async {
            self.listContractType = contractType
            self.tfContractTypeSH.pickerDataSource = self.listContractType
        }
    }
    
    func onSuccessPurchase() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.SUCCESS_PURCHASE,
                           hasCancelButton: false,
                           tag: Constants.Tags.SUCCESS_PURCHASE)
        }
    }
    
    func onFailedPurchase(_ responseMessage: String) {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .ERROR,
                      message: responseMessage,
                      hasCancelButton: false,
                      tag: Constants.Tags.DEFAULT_OK)
        }
    }
    
    
    
    func missingFullName() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfNameSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingBornDate() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfBornDateSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingExpirationDate() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfExpeditionDateSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingEducationalLevel() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfEducationalLevelSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingCivilStatus() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfCivilStatusSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingWorkActivity() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfWorkActivitySH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingContractType() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfContractTypeSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingEmail() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfEmailSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingSalary() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfSalarySH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingPhoneNumber() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfCellPhoneSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingDocumentType() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfDocumentTypeSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func missingDocumentNumber() {
        hideProgress()
        DispatchQueue.main.async {
            self.showPopover(self.tfDocumentNumberSH, message: Constants.Dialogs.FIELD_REQUIRED, icon: #imageLiteral(resourceName: "icon_warning"))
        }
    }
    
    func onProjectSaved() {
        hideProgress()
        DispatchQueue.main.async {
            self.isProjectSaved = true
            self.btSaveDraft.setTitle("ELIMINAR BORRADOR", for: .normal)
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.DRAFT_SAVED,
                           hasCancelButton: false,
                           tag: Constants.Tags.DEFAULT_OK)
        }
    }
    
    func onProjectDeleted() {
        hideProgress()
        DispatchQueue.main.async {
            self.isProjectSaved = false
            self.btSaveDraft.setTitle("GUARDAR BORRADOR", for: .normal)
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.DRAFT_DELETED,
                           hasCancelButton: false,
                           tag: Constants.Tags.DEFAULT_OK)
        }
    }
  
}

extension PropertyDetailViewController : DatePickerTextFieldDelegate {
    
    override func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    func onOkClick() {
    }
    
}

extension PropertyDetailViewController : UINavigationControllerDelegate {
}
