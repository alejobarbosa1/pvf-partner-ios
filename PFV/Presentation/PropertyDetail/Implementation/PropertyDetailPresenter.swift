//
//  PropertyDetailPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class PropertyDetailPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IPropertyDetailBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IPropertyDetailView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IPropertyDetailView) {
        super.init(baseView: view)
        self.bl = PropertyDetailBL(listener: self)
    }
    
    fileprivate func getView() -> IPropertyDetailView {
        return view as! IPropertyDetailView
    }
    
    fileprivate func getBL() -> IPropertyDetailBL {
        return bl as! IPropertyDetailBL
    }
}

// MARK: Presenter
extension PropertyDetailPresenter: IPropertyDetailPresenter {

      // MARK: Actions
    
    func getDocumentTypes() {
        getBL().getDocumentTypes()
    }
    
    func getFinancingList() {
        getBL().getFinancingList()
    }
    
    func getCustomerType() {
        getBL().getCustomerType()
    }
    
    func getEducationalLevel() {
        getBL().getEducationalLevel()
    }
    
    func getContractType() {
        getBL().getContractType()
    }
    
    func applyToPurchase(_ purchase: Purchase, isSecondHolder: Bool, isCredit: Bool) {
        getBL().applyToPurchase(purchase, isSecondHolder: isSecondHolder, isCredit: isCredit)
    }
    
    func saveProject(_ projectDb: ProjectDb) {
        getBL().saveProject(projectDb)
    }
    
    func deleteProject(byId id: NSNumber) {
        getBL().deleteProject(byId: id)
    }

}

// MARK: Listener
extension PropertyDetailPresenter: IPropertyDetailListener {

      // MARK: Listeners
    
    func setDocumentList(_ documentList: [DocumentType]) {
        getView().setDocumentList(documentList)
    }
    
    func setFinancingList(_ financinList: [Financing]) {
        getView().setFinancingList(financinList)
    }
    
    func setCustomerType(_ customerType: [CustomerType]) {
        getView().setCustomerType(customerType)
    }
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel]) {
        getView().setEducationalLevel(educationalLevel)
    }
    
    func setContractType(_ contractType: [ContractType]) {
        getView().setContractType(contractType)
    }
    
    func onSuccessPurchase() {
        getView().onSuccessPurchase()
    }
    
    func onFailedPurchase(_ responseMessage: String) {
        getView().onFailedPurchase(responseMessage)
    }
    
    func missingFullName() {
        getView().missingFullName()
    }
    
    func missingBornDate() {
        getView().missingBornDate()
    }
    
    func missingExpirationDate() {
        getView().missingExpirationDate()
    }
    
    func missingEducationalLevel() {
        getView().missingEducationalLevel()
    }
    
    func missingCivilStatus() {
        getView().missingCivilStatus()
    }
    
    func missingWorkActivity() {
        getView().missingWorkActivity()
    }
    
    func missingContractType() {
        getView().missingContractType()
    }
    
    func missingEmail() {
        getView().missingEmail()
    }
    
    func missingSalary() {
        getView().missingSalary()
    }
    
    func missingPhoneNumber() {
        getView().missingPhoneNumber()
    }
    
    func missingDocumentType() {
        getView().missingDocumentType()
    }
    
    func missingDocumentNumber() {
        getView().missingDocumentNumber()
    }
    
    func onProjectSaved() {
        getView().onProjectSaved()
    }
    
    func onProjectDeleted() {
        getView().onProjectDeleted()
    }
    
}

