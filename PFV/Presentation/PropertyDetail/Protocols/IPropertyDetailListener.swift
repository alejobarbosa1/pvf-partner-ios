//
//  IPropertyDetailListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IPropertyDetailListener: IBaseListener {
    
    func setDocumentList(_ documentList: [DocumentType])
    
    func setFinancingList(_ financinList: [Financing])
    
    func setCustomerType(_ customerType: [CustomerType])
    
    func setEducationalLevel(_ educationalLevel: [EducationalLevel])
    
    func setContractType(_ contractType: [ContractType])
    
    func onSuccessPurchase()
    
    func onFailedPurchase(_ responseMessage: String)
    
    func missingFullName()
    
    func missingBornDate()
    
    func missingExpirationDate()
    
    func missingEducationalLevel()
    
    func missingCivilStatus()
    
    func missingWorkActivity()
    
    func missingContractType()
    
    func missingEmail()
    
    func missingSalary()
    
    func missingPhoneNumber()
    
    func missingDocumentType()
    
    func missingDocumentNumber()
    
    func onProjectSaved()
    
    func onProjectDeleted()
  
}
