//
//  IPropertyDetailPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IPropertyDetailPresenter: IBasePresenter {
    
    func getDocumentTypes()
    
    func getFinancingList()
    
    func getCustomerType()
    
    func getEducationalLevel()
    
    func getContractType()
    
    func applyToPurchase(_ purchase: Purchase, isSecondHolder: Bool, isCredit: Bool)
    
    func saveProject(_ projectDb: ProjectDb)
    
    func deleteProject(byId id: NSNumber)
  
}
