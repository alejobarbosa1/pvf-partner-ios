//
//  IPropertyDetailBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IPropertyDetailBL: IBaseBL {
    
    func getDocumentTypes()
    
    func getFinancingList()
    
    func getCustomerType()
    
    func getEducationalLevel()
    
    func getContractType()
    
    func applyToPurchase(_ purchase: Purchase, isSecondHolder: Bool, isCredit: Bool)
    
    func saveProject(_ projectDb: ProjectDb)
    
    func deleteProject(byId id: NSNumber)
  
}
