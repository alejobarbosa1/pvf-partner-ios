//
//  ICreateTicketView.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: View.

protocol ICreateTicketView: IBaseView {
	
    func osSuccessTicket()
    
}
