//
//  CreateTicketBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class CreateTicketBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restSupport = RestSupport()
    fileprivate var restUser = RestUser()
    fileprivate var ticket = Ticket()
    fileprivate var newTicket = NewTicket()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ICreateTicketListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ICreateTicketListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ICreateTicketListener {
        return listener as! ICreateTicketListener
    }
    
    // MARK: Listeners
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        if tag == Constants.RepositoriesTags.SEND_TICKET {
            //Respuesta
            let array = (response as! ArrayResponse).objectsArray
            let arrayId: [Ticket] = Ticket.fromJsonArray(array)
            let id = arrayId.first!
            let date = NSNumber(value: NSDate().timeIntervalSince1970)
            
            // Crear ticket
            ticket.id = id.idchannel
            ticket.status = 1
            ticket.date = date
            ticket.viewed = Constants.ViewedStatus.VIEWED
            
            // Insertar el ticket a la base de datos interna
            DbManager.inserSupportTickets(ticket)
            
            // Llamar al Listener
            getListener().osSuccessTicket()
        }
    }
    
}

// MARK: BL
extension CreateTicketBL : ICreateTicketBL {
    
    // MARK: Actions
    
    func sendTicket(_ newTicket: NewTicket) {
        newTicket.iduser = restUser.getCurrentUser()!.iduser
        self.newTicket = newTicket
        ticket.message = newTicket.message
        ticket.title = newTicket.title
        restSupport.sendTicket(newTicket, listener: self)
    }
    
}
