//
//  CreateTicketViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class CreateTicketViewController: BaseViewController {

    // MARK: Outlets
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfMessage: UITextView!
    @IBOutlet weak var btSendTicket: UIButton!
  
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ICreateTicketPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ICreateTicketPresenter {
        return presenter as! ICreateTicketPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = CreateTicketPresenter(view: self)
        setCorner(btSendTicket.frame.height / 10, views: btSendTicket)
        tfTitle.delegate = self
        btSendTicket.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.GENERATE_TICKET)
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == tfTitle) {
            if range.location != 0 {
                btSendTicket.isEnabled = true
            } else {
                btSendTicket.isEnabled = false
            }
        }
        return true
    }

    // MARK: Actions
    
    @IBAction func onSendTicketClick(_ sender: Any){
        showProgress(withTitle: nil)
        let newTicket = NewTicket()
        let messageId = NSNumber(value: Date().millisecondsSince1970)
        newTicket.title = tfTitle.text
        newTicket.message = tfMessage.text
        newTicket.messageId = messageId
        DispatchQueue.global().async {
            self.getPresenter().sendTicket(newTicket)
        }
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        if alertTag == Constants.Tags.TICKET_SENT {
            hostViewController.onBackClick()
        }
    }
}

// MARK: View
extension CreateTicketViewController : ICreateTicketView {
    
    func osSuccessTicket() {
        hideProgress()
        DispatchQueue.main.async {
            self.showAlert(alertType: .DONE,
                           message: Constants.Dialogs.SUCCESSFUL_OPERATION,
                           hasCancelButton: false,
                           tag: Constants.Tags.TICKET_SENT)
        }
    }
    
}
