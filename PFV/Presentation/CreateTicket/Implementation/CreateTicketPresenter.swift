//
//  CreateTicketPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class CreateTicketPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ICreateTicketBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ICreateTicketView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ICreateTicketView) {
        super.init(baseView: view)
        self.bl = CreateTicketBL(listener: self)
    }
    
    fileprivate func getView() -> ICreateTicketView {
        return view as! ICreateTicketView
    }
    
    fileprivate func getBL() -> ICreateTicketBL {
        return bl as! ICreateTicketBL
    }
}

// MARK: Presenter
extension CreateTicketPresenter: ICreateTicketPresenter {

    // MARK: Actions
    
    func sendTicket(_ newTicket: NewTicket) {
        getBL().sendTicket(newTicket)
    }

}

// MARK: Listener
extension CreateTicketPresenter: ICreateTicketListener {

    // MARK: Listeners
    
    func osSuccessTicket() {
        getView().osSuccessTicket()
    }
    
}

