//
//  NewsViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class NewsViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var higthViewContent: NSLayoutConstraint!
    @IBOutlet weak var higthSuperView: NSLayoutConstraint!
    @IBOutlet weak var viewInf: UIView!
    @IBOutlet weak var lblUrl: UILabel!
    
    var news: News!
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? INewsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> INewsPresenter {
        return presenter as! INewsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = NewsPresenter(view: self)
        setCorner(viewContent.frame.height / 10, views: viewContent)
        lblUrl.attributedText = NSAttributedString(string: "www.pvf.com.co", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        lblUrl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NewsViewController.onLinkClick)))
        setData(news)
//        setFrame()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.NEWS)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if news.image != nil && !news.image.isEmpty {
            let num = ( lblTitle.frame.height + lblText.frame.height + imageNews.frame.height + 130)
            higthViewContent.constant = num
            print(num)
        } else {
            imageNews.isHidden = true
            let num1 = (80 + lblTitle.frame.height + lblText.frame.height )
            higthViewContent.constant = num1
            print(num1)
        }
        let heightScreen = (Constants.Sizes.SCREEN_SIZE.size.height - (self.hostViewController.navigationController!.navigationBar.frame.height + 20))
        if higthViewContent.constant + 75 + 130 > heightScreen {
            higthSuperView.constant = higthViewContent.constant + 215
            print(higthSuperView.constant)
        } else {
            higthSuperView.constant = heightScreen
        }
        
        //        self.view.setNeedsLayout()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func onLinkClick(){
        openUrl(urlStr: lblUrl.text)
    }
    
    func openUrl(urlStr:String!) {
        
        if let url = URL(string: "https://" + urlStr) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
    }
    
    func setFrame() {
        
    }
    
    func setData(_ news: News){
        let date = news.createdAt.toDate(withFormat: Constants.Formats.SERVER_OUT_DATE_EXTENDED)
        self.lblDate.text = date?.toString(withFormat: Constants.Formats.DATE_TO_CHAT)
        self.lblTitle.text = news.title
        self.lblText.text = news.body
        if news.image != nil {
            self.imageNews.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(news.image!)"))
        }
    }
}

// MARK: View
extension NewsViewController : INewsView {
    
  
}


