//
//  NewsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class NewsBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? INewsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: INewsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> INewsListener {
        return listener as! INewsListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension NewsBL : INewsBL {
    
    // MARK: Listeners
    
}
