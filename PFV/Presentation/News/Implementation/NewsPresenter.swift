//
//  NewsPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 4/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class NewsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? INewsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? INewsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: INewsView) {
        super.init(baseView: view)
        self.bl = NewsBL(listener: self)
    }
    
    fileprivate func getView() -> INewsView {
        return view as! INewsView
    }
    
    fileprivate func getBL() -> INewsBL {
        return bl as! INewsBL
    }
}

// MARK: Presenter
extension NewsPresenter: INewsPresenter {

      // MARK: Actions

}

// MARK: Listener
extension NewsPresenter: INewsListener {

      // MARK: Listeners
    
}

