//
//  IFilterPropertyListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IFilterPropertyListener: IBaseListener {
    
    func setProjectList(_ projectList: [Project])
    
    func onFailderProjectsList(_ responseMessage: String)
    
    func setFavoritesList(_ favoritesList: [Project])
    
    func setProjectsSaved(_ projectsDbList: [ProjectDb])
  
}
