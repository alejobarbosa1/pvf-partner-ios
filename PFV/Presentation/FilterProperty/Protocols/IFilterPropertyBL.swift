//
//  IFilterPropertyBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IFilterPropertyBL: IBaseBL {
    
    func filterSearch(_ filter: Filter)
    
    func getProjectList(_ project: Project)
    
    func setFavorite(_ projectFav: Project)
    
    func removeFavorite(_ projectFav: Project)
    
    func getFavoritesList()
    
    func getProjectsSaved()
  
}
