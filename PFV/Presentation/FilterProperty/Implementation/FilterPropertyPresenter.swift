//
//  FilterPropertyPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class FilterPropertyPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IFilterPropertyBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IFilterPropertyView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IFilterPropertyView) {
        super.init(baseView: view)
        self.bl = FilterPropertyBL(listener: self)
    }
    
    fileprivate func getView() -> IFilterPropertyView {
        return view as! IFilterPropertyView
    }
    
    fileprivate func getBL() -> IFilterPropertyBL {
        return bl as! IFilterPropertyBL
    }
}

// MARK: Presenter
extension FilterPropertyPresenter: IFilterPropertyPresenter {

      // MARK: Actions
    
    func getProjectList(_ project: Project) {
        getBL().getProjectList(project)
    }
    
    func filterSearch(_ filter: Filter) {
        getBL().filterSearch(filter)
    }
    
    func setFavorite(_ projectFav: Project) {
        getBL().setFavorite(projectFav)
    }
    
    func removeFavorite(_ projectFav: Project) {
        getBL().removeFavorite(projectFav)
    }
    
    func getFavoritesList() {
        getBL().getFavoritesList()
    }
    
    func getProjectsSaved() {
        getBL().getProjectsSaved()
    }

}

// MARK: Listener
extension FilterPropertyPresenter: IFilterPropertyListener {

      // MARK: Listeners
    
    func setProjectList(_ projectList: [Project]) {
        getView().setProjectList(projectList)
    }
    
    func onFailderProjectsList(_ responseMessage: String) {
        getView().onFailderProjectsList(responseMessage)
    }
    
    func setFavoritesList(_ favoritesList: [Project]) {
        getView().setFavoritesList(favoritesList)
    }
    
    func setProjectsSaved(_ projectsDbList: [ProjectDb]) {
        getView().setProjectsSaved(projectsDbList)
    }
    
}

