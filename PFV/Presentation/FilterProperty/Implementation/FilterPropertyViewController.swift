//
//  FilterPropertyViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class FilterPropertyViewController: BaseViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var tvProjects: UITableView!
    @IBOutlet weak var btFilterSearch: UIButton!
    @IBOutlet weak var btReload: UIButton!
    @IBOutlet weak var imgFooter: UIImageView!
    @IBOutlet weak var imgWaves: UIImageView!
    fileprivate let numberFormatter = NumberFormatter()
    var isProjectsSaved = false
    var page = 0
    var listProject = [Project]()
    let project = Project()
    var filter = Filter()
    var idcity: String!
    var isFilter: Bool! = false
    var isLoading = true
    var isShowingFavorites = false
    var listFavorites = [Project]()
    var projectsDbList = [ProjectDb]()
    
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IFilterPropertyPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IFilterPropertyPresenter {
        return presenter as! IFilterPropertyPresenter
    }
    
    // MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = FilterPropertyPresenter(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tvProjects.separatorStyle = .none
        self.tvProjects.delegate = self
        self.tvProjects.dataSource = self
        setCorner(btFilterSearch.frame.height / 10, views: btFilterSearch)
        setCorner(btReload.frame.height / 10, views: btReload)
        if !isShowingFavorites {
            if isFilter {
                showProgress(withTitle: nil)
                btReload.isHidden = false
                DispatchQueue.global().async {
                    self.getPresenter().filterSearch(self.filter)
                }
                return
            }
            if !isProjectsSaved{
                hostViewController.setTitle(title: Constants.Titles.FILTER_PROPERTY)
                showProgress(withTitle: nil)
                btReload.isHidden = true
                project.idcity = idcity
                project.start = NSNumber(value: page * 8)
                DispatchQueue.global().async {
                    self.getPresenter().getProjectList(self.project)
                }
                return
            }
            if isProjectsSaved{
                hostViewController.setTitle(title: Constants.Titles.DRAFTS)
                showProgress(withTitle: nil)
                btFilterSearch.isHidden = true
                DispatchQueue.global().async {
                    self.getPresenter().getProjectsSaved()
                }
                return
            }
        }
        if isShowingFavorites {
            hostViewController.setTitle(title: Constants.Titles.FAVORITES)
            showProgress(withTitle: nil)
            DispatchQueue.global().async {
                self.getPresenter().getFavoritesList()
            }
            btFilterSearch.isHidden = true
            return
        }
        
    }
    @IBAction func onFilterClick(_ sender: Any) {
        let filterSearchViewController: FilterSearchViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.FILTER_SEARCH_VIEW_CONTROLLER) as! FilterSearchViewController
        hostViewController.pushViewController(filterSearchViewController, animated: true)
    }
    @IBAction func onReloadClick(_ sender: Any) {
        btReload.isHidden = true
        project.idcity = idcity
        project.start = NSNumber(value: page * 8)
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getProjectList(self.project)
        }
    }
    
    override func onOkClick(_ alertTag: Int) {
        super.onOkClick(alertTag)
        if alertTag == Constants.Tags.FAILED {
            hostViewController.onBackClick()
        }
    }
    
}

// MARK: View
extension FilterPropertyViewController : IFilterPropertyView {
    
    func setProjectList(_ projectList: [Project]) {
        hideProgress()
        isLoading = true
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        for i in 0..<projectList.count {
            projectList[i].basic_price_string = numberFormatter.string(from: projectList[i].basic_price)
        }
        self.listProject = projectList
        tvProjects.reloadData()
    }
    
    func setProjectsSaved(_ projectsDbList: [ProjectDb]) {
        hideProgress()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        DispatchQueue.main.async {
            if projectsDbList.isEmpty {
                self.hostViewController.onBackClick()
                return
            }
            for i in 0..<projectsDbList.count {
                projectsDbList[i].basicPriceString = self.numberFormatter.string(from: projectsDbList[i].basicPrice)
            }
            self.projectsDbList = projectsDbList
            self.tvProjects.reloadData()
        }
    }
    
    func onFailderProjectsList(_ responseMessage: String) {
        hideProgress()
        if isLoading {
            DispatchQueue.main.async {
                self.showAlert(alertType: .ERROR,
                               message: responseMessage,
                               hasCancelButton: false,
                               tag: Constants.Tags.FAILED)
            }
        } else {
            isLoading = true
        }
        
    }
    
    func setFavoritesList(_ favoritesList: [Project]) {
        hideProgress()
        DispatchQueue.main.async {
            self.numberFormatter.numberStyle = .currency
            self.numberFormatter.locale = Constants.Formats.LOCALE
            for i in 0..<favoritesList.count {
                if let number = self.numberFormatter.string(from: favoritesList[i].basic_price!){
                    favoritesList[i].basic_price_string = number
                }
            }
            self.listFavorites = favoritesList
        }
        self.tvProjects.reloadData()
    }
    
}

extension FilterPropertyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isShowingFavorites {
            if !isProjectsSaved {
                return listProject.count
            } else {
                return projectsDbList.count
            }
        } else {
            return listFavorites.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if !isShowingFavorites {
            if !isProjectsSaved {
                if listProject.count <= 1 {
                    imgWaves.isHidden = false
                    imgFooter.isHidden = true
                } else {
                    imgFooter.isHidden = false
                    imgWaves.isHidden = true
                }
                imgFooter.isHidden = false
                let cell = listProject[indexPath.row].favorite == 0 ?tableView.dequeueReusableCell(withIdentifier: Constants.Cells.FILTER_PROPERTY_CELL) as! FilterPropertyCell: tableView.dequeueReusableCell(withIdentifier: Constants.Cells.FILTER_PROPERTY_CELL_2) as! FilterPropertyCell
                let floor = Floor(idproj: listProject[indexPath.row].idproj,
                                  logoproj: listProject[indexPath.row].logoproj,
                                  nameproj: listProject[indexPath.row].nameproj,
                                  basic_price: listProject[indexPath.row].basic_price_string,
                                  favorite: listProject[indexPath.row].favorite,
                                  logo_builder: listProject[indexPath.row].logo_builder)
                cell.setData(floor, delegate: self, position: indexPath.row)
                cell.selectionStyle = .none
                return cell
            } else {
                    if projectsDbList.count <= 1 {
                        imgWaves.isHidden = false
                        imgFooter.isHidden = true
                    } else {
                        imgFooter.isHidden = false
                        imgWaves.isHidden = true
                    }
                    let cell = projectsDbList[indexPath.row].isFavorite == 0 ?tableView.dequeueReusableCell(withIdentifier: Constants.Cells.FILTER_PROPERTY_CELL) as! FilterPropertyCell: tableView.dequeueReusableCell(withIdentifier: Constants.Cells.FILTER_PROPERTY_CELL_2) as! FilterPropertyCell
                    let floor = Floor(idproj: projectsDbList[indexPath.row].projectId,
                                      logoproj: projectsDbList[indexPath.row].projectLogo!,
                                      nameproj: projectsDbList[indexPath.row].projectName,
                                      basic_price: projectsDbList[indexPath.row].basicPriceString!,
                                      favorite: projectsDbList[indexPath.row].isFavorite,
                                      logo_builder: projectsDbList[indexPath.row].builderLogo!)
                    cell.setData(floor, delegate: self, position: indexPath.row)
                    cell.selectionStyle = .none
                    return cell
            }
        } else {
            if listFavorites.count <= 1 {
                imgWaves.isHidden = false
                imgFooter.isHidden = true
            } else {
                imgFooter.isHidden = false
                imgWaves.isHidden = true
            }
            let cell: FilterPropertyCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.FILTER_PROPERTY_CELL_2, for: indexPath) as! FilterPropertyCell
            let floor = Floor(idproj: listFavorites[indexPath.row].idproj,
                              logoproj: listFavorites[indexPath.row].logoproj,
                              nameproj: listFavorites[indexPath.row].nameproj,
                              basic_price: listFavorites[indexPath.row].basic_price_string,
                              favorite: 1,
                              logo_builder: listFavorites[indexPath.row].logo_builder)
            cell.setData(floor, delegate: self, position: indexPath.row)
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let propertyType: PropertyTypeViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_TYPE_VIEW_CONTROLLER) as! PropertyTypeViewController
        if !isShowingFavorites  && !isProjectsSaved{
            propertyType.project = listProject[indexPath.row]
            propertyType.arrayModels = listProject[indexPath.row].kindproj
            hostViewController.pushViewController(propertyType, animated: true)
            return
        }
        if isShowingFavorites {
            propertyType.project = listFavorites[indexPath.row]
            propertyType.arrayModels = listFavorites[indexPath.row].kindproj
            hostViewController.pushViewController(propertyType, animated: true)
            return
        }
        if isProjectsSaved {
            let propertyDetail: PropertyDetailViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_DETAIL_VIEW_CONTROLLER) as! PropertyDetailViewController
            propertyDetail.isProjectSaved = true
            propertyDetail.isFlatSelected = (projectsDbList[indexPath.row].isFlatSelected != 0)
            propertyDetail.isSecondHolder = (projectsDbList[indexPath.row].isSecondHolder != 0)
            propertyDetail.isCredit = (projectsDbList[indexPath.row].isCredit != 0)
            propertyDetail.projectDb = self.projectsDbList[indexPath.row]
//            hostViewController.pushViewController(propertyDetail, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 360
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //        print(scrollView.contentOffset.y)
        if scrollView.contentOffset.y > tvProjects.contentSize.height + 50 - tvProjects.frame.height  && tvProjects.contentSize.height >= tvProjects.frame.height && isLoading{
            if !isShowingFavorites  && !isProjectsSaved{
                isLoading = false
                self.page += 1
                if isFilter {
                    self.filter.start = NSNumber(value: self.page * 8)
                    DispatchQueue.global().async {
                        self.getPresenter().filterSearch(self.filter)
                    }
                } else {
                    self.project.idcity = self.idcity
                    self.project.start = NSNumber(value: self.page * 8)
                    DispatchQueue.global().async {
                        self.getPresenter().getProjectList(self.project)
                    }
                }
            }
        }
    }
    
}

extension FilterPropertyViewController : FilterPropertyDelegate {
    
    func clickFavorite(_ position: Int) {
        let projectFav = Project()
        if isShowingFavorites {
            projectFav.idproj = self.listFavorites[position].idproj
            DispatchQueue.global().async {
                self.getPresenter().removeFavorite(projectFav)
            }
            listFavorites.remove(at: position)
            if listFavorites.isEmpty {
                hostViewController.onBackClick()
            }
        } else {
            projectFav.idproj = self.listProject[position].idproj
            if self.listProject[position].favorite == 0 {
                self.listProject[position].favorite = 1
                DispatchQueue.global().async {
                    self.getPresenter().setFavorite(projectFav)
                }
            } else {
                self.listProject[position].favorite = 0
                DispatchQueue.global().async {
                    self.getPresenter().removeFavorite(projectFav)
                }
            }
        }
        self.tvProjects.reloadData()
    }
}
