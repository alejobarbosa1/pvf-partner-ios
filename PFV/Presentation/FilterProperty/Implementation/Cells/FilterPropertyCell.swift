//
//  CellFavoriteColor.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit
import Kingfisher

class FilterPropertyCell: UITableViewCell {
    
    @IBOutlet weak var imgProject: UIImageView!
    @IBOutlet weak var imageBuilder: UIImageView!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btFavorite: UIButton!
    @IBOutlet weak var viewContent: UIView!
    fileprivate var delegate: FilterPropertyDelegate!
    fileprivate var position: Int!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgProject.image = nil
        lblProjectName.text = ""
        lblPrice.text = ""
    }

    func setData(_ floor: Floor, delegate: FilterPropertyDelegate, position: Int){
        self.position = position
        self.delegate = delegate
        if floor.logoproj != nil {
            imgProject.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(floor.logoproj!)"))
        } else {
            imgProject.image = #imageLiteral(resourceName: "default_image")
        }
        if floor.logo_builder != nil {
            imageBuilder.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(floor.logo_builder!)"))
        }
        lblProjectName.text = floor.nameproj
        lblPrice.text = floor.basic_price
        DispatchQueue.main.async {
            self.viewContent.layoutIfNeeded()
            self.viewContent.layer.cornerRadius = self.viewContent.frame.height / 20
            self.viewContent.layer.masksToBounds = true
//            self.imgProject.image = self.imgProject.image?.crop(rect: CGRect(x: 0, y: 0, width: 1024, height: 768))
            
        }
    }
    
    
    @IBAction func onFavClick(_ sender: Any) {
        delegate.clickFavorite(position)
        
    }
    
}

protocol FilterPropertyDelegate {
    
    func clickFavorite(_ position: Int)
}
