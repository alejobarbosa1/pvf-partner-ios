//
//  FilterPropertyBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class FilterPropertyBL: BaseBL {
	
	// MARK: Properties
    
    var restProject = RestProject()
    var restUser = RestUser()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IFilterPropertyListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IFilterPropertyListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IFilterPropertyListener {
        return listener as! IFilterPropertyListener
    }
    
    // MARK: Actions
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .PROYECT_LIST:
            let array = (response as! ArrayResponse).objectsArray
            let projectList: [Project] = Project.fromJsonArray(array)
            getListener().setProjectList(projectList)
            break
        case .FAVORITES_LIST:
//            if response == ArrayResponse {
                let array = (response as! ArrayResponse).objectsArray
                let favoritesList: [Project] = Project.fromJsonArray(array)
                getListener().setFavoritesList(favoritesList)
//            } else {
//
//            }
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        switch tag {
        case .PROYECT_LIST:
            getListener().onFailderProjectsList(response.ResponseMessage!)
            break
        default:
            break
        }
    }
}

// MARK: BL
extension FilterPropertyBL : IFilterPropertyBL {
    
    // MARK: Listeners
    
    func getProjectList(_ project: Project) {
        project.iduser = restUser.getCurrentUser()!.iduser
        restProject.getProjectList(project: project, listener: self)
    }
    
    func filterSearch(_ filter: Filter) {
        filter.iduser = restUser.getCurrentUser()!.iduser
        restProject.getFilterProjectList(filter: filter, listener: self)
    }
    
    func setFavorite(_ projectFav: Project) {
        projectFav.iduser = restUser.getCurrentUser()!.iduser
        restProject.setFavorite(projectFav, listener: self)
    }
    
    func removeFavorite(_ projectFav: Project) {
        projectFav.iduser = restUser.getCurrentUser()!.iduser
        restProject.removeFavorite(projectFav, listener: self)
    }
    
    func getFavoritesList() {
        let user = User()
        user.iduser = restUser.getCurrentUser()!.iduser
        restProject.getFavoritesList(user: user, listener: self)
    }
    
    func getProjectsSaved() {
        var projectsDbList = [ProjectDb]()
        projectsDbList = DbManager.getInfoMainProject()!
        getListener().setProjectsSaved(projectsDbList)
    }
    
}
