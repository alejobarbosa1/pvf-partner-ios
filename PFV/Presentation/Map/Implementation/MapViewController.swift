//
//  MapViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import GoogleMaps

// MARK: Base
class MapViewController: BaseViewController {
  
  	// MARK: Properties
    
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var streetViewContainer: UIView!
    @IBOutlet weak var btStreetView: UIButton!
    @IBOutlet weak var btMap: UIButton!
    
    fileprivate var mapView: GMSMapView!
    fileprivate let locManager = CLLocationManager()
    var latitude: String!
    var longitude: String!
    var nameProject: String!

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IMapPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IMapPresenter {
        return presenter as! IMapPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MapPresenter(view: self)
        hideProgress()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startMonitoringSignificantLocationChanges()
        if !verifyLocationServices() {
            return
        }
        if CLLocationManager.authorizationStatus() == .notDetermined {
            return
        }
        setCorner(btStreetView.frame.height / 10, views: btStreetView, btMap)
        hostViewController.setTitle(title: nameProject)
        initMap()
    }
    
    func initMap(){
        let camera = GMSCameraPosition.camera(withLatitude: Double(latitude)!,
                                                longitude: Double(longitude)!,
                                                zoom: Constants.Sizes.DEFAULT_ZOOM)
        mapView = GMSMapView.map(withFrame: CGRect(x:0,
                                                    y:0,
                                                    width: Constants.Sizes.SCREEN_SIZE.width,
                                                    height: mapContainer.frame.height ),
                                    camera: camera)
        mapView!.isMyLocationEnabled = true
        mapView!.settings.compassButton = true
        mapView!.delegate = self
        mapContainer.addSubview(mapView!)
        addMarker(latitude: self.latitude,
                  longitude: self.longitude,
                  nameProject: self.nameProject)
    }
    
    func addMarker(latitude: String,
                   longitude: String,
                   nameProject: String) {
        let position = CLLocationCoordinate2D(latitude: Double(latitude)!,
                                              longitude: Double(longitude)!)
        let marker = GMSMarker(position: position)
        marker.title = nameProject
        marker.map = mapView
//        let position = CLLocationCoordinate2D(latitude: Double(latitude)!,
//                                              longitude: Double(longitude)!)
//        //        let carMarker = GMSMarker(position: position)
//        let carMarker = Marker(position: position, car: car)
//        carMarker.map = mapView
//        carMarker.icon = image.resizeImage(targetSize: Constants.Sizes.POINTER_DOWN)
//
    }
    
    func verifyLocationServices() -> Bool {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locManager.requestWhenInUseAuthorization()
            return false
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedAlways ||
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            return true
        }
        
        let urlObj = URL(string:UIApplicationOpenSettingsURLString)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(urlObj!,
                                      options: [ : ],
                                      completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
        
        return false
    }
    
    func initStreetView(){
        let panoView = GMSPanoramaView(frame: CGRect(x:0,
                                                     y:0,
                                                     width: Constants.Sizes.SCREEN_SIZE.width,
                                                     height: streetViewContainer.frame.height ))
        panoView.delegate = self
        panoView.moveNearCoordinate(CLLocationCoordinate2D(latitude: Double(self.latitude)!, longitude: Double(self.longitude)!))
        self.streetViewContainer.addSubview(panoView)
        
        
    }
    
    @IBAction func onStreetViewClick(_ sender: Any) {
        btMap.isHidden = false
        btStreetView.isHidden = true
        mapContainer.isHidden = true
        streetViewContainer.isHidden = false
        initStreetView()
    }
    
    @IBAction func onMapClick(_ sender: Any) {
        btStreetView.isHidden = false
        btMap.isHidden = true
        mapContainer.isHidden = false
        streetViewContainer.isHidden = true
        initMap()
    }
    
}

// MARK: View
extension MapViewController : IMapView {
  
}

extension MapViewController : GMSMapViewDelegate{
    

    
}

extension MapViewController : GMSPanoramaViewDelegate {
    func panoramaView(_ view: GMSPanoramaView, error: Error, onMoveNearCoordinate coordinate: CLLocationCoordinate2D) {
        NSLog("panoramaView(_,error: \(error), \(coordinate))")
        
        DispatchQueue.main.async(execute: { () -> Void in
            let alert:UIAlertController = UIAlertController(title: "Not available", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: {
                (action) -> Void in
                
            }))
            self.present(alert, animated: true, completion: nil)
        })
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManagerDidChangeAuthorization() {
        locationManager(locManager, didChangeAuthorization: CLLocationManager.authorizationStatus())
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus)
    {
        if status == .notDetermined {
            return
        }
        
        if status != .authorizedWhenInUse &&
            status != .authorizedAlways
        {
            return
        }
        initMap()
    }
}
