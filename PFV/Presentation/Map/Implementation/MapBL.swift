//
//  MapBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class MapBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IMapListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IMapListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IMapListener {
        return listener as! IMapListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension MapBL : IMapBL {
    
    // MARK: Listeners
    
}
