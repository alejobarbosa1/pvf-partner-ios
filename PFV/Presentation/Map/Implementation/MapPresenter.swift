//
//  MapPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 27/11/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class MapPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IMapBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IMapView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IMapView) {
        super.init(baseView: view)
        self.bl = MapBL(listener: self)
    }
    
    fileprivate func getView() -> IMapView {
        return view as! IMapView
    }
    
    fileprivate func getBL() -> IMapBL {
        return bl as! IMapBL
    }
}

// MARK: Presenter
extension MapPresenter: IMapPresenter {

      // MARK: Actions

}

// MARK: Listener
extension MapPresenter: IMapListener {

      // MARK: Listeners
    
}

