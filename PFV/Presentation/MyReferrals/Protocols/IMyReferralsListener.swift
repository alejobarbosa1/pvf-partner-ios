//
//  IMyReferralsListener.swift
//  PFV
//
//  Created by Alejandro Barbosa on 8/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IMyReferralsListener: IBaseListener {
    
    func setReferrals(_ users: [User])
    
    func setBuilders(_ builders: [Builder])
    
    func setCommissions(_ commission: GiantCommission)
    
    func onSuccess()
	
}
