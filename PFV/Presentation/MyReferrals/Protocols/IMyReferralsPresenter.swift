//
//  IMyReferralsPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 8/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IMyReferralsPresenter: IBasePresenter {
    
    func getReferrals()
	
}
