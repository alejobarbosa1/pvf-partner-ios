//
//  MyReferralsBL.swift
//  PFV
//
//  Created by Alejandro Barbosa on 8/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class MyReferralsBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restUser = RestUser()
    fileprivate var restReferrals = RestReferrals()
    fileprivate var restProject = RestProject()
    fileprivate var giantCommission: GiantCommission!
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IMyReferralsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IMyReferralsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IMyReferralsListener {
        return listener as! IMyReferralsListener
    }
    
    // MARK: Listeners
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        switch tag {
        case .FINANCING_LIST:
            let array = (response as! ArrayResponse).objectsArray
            let financingList: [Financing] = Financing.fromJsonArray(array)
            setFinancingName(financingList)
            break
        case .GET_REFERRALS:
            if response is ArrayResponse {
                let array = (response as! ArrayResponse).objectsArray
                let users: [User] = User.fromJsonArray(array)
                getListener().setReferrals(users)
                return
            } else {
                getListener().onSuccess()
            }
            break
        case .GET_BUILDERS:
            if response is ArrayResponse {
                let array = (response as! ArrayResponse).objectsArray
                let builders: [Builder] = Builder.fromJsonArray(array)
                getListener().setBuilders(builders)
            } else {
                getListener().onSuccess()
            }
            break
        case .GET_COMMISSIONS:
            let array = (response as! ArrayResponse).objectsArray
            let commissions: [GiantCommission] = GiantCommission.fromJsonArray(array)
            let commission = commissions.first
            if (commission?.credictpvf.count == 0) && (commission?.buypvf.count == 0) && (commission?.othc.count == 0){
                getListener().setCommissions(commission!)
            } else {
                self.giantCommission = commission
                restProject.getFinancingList(self)
            }
        default:
            break
        }
    }
    
    func setFinancingName(_ financingList: [Financing]){
        
        if giantCommission.credictpvf.count >= 1{
            for credit in giantCommission.credictpvf{
                for financing in financingList where financing.id_financing == credit.id_financing{
                    credit.financing_name = financing.financing_name
                }
                for model in credit.kindproj where NSNumber(value: Int(model.id_model)!) == credit.model.first!.id_model {
                    credit.nameModel = model.nameModel
                    credit.photo = model.photo
                    credit.baths = model.banhos.description
                    credit.rooms = model.hab
                    credit.area = model.area.description
                    credit.parking = model.cant_p
                }
            }
        }
        
        if giantCommission.buypvf.count >= 1 {
            for project in giantCommission.buypvf{
                for model in project.kindproj where model.id_model == project.model_buy.first!.id_model.description {
                    project.nameModel = model.nameModel
                    project.photo = model.photo
                    project.baths = model.banhos.description
                    project.rooms = model.hab
                    project.area = model.area.description
                    project.parking = model.cant_p
                }
            }
        }
        
        if giantCommission.othc.count >= 1{
            for credit in giantCommission.othc {
                for financing in financingList where financing.id_financing == credit.id_financing {
                    credit.financing_name = financing.financing_name
                }
            }
        }
        
        getListener().setCommissions(giantCommission)
        
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        switch tag {
        case .FINANCING_LIST:
            restProject.getFinancingList(self)
            break
        case .GET_REFERRALS:
            restReferrals.getReferrals(user, listener: self)
            break
        case .GET_BUILDERS:
            restReferrals.getBuilders(user, listener: self)
            break
        case .GET_COMMISSIONS:
            restReferrals.getCommissions(user, listener: self)
        default:
            break
        }
    }
}

// MARK: BL
extension MyReferralsBL : IMyReferralsBL {
    
    // MARK: Actions
    
    func getReferrals() {
        let user = User()
        user.iduser = restUser.getCurrentUser()?.iduser
        restReferrals.getCommissions(user, listener: self)
        restReferrals.getReferrals(user, listener: self)
        restReferrals.getBuilders(user, listener: self)
    }
    
}
