//
//  MyReferralsPresenter.swift
//  PFV
//
//  Created by Alejandro Barbosa on 8/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class MyReferralsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IMyReferralsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IMyReferralsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IMyReferralsView) {
        super.init(baseView: view)
        self.bl = MyReferralsBL(listener: self)
    }
    
    fileprivate func getView() -> IMyReferralsView {
        return view as! IMyReferralsView
    }
    
    fileprivate func getBL() -> IMyReferralsBL {
        return bl as! IMyReferralsBL
    }
}

// MARK: Presenter
extension MyReferralsPresenter: IMyReferralsPresenter {

    // MARK: Actions
    
    func getReferrals() {
        getBL().getReferrals()
    }

}

// MARK: Listener
extension MyReferralsPresenter: IMyReferralsListener {

    // MARK: Listeners
    
    func setReferrals(_ users: [User]) {
        getView().setReferrals(users)
    }
    
    func setBuilders(_ builders: [Builder]) {
        getView().setBuilders(builders)
    }
    
    func setCommissions(_ commission: GiantCommission) {
        getView().setCommissions(commission)
    }
    
    func onSuccess() {
        getView().onSuccess()
    }
    
}

