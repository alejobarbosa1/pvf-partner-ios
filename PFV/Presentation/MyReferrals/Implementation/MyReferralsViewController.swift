//
//  MyReferralsViewController.swift
//  PFV
//
//  Created by Alejandro Barbosa on 8/02/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit

// MARK: Base
class MyReferralsViewController: BaseViewController {

    // MARK: Outlets
    @IBOutlet weak var btPeople: UIButton!
    @IBOutlet weak var btBuilders: UIButton!
    @IBOutlet weak var btPayments: UIButton!
    @IBOutlet weak var viewPeopleSelector: UIView!
    @IBOutlet weak var viewBuildersSelector: UIView!
    @IBOutlet weak var viewPaymentsSelector: UIView!
    @IBOutlet weak var tvReferrals: UITableView!
    @IBOutlet weak var imgWavesFooter: UIImageView!
    @IBOutlet weak var imgBgWaves: UIImageView!
    fileprivate var isPeople = true
    fileprivate var isBuilders = false
    fileprivate var isPayments = false
    fileprivate var arrayUsers = [User]()
    fileprivate var arrayBuilders = [Builder]()
    fileprivate var giantCommission: GiantCommission!
    
  	// MARK: Properties

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IMyReferralsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IMyReferralsPresenter {
        return presenter as! IMyReferralsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MyReferralsPresenter(view: self)
        tvReferrals.delegate = self
        tvReferrals.dataSource = self
        tvReferrals.separatorStyle = .none
        showProgress(withTitle: nil)
        DispatchQueue.global().async {
            self.getPresenter().getReferrals()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.MY_REFERRALS)
    }

    // MARK: Actions
    
    @IBAction func onPeopleClick(_ sender: Any) {
        if !isPeople {
            isPeople = true
            isBuilders = false
            isPayments = false
            btPeople.setTitleColor(.white, for: .normal)
            btBuilders.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            btPayments.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            viewPeopleSelector.isHidden = false
            viewBuildersSelector.isHidden = true
            viewPaymentsSelector.isHidden = true
            tvReferrals.reloadData()
        }
    }
    
    @IBAction func onBuildersClick(_ sender: Any) {
        if !isBuilders {
            isPeople = false
            isBuilders = true
            isPayments = false
            btPeople.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            btBuilders.setTitleColor(.white, for: .normal)
            btPayments.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            viewPeopleSelector.isHidden = true
            viewBuildersSelector.isHidden = false
            viewPaymentsSelector.isHidden = true
            tvReferrals.reloadData()
        }
    }
    
    @IBAction func onPaymentsClick(_ sender: Any) {
        if !isPayments {
            isPeople = false
            isBuilders = false
            isPayments = true
            btPeople.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            btBuilders.setTitleColor(Constants.Colors.PURPLE_LIGHT, for: .normal)
            btPayments.setTitleColor(.white, for: .normal)
            viewPeopleSelector.isHidden = true
            viewBuildersSelector.isHidden = true
            viewPaymentsSelector.isHidden = false
            tvReferrals.reloadData()
        }
    }
}

// MARK: View
extension MyReferralsViewController : IMyReferralsView {
    
    func setReferrals(_ users: [User]) {
        hideProgress()
        DispatchQueue.main.async {
            self.arrayUsers = users
            self.tvReferrals.reloadData()
        }
    }
    
    func setBuilders(_ builders: [Builder]) {
        DispatchQueue.main.async {
            self.arrayBuilders = builders
        }
    }
    
    func setCommissions(_ commission: GiantCommission) {
        DispatchQueue.main.async {
            self.giantCommission = commission
        }
    }
    
    func onSuccess() {
        if isPeople{
            hideProgress()
        }
        tvReferrals.reloadData()
    }
}

extension MyReferralsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isPeople || isBuilders {
            return 1
        } else {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isPeople {
            return arrayUsers.count
        } else if isBuilders{
            return arrayBuilders.count
        } else {
            if giantCommission != nil {
                switch section {
                case 0:
                    return giantCommission.buypvf.count
                case 1:
                    return giantCommission.credictpvf.count
                case 2:
                    return giantCommission.othc.count
                default:
                    return giantCommission.builder.count
                }
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isPeople {
            if arrayUsers.count >= 8 {
                imgWavesFooter.isHidden = false
                imgBgWaves.isHidden = true
            } else {
                imgWavesFooter.isHidden = true
                imgBgWaves.isHidden = false
            }
            let newCell: ReferredCell! = tvReferrals.dequeueReusableCell(withIdentifier: Constants.Cells.USER_CELL) as! ReferredCell
            newCell.setUserCell(user: arrayUsers[indexPath.row])
            newCell.selectionStyle = .none
            return newCell
        } else  if isBuilders{
            if arrayBuilders.count >= 8 {
                imgWavesFooter.isHidden = false
                imgBgWaves.isHidden = true
            } else {
                imgWavesFooter.isHidden = true
                imgBgWaves.isHidden = false
            }
            let newCell: ReferredCell! = tvReferrals.dequeueReusableCell(withIdentifier: Constants.Cells.BUILDER_CELL) as! ReferredCell
            newCell.setBuilderCell(builder: arrayBuilders[indexPath.row])
            newCell.selectionStyle = .none
            return newCell
        } else {
            let number = (giantCommission.buypvf.count + giantCommission.credictpvf.count + giantCommission.othc.count + giantCommission.builder.count)
            if number >= 6 {
                imgWavesFooter.isHidden = false
                imgBgWaves.isHidden = true
            } else {
                imgWavesFooter.isHidden = true
                imgBgWaves.isHidden = false
            }
            
            let userCell: ReferredCell! = tvReferrals.dequeueReusableCell(withIdentifier: Constants.Cells.COMMISSION_USER_CELL) as! ReferredCell
            let builderCell: ReferredCell! = tvReferrals.dequeueReusableCell(withIdentifier: Constants.Cells.COMMISSION_BUILDER_CELL) as! ReferredCell
            
            switch (indexPath.section) {
            case 0:
                let current = giantCommission.buypvf[indexPath.row]
                userCell.setCommissionCell(image: current.image,
                                           name: current.fullname,
                                           typeCommision: Constants.CommissionType.PROJECT_PVF,
                                           document: current.fullid,
                                           commission: current.commission)
                userCell.selectionStyle = .none
                return userCell
            case 1:
                let current = giantCommission.credictpvf[indexPath.row]
                userCell.setCommissionCell(image: current.image,
                                           name: current.fullname,
                                           typeCommision: Constants.CommissionType.PROJECT_PVF,
                                           document: current.fullid,
                                           commission: current.commission)
                userCell.selectionStyle = .none
                return userCell
            case 2:
                let current = giantCommission.othc[indexPath.row]
                userCell.setCommissionCell(image: current.image,
                                           name: current.fullname,
                                           typeCommision: Constants.CommissionType.CREDIT,
                                           document: current.fullid,
                                           commission: current.commission)
                userCell.selectionStyle = .none
                return userCell
            default:
                let current = giantCommission.builder[indexPath.row]
                builderCell.setCommissionCell(image: nil,
                                              name: current.namebuilder,
                                              typeCommision: Constants.CommissionType.BUILDER,
                                              document: "\("NIT ")\(current.nit!)",
                                              commission: current.commission)
                builderCell.selectionStyle = .none
                return builderCell
            }
        }
    }   
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isPeople {
            let userReferredVC: UserReferredViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.USER_REFERRED_VIEW_CONTROLLER) as! UserReferredViewController
            userReferredVC.user = arrayUsers[indexPath.row]
            hostViewController.pushViewController(userReferredVC, animated: true)
            return
        }
        
        if isBuilders {
            let builderReferredVC: BuilderReferredViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.BUILDER_REFERRED_VIEW_CONTROLLER) as! BuilderReferredViewController
            builderReferredVC.builder = arrayBuilders[indexPath.row]
            hostViewController.pushViewController(builderReferredVC, animated: true)
        }
        
        if isPayments {
            switch (indexPath.section) {
            case 0:
                let projecVC: PropertyDetailViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_DETAIL_VIEW_CONTROLLER) as! PropertyDetailViewController
                projecVC.projectPVF = giantCommission.buypvf[indexPath.row]
                projecVC.isCredit = false
                hostViewController.pushViewController(projecVC, animated: true)
                return
            case 1:
                let projecVC: PropertyDetailViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_DETAIL_VIEW_CONTROLLER) as! PropertyDetailViewController
                projecVC.creditPVF = giantCommission.credictpvf[indexPath.row]
                projecVC.isCredit = true
                hostViewController.pushViewController(projecVC, animated: true)
                return
            case 2:
                let creditVC: GenerateCreditViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.GENERATE_CREDIT) as! GenerateCreditViewController
                creditVC.otherCredit = giantCommission.othc[indexPath.row]
                creditVC.isShowingInfo = true
                hostViewController.pushViewController(creditVC, animated: true)
                return
            default:
                let builderReferredVC: BuilderReferredViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.BUILDER_REFERRED_VIEW_CONTROLLER) as! BuilderReferredViewController
                builderReferredVC.builderCommission = giantCommission.builder[indexPath.row]
                hostViewController.pushViewController(builderReferredVC, animated: true)
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isPeople || isBuilders{
            return 80
        } else {
            return 100
        }
    }
    
    
    
}
