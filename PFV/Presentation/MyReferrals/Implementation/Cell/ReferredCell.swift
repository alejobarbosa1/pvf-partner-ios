//
//  ReferredCell.swift
//  PFV
//
//  Created by Alejandro Barbosa on 9/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class ReferredCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblDocument: UILabel!
    @IBOutlet weak var lblCommission: UILabel!
    @IBOutlet weak var lblCommissionType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUserCell(user: User){
        viewContent.setCustomStyle(borderColor: .clear,
                                   borderWidth: 0,
                                   cornerRadius: viewContent.frame.height / 10 ,
                                   views: viewContent)
        imgUser.setCustomStyle(borderColor: .clear,
                               borderWidth: 0,
                               cornerRadius: imgUser.frame.height / 10,
                               views: imgUser)
        imgUser.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(user.image!)"))
        lblFullName.text = user.fullname
        lblDocument.text = user.fullid
    }
    
    func setBuilderCell(builder: Builder) {
        viewContent.setCustomStyle(borderColor: .clear,
                                   borderWidth: 0,
                                   cornerRadius: viewContent.frame.height / 10 ,
                                   views: viewContent)
        lblFullName.text = builder.namebuilder
        lblDocument.text = "\("NIT ")\(builder.nit!)"
    }
    
    func setCommissionCell(image: String?, name: String!, typeCommision: String!, document: String!, commission: String!) {
        viewContent.setCustomStyle(borderColor: .clear,
                                   borderWidth: 0,
                                   cornerRadius: viewContent.frame.height / 10 ,
                                   views: viewContent)
        if image != nil {
            imgUser.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(image!)"))
            imgUser.setCustomStyle(borderColor: .clear,
                                   borderWidth: 0,
                                   cornerRadius: imgUser.frame.height / 10,
                                   views: imgUser)
        }
        lblFullName.text = name
        lblCommission.text = commission
        lblCommissionType.text = typeCommision
    }

}
