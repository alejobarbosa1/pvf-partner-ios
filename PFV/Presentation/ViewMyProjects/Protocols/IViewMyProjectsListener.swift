//
//  IViewMyProjectsListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IViewMyProjectsListener: IBaseListener {
  
}
