//
//  ViewMyProjectsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ViewMyProjectsBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IViewMyProjectsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IViewMyProjectsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IViewMyProjectsListener {
        return listener as! IViewMyProjectsListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension ViewMyProjectsBL : IViewMyProjectsBL {
    
    // MARK: Listeners
    
}
