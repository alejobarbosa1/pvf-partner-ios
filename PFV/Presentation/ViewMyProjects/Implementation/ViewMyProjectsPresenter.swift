//
//  ViewMyProjectsPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class ViewMyProjectsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IViewMyProjectsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IViewMyProjectsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IViewMyProjectsView) {
        super.init(baseView: view)
        self.bl = ViewMyProjectsBL(listener: self)
    }
    
    private func getView() -> IViewMyProjectsView {
        return view as! IViewMyProjectsView
    }
    
    private func getBL() -> IViewMyProjectsBL {
        return bl as! IViewMyProjectsBL
    }
}

// MARK: Presenter
extension ViewMyProjectsPresenter: IViewMyProjectsPresenter {

      // MARK: Actions

}

// MARK: Listener
extension ViewMyProjectsPresenter: IViewMyProjectsListener {

      // MARK: Listeners
    
}

