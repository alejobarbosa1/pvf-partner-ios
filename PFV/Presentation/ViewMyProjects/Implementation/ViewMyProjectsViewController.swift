//
//  ViewMyProjectsViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit
import Kingfisher

// MARK: Base
class ViewMyProjectsViewController: BaseViewController {
  
  	// MARK: Properties

    @IBOutlet weak var viewProject: UIView!
    @IBOutlet weak var scrollImages: ImageSlideshow!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblProjectPrice: UILabel!
    @IBOutlet weak var lblRoomsNumber: UILabel!
    @IBOutlet weak var lblBathsNumber: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblProjectStatus: UILabel!
    @IBOutlet weak var viewProjectStatus: UIView!
    @IBOutlet weak var btAdviseChat: UIButton!
    @IBOutlet weak var btBuilderChat: UIButton!
    @IBOutlet weak var viewAdviserChatStatus: UIView!
    @IBOutlet weak var viewBuilderChatStatus: UIView!
    
    var project: Project!
    var isCredit: Bool!
    fileprivate let numberFormatter = NumberFormatter()
    
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IViewMyProjectsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    private func getPresenter() -> IViewMyProjectsPresenter {
        return presenter as! IViewMyProjectsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ViewMyProjectsPresenter(view: self)
        setCorner(viewProject.frame.height / 20, views: viewProject)
        setCorner(btAdviseChat.frame.height / 10, views: btAdviseChat, btBuilderChat)
        setCorner(viewProjectStatus.frame.height / 2,
                  views: viewProjectStatus,
                  viewAdviserChatStatus,
                  viewBuilderChatStatus)
        setData()
        viewProject.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ViewMyProjectsViewController.onClickProject)))
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.setTitle(title: Constants.Titles.MY_PROJECTS)
        hostViewController.navigationItem.rightBarButtonItem = UIBarButtonItem.barButton(nil,
                                                                                        image: #imageLiteral(resourceName: "icon_arrow_left"),
                                                                                        titleColor: UIColor.white,
                                                                                        font: UIFont.systemFont(ofSize: 12),
                                                                                        inContext: self,
                                                                                        selector: #selector(hostViewController.onBackClick))
    }
    
    func setData(){
        var arrayImages = [InputSource]()
        if project.logoproj == nil {
            arrayImages.append(ImageSource(image: #imageLiteral(resourceName: "default_image")))
        } else {
            arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(project.logoproj!)")!)
        }
        if project.kindproj.first!.photo != nil && !project.kindproj.first!.photo.isEmpty{
            for image in project.kindproj.first!.photo {
                arrayImages.append(KingfisherSource(urlString: "\(Constants.Connection.API_BASE_URL)\(image.photo!)")!)
            }
        }
        scrollImages.setImageInputs(arrayImages)
        let webView = UIWebView()
        //        webView.loadRequest(URLRequest(url: URL(string: project.videolink)!))
        webView.loadRequest(URLRequest(url: URL(string: "https://www.youtube.com/watch?v=jJUm1VBnR_U")!))
        let videoView = ViewInputSource(view: webView)
        scrollImages.setView(videoView)
        scrollImages.contentScaleMode = .scaleAspectFill
        scrollImages.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ViewMyProjectsViewController.didTap)))
        lblProjectName.text = project.kindproj.first!.nameModel
        
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        let num: NSNumber = project.kindproj.first!.price_hou! as NSNumber
        let number = numberFormatter.string(from: num)
        lblProjectPrice.text = number! + Constants.Commons.COP
        lblRoomsNumber.text = project.kindproj.first!.hab
        lblBathsNumber.text = project.kindproj.first!.banhos.description
        lblArea.text = project.kindproj.first!.area.description
        if isCredit {
            if project.statuscredict == 0 {
                lblProjectStatus.text = Constants.ProjectStatus.REJECTED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.RED.cgColor
            } else if project.statuscredict.intValue == 1{
                lblProjectStatus.text = Constants.ProjectStatus.APPROVED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.GREEN_LIGHT.cgColor
            } else if project.statuscredict.intValue == 2{
                lblProjectStatus.text = Constants.ProjectStatus.IN_PROCESS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            } else if project.statuscredict.intValue >= 3{
                lblProjectStatus.text = Constants.ProjectStatus.MISSING_DOCUMENTS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            }
        } else {
            if project.statusshop == 0 {
                lblProjectStatus.text = Constants.ProjectStatus.REJECTED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.RED.cgColor
            } else if project.statusshop.intValue == 1{
                lblProjectStatus.text = Constants.ProjectStatus.APPROVED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.GREEN_LIGHT.cgColor
            } else if project.statusshop.intValue == 2{
                lblProjectStatus.text = Constants.ProjectStatus.IN_PROCESS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            } else if project.statusshop.intValue >= 3{
                lblProjectStatus.text = Constants.ProjectStatus.MISSING_DOCUMENTS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            }
        }
        hideProgress()
        
//        lblProjectPrice.text = project.kindproj.first!.
    }
    
    @objc func didTap(){
        scrollImages.presentFullScreenController(from: self)
    }
    
    @objc func onClickProject(){
        showProgress(withTitle: nil)
        let propertyDetail: PropertyDetailViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.PROPERTY_DETAIL_VIEW_CONTROLLER) as! PropertyDetailViewController
        propertyDetail.project = self.project
        propertyDetail.isShowingInfo = true
        propertyDetail.isCredit = self.isCredit
        propertyDetail.modelSelected = self.project.kindproj.first!
        hostViewController.pushViewController(propertyDetail, animated: true)
    }
    
    @IBAction func onBuilderChatClick (_ sender: Any){
        let chatVC: ChatViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.CHAT_VIEW_CONTROLLER) as! ChatViewController
        chatVC.builder = Builder(builder_iduser: project.builder_iduser, namebuilder: project.namebuilder, logo_builder: project.logo_builder, builder_phone: project.builder_phone)
        chatVC.isBuilder = true
        hostViewController.pushViewController(chatVC, animated: true)
    }
    
    @IBAction func onAssesorChatClick (_ sender: Any){
        let chatVC: ChatViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.CHAT_VIEW_CONTROLLER) as! ChatViewController
        chatVC.isBuilder = false
        hostViewController.pushViewController(chatVC, animated: true)
    }
}

// MARK: View
extension ViewMyProjectsViewController : IViewMyProjectsView {
  
}
