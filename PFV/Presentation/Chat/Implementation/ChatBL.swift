//
//  ChatBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class ChatBL: BaseBL {
	
	// MARK: Properties
    
    fileprivate var restUser = RestUser()
    fileprivate var restChat = RestChat()
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IChatListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IChatListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IChatListener {
        return listener as! IChatListener
    }
    
    // MARK: Listeners
    
    override func onDataResponse<T>(_ response: T, tag: Constants.RepositoriesTags) where T : IBaseModel {
        super.onDataResponse(response, tag: tag)
        switch tag {
        case .SEND_MESSAGE:
            let messageId: NSNumber = NSNumber(value: Int(response.toDictionary()["ResponseMessage"] as! String)!)
            DbManager.updateMessage(withStatus: Constants.StatusMessage.SEND, idMessage: messageId)
            getListener().onSuccessMessageSent(byId: messageId)
            break
        default:
            break
        }
    }
    
    override func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags) {
        super.onFailedResponse(response, tag: tag)
        if tag == Constants.RepositoriesTags.SEND_MESSAGE {
            if response.ResponseCode == 0.description {
                let messageId: NSNumber = NSNumber(value: Int(response.ResponseMessage!)!)
                getListener().onFailedMessage(byId: messageId)
                DbManager.updateMessage(withStatus: Constants.StatusMessage.FAILED, idMessage: messageId)
            }
        }
    }
}

// MARK: BL
extension ChatBL : IChatBL {
    
    // MARK: Actions
    
    func getMessages(byId id: NSNumber) {
        var messages = [Message]()
        let userId = restUser.getCurrentUser()?.iduser
        messages = DbManager.getMessages(withChatUserId: id, andUserId: userId!)!
        getListener().onMessagesReceived(messages)
    }
    
    func sendMessage(_ message: Message) {
        let date = NSNumber(value: NSDate().timeIntervalSince1970)
        var newMessage: Message
        if message.builder_iduser != nil {
            newMessage = Message(messageId: message.messageId,
                                 rolId: Constants.Rols.BUILDER,
                                 body: message.body,
                                 date: date,
                                 sender_id: restUser.getCurrentUser()!.iduser,
                                 receiver_id: message.builder_iduser,
                                 status: message.status)
        } else {
            newMessage = Message(messageId: message.messageId,
                                 rolId: Constants.Rols.BUILDER,
                                 body: message.body,
                                 date: date,
                                 sender_id: restUser.getCurrentUser()!.iduser,
                                 receiver_id: message.rol_main,
                                 status: message.status)
        }
        if message.builder_iduser != nil {
            newMessage.builder_iduser = message.builder_iduser
        }
        newMessage.id_user_sec = newMessage.sender_id
        newMessage.send_id = newMessage.sender_id
        newMessage.id_user_main = newMessage.builder_iduser
        newMessage.text = newMessage.body
        newMessage.rol = newMessage.idrol
        restChat.sendMessage(newMessage, listener: self)
        DbManager.insertMessage(newMessage)
    }
    
}
