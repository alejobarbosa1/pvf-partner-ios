//
//  ChatViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import UIKit
import Firebase

// MARK: Base
class ChatViewController: BaseViewController {

    // MARK: Outlets
    
  
  	// MARK: Properties
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topTableView: NSLayoutConstraint!
    @IBOutlet weak var tfText: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    fileprivate var arrayCells = [MessageCell]()
    fileprivate var activityIndicator: UIActivityIndicatorView! = UIActivityIndicatorView()
    var builder: Builder!
    var isBuilder = false
    var arrayMessages = [String]()
    var messages = [Message]()
    var message: String!
    var user: User!

  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IChatPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IChatPresenter {
        return presenter as! IChatPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ChatPresenter(view: self)
        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ChatViewController.endEditing)))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tfText.delegate = self
        showProgress(withTitle: nil)
        user = SystemPreferencesManager.getCurrentUser()
        if isBuilder {
            DispatchQueue.global().async {
                self.getPresenter().getMessages(byId: self.builder.builder_iduser)
            }
        } else {
            DispatchQueue.global().async {
                self.getPresenter().getMessages(byId: Constants.Rols.ADMINISTRATOR)
            }
        }
        sendButton.isEnabled = false
        tfText.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if isBuilder{
            setUpNavBarWithBuilder(builder)
        } else {
            hostViewController.navigationItem.rightBarButtonItem = nil
            hostViewController.setTitle(title: Constants.Titles.ADMINISTRATOR)
        }
        hostViewController.navigationItem.leftBarButtonItem = UIBarButtonItem.barButton(nil,
                                                                                        image: #imageLiteral(resourceName: "icon_arrow_left"),
                                                                                        titleColor: UIColor.white,
                                                                                        font: UIFont.systemFont(ofSize: 12),
                                                                                        inContext: self,
                                                                                        selector: #selector(hostViewController.onBackClick))
    }
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location != 0 {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
        return true
    }
    
    @objc func onCallClick(){
        if let phoneNumber = builder.builder_phone {
            let url = URL(string: "TEL://" + phoneNumber)
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func setUpNavBarWithBuilder(_ builder: Builder){
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        titleView.backgroundColor = UIColor.clear
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.clear
        titleView.addSubview(containerView)
        
        let builderImageView = UIImageView()
        builderImageView.translatesAutoresizingMaskIntoConstraints = false
        builderImageView.contentMode = .scaleAspectFill
//        if let logoBuilderUrl = builder.logo_builder {
//            builderImageView.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(logoBuilderUrl)"))
//        }
        builderImageView.image = #imageLiteral(resourceName: "icon_support")
        containerView.addSubview(builderImageView)
        
        builderImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        builderImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        builderImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        builderImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let nameLabel = UILabel()
        containerView.addSubview(nameLabel)
        nameLabel.text = builder.namebuilder
        nameLabel.textColor = UIColor.white
        nameLabel.font = UIFont(name: Constants.Formats.DEFAULT_FONT, size: 15)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.leftAnchor.constraint(equalTo: builderImageView.rightAnchor, constant: 0).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: builderImageView.centerYAnchor).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalTo: builderImageView.heightAnchor).isActive = true
        
        containerView.centerXAnchor.constraint(equalTo: titleView.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        
        hostViewController.navigationItem.titleView = titleView
        let image = UIImage(named: "icon_call")
        let callButton = UIBarButtonItem(image: image,
                                    style: .plain,
                                    target: self,
                                    action: #selector(onCallClick))
        callButton.width = 40
        hostViewController.navigationItem.rightBarButtonItem = callButton
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        hostViewController.navigationItem.titleView = nil
        hostViewController.setProperties()
    }
    
    override func keyboardWillShow(notification: NSNotification) {
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        super.keyboardWillShow(notification: notification)
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.topTableView.constant += keyboardHeight
                self.tableView.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    override func endEditing() {
        super.endEditing()
        DispatchQueue.main.async(execute: { () -> Void in
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.topTableView.constant = 10
                self.view.layoutIfNeeded()
            }, completion: nil )
        })
    }
    
    func prepareCells() {
        for message in messages {
            if message.sender_id == 43 {
                let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SENT_CELL) as! MessageCell
                newCell.setCell(message, isFromUser: true)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            } else if message.receiver_id == 43 {
                let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_RECEIVED_CELL) as! MessageCell
                newCell.setCell(message, isFromUser: false)
                newCell.selectionStyle = .none
                arrayCells.append(newCell)
            }
        }
        self.tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: arrayCells.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
    }
    
    @IBAction func onSendClick(_ sender: Any) {
        let message = Message()
        let messageId = NSNumber(value: Date().millisecondsSince1970)
        message.body = tfText.text
        message.status = Constants.StatusMessage.PENDING
        message.messageId = messageId
        if isBuilder {
            message.builder_iduser = builder.builder_iduser
        } else {
            message.rol_main = Constants.Rols.ADMINISTRATOR
        }
        getPresenter().sendMessage(message)
        self.tfText.text = ""
//        let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SENT_CELL) as! MessageCell
//        newCell.setCell(message, isFromUser: true)
//        newCell.selectionStyle = .none
//        arrayCells.append(newCell)
        message.sender_id = user.iduser
        message.send_id = user.iduser
        messages.append(message)
        tableView.reloadData()
        self.tableView.scrollToRow(at: IndexPath(row: messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
    }
    
    func changeStatusMessage(withId id: NSNumber, status: NSNumber){
        for message in messages where message.messageId == id {
            message.status = status
        }
        tableView.reloadData()
//        prepareCells()
    }
    

    // MARK: Actions
}

// MARK: View
extension ChatViewController : IChatView {
    
    func onMessagesReceived(_ messages: [Message]){
        hideProgress()
        DispatchQueue.main.async {
            if !messages.isEmpty {
                self.messages = messages
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
            }
        }
    }
    
    func onSuccessMessageSent(byId messageId: NSNumber){
        changeStatusMessage(withId: messageId, status: Constants.StatusMessage.SEND)
    }
    
    func onFailedMessage(byId messageId: NSNumber) {
        changeStatusMessage(withId: messageId, status: Constants.StatusMessage.FAILED)
    }
    
}

extension ChatViewController : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messages[indexPath.row].sender_id == user.iduser {
            let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_SENT_CELL) as! MessageCell
            newCell.setCell(messages[indexPath.row], isFromUser: true)
            newCell.selectionStyle = .none
            return newCell
        } else {
            let newCell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.MESSAGE_RECEIVED_CELL) as! MessageCell
            newCell.setCell(messages[indexPath.row], isFromUser: false)
            newCell.selectionStyle = .none
            return newCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return arrayCells[indexPath.row].heightCell
        let char = messages[indexPath.row].body.count
        return (CGFloat((char * 3) / 13)+70)
    }

}

