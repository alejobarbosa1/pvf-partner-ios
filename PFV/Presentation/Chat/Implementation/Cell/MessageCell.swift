//
//  MessageCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    var heightCell: CGFloat!
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var message: Message!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.message = nil
//        lblMessage.text = ""
//    }

    func setCell(_ message: Message, isFromUser: Bool){
        self.message = message
        lblMessage.setCustomStyle(borderColor: UIColor(hex: "#000000",
                                                       alpha: 0.1),
                                  borderWidth: 2.0,
                                  cornerRadius: lblMessage.frame.height / 20,
                                  views: lblMessage)
        if isFromUser {
            switch message.status {
            case Constants.StatusMessage.PENDING:
                activityIndicator.frame = CGRect(x: Int(self.frame.width / 2),
                                                 y: Int(self.frame.height / 2),
                                                 width: Int(self.frame.height),
                                                 height: Int(self.frame.height))
                activityIndicator.hidesWhenStopped = true
                activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
                self.viewStatus.addSubview(activityIndicator)
                activityIndicator.startAnimating()
                break
            case Constants.StatusMessage.SEND:
                activityIndicator.stopAnimating()
                let imageView = UIImageView(image: #imageLiteral(resourceName: "icon_done"))
                imageView.frame = viewStatus.frame
                self.viewStatus.addSubview(imageView)
                break
            case Constants.StatusMessage.FAILED:
                activityIndicator.stopAnimating()
                let imageView = UIImageView(image: #imageLiteral(resourceName: "icon_warning"))
                imageView.frame = viewStatus.frame
                self.viewStatus.addSubview(imageView)
                lblMessage.textColor = Constants.Colors.GRAY
                break
            default:
                break
            }
        }
        lblMessage.text = message.body
        
        self.heightCell = lblMessage.frame.height + 15
    }
}

extension UILabel {
    
    
}
