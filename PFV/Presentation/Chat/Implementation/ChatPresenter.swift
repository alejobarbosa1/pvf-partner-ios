//
//  ChatPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Base
class ChatPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IChatBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IChatView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IChatView) {
        super.init(baseView: view)
        self.bl = ChatBL(listener: self)
    }
    
    fileprivate func getView() -> IChatView {
        return view as! IChatView
    }
    
    fileprivate func getBL() -> IChatBL {
        return bl as! IChatBL
    }
}

// MARK: Presenter
extension ChatPresenter: IChatPresenter {

    // MARK: Actions
    
    func getMessages(byId id: NSNumber) {
        getBL().getMessages(byId: id)
    }
    
    func sendMessage(_ message: Message) {
        getBL().sendMessage(message)
    }

}

// MARK: Listener
extension ChatPresenter: IChatListener {

    // MARK: Listeners
    
    func onMessagesReceived(_ messages: [Message]){
        getView().onMessagesReceived(messages)
    }
    
    func onSuccessMessageSent(byId messageId: NSNumber) {
        getView().onSuccessMessageSent(byId: messageId)
    }
    
    func onFailedMessage(byId messageId: NSNumber) {
        getView().onFailedMessage(byId: messageId)
    }
    
}

