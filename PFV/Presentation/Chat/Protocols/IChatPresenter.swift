//
//  IChatPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Presenter.

protocol IChatPresenter: IBasePresenter {
    
    func getMessages(byId id: NSNumber)
    
    func sendMessage(_ message: Message)
	
}
