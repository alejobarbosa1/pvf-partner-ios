//
//  IChatListener.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Listener.

protocol IChatListener: IBaseListener {
    
    func onMessagesReceived(_ messages: [Message])
    
    func onSuccessMessageSent(byId messageId: NSNumber)
    
    func onFailedMessage(byId messageId: NSNumber)
	
}
