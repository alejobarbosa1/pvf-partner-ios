//
//  IChatBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 12/01/18.
//  Copyright (c) 2018 CRIZZ. All rights reserved.
//
//  MVP architecture pattern.
//

import Foundation

// MARK: Business logic.

protocol IChatBL: IBaseBL {
    
    func getMessages(byId id: NSNumber)
    
    func sendMessage(_ message: Message)
	
}
