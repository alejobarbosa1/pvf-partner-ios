//
//  MyProjectsListCell.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class MyProjectsListCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var imageBuilder: UIImageView!
    @IBOutlet weak var imageProject: UIImageView!
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var lblModelName: UILabel!
    @IBOutlet weak var lblProjectPrice: UILabel!
    @IBOutlet weak var lblRoomsNumber: UILabel!
    @IBOutlet weak var lblBathsNumber: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var viewProjectStatus: UIView!
    @IBOutlet weak var lblProjectStatus: UILabel!
    fileprivate let numberFormatter = NumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCell(_ project: Project, isCredit: Bool){
        if project.logoproj == nil {
            imageProject.image = #imageLiteral(resourceName: "default_image")
        } else {
            imageProject.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(project.logoproj!)"))
        }
        if project.logo_builder != nil {
            imageBuilder.kf.setImage(with: URL(string: "\(Constants.Connection.API_BASE_URL)\(project.logo_builder!)"))
        }
        lblProjectName.text = project.nameproj
        lblModelName.text = project.kindproj.first!.nameModel
        lblRoomsNumber.text = project.kindproj.first!.hab
        lblBathsNumber.text = project.kindproj.first!.banhos.description
        lblArea.text = project.kindproj.first!.area.description
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Constants.Formats.LOCALE
        lblProjectPrice.text = numberFormatter.string(from: project.kindproj.first!.price_hou)
        if isCredit {
            if project.statuscredict == 0 {
                lblProjectStatus.text = Constants.ProjectStatus.REJECTED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.RED.cgColor
            } else if project.statuscredict.intValue == 1{
                lblProjectStatus.text = Constants.ProjectStatus.APPROVED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.GREEN_LIGHT.cgColor
            } else if project.statuscredict.intValue == 2{
                lblProjectStatus.text = Constants.ProjectStatus.IN_PROCESS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            } else if project.statuscredict.intValue >= 3{
                lblProjectStatus.text = Constants.ProjectStatus.MISSING_DOCUMENTS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            }
        } else {
            if project.statusshop == 0 {
                lblProjectStatus.text = Constants.ProjectStatus.REJECTED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.RED.cgColor
            } else if project.statusshop.intValue == 1{
                lblProjectStatus.text = Constants.ProjectStatus.APPROVED
                viewProjectStatus.layer.backgroundColor = Constants.Colors.GREEN_LIGHT.cgColor
            } else if project.statusshop.intValue == 2{
                lblProjectStatus.text = Constants.ProjectStatus.IN_PROCESS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            } else if project.statusshop.intValue >= 3{
                lblProjectStatus.text = Constants.ProjectStatus.MISSING_DOCUMENTS
                viewProjectStatus.layer.backgroundColor = Constants.Colors.YELLOW.cgColor
            }
        }
        DispatchQueue.main.async {
            self.viewContent.layoutIfNeeded()
            self.viewContent.layer.cornerRadius = self.viewContent.frame.height / 30
            self.viewContent.layer.masksToBounds = true
            self.viewProjectStatus.layoutIfNeeded()
            self.viewProjectStatus.layer.cornerRadius = self.viewProjectStatus.frame.height / 2
            self.viewProjectStatus.layer.masksToBounds = true
        }
    }

}
