//
//  MyProjectsListBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class MyProjectsListBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? IMyProjectsListListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: IMyProjectsListListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> IMyProjectsListListener {
        return listener as! IMyProjectsListListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension MyProjectsListBL : IMyProjectsListBL {
    
    // MARK: Listeners
    
}
