//
//  MyProjectsListPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class MyProjectsListPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? IMyProjectsListBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? IMyProjectsListView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: IMyProjectsListView) {
        super.init(baseView: view)
        self.bl = MyProjectsListBL(listener: self)
    }
    
    fileprivate func getView() -> IMyProjectsListView {
        return view as! IMyProjectsListView
    }
    
    fileprivate func getBL() -> IMyProjectsListBL {
        return bl as! IMyProjectsListBL
    }
}

// MARK: Presenter
extension MyProjectsListPresenter: IMyProjectsListPresenter {

      // MARK: Actions

}

// MARK: Listener
extension MyProjectsListPresenter: IMyProjectsListListener {

      // MARK: Listeners
    
}

