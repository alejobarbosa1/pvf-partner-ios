//
//  MyProjectsListViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class MyProjectsListViewController: BaseViewController {
  
  	// MARK: Properties
    @IBOutlet weak var tvProjects: UITableView!
    @IBOutlet weak var imgFooter: UIImageView!
    
    var purchasesAndCredits: PurchasesAndCredits!
    fileprivate var arrayCells = [MyProjectsListCell]()
    fileprivate var arrayProjects = [Project]()
    
  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? IMyProjectsListPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> IMyProjectsListPresenter {
        return presenter as! IMyProjectsListPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MyProjectsListPresenter(view: self)
        tvProjects.delegate = self
        tvProjects.dataSource = self
        tvProjects.separatorStyle = .none
        prepareCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hostViewController.navigationItem.rightBarButtonItem = UIBarButtonItem.barButton(nil,
                                                                                         image: #imageLiteral(resourceName: "icon_arrow_left"),
                                                                                         titleColor: UIColor.white,
                                                                                         font: UIFont.systemFont(ofSize: 12),
                                                                                         inContext: self,
                                                                                         selector: #selector(MyProjectsListViewController.onBackClick))
    }
    
    override func onBackClick() {
        hostViewController.onBackClick()
    }
    
    func prepareCells(){
        for purchase in purchasesAndCredits.compras {
            let purchaseCell = tvProjects.dequeueReusableCell(withIdentifier: Constants.Cells.MY_PROJECTS_LIST_CELL) as! MyProjectsListCell
            purchaseCell.setCell(purchase, isCredit: false)
            purchaseCell.selectionStyle = .none
            arrayCells.append(purchaseCell)
            arrayProjects.append(purchase)
        }
        for credit in purchasesAndCredits.creditos {
            let creditCell = tvProjects.dequeueReusableCell(withIdentifier: Constants.Cells.MY_PROJECTS_LIST_CELL) as! MyProjectsListCell
            creditCell.setCell(credit, isCredit: true)
            creditCell.selectionStyle = .none
            arrayCells.append(creditCell)
            arrayProjects.append(credit)
        }
    }
}

// MARK: View
extension MyProjectsListViewController : IMyProjectsListView {
  
}

extension MyProjectsListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (purchasesAndCredits.compras.count + purchasesAndCredits.creditos.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return arrayCells[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showProgress(withTitle: nil)
        self.imgFooter.isHidden = false
        let viewMyProject: ViewMyProjectsViewController! = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.VIEW_MY_PROJECTS_VIEW_CONTROLLER) as! ViewMyProjectsViewController
        viewMyProject.project = arrayProjects[indexPath.row]
        if arrayProjects[indexPath.row].statuscredict == nil {
            viewMyProject.isCredit = false
        } else {
            viewMyProject.isCredit = true
        }
        self.hostViewController.pushViewController(viewMyProject, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
}
