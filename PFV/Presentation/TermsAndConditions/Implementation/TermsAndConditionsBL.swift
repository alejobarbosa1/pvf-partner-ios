//
//  TermsAndConditionsBL.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 5/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class TermsAndConditionsBL: BaseBL {
	
	// MARK: Properties
    
    override var listener: IBaseListener! {
        get {
            return super.listener as? ITermsAndConditionsListener
        }
        set {
            super.listener = newValue
        }
    }
    
    required init(baseListener: IBaseListener) {
        super.init(baseListener: baseListener)
    }
    
    required convenience init(listener: ITermsAndConditionsListener) {
        self.init(baseListener: listener)
    }
    
    func getListener() -> ITermsAndConditionsListener {
        return listener as! ITermsAndConditionsListener
    }
    
    // MARK: Actions
}

// MARK: BL
extension TermsAndConditionsBL : ITermsAndConditionsBL {
    
    // MARK: Listeners
    
}
