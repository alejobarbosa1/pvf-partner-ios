//
//  TermsAndConditionsPresenter.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 5/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import Foundation

// MARK: Base
class TermsAndConditionsPresenter: BasePresenter {

	  // MARK: Properties

	  override var bl: IBaseBL! {
        get {
            return super.bl as? ITermsAndConditionsBL
        }
        set {
            super.bl = newValue
        }
    }
    
    override var view: IBaseView! {
        get {
            return super.view as? ITermsAndConditionsView
        }
        set {
            super.view = newValue
        }
    }
    
    required init(baseView: IBaseView) {
        super.init(baseView: baseView)
    }
    
    init(view: ITermsAndConditionsView) {
        super.init(baseView: view)
        self.bl = TermsAndConditionsBL(listener: self)
    }
    
    fileprivate func getView() -> ITermsAndConditionsView {
        return view as! ITermsAndConditionsView
    }
    
    fileprivate func getBL() -> ITermsAndConditionsBL {
        return bl as! ITermsAndConditionsBL
    }
}

// MARK: Presenter
extension TermsAndConditionsPresenter: ITermsAndConditionsPresenter {

      // MARK: Actions

}

// MARK: Listener
extension TermsAndConditionsPresenter: ITermsAndConditionsListener {

      // MARK: Listeners
    
}

