//
//  TermsAndConditionsViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 5/12/17.
//  Copyright (c) 2017 CRIZZ. All rights reserved.
//
//  MVP-Clean architecture pattern.
//

import UIKit

// MARK: Base
class TermsAndConditionsViewController: BaseViewController {
  
  	// MARK: Properties

    var isRegister = false
    
  	override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ITermsAndConditionsPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ITermsAndConditionsPresenter {
        return presenter as! ITermsAndConditionsPresenter
    }

	// MARK: Lyfecicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = TermsAndConditionsPresenter(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !isRegister{
            hostViewController.setTitle(title: Constants.Titles.TERMS_AND_CONDITIONS)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func onBackClick(_ sender: Any) {
        onBackClick()
    }
    
    
}

// MARK: View
extension TermsAndConditionsViewController : ITermsAndConditionsView {
  
}
