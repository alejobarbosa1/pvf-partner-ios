//
//  LABInternalViewController.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 5/12/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
import UIKit

open class LABMenuInternalViewController: UIViewController {
    
    open var allowRepeatedViewControllers: Bool = false
    
}

