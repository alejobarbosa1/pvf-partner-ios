//
//  ViewInputSource.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 11/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import UIKit

class ViewInputSource: NSObject, InputSource {
    
    var view: UIView!
    
    public init(view: UIView) {
        self.view = view
    }
    
    
    func load(to view: UIView, with callback: @escaping (UIView?) -> Void) {
        callback(self.view)
    }
    

}
