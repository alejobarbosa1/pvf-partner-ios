//
//  MonthYearPickerView.swift
//  MonthYearPicker
//
//  Copyright (c) 2016 Alexander Edge <alex@alexedge.co.uk>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
import UIKit

public class MonthYearPickerView: UIPickerView {
    
    public var date: NSDate = NSDate() {
        didSet {
            
            var dateComponents = calendar.components([.year, .month], from: date as Date)
            dateComponents.hour = 12
            
            guard let date = calendar.date(from: dateComponents) else {
                return
            }
            
            setDate(date: date as NSDate, animated: true)
            dateSelectionHandler?(date as NSDate)
        }
    }
    
    public var calendar: NSCalendar = NSCalendar.current as NSCalendar {
        didSet {
            monthDateFormatter.calendar = calendar as Calendar!
            yearDateFormatter.calendar = calendar as Calendar!
        }
    }
    
    public var locale: NSLocale? {
        didSet {
            calendar.locale = locale as Locale?
            monthDateFormatter.locale = locale as Locale!
            yearDateFormatter.locale = locale as Locale!
        }
    }
    
    public var dateSelectionHandler: ((NSDate) -> Void)?
    
    lazy fileprivate var monthDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        return formatter
    }()
    
    lazy fileprivate var yearDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "y"
        return formatter
    }()
    
    fileprivate enum Component: Int {
        case Month
        case Year
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        dataSource = self
        delegate = self
        setDate(date: date, animated: false)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        dataSource = self
        delegate = self
        setDate(date: date, animated: false)
    }
    
    public func setDate(date: NSDate, animated: Bool) {
        let month = calendar.component(.month, from: date as Date) - calendar.maximumRange(of: .month).location
        selectRow(month, inComponent: Component.Month.rawValue, animated: animated)
        let year = calendar.component(.year, from: date as Date) - calendar.maximumRange(of: .year).location
        selectRow(year, inComponent: Component.Year.rawValue, animated: animated)
    }
    
}

extension MonthYearPickerView: UIPickerViewDelegate {
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dateComponents = NSDateComponents()
        dateComponents.year = calendar.maximumRange(of: .year).location + pickerView.selectedRow(inComponent: Component.Year.rawValue)
        dateComponents.month = calendar.maximumRange(of: .month).location + pickerView.selectedRow(inComponent: Component.Month.rawValue)
        dateComponents.hour = 12
        guard let date = calendar.date(from: dateComponents as DateComponents) else {
            return
        }
        self.date = date as NSDate
    }
    
}

extension MonthYearPickerView: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let component = Component(rawValue: component) else { return 0 }
        switch component {
        case .Month:
            return calendar.maximumRange(of: .month).length
        case .Year:
            return calendar.maximumRange(of: .year).length
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let component = Component(rawValue: component) else { return nil }
        
        switch component {
        case .Month:
            let month = calendar.maximumRange(of: .month).location + row
            let dateComponents = NSDateComponents()
            dateComponents.month = month
            dateComponents.hour = 12
            guard let date = calendar.date(from: dateComponents as DateComponents) else {
                return nil
            }
            return monthDateFormatter.string(from: date)
        case .Year:
            let year = calendar.maximumRange(of: .year).location + row
            let dateComponents = NSDateComponents()
            dateComponents.year = year
            dateComponents.hour = 12
            guard let date = calendar.date(from: dateComponents as DateComponents) else {
                return nil
            }
            return yearDateFormatter.string(from: date)
        }
    }
    
}
