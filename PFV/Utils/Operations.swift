//
//  Operations.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 22/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation
import ReachabilitySwift
class Operations {
    
    static func isNetworkAvaible() -> Bool {
        var reachabilityStatus: Bool?
        let reachability = Reachability()
        
        reachability?.whenReachable = { reachability in
            reachabilityStatus = true
        }
        reachability?.whenUnreachable = { reachability in
            reachabilityStatus = false
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            reachabilityStatus = false
        }
        
        sleep(1)
        return reachabilityStatus != nil && reachabilityStatus!
    }
    static func barButtom(_ icon: UIImage, inContext context: UIViewController, selector: Selector, backgroundColor: UIColor) -> UIBarButtonItem {
        let internalButton = UIButton.init(type: UIButtonType.custom)
        
        // Configuración del botón
        internalButton.frame = CGRect(x: 0, y: 0, width: UINavigationController().navigationBar.frame.height, height: UINavigationController().navigationBar.frame.height)
        internalButton.backgroundColor = backgroundColor
        //internalButton.sizeToFit()
        let icono = UIImageView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: internalButton.frame.width * 0.5 ,
                                              height: internalButton.frame.height * 0.5))
        icono.frame.origin = CGPoint(x: internalButton.frame.width / 2  - icono.frame.width / 2 , y: internalButton.frame.height / 2  - icono.frame.height / 2)
        icono.image = icon
        icono.contentMode = .scaleAspectFit
        internalButton.frame = CGRect(x: 0, y: 0, width:  UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        internalButton.setImage(icon, for: UIControlState())
        internalButton.addSubview(icono)
        internalButton.addTarget(context, action: selector, for: UIControlEvents.touchDown)
        return UIBarButtonItem.init(customView: internalButton)
    }
    
    static func addShadow(_ shadow: CGFloat, corner: CGFloat,  views:UIView...){
        DispatchQueue.main.async {
            for view in views{
                view.layer.shadowColor = UIColor.black.cgColor
                view.layer.shadowOpacity = 0.3
                view.layer.shadowOffset = CGSize.zero
                view.layer.shadowRadius = shadow
                view.layer.cornerRadius = corner
            }
        }
        
    }
}
