//
//  Constants.swift
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import UIKit
struct Constants {
    
    struct Connection{
//        static let API_BASE_URL: String = "http://app.pvf.com.co"
//        static let API_BASE_URL: String = "http://158.69.209.248:1337"
        static let API_BASE_URL: String = "http://142.44.246.131:1337"
        static let LOGIN: String = "/singin"
        static let REGISTER_USER: String = "/api/newUserByPartner"
        static let REGISTER_FB: String = "/api/users/createPartnerFacebook"
        static let REGISTER_PARTNER: String = "/api/storePartner"
        static let REGISTER_BUILDER: String = "/api/builder/saveBuilder"
        static let RESEND_CODE: String = "/api/user/newCode"
        static let PICKER_VIEW_DOCUMENT_TYPE: String = "/api/doctype/listingDoctype"
        static let LOG_OUT: String = "/logout"
        static let CLOSE_SESSION: String = "/api/logout/movil"
        static let VALIDATED_CODE: String = "/api/user/validatedCode"
        static let FORGET_PASSWORD: String = "/api/user/newPassword"
        static let SET_PASSWORD: String = "/api/user/resetPassword"
        static let COUNTRIES: String = "/api/country/listCountry"
        static let CITIES: String = "/api/city/listCity2"
        static let CUSTOMER_TYPE: String = "/api/typeClients/list"
        static let EDUCATIONAL_LEVEL: String = "/api/educationalLevel/list"
        static let CONTRACT_TYPE: String = "/api/typeContract/list"
        static let COMPLETE_PERFIL: String = "/api/user/completeProfile"
        static let FAVORITE_PROJECTS: String = "/api/project/myProjects"
        static let PROYECT_LIST: String = "/api/project/listproject"
        static let UPDATE_IMAGE: String = "/api/user/updateImage"
        static let VERIFY_PROFILE: String = "/api/user/verifyProfile"
        static let FINANCING_LIST: String = "/api/financing/list"
        static let BANK_LIST: String = "/api/financial/listingFinancial"
        static let APPLY_TO_PURCHASE: String = "/api/buy/store"
        static let APPLY_TO_PURCHASE_CREDIT: String = "/api/credicts/store"
        static let PURCHASES_AND_CREDITS: String = "/api/project/myProjectsList"
        static let SET_FAVORITE: String = "/api/project/addfavorites"
        static let FAVORITES_LIST: String = "/api/project/listfavorites"
        static let LIST_CREDITS: String = "/api/otherCredicts/myCredictListOtherApp"
        static let REMOVE_FAVORITE: String = "/api/project/removefavorites"
        static let GENERATE_CREDIT: String = "/api/otherCredicts/store"
        static let UPDATE_FIREBASE_TOKEN: String = "/api/user/token_firebase"
        static let SEND_MESSAGE: String = "/api/canalChat/storeApp"
        static let GET_TICKETS: String = "/api/showTickets"
        static let SEND_TICKET: String = "/api/saveTicket"
        static let SEND_SUPPORT_MESSAGE: String = "/api/saveResponse"
        static let GET_REFERRALS: String = "/api/partner/listPersonPartnerUserApp"
        static let GET_BUILDERS: String = "/api/partner/listPersonPartnerBuilderApp"
        static let GET_COMMISSIONS: String = "/api/pays/allPays"
        static let GET_NEWS: String = "/api/news/listingapp"
    }
    
    enum RepositoriesTags {
        
        case NOT_INTERNET_CONECTION
        case LOGIN
        case REGISTER_USER
        case REGISTER_FB
        case REGISTER_PARTNER
        case REGISTER_BUILDER
        case RESEND_CODE
        case PICKER_VIEW_DOCUMENT_TYPE
        case LOG_OUT
        case CLOSE_SESSION
        case VALIDATED_CODE
        case FORGET_PASSWORD
        case SET_PASSWORD
        case COUNTRIES
        case CITIES
        case CUSTOMER_TYPE
        case EDUCATIONAL_LEVEL
        case CONTRACT_TYPE
        case COMPLETE_PERFIL
        case FAVORITE_PROJECTS
        case PROYECT_LIST
        case UPDATE_IMAGE
        case VERIFY_PROFILE
        case FINANCING_LIST
        case BANK_LIST
        case APPLY_TO_PURCHASE
        case APPLY_TO_PURCHASE_CREDIT
        case PURCHASES_AND_CREDITS
        case SET_FAVORITE
        case FAVORITES_LIST
        case LIST_CREDITS
        case REMOVE_FAVORITE
        case GENERATE_CREDIT
        case UPDATE_FIREBASE_TOKEN
        case SEND_MESSAGE
        case GET_TICKETS
        case SEND_TICKET
        case SEND_SUPPORT_MESSAGE
        case GET_REFERRALS
        case GET_BUILDERS
        case GET_COMMISSIONS
        case GET_NEWS
    }
    
    struct ViewControllers {
        static let POPOVER_VIEW: String = "PopoverView"
        static let LARGE_POPOVER_VIEW: String = "LargePopoverView"
        static let LOGIN_VIEW_CONTROLLER: String = "LoginViewController"
        static let REGISTER_VIEW_CONTROLLER: String = "RegisterViewController"
        static let FORGET_PASSWORD_VIEW_CONTROLLER: String = "ForgetPasswordViewController"
        static let COMPLETE_PROFILE_VIEW_CONTROLLER: String = "CompleteProfileViewController"
        static let HOST_VIEW_CONTROLLER: String = "HostViewController"
        static let HOME_VIEW_CONTROLLER: String = "HomeViewController"
        static let MY_REFERRALS_VIEW_CONTROLLER: String = "MyReferralsViewController"
        static let PROFILE_VIEW_CONTROLLER: String = "ProfileViewController"
        static let SEARCH_PROJECTS_VIEW_CONTROLLER: String = "SearchProjectsViewController"
        static let FILTER_PROPERTY_VIEW_CONTROLLER: String = "FilterPropertyViewController"
        static let FILTER_SEARCH_VIEW_CONTROLLER: String = "FilterSearchViewController"
        static let VIEW_MY_PROJECTS_VIEW_CONTROLLER: String = "ViewMyProjectsViewController"
        static let PROPERTY_DETAIL_VIEW_CONTROLLER: String = "PropertyDetailViewController"
        static let PROPERTY_TYPE_VIEW_CONTROLLER: String = "PropertyTypeViewController"
        static let GENERATE_CREDIT: String = "GenerateCreditViewController"
        static let MAP_VIEW_CONTROLLER: String = "MapViewController"
        static let SUPPORT_VIEW_CONTROLLER: String = "SupportViewController"
        static let CREATE_TICKET_VIEW_CONTROLLER: String = "CreateTicketViewController"
        static let SUPPORT_CHAT_VIEW_CONTROLLER: String = "SupportChatViewController"
        static let NEWS_VIEW_CONTROLLER: String = "NewsViewController"
        static let NEWS_LIST_VIEW_CONTROLLER: String = "NewsListViewController"
        static let TERMS_AND_CONDITIONS: String = "TermsAndConditionsViewController"
        static let TERMS_AND_CONDITIONS_2: String = "TermsAndConditionsViewController_2"
        static let BUILDING_VIEW_CONTROLLER: String  = "BuildingViewController"
        static let MY_PROJECTS_LIST_VIEW_CONTROLLER: String = "MyProjectsListViewController"
        static let CREDITS_VIEW_CONTROLLER: String = "CreditsViewController"
        static let CHAT_VIEW_CONTROLLER: String = "ChatViewController"
        static let NEW_REFERRED_VIEW_CONTROLLER: String = "NewReferredViewController"
        static let USER_REFERRED_VIEW_CONTROLLER: String = "UserReferredViewController"
        static let BUILDER_REFERRED_VIEW_CONTROLLER: String = "BuilderReferredViewController"
        static let AUTHORIZATION_VIEW_CONTROLLER: String = "AuthorizationViewController"
    }
    
    
    struct Dialogs {
        static let NOT_INTERNET_CONNECTION: String = "El dispositivo no cuenta con conexión a internet"
        static let ERROR_INVALID_RESPONSE: String = "Respuesta del servidor inesperada. Intente nuevamente"
        static let FIELD_REQUIRED: String = "Este campo es requerido"
        static let EMAIL_INVALID: String = "Por favor digite un email valido"
        static let LOGOUT_ACCOUNT: String = "Actualmente existe un sesión activa, desea cerrarla?"
        static let VERIFICATION_SUCCES: String = "El usuario fue activado correctamente. Bienvenido a PVF"
        static let CHANGE_PASSWORD: String = "CAMBIAR CONTRASEÑA"
        static let NO_CAMERA: String = "No hay camara disponible"
        static let CONFIRM_ACCOUNT: String = "Debe comprobar el código de validación"
        static let SIGN_OFF: String = "¿Realmente desea cerrar la sesión actual? Esto borraría el historial de soporte "
        static let NO_CITIES: String = "Este País no tiene ciudades disponibles"
        static let PRICE_INVALID: String = "Por favor digite un valor valido"
        static let REDUCE_DEBT: String = "Registra los datos de solicitud en el siguiente formulario y en breve nos pondremos en contacto contigo para ayudarte a resolver tu deuda."
        static let SELECT_APARTMENT: String = "Por favor selecciona un apartamento"
        static let SELECT_MODEL: String = "Por favor selecciona un modelo"
        static let SUCCESS_PURCHASE: String = "En breves minutos nuestros asesores enviarán a tu correo un paquete de documentos que tendrás que cargar en la web antes de seguir adelante en el proceso"
        static let SUCCESSFUL_OPERATION: String = "Operación realizada con éxito"
        static let DRAFT_SAVED: String = "Su borrador ha sido guardado con éxito"
        static let DRAFT_DELETED: String = "Borrador eliminado correctamente"
        static let REGISTER: String = "Estás más cerca de ser Partner. Se envió un correo con los documentos a enviar."
        static let PARTNER_NOT_REGISTER: String = "Usted no está registrado como Partner"
        static let PARTNER_NOT_ACTIVED: String = "Su usuario no ha sido activado como Partner aún"
        static let COMPLETE_PROFILE: String = "Por favor complete su perfil antes de generar cualquier solicitud"
        static let AUTHORIZATION_REQUIRED: String = "Si no autorizas la consulta en centrales de reiesgo no podrás seguir con el proceso de registro"
        static let SESSION_EXPIRED: String = "Sesión expirada. Por favor inicie sesión otra vez"
    }
    
    struct Titles {
        static let CATEGORIES: String = "CATEGORIAS"
        static let PROFILE: String = "MI PERFIL"
        static let PROJECTS: String = "PROYECTOS"
        static let COMPLETE_PROFILE: String = "COMPLETAR PERFIL"
        static let EDIT_PROFILE: String = "EDITAR PERFIL"
        static let GENERATE_CREDIT: String = "SIMULAR CRÉDITO"
        static let FILTER_SEARCH: String = "FILTRAR BUSQUEDA"
        static let FILTER_PROPERTY: String = "FILTRAR INMUEBLE"
        static let REDUCE_DEBT: String = "REDUCE TU DEUDA"
        static let PROPERTY_DETAIL: String = "DETALLE DE INMUEBLE"
        static let SUPPORT: String = "SOPORTE"
        static let NEWS: String = "NOTICIAS"
        static let APPARTMENT: String = "APARTAMENTO"
        static let PROPERTY_TYPE: String = "TIPO"
        static let MY_PROJECTS: String = "MIS PROYECTOS"
        static let FAVORITES: String = "Favoritos"
        static let TERMS_AND_CONDITIONS: String = "TERMINOS Y CONDICIONES"
        static let CREDITS: String = "MIS SOLICITUDES"
        static let GENERATE_TICKET: String = "GENERAR TICKET"
        static let ADMINISTRATOR: String = "Administrador"
        static let DRAFTS: String = "BORRADORES"
        static let NEW_REFERRED: String = "NUEVO REFERIDO"
        static let BUILDER: String = "CONSTRUCTORA"
        static let USER: String = "USUARIO"
        static let MY_REFERRALS: String = "MIS REFERIDOS"
        static let AUTHORIZATION: String = "AUTORIZACIÓN"
    }
    
    struct Rols {
        static let ADMINISTRATOR: NSNumber = 1
        static let PARTNER: NSNumber = 2
        static let USER: NSNumber = 3
        static let BUILDER: NSNumber = 4
        static let REAL_ESTATE: NSNumber = 5
        static let SUPPORT: NSNumber = 6
    }
    
    struct CommissionType {
        static let PROJECT_PVF: String = "Proyecto PVF"
        static let CREDIT: String = "Crédito Hipotecario"
        static let BUILDER: String = "Constructora"
    }
    
    struct ChatString {
        static let ADVISER: String = "Administrador de PVF"
        static let PARTNER: String = "Partner"
        static let USER: String = "Usuario"
        static let BUILDER: String = "Constructora"
    }
    
    
    struct ResponseCodes {
        static let UNEXPECTED_ERROR: String = "800"
        static let DATABASE_WRITE_ERROR: String = "900"
        static let DATABASE_READ_ERROR: String = "950"
    }
    
    struct ProjectStatus {
        static let REJECTED: String = "Rechazado"
        static let APPROVED: String = "Aprobado"
        static let IN_PROCESS: String = "En Trámite"
        static let MISSING_DOCUMENTS: String = "Sin asignar documentos"
        
    }
    
    struct Database {
        static let DB_PATH: String = "PVF.sqlite"
        static let assessor: ChatUser = ChatUser(id: 1, rolId: 1, phone: nil)
    }
    
    struct SqlStatements {
        static let ENABLE_FOREIGN_KEYS: String = "PRAGMA foreign_keys = ON"
        static let CREATE_USER: String = "CREATE TABLE IF NOT EXISTS User(id INTEGER PRIMARY KEY, rolId INTEGER NOT NULL, phone TEXT)"
        static let CREATE_MESSAGGE: String = "CREATE TABLE IF NOT EXISTS Messagge(messageId INTEGER PRIMARY KEY,rolId INTEGER NOT NULL,body TEXT NOT NULL,date INTEGER NOT NULL,sender_id INTEGER NOT NULL, receiver_id INTEGER NOT NULL,status INTEGER NOT NULL,FOREIGN KEY(sender_id) REFERENCES User(id), FOREIGN KEY(receiver_id) REFERENCES User(id))"
        static let CREATE_NEWS: String = "CREATE TABLE IF NOT EXISTS News(id INTEGER PRIMARY KEY, image text NOT NULL, title TEXT, body TEXT, date INTEGER NOT NULL,viewed INTEGER NOT NULL)"
        static let CREATE_SUPPORT_TICKETS: String = "CREATE TABLE IF NOT EXISTS SupportTickets(id INTEGER PRIMARY KEY,date INTEGER NOT NULL,title TEXT NOT NULL,message TEXT NOT NULL, status INTEGER NOT NULL, viewed INTEGER NOT NULL)"
        static let CREATE_PROJECT: String = "CREATE TABLE IF NOT EXISTS Project(projectId INTEGER PRIMARY KEY, projectLogo TEXT, builderLogo TEXT, projectName TEXT NOT NULL, basicPrice INTEGER NOT NULL, isFavorite INTEGER NOT NULL, rooms INTEGER NOT NULL, bathrooms INTEGER NOT NULL, area INTEGER NOT NULL, hasParking INTEGER NOT NULL, address TEXT NOT NULL, latitude TEXT, longitude TEXT, endDate TEXT NOT NULL, modelId INTEGER NOT NULL, modelName TEXT NOT NULL, modelStatus TEXT NOT NULL, modelType TEXT NOT NULL, isFlatSelected INTEGER NOT NULL, flatNumber INTEGER, flatPlace TEXT, flatPrice INTEGER, flat INTEGER, financingName TEXT, financingId TEXT, isCredit INTEGER NOT NULL, financingInterest TEXT, financingValue INTEGER, termInYears TEXT, monthlyValue INTEGER, interestRate TEXT, interestRateValue INTEGER, isSecondHolder INTEGER NOT NULL, fullNameSH TEXT,documentTypeSH TEXT, docmuentNumberSH TEXT, emailSH TEXT, cellPhoneSH TEXT, customerTypeSH TEXT, bornDateSH INTEGER, expeditionDateSH INTEGER, educationalLevelSH TEXT, civilStatusSH TEXT, workActivitySH TEXT, antiquityYearsSH TEXT, antiquityMonthsSH TEXT, contractTypeSH TEXT, salarySH TEXT)"
        static let DROP_MESSAGE: String = "DROP TABLE IF EXISTS Messagge"
        static let DROP_USER: String = "DROP TABLE IF EXISTS User"
        static let DROP_NEWS: String = "DROP TABLE IF EXISTS News"
        static let DROP_SUPPORT_TICKETS: String = "DROP TABLE IF EXISTS SupportTickets"
        static let DROP_PROJECT: String = "DROP TABLE IF EXISTS Project"
        static let INSERT_MESSAGGE: String = "insert into Messagge(messageId, rolId, body, date, sender_id, receiver_id,status)values(?,?,?,?,?,?,?)"
        static let INSERT_USER: String = "insert or replace into User(id, rolId, phone) values(?,?,?)"
        static let INSERT_NEWS: String = "insert or replace into News(id, image, title, body, date, viewed) values(?,?,?,?,?,?)"
        static let INSERT_SUPPORT_TICKETS: String = "insert or replace into SupportTickets(id, date, title, message, status, viewed) values(?,?,?,?,?,?)"
        static let INSERT_PROJECT: String = "insert or replace into Project(projectId, projectLogo, builderLogo, projectName, basicPrice, isFavorite, rooms, bathrooms, area, hasParking, address, latitude, longitude, endDate, modelId, modelName, modelStatus, modelType, isFlatSelected, flatNumber, flatPlace, flatPrice, flat, financingName, financingId, isCredit, financingInterest, financingValue, termInYears, monthlyValue, interestRate, interestRateValue, isSecondHolder, fullNameSH, documentTypeSH, docmuentNumberSH, emailSH, cellPhoneSH, customerTypeSH, bornDateSH, expeditionDateSH, educationalLevelSH, civilStatusSH, workActivitySH, antiquityYearsSH, antiquityMonthsSH, contractTypeSH, salarySH) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        static let SELECT_MESSAGGES_BY_CHAT: String = "select body, date, sender_id, receiver_id, messageId, status from Messagge where sender_id = ? OR (sender_id = ? AND receiver_id = ?) order by date"
        static let GET_NEWS: String = "select id, image, title, body, date, viewed from News"
        static let GET_SUPPORT_TICKETS: String = "select id,date,title,message,status,viewed from SupportTickets"
        static let GET_PROJECTS: String = "select projectId, projectLogo, builderLogo, projectName, basicPrice, isFavorite, rooms, bathrooms, area, hasParking, address, latitude, longitude, endDate, modelId, modelName, modelStatus, modelType, isFlatSelected, flatNumber, flatPlace, flatPrice, flat, financingName, financingId, isCredit, financingInterest, financingValue, termInYears, monthlyValue, interestRate, interestRateValue, isSecondHolder, fullNameSH, documentTypeSH, docmuentNumberSH, emailSH, cellPhoneSH, customerTypeSH, bornDateSH, expeditionDateSH, educationalLevelSH, civilStatusSH, workActivitySH, antiquityYearsSH, antiquityMonthsSH, contractTypeSH, salarySH from Project"
        static let UPDATE_MESSAGE: String = "UPDATE Message SET status = ? WHERE id = ?"
        static let UPDATE_VIEDED_NEWS: String = "UPDATE News SET viewed = ? WHERE id = ?"
        static let UPDATE_VIEDED_SUPPORT_TICKET: String = "UPDATE SupportTickets SET viewed = ? WHERE id = ?"
        static let UPDATE_STATUS_SUPPORT_TTCKET: String = "UPDATE SupportTickets SET status = ? WHERE id = ?"
        static let DELETE_PROJECT: String = "delete from Project where projectId = ?"
        static let GET_COUNT_PROJECTS: String = "select count(projectId) from Project"
    }
    
    struct StatusMessage {
        static let PENDING: NSNumber = 0
        static let SEND: NSNumber = 1
        static let FAILED: NSNumber = 2
    }
    
    struct StatusTicket {
        static let OPEN: NSNumber = 1
        static let CLOSED: NSNumber = 0
    }
    
    struct ViewedStatus {
        static let UNVIEWED: NSNumber = 0
        static let VIEWED: NSNumber = 1
    }
    
    
    struct DbCodes {
        static let CANNOT_OPEN_DB: Int = -100
        static let STATEMENT_EXECUTED: Int = 100
        static let INITIALIZED_DB: Int = 200
        static let CLEANED_DB: Int = 300
    }
    
    struct Sounds {
    }
    
    struct Extensions {
    }
    
    struct Cells {
        static let SLIDE_ITEM_CELL: String = "SlideItemCell"
        static let FILTER_PROPERTY_CELL: String = "FilterPropertyCell"
        static let FILTER_PROPERTY_CELL_2: String = "FilterPropertyCell2"
        static let BUILDING_CELL: String = "BuildingCell"
        static let FLAT_CELL: String = "FlatCell"
        static let FLAT_CELL_2: String = "FlatCell2"
        static let PROPERTY_TYPE_CELL: String = "PropertyTypeCell"
        static let PROPERTY_DETAIL_CELL: String = "PropertyDetailCell"
        static let PROPERTY_DETAIL_CELL_2: String = "PropertyDetailCell2"
        static let MY_PROJECTS_LIST_CELL: String = "MyProjectsListCell"
        static let CREDIT_CELL: String = "CreditCell"
        static let MESSAGE_RECEIVED_CELL: String = "MessageReceivedCell"
        static let MESSAGE_SENT_CELL: String = "MessageSentCell"
        static let NEWS_CELL: String = "NewsCell"
        static let TICKET_CELL: String = "TicketCell"
        static let TITLE_MESSAGE_CELL: String = "TitleMessageCell"
        static let MESSAGE_SUPPORT_CELL_SENT: String = "MessageSupportCell_Sent"
        static let MESSAGE_SUPPORT_CELL_RECEIVED: String = "MessageSupportCell_Received"
        static let USER_CELL: String = "UserCell"
        static let BUILDER_CELL: String = "BuilderCell"
        static let COMMISSION_USER_CELL: String = "CommissionUserCell"
        static let COMMISSION_BUILDER_CELL: String = "CommissionBuilderCell"
    }
    
    struct HousingType {
        static let HOUSE: String = "Casa"
        static let APARTMENT: String = "Apartamento"
        static let LOT: String = "Lote"
        static let CELLAR: String = "Bodega"
        static let LOCAL: String = "Local"
        static let OFFICE: String = "Oficina"
    }
    
    struct Categories {
        static let OVER_FLAT: String = "Planos"
        static let OVER_FLAT_FOR_PICKERTF: String = "Sobre planos"
        static let NEW: String = "Nuevo"
        static let USED: String = "Usado"
    }
    
    enum Position {
        case LEFT
        case RIGHT
    }
    
    struct Colors {
        static let PURPLE = UIColor(hex: "56416C")
        static let PURPLE_LIGHT = UIColor(hex: "A09CAE")
        static let BLUE_DARK = UIColor(hex: "2D3963")
        static let YELLOW = UIColor(hex: "F5A623")
        static let PINK = UIColor(hex: "E64AB7")
        static let BLUE_LIGHT = UIColor(hex: "538DDF")
        static let BLUE_LIGHTER = UIColor(hex: "A7C2DF")
        static let BLUE_BLACK = UIColor(hex: "24273F")
        static let GREEN_LIGHT = UIColor(hex: "53DFB6")
        static let GRAY = UIColor(hex: "A6A6AA")
        static let PRUEBA = UIColor(hex: "553E6D")
        static let RED = UIColor(hex: "FD2D2D")
        
    }
    
    struct Formats {
        static let SIMPLE_DATE: String = "dd/MMM/yyyy"
        static let DATE_TO_SERVER: String = "yyyy-MM-dd"
        static let SIMPLE_TIME: String = "dd/MMM/yyyy HH:mm"
        static let SERVER_OUT_DATE: String = "yyyy-MM-dd'T'HH:mm:ss"
        static let SERVER_OUT_DATE_EXTENDED: String = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let DATE_FROM_TIMEINTERVAL: String = "yyyy-MM-dd HH:mm:ss Z"
        static let DATE_TO_CHAT: String = "dd/MM/yyyy HH:mm a"
        static let SERVER_IN_DATE: String = "yyyy-MM-dd"
        static let HOURS_MINUTES_AA: String = "hh:mm a"
        static let HOUR_AA: String = "h a"
        static let PERIOD: String = "a"
        static let DAY: String = "EEEE"
        static let DEFAULT_FONT: String = "MediumAlternate"
        static let BOLD_FONT: String = "DIN Alternate Bold"
        static let LOCALE: Locale = Locale(identifier: "es_CO")
    }
    
    struct Abbreviation {
        static let GMT: String = "GMT"
    }
    
    struct Commons  {
        static let COLOMBIA_ID: Int = 1
        static let COP: String = " COP"
        static let COP_MONTH: String = " COP/mes"
        static let YEAR: String = " año"
        static let YEARS: String = " años"
        static let MONTH: String = " mes"
        static let MONTHS: String = " meses"
        static let INTEREST_SIGN: String = " %"
        static let LENGTH_PROJECT_LIST: Int = 8
        static let START_0: Int = 0
        static let BASE_IMAGE: String = "data:image/jpeg;base64,"
        static let OUTSIDE: String = "Exterior"
        static let INSIDE: String = "Interior"
    }
    
    enum AlertTypes {
        case DONE
        case QUEST
        case WARNING
        case ERROR
        case NOT_INTERNET
        case INFO
    }
    
    struct Keys {
        static let ES: String = "es"
        static let EN: String = "en"
        static let OK_CODE_STATUS: String = "1"
        static let ERROR_CODE_STATUS: String = "0"
        static let OK: String = "ok"
        static let RESPONSE_DATA: String = "ResponseData"
        static let TOKEN: String = "token"
        static let PASSPORT_DOC_TYPE: String = "PA"
        static let TEMPORAL_USER: String = "tmpUser"
        static let CURRENT_USER: String = "currentUser"
        static let ACCOUNT_ACTIVATE: String = "AccountActive"
        static let SEQUENCE: String = "Sequence"
        static let MOBILE: String = "movil"
        static let SETTINGS: String = "settings"
        static let NOTIFICATION: String = "notification"
        static let BODY: String = "body"
        static let STOPS: String = "stops"
        static let BUS: String = "bus"
        static let PENDING_CODE_STATUS: String = "3"
        static let CHAT: String = "chat"
        static let ID_ROL: String = "idrol"
        static let TITLE: String = "title"
        static let BUILDER_ID: String = "builder_iduser"
        static let NEWS: String = "news"
        static let IMAGE: String = "image"
        static let SUPPORT: String = "support"
        static let CHANNEL_ID: String = "idchannel"
        static let CLOSE: String = "close"
    }
    
    struct Links {
    }
    
    struct Tags {
        static let DEFAULT_OK: Int = 0
        static let LOGOUT_ACCOUNT: Int = 1
        static let USER_VERIFED: Int = 2
        static let SUCCES_PASSWORD: Int = 3
        static let SEND_CODE: Int = 4
        static let CONFIRM_ACCOUNT: Int = 5
        static let COUNTRY_TEXT_FIELD: Int = 6
        static let CITY_TEXT_FIELD: Int = 7
        static let SUCCESS_COMPLETE_PROFILE: Int = 8
        static let FINANCING_TYPE_TEXT_FIELD: Int = 9
        static let PERCENTAGE_TEXT_FILD: Int = 10
        static let YEARS_TEXT_FIEL: Int = 11
        static let INTERES_TEXT_FIELD: Int = 12
        static let PRICE_FROM_TF: Int = 13
        static let PRICE_UNTIL_TF: Int = 14
        static let YEARS2_TEXT_FIELD: Int = 15
        static let SUCCESS_PURCHASE: Int = 16
        static let SUCCESS_CREDIT: Int = 17
        static let DOCUMENT_YPE_TF: Int = 18
        static let TICKET_SENT: Int = 19
        static let FAILED: Int = 20
        static let COMPLETE_PROFILE: Int = 21
    }
    
    struct TagsViews {
        static let HOME: Int = 0
        static let PROFILE: Int = 1
        static let TERMS: Int = 2
        static let LOGOUT: Int = 3
    }
    
    struct DocTypes {
        static let CC: Int = 1
        static let CE: Int = 2
        static let TI: Int = 3
    }
    
    struct DocTypesNames {
        static let CC: String = "CC"
        static let CE: String = "CE"
        static let TI: String = "TI"
    }
    
    struct CivilStatus {
        static let SINGLE: String = "Soltero/a"
        static let FREE_UNION: String = "Union Libre"
        static let MARRIED: String = "Casado/a"
        static let DIVORCED: String = "Divorciado/a"
        static let WIDOWER: String = "Viudo/a"
    }
    
    struct WorkActivity {
        static let EMPLOYEE: String = "Empleado/a"
        static let INDEPENDENT: String = "Independiente"
        static let RETIRED: String = "Jubilado/a"
    }
    
    struct Arrays {
    }
    
    struct Sizes {
        static let SCREEN_SIZE: CGRect = UIScreen.main.bounds
        static let TEXT_FIELD_FONT: CGFloat = 18
        static let TEXT_FIELD_BORDER_WIDTH: CGFloat = 2
        static let DEFAULT_FIELD_RADIUS: CGFloat = 20
        static let TEXTFIELD_ICON_RATIO: CGFloat = 28
        static let DEFAULT_FIELD_HEIGHT: CGFloat = 40
        static let ALERT_VIEW_CORNER_RADIUS: CGFloat = 10
        static let DEFAULT_CORNER_RADIUS: CGFloat = 8
        static let DEFAULT_BORDER_WIDTH: CGFloat = 1
        static let DEFAULT_ZOOM: Float = 15.0
        static let COUNTRY_ZOOM: Float = 6.0
        static let POINTER_DOWN: CGSize = CGSize(width: 52, height: 50)
        static let ORIGIN_Y_GAP: CGFloat = 60
        static let LIMIT_LENGTH_CURRENCY_TF: Int = 19
    }
    
    struct Indentifiers {
        
    }
    
    struct RegExps {
        static let SELFT_MATCHES: String = "SELF MATCHES %@"
        static let LENGHT_3_OR_MORE: String = ".{3,}"
        static let LENGHT_5_OR_MORE: String = ".{5,}"
        static let LENGHT_7_OR_MORE: String = ".{7,}"
        static let LENGHT_10_OR_MORE: String = ".{10,}"
        static let PATTERN_EMAIL: String = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        static let VALIDATE_USERNAME: String = "([^0-9])([A-Z]{1})([0-9]{5,15})"
        static let PATTERN_PASSWD: String = "(?=.*[0-9])(?=.*[A-Z]).{6,15}"
        static let ONLY_DIGITS: String = "^[0-9]*$"
        static let ONLY_DIGITS_AND_LETTERS: String = "^[a-zA-Z0-9]*$"
        static let ONLY_DIGITS_6_LENGHT: String = "(?:\\b\\d{6}\\b)"
        static let ONLY_ALPHANUMERICS: String = "([^a-zA-Z0-9ñ])+"
    }
    
    struct ViewStrings {
        static let OK_BUTTON: String = "Aceptar"
        static let CANCEL_BUTTON: String = "Cancelar"
        static let INFO_TITLE: String = "Información"
        static let ERROR_TITLE: String = "Error"
        static let VERIFY: String = "Verificar"
        static let LOGIN: String = "Ingresar"
        static let SELECT: String = "Seleccione..."
        static let CREDIT_VALUE: String = "Valor del Crédito"
        static let MONTHLY_TERM: String = "Plazo en meses"
        static let YEAR_TERM: String = "Plazo en años"
        static let HOUSE_VALUE: String = "Precio del inmueble"
        static let NOT_INTERNET_CONNECTION: String = "El dispositivo no cuenta con conexión a internet."
        static let ERROR_INVALID_RESPONSE: String = "Respuesta del servidor inesperada. Intente nuevamente."
        static let UNAUTHORIZED_USER_OPERATION: String = "Operación no autorizada. Debe volver a iniciar sesión."
        static let SEARCH: String = "Buscar"
        static let PASSWORD_SETTED: String = "Registro actualizado correctamente"
        static let LOCATION_SERVICE_MUST_ALLOW: String = "Los servicios de localización deben estar activos"
        static let CANT_GET_CURRENT_LOCATION: String = "No se puede obtener la ubicación actual, por favor active los servicios de localización de su dispositivo."
        static let CARD_NUMBER_PLACEHOLDER: String = "XXXX XXXX XXXX XXXX"
        static let DATE_SHORT_PLACEHOLDER: String = " MM / YYYY "
        static let DATE_DEFAULT_PLACEHOLDER: String = "dd / MM / YYYY"
        static let HOME: String = "INICIO"
        static let TERMS_AND_CONDITIONS: String = "TÉRMINOS Y CONDICIONES"
        static let LOG_OUT: String = "CERRAR SESIÓN"
        static let UNAUTHORIZED: String = "unauthorized"
    }
    
    struct CardFormats {
        static let CARD_NUMBER_CHAR: String = "X"
        static let VISA_LENGHT: [Int] = [13, 16, 19]
        static let MASTERCARD_LENGTH: Int = 16
        static let DINERS_LENGTH: [Int] = [14, 15, 16]
        static let AMEX_LENGTH: Int = 15
        // TOTEST Se quema el prefijo 1 y 5 respectivamente, revisar el prefix correcto de cada caso
        //        static let VISA_PREFIX: String = "4"
        static let VISA_PREFIX: String = "4"
        //        static let MASTERCARD_PREFIX: [[Int]] = [[51, 55], [2221, 2720]]
        static let MASTERCARD_PREFIX: [String] = ["51", "52", "53", "54", "55", "222100", "272099"]
        static let AMEX_PREFIX: [String] = ["34", "37"]
        static let DINERS_PREFIX: [String] = ["300", "301", "302", "303", "304", "305", "36", "38", "39", "2014", "2149" ]
        static let CARD_TYPE: [String] = ["vi", "mc", "ax", "di"]
    }
}
