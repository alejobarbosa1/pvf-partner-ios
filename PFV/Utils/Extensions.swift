//
//  Extensions.swift
//  EVA Rentista
//
//  Created by Pablo Linares on 16/04/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//


import Foundation
import UIKit
extension Date{
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }

    func toString(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self as Date)
    }
    
    func addTime(minutes: Int, seconds: Int) -> Date {
        var dateComponent = DateComponents()
        let cal = Calendar(identifier: Calendar.Identifier.gregorian)
        dateComponent.minute = minutes
        dateComponent.second = seconds
        
        return (cal as NSCalendar).date(byAdding: dateComponent, to: self as Date, options: NSCalendar.Options(rawValue: 0))!
    }
    
    static func toTimestamp() -> NSNumber {
        return NSNumber(value: self.timeIntervalBetween1970AndReferenceDate)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension UIColor {
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}
extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func toDate(withFormat format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: Constants.Abbreviation.GMT)
        formatter.dateFormat = format
        return formatter.date(from: self)
    }
    
    func replaceFirstOccurrenceOf(target: String, withString replaceString: String) -> String {
        if let range = self.range(of: target) {
            return self.replacingCharacters(in: range, with: replaceString)
        }
        
        return self
    }
    
    subscript(i: Int) -> String{
        guard i >= 0 && i < characters.count else { return "" }
        return String(self[index(startIndex, offsetBy: i)])
    }
    
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? "."
    }
}

extension UIResponder {
    
    fileprivate struct CurrentFirstResponder {
        weak static var currentFirstResponder: UIResponder?
    }
    fileprivate class var currentFirstResponder: UIResponder? {
        get { return CurrentFirstResponder.currentFirstResponder }
        set(newValue) { CurrentFirstResponder.currentFirstResponder = newValue }
    }
    
    class func getCurrentFirstResponder() -> UIResponder? {
        currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder), to: nil, from: nil, for: nil)
        return currentFirstResponder
    }
    
    @objc func findFirstResponder() {
        UIResponder.currentFirstResponder = self
    }
}
public extension NSObject {
    func swiftClassFromString(_ className: String) -> NSObject.Type {
        let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as! String
        var path = appName + "." + className
        if path.contains(" ") {
            path = path.replacingOccurrences(of: " ", with: "_", options: NSString.CompareOptions.literal, range: nil)
        }
        let anyClass: AnyClass = NSClassFromString(path)!
        return anyClass as! NSObject.Type
    }
}

extension UITextField{
    
    func setSecurePassword()
    {
        var button: UIButton!
        for view in self.subviews where view is UIButton
        {
            button = view as! UIButton
            break
        }
        let font = self.font
        if(self.isSecureTextEntry)
        {
            button.setImage(UIImage(named: "icon_eye_off.png"), for: .normal)
            self.isSecureTextEntry = false
            self.font = font
        }
        else
        {
            button.setImage(UIImage(named: "icon_eye_on.png"), for: .normal)
            self.isSecureTextEntry = true
            self.font = font
        }
        
        self.font = UIFont(name: Constants.Formats.DEFAULT_FONT, size: Constants.Sizes.TEXT_FIELD_FONT)
    }
    
    func setImage(image: UIImage, orientation: Constants.Position) {
        var image = image
        var paddingFactor: CGFloat = 0
        let standarSize = CGSize(width: Constants.Sizes.TEXTFIELD_ICON_RATIO,
                                 height: Constants.Sizes.TEXTFIELD_ICON_RATIO)
        
        if image.size.width > Constants.Sizes.DEFAULT_FIELD_HEIGHT {
            image = image.resizeImage(targetSize: standarSize)
            
            paddingFactor = 15
        }
        
        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: standarSize))
        imageView.image = image
        imageView.frame.origin = CGPoint(x: 0, y: 0)
        
        let view = UIView(frame: CGRect(x: 0,
                                        y: imageView.frame.origin.y,
                                        width: imageView.frame.size.width + paddingFactor,
                                        height: imageView.frame.size.height))
        
        view.addSubview(imageView)
        view.isUserInteractionEnabled = false
        
        switch orientation {
        case Constants.Position.LEFT:
            imageView.contentMode = UIViewContentMode.left
            self.leftView = view
            self.leftViewMode = UITextFieldViewMode.always
            break
        case Constants.Position.RIGHT:
            imageView.contentMode = UIViewContentMode.right
            self.rightView = view
            self.rightViewMode = UITextFieldViewMode.always
            break
        }
    }
}

extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(targetSize, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: targetSize.width, height: targetSize.height)))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage!
    }
    
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale, orientation: self.imageOrientation)
        return image
    }
}

extension UIViewController{
    func buttonOnViewPassword(_ delegate: BaseViewController,  textFields: UITextField ...)
    {
        for texField in textFields
        {
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            button.setImage(UIImage(named: "icon_eye_on.png"), for: UIControlState())
            button.addTarget(delegate, action: #selector(BaseViewController.onShowHidePassword(_:)), for: .touchUpInside)
            button.contentMode = UIViewContentMode.right
            texField.rightView = button
            texField.rightViewMode = UITextFieldViewMode.always
        }
        
    }
    
    func setCustomStyle(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat, views: UIView...) {
        for view in views{
            view.layoutIfNeeded()
            view.layer.borderColor = borderColor.cgColor
            view.layer.borderWidth = borderWidth
            view.layer.cornerRadius = cornerRadius
            view.layer.masksToBounds = true
        }
        
    }
    
    func hideBar(inContext context: UIViewController, shouldHide hide: Bool) {
        context.navigationController?.isNavigationBarHidden = hide
    }
    
    func barButtom(_ title: String?, icon: UIImage, inContext context: UIViewController, selector: Selector) -> UIBarButtonItem {
        let internalButton = UIButton.init(type: UIButtonType.custom)
        
        // Configuración del botón
        internalButton.frame = CGRect(x: -16, y: 0, width: Constants.Sizes.SCREEN_SIZE.size.width, height: Constants.Sizes.SCREEN_SIZE.size.width)
        internalButton.backgroundColor = UIColor.clear
        internalButton.setTitle(title, for: UIControlState())
        internalButton.setTitleColor(UIColor.darkGray, for: UIControlState())
        internalButton.titleLabel?.textAlignment = NSTextAlignment.left
        internalButton.titleLabel?.sizeToFit()
        internalButton.sizeToFit()
        internalButton.setImage(icon, for: UIControlState())
        internalButton.addTarget(context, action: selector, for: UIControlEvents.touchDown)
        return UIBarButtonItem.init(customView: internalButton)
    }
    
    func addGradient(_ color1: UIColor, color2: UIColor, views: UIView...){
        
        for view in views{
            let gradient = CAGradientLayer()
            gradient.frame = view.bounds
            gradient.colors = [color1.cgColor, color2.cgColor]
            view.layer.insertSublayer(gradient, at: 0)
        }
    }
    func setCorner(_ cornerRadius: CGFloat, views: UIView...){
        for view in views{
            view.layoutIfNeeded()
            view.layer.cornerRadius = cornerRadius
            view.layer.masksToBounds = true
        }
    }
    
    func addShadow(_ views:UIView...){
        DispatchQueue.main.async {
            for view in views{
                view.layer.shadowColor = UIColor.black.cgColor
                view.layer.shadowOpacity = 0.7
                view.layer.shadowOffset = CGSize.zero
                view.layer.shadowRadius = 20
            }
        }
        
    }
}

extension CALayer {
    func addGradienBorder(colors: [UIColor] ,width: CGFloat, corner: CGFloat) {
        if self.sublayers != nil {
            for layer in self.sublayers!{
                if layer is CAGradientLayer{
                    layer.removeFromSuperlayer()
                    break
                }
            }
        }
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame =  CGRect(origin: CGPoint.zero, size: self.bounds.size)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.colors = colors.map({$0.cgColor})
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = width
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: corner).cgPath
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor.black.cgColor
        gradientLayer.cornerRadius = corner
        gradientLayer.mask = shapeLayer
        self.addSublayer(gradientLayer)
    }
    
}

extension UIView{
    
    func addShadow(){
        DispatchQueue.main.async {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 1
            self.layer.shadowOffset = CGSize.zero
            self.layer.shadowRadius = 20
        }
        
    }
    func setCustomStyle(borderColor: UIColor, borderWidth: CGFloat, cornerRadius: CGFloat, views: UIView...) {
        for view in views{
            view.layoutIfNeeded()
            view.layer.borderColor = borderColor.cgColor
            view.layer.borderWidth = borderWidth
            view.layer.cornerRadius = cornerRadius
            view.layer.masksToBounds = true
        }
        
    }
    
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 20
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func firstSuperView() -> UIView {
        var superView = self.superview
        
        while superView!.superview != nil {
            superView = superView!.superview
        }
        
        return superView!
    }
}

extension UIButton {
    
    func centerTextAndImage(){
        guard let imageView = self.imageView,
            let titleLabel = self.titleLabel else { return }
        
        let imageSize = imageView.frame.size
        let titleSize = titleLabel.bounds.size
        // top, left, bot, right
        self.contentEdgeInsets = UIEdgeInsetsMake(frame.height * 0.1,
                                                   0,
                                                  0,
                                                  0)
        self.titleEdgeInsets = UIEdgeInsetsMake(0,
                                                0,
                                                0,
                                                (frame.width-titleSize.width)/2)
        self.imageEdgeInsets = UIEdgeInsetsMake((imageSize.height * 0.15),
                                                (imageSize.width * 0.3),
                                                (imageSize.height * 0.3),
                                                (imageSize.width * 0.3))
    }
    
}

extension CALayer {
    var borderUIColor: UIColor {
        set {
            self.borderColor = newValue.cgColor
        }
        
        get {
            return UIColor(cgColor: self.borderColor!)
        }
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension NSNumber {
    
    func timestampToDate() -> Date {
        return NSDate(timeIntervalSince1970: TimeInterval(self)) as Date
        
    }
}






extension UITextField{
    func setWarnings(message: String, controler: BaseViewController)-> Bool{
        if !(self.text?.isEmpty)! {return false}
        else{
            controler.showPopover(self, message: message, icon: #imageLiteral(resourceName: "icon_warning"))
            return true
        }
    }
    
    func setRequestWarning(controller: BaseViewController)-> Bool{
        return self.setWarnings(message: Constants.Dialogs.FIELD_REQUIRED, controler: controller)
    }
}

extension UIViewController{
    func setWarningsRequest() -> Bool{
        return self.view.setWarnings(controller: self as! BaseViewController)
    }
    
}

extension UIView{
    func setWarnings(controller: BaseViewController) -> Bool{
        let result = false
        for view in self.subviews where !view.isHidden{
            if view is UITextField{
                if (view as! UITextField).setRequestWarning(controller: controller) {return true}
            }
            else{
                if (view.setWarnings(controller: controller)) { return true}
            }
        }
        return result
    }
}

extension UIScrollView {
    
    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x: 0, y: childStartPoint.y, width: 1, height: self.frame.height), animated: animated)
        }
    }
    
    func scrollToViewNavigation(view:UIView, animated: Bool) {
        if let origin = view.superview {
            // Get the Y position of your child view
            let childStartPoint = origin.convert(view.frame.origin, to: self)
            // Scroll to a rectangle starting at the Y of your subview, with a height of the scrollview
            self.scrollRectToVisible(CGRect(x: 0, y: childStartPoint.y - view.frame.height - UINavigationController().navigationBar.frame.height, width: 1, height: self.frame.height), animated: animated)
        }
    }
    func validateOkScrollNavigation(view: UIView)-> Bool{
        if  view.superview!.convert(view.frame.origin, to: self).y - view.frame.height - UINavigationController().navigationBar.frame.height < self.contentOffset.y {
            return true
        }
        else {return false}
    }
    
    // Bonus: Scroll to top
    func scrollToTop(animated: Bool) {
        let topOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(topOffset, animated: animated)
    }
    
    // Bonus: Scroll to bottom
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        if(bottomOffset.y > 0) {
            setContentOffset(bottomOffset, animated: true)
        }
    }
    
}
