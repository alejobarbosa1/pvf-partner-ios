//
//  Validator.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation


struct Validator {
    
    static func isOnlyDigits(_ field: String?) -> Bool {
        if field == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.ONLY_DIGITS).evaluate(with: field)
    }
    
    static func isOnlyDigitsAndLetters(_ field: String?) -> Bool {
        if field == nil {
            return false
        }
        
        return NSPredicate(format: Constants.RegExps.SELFT_MATCHES, Constants.RegExps.ONLY_DIGITS_AND_LETTERS).evaluate(with: field)
    }
    
    static func isValidField(_ field: String?) -> Bool {
        if field == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.LENGHT_3_OR_MORE).evaluate(with: field)
    }
    
    static func isValidDocument(_ document: String?) -> Bool {
        if document == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.LENGHT_5_OR_MORE).evaluate(with: document)
    }
    
    static func isValidPassport(_ document: String?) -> Bool {
        if document == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.LENGHT_5_OR_MORE).evaluate(with: document) &&
            isOnlyDigitsAndLetters(document)
    }
    
    static func isValidPhone(_ phone: String?) -> Bool {
        if phone == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.LENGHT_7_OR_MORE).evaluate(with: phone)
    }
    
    static func isValidMobile(_ mobile: String?) -> Bool {
        if mobile == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.LENGHT_10_OR_MORE).evaluate(with: mobile)
    }
    
    static func isValidEmail(_ email: String?) -> Bool {
        if email == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.PATTERN_EMAIL).evaluate(with: email)
    }
    
    static func isValidPassword(_ passwd: String?) -> Bool {
        if passwd == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.PATTERN_PASSWD).evaluate(with: passwd)
    }
    
    static func isValidUsername(_ username: String?) -> Bool {
        if username == nil{
            return false
        }
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.VALIDATE_USERNAME).evaluate(with: username)
    }
    
    static func isValidVerificationCode(_ code: String?) -> Bool {
        if code == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.ONLY_DIGITS_6_LENGHT).evaluate(with: code)
    }
    
    static func isValidNotes(_ note: String?) -> Bool {
        if note == nil {
            return false
        }
        
        return NSPredicate(format:Constants.RegExps.SELFT_MATCHES, Constants.RegExps.ONLY_ALPHANUMERICS).evaluate(with: note)
    }
}
