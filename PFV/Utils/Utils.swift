//
//  Utils.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

import ReachabilitySwift

class Utils{
    
    static func isNetworkAvaible() -> Bool {
        var reachabilityStatus: Bool?
        let reachability = Reachability()
        
        reachability?.whenReachable = { reachability in
            reachabilityStatus = true
        }
        reachability?.whenUnreachable = { reachability in
            reachabilityStatus = false
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            reachabilityStatus = false
        }
        
        sleep(1)
        return reachabilityStatus != nil && reachabilityStatus!
    }
}
