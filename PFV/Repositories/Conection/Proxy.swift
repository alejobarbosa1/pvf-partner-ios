//
//  Proxy.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

import Alamofire

class Proxy<T: IBaseModel> {
    
    static func execute(_ method: Alamofire.HTTPMethod,
                        url: String,
                        array:[[String: AnyObject]]?,
                        listener: IResponse,
                        tag: Constants.RepositoriesTags) {
        
        if !Utils.isNetworkAvaible() {
            let objectResponse: Response = Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage: Constants.Dialogs.NOT_INTERNET_CONNECTION)
            let notInternet = Constants.RepositoriesTags.NOT_INTERNET_CONECTION
            listener.onFailedResponse(objectResponse, tag: notInternet)
            return
        }
        
        var headers = [String: String]()
        let token: String? = SystemPreferencesManager.getCurrentUser()?.token
        
        if token != nil {
            headers[Constants.Keys.TOKEN] = token!
        }
        
        let urlPath: String
        
        urlPath = Constants.Connection.API_BASE_URL + url
        
        let request = NSMutableURLRequest(url: URL(string: urlPath)!)
        if method == .get {
            request.httpMethod = "GET"
        }
        else if method == .post {
            request.httpMethod = "POST"
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: array!, options: [])
        
        Alamofire.request(request as! URLRequestConvertible)
            .responseJSON {
                response in processResponse(response, listener: listener, tag: tag)
        }
    }
    
    static func execute(_ method: Alamofire.HTTPMethod,
                        url: String,
                        parameters:[String: AnyObject]?,
                        listener: IResponse,
                        tag: Constants.RepositoriesTags) {
        
        if !Utils.isNetworkAvaible() {
            let objectResponse: Response = Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage: Constants.Dialogs.NOT_INTERNET_CONNECTION)
            let notInternet = Constants.RepositoriesTags.NOT_INTERNET_CONECTION
            listener.onFailedResponse(objectResponse, tag: notInternet)
            return
        }
        
        var headers = [String: String]()
        let token: String?
        if SystemPreferencesManager.getCurrentUser() != nil {
            token = SystemPreferencesManager.getCurrentUser()?.token
        }
        else{
            token = nil
        }
        if token != nil {
            headers[Constants.Keys.TOKEN] = token!
            headers["authenticated"] = "1"
        }
        
        let urlPath: String
        urlPath = Constants.Connection.API_BASE_URL + url
        
        do{
            try Alamofire.request(urlPath.asURL(),
                                  method: method,
                                  parameters: parameters,
                                  encoding: JSONEncoding.default,
                                  headers: headers).responseJSON{ response in
                                    processResponse(response, listener: listener, tag: tag)
            }
        }
        catch {
            listener.onFailedResponse(Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage:Constants.Dialogs.ERROR_INVALID_RESPONSE), tag: tag)
        }
        
//        do{
//            let manager = Alamofire.SessionManager.default
//            manager.session.configuration.timeoutIntervalForRequest = 10
//            try manager.request(urlPath.asURL(),
//                                method: method,
//                                parameters: parameters,
//                                encoding: JSONEncoding.default,
//                                headers: headers).responseJSON{ response in
//                                    processResponse(response, listener: listener, tag: tag)
//            }
//        }
//        catch {
//            listener.onFailedResponse(Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage:Constants.Dialogs.ERROR_INVALID_RESPONSE), tag: tag)
//        }
    }
    
    fileprivate static func processResponse(_ response: Alamofire.DataResponse<Any>, listener: IResponse, tag: Constants.RepositoriesTags) {
        let httpError = response.result.error
        let codeStatuts = response.response?.statusCode
        let dictionary = response.result.value as? [String: AnyObject]
        
        // Validación de error
        
        if httpError?.localizedDescription != nil {
            // Se intenta capturar como Response
            let objectResponse = Response()
            objectResponse.ResponseCode = String(describing: httpError?.localizedDescription)
            
            if codeStatuts == nil {
                objectResponse.ResponseMessage = Constants.Dialogs.ERROR_INVALID_RESPONSE
            } else {
                objectResponse.ResponseMessage = HTTPURLResponse.localizedString(forStatusCode: codeStatuts as Int!)
            }
            
            listener.onFailedResponse(objectResponse, tag: tag)
            return
        }
        
        if dictionary == nil {
            let objectResponse: Response = Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage:Constants.Dialogs.ERROR_INVALID_RESPONSE)
            
            listener.onFailedResponse(objectResponse, tag: tag)
            return
        }
        
        // An error has ocurred
        if codeStatuts != 200 {
            let objectResponse = Response()
            objectResponse.ResponseMessage = String(describing: codeStatuts)
            objectResponse.ResponseMessage = Constants.Dialogs.ERROR_INVALID_RESPONSE
            
            listener.onFailedResponse(objectResponse, tag: tag)
            return
        }
        
        // Primero se debe recibir el response
        let objectResponse = Response()
        objectResponse.fromDictionary(dictionary!)
        
        // Si response no contiene responseData se debe enviar el objeto.
        let responseData = dictionary?[Constants.Keys.RESPONSE_DATA]
        if responseData is NSNull || responseData == nil {
            // Se valida si el código es 0 para respuesta exitosa.
            if objectResponse.ResponseCode == Constants.Keys.OK_CODE_STATUS {
                listener.onDataResponse(objectResponse, tag: tag)
            } else if objectResponse.ResponseCode == "2" {
                listener.onFailedPending(objectResponse, tag: tag)
            }
            else{
                listener.onFailedResponse(objectResponse, tag: tag)
            }
            
            return
        }
        
        // Si response contiene un objeto en responseData se debe enviar como respuesta exitosa
        let dataDictionary = responseData as? [String: AnyObject]
        if dataDictionary != nil {
            let dataObjectResponse = T()
            dataObjectResponse.fromDictionary(dataDictionary!)
            
            listener.onDataResponse(dataObjectResponse, tag: tag)
            return
        }
        
        // Si response contiene un array en responseData se debe enviar como ArrayResponse, como respuesta exitosa
        let dataArray = responseData as? [[String: AnyObject]]
        if dataArray != nil {
            let dataArrayResponse = ArrayResponse(objectsArray: dataArray!)
            
            listener.onDataResponse(dataArrayResponse, tag: tag)
            return
        }
        
        // Si response no contiene un JSON Válido y no es nil se debe notificar respuesta fallida
        let fallidResponse: Response = Response(ResponseCode: Constants.ResponseCodes.UNEXPECTED_ERROR, ResponseMessage:Constants.Dialogs.ERROR_INVALID_RESPONSE)
        
        listener.onFailedResponse(fallidResponse, tag: tag)
    }
}

