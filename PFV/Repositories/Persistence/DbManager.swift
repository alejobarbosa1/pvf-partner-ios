//
//  DbManager.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 16/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation

class DbManager {
    
    static let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    static let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    
    private static func getPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                                                        FileManager.SearchPathDomainMask.userDomainMask,
                                                        true)
        let documentDirectory = paths.first! as NSString
        return documentDirectory.appendingPathComponent(Constants.Database.DB_PATH) as String
    }
    
    fileprivate static func getDatabase() -> OpaquePointer? {
        var database: OpaquePointer? = nil
        sqlite3_open((getPath() as NSString).utf8String,
                     &database)
        return database
    }
    
    @discardableResult static func cleanData() -> Int {
        let db = getDatabase()
        if db == nil {
            return Constants.DbCodes.CANNOT_OPEN_DB
        }
        
        //Drops
        
        if sqlite3_exec(db, Constants.SqlStatements.DROP_MESSAGE, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.DROP_USER, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.DROP_NEWS, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.DROP_SUPPORT_TICKETS, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.DROP_PROJECT, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        
        //Settings
        if sqlite3_exec(db, Constants.SqlStatements.ENABLE_FOREIGN_KEYS, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        
        //Creates
        if sqlite3_exec(db, Constants.SqlStatements.CREATE_USER, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.CREATE_MESSAGGE, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.CREATE_NEWS, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.CREATE_SUPPORT_TICKETS, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        if sqlite3_exec(db, Constants.SqlStatements.CREATE_PROJECT, nil, nil, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        
        // Insert Assessor
        let status = insertUser(Constants.Database.assessor)
        if status != Constants.DbCodes.STATEMENT_EXECUTED {
            return status
        }
        
        //Close
        sqlite3_close(db)
        return Constants.DbCodes.INITIALIZED_DB
    }
    
    @discardableResult static func insertUser(_ chatUser: ChatUser) -> Int {
        return executeSqlStatement(Constants.SqlStatements.INSERT_USER,
                                               parameters: chatUser.id,
                                               chatUser.rolId,
                                               chatUser.phone as AnyObject)
    }
    
    @discardableResult static func insertMessage(_ message: Message) -> Int {
        return executeSqlStatement(Constants.SqlStatements.INSERT_MESSAGGE,
                                   parameters: message.messageId,
                                   message.idrol,
                                   message.body as AnyObject,
                                   message.date,
                                   message.sender_id,
                                   message.receiver_id,
                                   message.status)
        

    }
    
    @discardableResult static func inserNews(_ news: News) -> Int {
        return executeSqlStatement(Constants.SqlStatements.INSERT_NEWS,
                                   parameters: news.id,
                                   news.image as AnyObject,
                                   news.title as AnyObject,
                                   news.body as AnyObject,
                                   news.date,
                                   news.viewed)
    }
    
    @discardableResult static func inserSupportTickets(_ ticket: Ticket) -> Int {
        return executeSqlStatement(Constants.SqlStatements.INSERT_SUPPORT_TICKETS,
                                   parameters: ticket.id,
                                   ticket.date,
                                   ticket.title as AnyObject,
                                   ticket.message as AnyObject,
                                   ticket.status,
                                   ticket.viewed)
    }
    
    @discardableResult static func insertProject(_ project: ProjectDb) -> Int {
        var flatPrice: NSObject! = NSNull()
        var flat: NSObject! = NSNull()
        var financingValue: NSObject! = NSNull()
        var monthlyValue: NSObject! = NSNull()
        var interestRateValue: NSObject! = NSNull()
        var bornDateSH: NSObject! = NSNull()
        var expeditionDateSH: NSObject! = NSNull()
        if project.flatPrice != nil {
            flatPrice = project.flatPrice
        }
        if project.flat != nil {
            flat = project.flat
        }
        if project.financingValue != nil {
            financingValue = project.financingValue
        }
        if project.monthlyValue != nil {
            monthlyValue = project.monthlyValue
        }
        if project.interestRateValue != nil {
            interestRateValue = project.interestRateValue
        }
        if project.bornDateSH != nil {
            bornDateSH = project.bornDateSH
        }
        if project.expeditionDateSH != nil {
            expeditionDateSH = project.expeditionDateSH
        }
        
        return executeSqlStatement(Constants.SqlStatements.INSERT_PROJECT,
                                   parameters: project.projectId,
                                   project.projectLogo as AnyObject,
                                   project.builderLogo as AnyObject,
                                   project.projectName as AnyObject,
                                   project.basicPrice,
                                   project.isFavorite,
                                   project.rooms,
                                   project.bathrooms,
                                   project.area,
                                   project.hasParking,
                                   project.address as AnyObject,
                                   project.latitude as AnyObject,
                                   project.longitude as AnyObject,
                                   project.endDate as AnyObject,
                                   project.modelId,
                                   project.modelName as AnyObject,
                                   project.modelStatus as AnyObject,
                                   project.modelType as AnyObject,
                                   project.isFlatSelected,
                                   project.flatNumber as AnyObject,
                                   project.flatPlace as AnyObject,
                                   flatPrice,
                                   flat,
                                   project.financingName as AnyObject,
                                   project.financingId as AnyObject,
                                   project.isCredit,
                                   project.financingInterest as AnyObject,
                                   financingValue,
                                   project.termInYears as AnyObject,
                                   monthlyValue,
                                   project.interestRate as AnyObject,
                                   interestRateValue,
                                   project.isSecondHolder,
                                   project.fullNameSH as AnyObject,
                                   project.documentTypeSH as AnyObject,
                                   project.docmuentNumberSH as AnyObject,
                                   project.emailSH as AnyObject,
                                   project.cellPhoneSH as AnyObject,
                                   project.customerTypeSH as AnyObject,
                                   bornDateSH,
                                   expeditionDateSH,
                                   project.educationalLevelSH as AnyObject,
                                   project.civilStatusSH as AnyObject,
                                   project.workActivitySH as AnyObject,
                                   project.antiquityYearsSH as AnyObject,
                                   project.antiquityMonthsSH as AnyObject,
                                   project.contractTypeSH as AnyObject,
                                   project.salarySH as AnyObject)
    }
    
    @discardableResult static func updateMessage(withStatus newStatus: NSNumber, idMessage: NSNumber) -> Int {
        return executeSqlStatement(Constants.SqlStatements.UPDATE_MESSAGE,
                                   parameters: newStatus, idMessage)
    }
    
    @discardableResult static func updateViewedNews(viewed: NSNumber, id: NSNumber) -> Int {
        return executeSqlStatement(Constants.SqlStatements.UPDATE_VIEDED_NEWS,
                                   parameters: viewed, id)
    }
    
    @discardableResult static func updateViewedSupportTicket(viewed: NSNumber, id: NSNumber) -> Int {
        return executeSqlStatement(Constants.SqlStatements.UPDATE_VIEDED_SUPPORT_TICKET,
                                   parameters: viewed, id)
    }
    
    @discardableResult static func updateStatusSupportTicket(whitStatus newStatus: NSNumber, id: NSNumber) -> Int {
        return executeSqlStatement(Constants.SqlStatements.UPDATE_STATUS_SUPPORT_TTCKET,
                                   parameters: newStatus, id)
    }
    
    @discardableResult static func deleteProject(withId id: NSNumber) -> Int{
        return executeSqlStatement(Constants.SqlStatements.DELETE_PROJECT,
                                   parameters: id)
    }
    
//    static func getProjectsCount() -> Int? {
//        
//    }
    
    static func getMessages(withChatUserId id: NSNumber, andUserId userId: NSNumber) -> [Message]?{
        var statement: OpaquePointer? = nil
        let db = getDatabase()
        var messages = [Message]()
        
        if db == nil {
            return nil
        }
        
        if sqlite3_prepare_v2(db, Constants.SqlStatements.SELECT_MESSAGGES_BY_CHAT, -1, &statement, nil) != SQLITE_OK {
            return nil
        }
        
        if sqlite3_bind_int(statement, 1, id.int32Value) != SQLITE_OK {
            return nil
        }
        
        if sqlite3_bind_int(statement, 2, userId.int32Value) != SQLITE_OK {
            return nil
        }
        
        if sqlite3_bind_int(statement, 3, id.int32Value) != SQLITE_OK {
            return nil
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let message = Message()
            setProperties(fromStatement: statement!, inObject: message)
            messages.append(message)
        }
        if sqlite3_finalize(statement) != SQLITE_OK {
            return nil
        }
        
        sqlite3_close(db)
        
        return messages
    }
    
    static func getNews() -> [News]? {
        var statement: OpaquePointer? = nil
        let db = getDatabase()
        var arrayNews = [News]()
        
        if db == nil {
            return nil
        }
        
        if sqlite3_prepare_v2(db, Constants.SqlStatements.GET_NEWS, -1, &statement, nil) != SQLITE_OK {
            return nil
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let news = News()
            setProperties(fromStatement: statement!, inObject: news)
            arrayNews.append(news)
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            return nil
        }
        
        sqlite3_close(db)
        
        return arrayNews
    }
    
    static func getSupportTickets() -> [Ticket]? {
        var statement: OpaquePointer? = nil
        let db = getDatabase()
        var arrayTickets = [Ticket]()
        
        if db == nil {
            return nil
        }
        
        if sqlite3_prepare_v2(db, Constants.SqlStatements.GET_SUPPORT_TICKETS, -1, &statement, nil) != SQLITE_OK {
            return nil
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let ticket = Ticket()
            setProperties(fromStatement: statement!, inObject: ticket)
            arrayTickets.append(ticket)
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            return nil
        }
        
        sqlite3_close(db)
        
        return arrayTickets
    }
    
    static func getInfoMainProject() -> [ProjectDb]? {
        var statement: OpaquePointer? = nil
        let db = getDatabase()
        var projects = [ProjectDb]()
        
        if db == nil {
            return nil
        }
        
        if sqlite3_prepare_v2(db, Constants.SqlStatements.GET_PROJECTS, -1, &statement, nil) != SQLITE_OK {
            return nil
        }
        
        while sqlite3_step(statement) == SQLITE_ROW {
            let project = ProjectDb()
            setProperties(fromStatement: statement!, inObject: project)
            projects.append(project)
        }
        
        if sqlite3_finalize(statement) != SQLITE_OK {
            return nil
        }
        
        sqlite3_close(db)
        
        return projects
    }
    

}

// DBUtils
extension DbManager {
    
    fileprivate static func setProperties(fromStatement statement: OpaquePointer,
                                          inObject object: NSObject)
    {
        let columnCount = sqlite3_column_count(statement)
        
        for index in 0..<columnCount {
            let key = String(validatingUTF8: sqlite3_column_name(statement, index))!
            setProperty(toKey: key,
                        withStatement: statement,
                        atIndex: index,
                        inObject: object)
        }
    }
    
    private static func setProperty(toKey key: String,
                                    withStatement statement: OpaquePointer,
                                    atIndex index: Int32,
                                    inObject object: NSObject)
    {
        var value: Any?
        let type = sqlite3_column_type(statement, index)
        
        switch type {
        case SQLITE_NULL:
            return
        case SQLITE_INTEGER:
            value = NSNumber(value: sqlite3_column_int64(statement, index) as Int64)
            break
        case SQLITE_FLOAT:
            value = NSNumber(value: sqlite3_column_double(statement, index))
            break
        case SQLITE_BLOB:
            value = sqlite3_column_text(statement, index) == nil ?
                "" : String(cString: sqlite3_column_text(statement, index)!)
            break
        case SQLITE_TEXT:
            value = sqlite3_column_text(statement, index) == nil ?
                "" : String(cString: sqlite3_column_text(statement, index)!)
            break
        case SQLITE3_TEXT:
            value = sqlite3_column_text(statement, index) == nil ?
                "" : String(cString: sqlite3_column_text(statement, index)!)
            break
        default:
            return
        }
        
        object.setValue(value!, forKey: key)
    }
    
    fileprivate static func executeSqlStatement(_ sqlStatement: String, parameters: AnyObject...) -> Int {
        
        var statement: OpaquePointer? = nil
        let db = getDatabase()
        
        if db == nil {
            return Constants.DbCodes.CANNOT_OPEN_DB
        }
        
        if sqlite3_prepare_v2(db, sqlStatement, -1, &statement, nil) != SQLITE_OK {
            return Int(sqlite3_errcode(db))
        }
        
        var parameterIndex: Int32 = 1
        for parameter in parameters {
            
            if parameter is NSNumber &&
                sqlite3_bind_int(statement, parameterIndex, parameter.int32Value) != SQLITE_OK
            {
                return Int(sqlite3_errcode(db))
            }
                
            else if parameter is String &&
                sqlite3_bind_text(statement, parameterIndex, parameter as! String, -1, SQLITE_TRANSIENT) != SQLITE_OK
            {
                return Int(sqlite3_errcode(db))
            }
                
            else if parameter is Bool &&
                sqlite3_bind_int(statement, parameterIndex, parameter.int32Value) != SQLITE_OK
            {
                return Int(sqlite3_errcode(db))
            }
                
            else if parameter is NSNull &&
                sqlite3_bind_null(statement, parameterIndex) != SQLITE_OK
            {
                return Int(sqlite3_errcode(db))
            }
            
            parameterIndex += 1
        }
        
        if sqlite3_step(statement) != SQLITE_DONE {
            return Int(sqlite3_errcode(db))
        }
        
        sqlite3_close(db)
        
        return Constants.DbCodes.STATEMENT_EXECUTED
    }
}
