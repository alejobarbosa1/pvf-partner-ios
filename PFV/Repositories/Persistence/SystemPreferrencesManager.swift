//
//  SystemPreferrencesManager.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class SystemPreferencesManager {
    
    static func clean() {
        SystemPreferencesManager.setCurrentUser(nil)
    }
    
    static func setCurrentUser(_ user: User?) {
        let userDefaults = UserDefaults.standard
        
        if user == nil {
            userDefaults.removeObject(forKey: Constants.Keys.CURRENT_USER)
        }
        
        userDefaults.set(user?.toDictionary(), forKey: Constants.Keys.CURRENT_USER)
        userDefaults.synchronize()
    }
    
    static func getCurrentUser() -> User? {
        var  user: User?
        
        let userDefaults = UserDefaults.standard
        let json = userDefaults.object(forKey: Constants.Keys.CURRENT_USER) as! [String: AnyObject]?
        
        if json == nil {
            return nil
        }
        
        user = User()
        user?.fromDictionary(json!)
        
        return user
    }
    
    static func setSettings(_ settings: Settings?) {
        let userDefaults = UserDefaults.standard
        
        if settings == nil {
            userDefaults.removeObject(forKey: Constants.Keys.SETTINGS)
        }
        
        userDefaults.set(settings?.toDictionary(), forKey: Constants.Keys.SETTINGS)
        userDefaults.synchronize()
    }
    
    static func getSettings() -> Settings? {
        var  settings: Settings?
        
        let userDefaults = UserDefaults.standard
        let json = userDefaults.object(forKey: Constants.Keys.SETTINGS) as! [String: AnyObject]?
        
        if json == nil {
            return nil
        }
        
        settings = Settings()
        settings?.fromDictionary(json!)
        
        return settings
    }
    
    
}
