//
//  IResponse.swift
//  EVA Usuario
//
//  Created by Alejandro Barbosa on 28/09/17.
//  Copyright © 2017 Pablo Linares. All rights reserved.
//

import Foundation

/**
 Proxy or DB response portocol. All of BL to use Proxy to communicate
 with a data source must extends this.
 */
protocol IResponse {
    /**
     Notify a succesfull response with a model object as data source
     response. To receive this response must be any type of IBaseModel
     (usually a child of BaseModel class).
     
     - parameter response: Data source response. This must be any child of BaseModel.
     */
//    func onDataResponse<T: IBaseModel>(_ response: T, model: IBaseModel, tag: Constants.RepositoriesTags)
//    func onDataResponse(_ response: Response, model: IBaseModel?, tag: Constants.RepositoriesTags)
    
    func onDataResponse<T: IBaseModel>(_ response: T, tag: Constants.RepositoriesTags)
    
    /**
     Notify a failed response with a response model. This model contains
     the error code status and message.
     
     - parameter response: Response model with error code status and message.
     */
    func onFailedResponse(_ response: Response, tag: Constants.RepositoriesTags)
    
    
    func onFailedPending(_ response: Response, tag: Constants.RepositoriesTags)
    //
//    func onFailedWhitData<T: IBaseModel>(_ model: T, response: Response, tag: Constants.RepositoriesTags)
}

