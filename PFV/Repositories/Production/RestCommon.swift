//
//  RestCommon.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 25/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class RestCommon {
    func getDocumentTypes(_ listener: IResponse)  {
        Proxy<DocumentType>.execute(.get,
                                url: Constants.Connection.PICKER_VIEW_DOCUMENT_TYPE,
                                parameters: nil,
                                listener: listener,
                                tag: Constants.RepositoriesTags.PICKER_VIEW_DOCUMENT_TYPE)
    }
}
