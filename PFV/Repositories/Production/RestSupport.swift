//
//  RestSupport.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 29/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation

class RestSupport {
    
    func getTickets(_ user: User, listener: IResponse){
        Proxy<Ticket>.execute(.post,
                              url: Constants.Connection.GET_TICKETS,
                              parameters: user.toDictionary(),
                              listener: listener,
                              tag: Constants.RepositoriesTags.GET_TICKETS)
    }
    
    func sendTicket(_ newTicket: NewTicket, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.SEND_TICKET,
                                parameters: newTicket.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.SEND_TICKET)
    }
    
    func sendMessage(_ message: SupportMessage, listener: IResponse) {
        Proxy<SupportMessage>.execute(.post,
                                      url: Constants.Connection.SEND_SUPPORT_MESSAGE,
                                      parameters: message.toDictionary(),
                                      listener: listener,
                                      tag: Constants.RepositoriesTags.SEND_SUPPORT_MESSAGE)
    }
}
