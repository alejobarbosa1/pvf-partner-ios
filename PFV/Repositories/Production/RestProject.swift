//
//  RestProjects.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 10/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class RestProject {
    
    func getFavorites(_ user: User, listener: IResponse){
        Proxy<Project>.execute(.post,
                                url: Constants.Connection.FAVORITE_PROJECTS,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.FAVORITE_PROJECTS)
    }
    
    func getProjectList(project: Project, listener: IResponse){
        Proxy<Project>.execute(.post,
                               url: Constants.Connection.PROYECT_LIST,
                               parameters: project.toDictionary(),
                               listener: listener,
                               tag: Constants.RepositoriesTags.PROYECT_LIST)
    }
    
    func getFilterProjectList(filter: Filter, listener: IResponse){
        Proxy<Project>.execute(.post,
                               url: Constants.Connection.PROYECT_LIST,
                               parameters: filter.toDictionary(),
                               listener: listener,
                               tag: Constants.RepositoriesTags.PROYECT_LIST)
    }
    
    func getFinancingList(_ listener: IResponse){
        Proxy<Financing>.execute(.get,
                               url: Constants.Connection.FINANCING_LIST,
                               parameters: nil,
                               listener: listener,
                               tag: Constants.RepositoriesTags.FINANCING_LIST)
    }
    
    func applyToPurchase(_ purchase: Purchase, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.APPLY_TO_PURCHASE,
                                parameters: purchase.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.APPLY_TO_PURCHASE)
    }
    
    func applyToPurchaseCredit(_ purchase: Purchase, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.APPLY_TO_PURCHASE_CREDIT,
                                parameters: purchase.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.APPLY_TO_PURCHASE_CREDIT)
    }
    
    func getPurchasesAndCredits(user: User, listener: IResponse){
        Proxy<PurchasesAndCredits>.execute(.post,
                                           url: Constants.Connection.PURCHASES_AND_CREDITS,
                                           parameters: user.toDictionary(),
                                           listener: listener,
                                           tag: Constants.RepositoriesTags.PURCHASES_AND_CREDITS)
    }
    
    func getFavoritesList(user: User, listener: IResponse){
        Proxy<Project>.execute(.post,
                                    url: Constants.Connection.FAVORITES_LIST,
                                    parameters: user.toDictionary(),
                                    listener: listener,
                                    tag: Constants.RepositoriesTags.FAVORITES_LIST)
    }
    
    func setFavorite(_ project: Project, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.SET_FAVORITE,
                                parameters: project.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.SET_FAVORITE)
    }
    
    func removeFavorite(_ project: Project, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.REMOVE_FAVORITE,
                                parameters: project.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.REMOVE_FAVORITE)
    }
    
}
