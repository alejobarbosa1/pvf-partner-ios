//
//  RestChat.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 23/01/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation

class RestChat {
    
    func sendMessage(_ message: Message, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.SEND_MESSAGE,
                                parameters: message.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.SEND_MESSAGE)
    }
    
    
    
    
    
    
    
    
    
}
