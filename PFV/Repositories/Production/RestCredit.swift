//
//  RestCredit.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 28/11/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation


class RestCredit {
    
    func getBankList(_ listener: IResponse){
        Proxy<Bank>.execute(.get,
                                 url: Constants.Connection.BANK_LIST,
                                 parameters: nil,
                                 listener: listener,
                                 tag: Constants.RepositoriesTags.BANK_LIST)
    }
    
    func generateCredit(_ credit: Credit, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.GENERATE_CREDIT,
                                parameters: credit.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.GENERATE_CREDIT)
    }
    
    func getCreditsList(_ user: User, listener: IResponse){
        Proxy<Credit>.execute(.post,
                              url: Constants.Connection.LIST_CREDITS,
                              parameters: user.toDictionary(),
                              listener: listener,
                              tag: Constants.RepositoriesTags.LIST_CREDITS)
    }
    
    
}
