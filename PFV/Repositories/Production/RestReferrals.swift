//
//  RestReferrals.swift
//  PFV
//
//  Created by Alejandro Barbosa on 9/02/18.
//  Copyright © 2018 CRIZZ. All rights reserved.
//

import Foundation

class RestReferrals {
    
    func getReferrals(_ user: User, listener: IResponse) {
        Proxy<User>.execute(.post,
                            url: Constants.Connection.GET_REFERRALS,
                            parameters: user.toDictionary(),
                            listener: listener,
                            tag: Constants.RepositoriesTags.GET_REFERRALS)
    }
    
    func getBuilders(_ user: User, listener: IResponse){
        Proxy<Builder>.execute(.post,
                               url: Constants.Connection.GET_BUILDERS,
                               parameters: user.toDictionary(),
                               listener: listener,
                               tag: Constants.RepositoriesTags.GET_BUILDERS)
    }
    
    func getCommissions(_ user: User, listener: IResponse){
        Proxy<GiantCommission>.execute(.post,
                                 url: Constants.Connection.GET_COMMISSIONS,
                                 parameters: user.toDictionary(),
                                 listener: listener,
                                 tag: Constants.RepositoriesTags.GET_COMMISSIONS)
    }
    
}
