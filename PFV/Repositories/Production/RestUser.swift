//
//  RestUser.swift
//  PVF Partner
//
//  Created by Alejandro Barbosa on 21/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import Foundation

class RestUser {
    func login(_ user: User, listener: IResponse) {
        Proxy<User>.execute(.post,
                            url: Constants.Connection.LOGIN,
                            parameters: user.toDictionary(),
                            listener: listener,
                            tag: Constants.RepositoriesTags.LOGIN)
    }
    
    func verifyuser(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.REGISTER_USER,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.REGISTER_USER)
    }
    
    func registerByFB(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.REGISTER_FB,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.REGISTER_FB)
    }
    
    func registerPartner(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.REGISTER_PARTNER,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.REGISTER_PARTNER)
    }
    
    func registerBuilder(_ builder: Builder, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.REGISTER_BUILDER,
                                parameters: builder.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.REGISTER_BUILDER)
    }
    
    func verificationCode(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                            url: Constants.Connection.VALIDATED_CODE,
                            parameters: user.toDictionary(),
                            listener: listener,
                            tag: Constants.RepositoriesTags.VALIDATED_CODE)
    }
    
    func resendCode(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.RESEND_CODE,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.RESEND_CODE)
    }
    
    func forgetPassword(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.FORGET_PASSWORD,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.FORGET_PASSWORD)
    }
    
    func setPassword(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.SET_PASSWORD,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.SET_PASSWORD)
    }
    
    func verifyProfile(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.VERIFY_PROFILE,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.VERIFY_PROFILE)
    }
    
    func getCountries(_ listener: IResponse){
        Proxy<Country>.execute(.get,
                                url: Constants.Connection.COUNTRIES,
                                parameters: nil,
                                listener: listener,
                                tag: Constants.RepositoriesTags.COUNTRIES)
    }
    
    func getCities(_ country: Country, listener: IResponse)  {
        Proxy<City>.execute(.post,
                            url: Constants.Connection.CITIES,
                            parameters: country.toDictionary(),
                            listener: listener,
                            tag: Constants.RepositoriesTags.CITIES)
    }
    
    func getCustomerType(_ listener: IResponse){
        Proxy<CustomerType>.execute(.get,
                               url: Constants.Connection.CUSTOMER_TYPE,
                               parameters: nil,
                               listener: listener,
                               tag: Constants.RepositoriesTags.CUSTOMER_TYPE)
    }
    
    func getEducationalList(_ listener: IResponse){
        Proxy<EducationalLevel>.execute(.get,
                                        url: Constants.Connection.EDUCATIONAL_LEVEL,
                                        parameters: nil,
                                        listener: listener,
                                        tag: Constants.RepositoriesTags.EDUCATIONAL_LEVEL)
    }
    
    func getContractTypeList(_ listener: IResponse){
        Proxy<ContractType>.execute(.get,
                                    url: Constants.Connection.CONTRACT_TYPE,
                                    parameters: nil,
                                    listener: listener,
                                    tag: Constants.RepositoriesTags.CONTRACT_TYPE)
    }
    
    func completeProfile(_ user: User, listener: IResponse){
        Proxy<User>.execute(.put,
                                url: Constants.Connection.COMPLETE_PERFIL,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.COMPLETE_PERFIL)
    }
    
    func updateImage(_ userImage: User, listener: IResponse){
        Proxy<Response>.execute(.put,
                                url: Constants.Connection.UPDATE_IMAGE,
                                parameters: userImage.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.UPDATE_IMAGE)
    }
    
    func updateFireBaseToken(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.UPDATE_FIREBASE_TOKEN,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.UPDATE_FIREBASE_TOKEN)
    }
    
    func getNews(_ user: User, listener: IResponse){
        Proxy<News>.execute(.post,
                            url: Constants.Connection.GET_NEWS,
                            parameters: user.toDictionary(),
                            listener: listener,
                            tag: Constants.RepositoriesTags.GET_NEWS)
    }

    
    func setCurrentUser(_ user: User?)
    {
        SystemPreferencesManager.setCurrentUser(user)
    }
    
    func setSettings(_ settings: Settings?){
        SystemPreferencesManager.setSettings(settings)
    }
    
    func  getCurrentUser() -> User?{
        
        return SystemPreferencesManager.getCurrentUser()
    }
    
    func getSettings() ->Settings?{
        
        return SystemPreferencesManager.getSettings()
    }
    
    func logOut(_ listener: IResponse, user: User){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.LOG_OUT,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.LOG_OUT)
    }
    
    func closeSession(_ user: User, listener: IResponse){
        Proxy<Response>.execute(.post,
                                url: Constants.Connection.CLOSE_SESSION,
                                parameters: user.toDictionary(),
                                listener: listener,
                                tag: Constants.RepositoriesTags.CLOSE_SESSION)
        
    }
    
}
