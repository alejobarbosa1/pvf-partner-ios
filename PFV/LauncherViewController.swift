//
//  ViewController.swift
//  PFV
//
//  Created by Pablo Linares on 19/09/17.
//  Copyright © 2017 CRIZZ. All rights reserved.
//

import UIKit

class LauncherViewController: BaseViewController {
    
    // MARK: Properties
    
    override var presenter: IBasePresenter! {
        get {
            return super.presenter as? ILauncherPresenter
        }
        set {
            super.presenter = newValue
        }
    }
    
    fileprivate func getPresenter() -> ILauncherPresenter {
        return presenter as! ILauncherPresenter
    }
    
    // MARK: Lyfecicle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigateToLogin()
        
    }
    
    func navigateToLogin(){
        let LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: Constants.ViewControllers.LOGIN_VIEW_CONTROLLER) as! LoginViewController
        DispatchQueue.main.async {
            sleep(3)
            self.navigationController?.pushViewController(LoginViewController, animated: false)
        }
    }
}

// MARK: View
extension LauncherViewController : ILauncherView {
    
}

